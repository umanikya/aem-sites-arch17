package com.ftd.platform.commons.constants;

/**
 * This is the class holding the list of constants.
 * 
 * @author nikkumar8
 * 
 */
public final class Constants
{

    /**
     * Private constructor
     */
    private Constants()
    {
    }

    public static final String BANNER_COMPONENT_TITLE = "banner";
    public static final String GIFT_FINDER_PLACEHOLDER_TITLE = "gift-finder-placeholder";
    public static final String GIFT_FINDER_TITLE = "gift-finder";
    public static final String ADVERTISING_BANNER_COMPONENT_TITLE = "advertising-banner";
    public static final String VIDEO_BRAND_CONTENT_TITLE = "video-static-blocks";
    public static final String SPLIT_VIDEO_BANNER_KEY = "split-video-banner";
    public static final String BANNER_THREE_PRODUCTS_COMPONENT_TITLE = "banner-featured-products";
    public static final String THREE_PRODUCT_GRIDS_TITLE = "featured-products";
    public static final String TWO_PRODUCT_GRIDS_TITLE = "half-banner-featured-products";
    public static final String EXPERIENCE_FRAGMENT_TITLE = "experience-fragment";
    public static final String PROMOTION_BANNER_TITLE = "promotion-banner";
    public static final String HEADER_AND_DESCRIPTION_TITLE = "header-and-description";
    public static final String ONE_COLUMN_LAYOUT_TITLE = "one-column-layout";
    public static final String GENERIC_LAYOUT_TITLE = "generic-layout";
    public static final String GLOBAL_LAYOUT_TITLE = "global-layout";
    public static final String FEATURED_TILES_TITLE = "featured-tile";
    public static final String CARD_LIST_MODULE_TITLE = "card-list";
    public static final String EVERGREEN_CONTENT_TITLE = "evergreen-content";
    public static final String PROMO_CARD_COMPONENT_TITLE = "promo-card";
    public static final String PRIMARY_NAVIGATION_TITLE = "navigation";
    public static final String FOOTER_TITLE = "footer";
    public static final String PRODUCT_LIST_TITLE = "product-list";
    public static final String PRODUCT_COLLECTION_TITLE = "product-collection";
    public static final String SLIDER_COMPONENT_TITLE = "slider";
    public static final String MEMBERSHIP_SLIDER_COMPONENT_TITLE = "membership-slider";
    public static final String DELIVERY_DETAILS = "delivery-details";
    public static final String ISCONDITION = "isCondition";
    public static final String CONDITIONALCONTENTPATH = "conditionalContentPath";
    public static final String TYPE = "type";
    public static final String RESOURCE = "resource";
    public static final String SPLIT_VIDEO_BANNER_RESOURCE_TYPE = "web/platform/components/content/splitbanner";
    public static final String GENERIC_PAGE_MODEL_RESOURCE_TYPE = "web/platform/components/structure/genericpage";
    public static final String GLOBAL_PAGE_MODEL_RESOURCE_TYPE = "web/platform/components/structure/globalpage";
    public static final String FOOTER_RESOURCE_TYPE = "web/platform/components/content/footer";
    public static final String ONE_COLUMN_LAYOUT_PAGE_MODEL_RESOURCE_TYPE = "web/platform/components/structure/onecolumnlayout";
    public static final String EXPERIENCE_FRAGMENT_RESOURCE_PATH = "web/platform/components/content/experiencefragment";
    public static final String CATEGORY_TILES_RESOURCE_TYPE = "web/platform/components/content/featuredtiles";
    public static final String PROMOTIONAL_BANNER_RESOURCE_TYPE = "web/platform/components/content/promotionalbanner";
    public static final String HEADING_DESCRIPTION_RESOURCE_TYPE = "web/platform/components/content/headingdescription";
    public static final String EVERGREEN_RESOURCE_TYPE = "web/platform/components/content/evergreencontent";
    public static final String CARD_LIST_MODULE_RESOURCE_TYPE = "web/platform/components/content/cardlistmodule";
    public static final String VIDEO_BRAND_CONTENT_RESOURCE_TYPE = "web/platform/components/content/videobrandcontent";
    public static final String MODAL_RESOURCE_TYPE = "web/platform/components/content/modal";
    public static final String PROMO_CARD_RESOURCE_TYPE = "web/platform/components/content/promocard";
    public static final String PRODUCT_LIST_RESOURCE_TYPE = "web/platform/components/content/productlist";
    public static final String PRODUCT_COLLECTION_RESOURCE_TYPE = "web/platform/components/content/productcollection";
    public static final String LINK_LIST_RESOURCE_TYPE= "web/platform/components/content/linklist";
    public static final String ROOT_JCR_CONTENT_PATH = "/jcr" + ":content/root";
    public static final String COMPONENT_KEY = "component";
    public static final String TITLE_KEY = "title";
    public static final String DESCRIPTION_KEY = "description";
    public static final String TEXT_KEY = "text";
    public static final String URL_KEY = "url";
    public static final String PRIMARY_CTA_KEY = "primaryCTA";
    public static final String SECONDARY_CTA_KEY = "secondaryCTA";
    public static final String THEME_KEY = "theme";
    public static final String COMPONENT_THEME_KEY = "componentTheme";
    public static final String ALIGNMENT_KEY = "alignment";
    public static final String TARGET_KEY = "target";
    public static final String ALT_KEY = "alt";
    public static final String IMAGE_KEY = "image";
    public static final String HELP_LABEL_KEY = "helpLabel";
    public static final String HELP_TEXT_KEY = "helpText";
    public static final String BUTTON_LABEL_KEY = "buttonLabel";
    public static final String CATEGORY_ID_KEY = "categoryId";
    public static final String NEW_TAB_VALUE = "newtab";
    public static final String TARGET_KEY_VALUE = "_blank";
    public static final String LABEL_KEY = "label";
    public static final String DESCRIPTIONS_KEY = "descriptions";
    public static final String ATTRIBUTES_KEY = "attributes";
    public static final String COMPONENTS_KEY = "components";
    public static final String ASSET_KEY = "asset";
    public static final String SERVER_URL_KEY = "serverurl";
    public static final String CONTENT_URL_KEY = "contenturl";
    public static final String EMAIL_URL_KEY = "emailurl";
    public static final String VIDEO_SERVER_URL_KEY = "videoserverurl";
    public static final String POSTER_IMAGE_KEY = "posterimage";
    public static final String VIDEO_KEY = "video";
    public static final String BLOCK_KEY = "block";
    public static final String VIDEO_LEFT_KEY = "video-left";
    public static final String VIDEO_RIGHT_KEY = "video-right";
    public static final String TRUE_KEY = "true";
    public static final String FALSE_KEY = "false";
    public static final String JSON_KEY = "json";
    public static final String JACKSON_KEY = "jackson";
    public static final String SLING_MODEL_DEFAULT_ORDER = "MapperFeature.SORT_PROPERTIES_ALPHABETICALLY";
    public static final String PRODUCT_KEY = "productIds";
    public static final String PRODUCTID_KEY = "prod";
    public static final String MODAL_KEY = "modal";
    public static final String LINK_LIST_KEY = "link-list";
    public static final String SIX_CATEGORY_TILES_CONSTANT = "sixth";
    public static final String TWO_OPTIONS_IN_MENU = "two";
    public static final String THREE_OPTIONS_IN_MENU = "three";
    public static final String FOUR_OPTIONS_IN_MENU = "four";
    public static final String FIVE_OPTIONS_IN_MENU = "five";
    public static final String TWO_VALUE = "two";
    public static final String LINKS_KEY = "links";
    public static final String NAVIGATION_CONTENT_CONFIG_ID_KEY = "NavigationContentConfigurationID";
    public static final String PHONE_ICON_VALUE = "PhoneSvg";
    public static final String EMAIL_ICON_VALUE = "EmailSvg";
    public static final String CHAT_ICON_VALUE = "ChatSvg";
    public static final String PHONE_SUFFIX = "tel:";
    public static final String EMAIL_SUFFIX = "mailto:";
    public static final String DISCLAIMERS_KEY = "disclaimers";
    public static final String CARDS_KEY = "cards";
    public static final String LINK_KEY = "link";
    public static final String MODAL_CONTRACT_KEY = "modal";
    public static final String MODAL_TITLE_KEY = "modalTitle";
    public static final String MODAL_CONTENT_KEY = "modalContent";
    public static final String NEW_WINDOW_KEY = "newWindow";
    public static final String NO_VALUE = "no";
    public static final String YES_VALUE = "yes";
    public static final String CATEGORIES_KEY = "categories";
    public static final String VERSION_KEY = "version";
    public static final String SAME_PAGE_CONSTANT = "same-page";
    public static final String NEW_PAGE_CONSTANT = "new-page";
    public static final String ANCHOR_KEY = "anchor";
    public static final String DETAILS_KEY = "details";
    public static final String CTA_KEY = "cta";
    public static final String CONTENT_KEY = "content";
    public static final String SECTIONS_KEY = "sections";
    public static final String DISPLAY_TYPE_KEY = "diaplayType";
    public static final String PAGINATION_KEY = "pagination";
    public static final String DISPLAY_FILTERS_KEY = "displayFilters";
    public static final String DISPLAY_SORT_OPTIONS_KEY = "displaySortOptions";
    public static final String DISPLAY_GIFT_FINDER_KEY = "displayGiftFinder";
    public static final String SPECIFY_CATEGORIES_KEY = "specifyCategories";
    public static final String PROMO_CARD_LISTING = "promoCardListing";
    public static final String PROMOCARD = "promocard";
    public static final String CONTENT_CONFIG = "contentConfig";
    public static final String CONTENT_CONFIG_CAP = "ContentConfig";
    public static final String VARIANT_ID = "variantId";
    public static final String VARIATION_ID = "variationId";
    public static final String TAGS = "tags";
    public static final String SERVICE_VENDOR_PROPERTY = org.osgi.framework.Constants.SERVICE_VENDOR
            + " = Platform Service Dynamic Participant";
    public static final String SERVICE_DESCRIPTION_PROPERTY = "service.description = Platform Dynamic Participant Chooser";
    public static final String CHOOSER_LABEL_PROPERTY = "chooser.label = Platform Participant Chooser";
    public static final String SYSTEM_USER = "ftdi-system-user";
    public static final String FORWARD_SLASH_DELIMETER = "/";
    public static final String COMMA_DELIMETER = ",";
    public static final String PROCESS_ARGS = "PROCESS_ARGS";
    public static final String HYPHEN_CONSTANT = "-";
    public static final String COLUMN_SELECTOR_KEY = "columnSelector";
    public static final String TAGS_BASE_PATH = "/etc" + "/tags/ftdi/";
    public static final String EXPERIENCE_FRAGMENTS_CONSTANT = "experience-fragments";
    public static final String SERVICE_DESCRIPTION = "service.description";
    public static final String SERVICE_VENDOR = org.osgi.framework.Constants.SERVICE_VENDOR;
    public static final String PROCESS_LABEL = "process.label";
    public static final String JCR_CONTENT = "jcr:content";
    public static final String PDP_LAYOUT_PAGE_MODEL_RESOURCE_TYPE = "web/platform/components/structure/pdplayout";
    public static final String PLP_LAYOUT_PAGE_MODEL_RESOURCE_TYPE = "web/platform/components/structure/plplayout";
    public static final String BASE_PAGE_LAYOUT_TITLE = "base-layout";
    public static final String PRICE_KEY = "price";
    public static final String POSITION_KEY = "position";
    public static final String TYPE_KEY = "type";
    public static final String BADGE_KEY = "badge";
    public static final String SIZE_KEY = "size";
    public static final String FOUR_CATEGORY_TILES_CONSTANT = "fourth";
    public static final String ON_CLICK_KEY = "onClick";
    public static final String PHOTO_KEY = "photo";
    public static final String LIFE_STYLE_CONTENT = "lifeStyleContent";
    public static final String LIFE_STYLE_CONTENT_FIRST = "lifeStyleContentFirst";
    public static final String LIFE_STYLE_CONTENT_SECOND = "lifeStyleContentSecond";
    public static final String LIFE_STYLE_CONTENT_THIRD = "lifeStyleContentThird";
    public static final String LIFE_STYLE_CONTENT_FOURTH = "lifeStyleContentFourth";
    public static final String LIFE_STYLE_CONTENT_FIFTH = "lifeStyleContentFifth";
    public static final String THREE_VALUE = "three";
    public static final String ROWNUMBER = "rowNumber";
    public static final String COLPOSITION = "colPosition";
    public static final String HEADING_KEY = "heading";
    public static final String HEADER_LOCATION = "header";
    public static final String FOOTER_LOCATION = "footer";
    public static final String MODAL_LOCATION = "modal";
    public static final String MODAL_ID_KEY = "modalId";
    public static final String OCCASIONS = "occasions";

}
