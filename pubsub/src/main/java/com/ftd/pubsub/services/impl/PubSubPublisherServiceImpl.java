package com.ftd.pubsub.services.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.threeten.bp.Duration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ftd.pubsub.beans.MessageResponseBean;
import com.ftd.pubsub.beans.PubSubPublisherConfigBean;
import com.ftd.pubsub.constants.Constants;
import com.ftd.pubsub.services.PubSubPublisherService;
import com.google.api.core.ApiFuture;
import com.google.api.core.ApiFutureCallback;
import com.google.api.core.ApiFutures;
import com.google.api.gax.batching.BatchingSettings;
import com.google.api.gax.core.CredentialsProvider;
import com.google.api.gax.core.ExecutorProvider;
import com.google.api.gax.core.FixedCredentialsProvider;
import com.google.api.gax.core.InstantiatingExecutorProvider;
import com.google.api.gax.retrying.RetrySettings;
import com.google.api.gax.rpc.ApiException;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.pubsub.v1.Publisher;
import com.google.protobuf.ByteString;
import com.google.pubsub.v1.ProjectTopicName;
import com.google.pubsub.v1.PubsubMessage;

/**
 * @author sunkatep
 *
 */
@Component(
        name = "PubSubPublisherService",
        service = PubSubPublisherService.class,
        immediate = true)
public class PubSubPublisherServiceImpl implements PubSubPublisherService
{
    private static Logger logger = LoggerFactory.getLogger(PubSubPublisherServiceImpl.class);
    
    @Reference
    private SlingSettingsService slingSettingsService;

    /**
     * This method publishes the message into the topic.
     * 
     * @param pagePath
     * @throws Exception
     */
    public void publishMessageToTopic(final String pagePath, PubSubPublisherConfigBean configBean) throws Exception
    {
        MessageResponseBean event = new MessageResponseBean();
        event.setPayloadPath(pagePath);
        event.setTranxTime(System.currentTimeMillis());
        event.setAction(configBean.getAction());
        event.setEnvironment(configBean.getEnvironment());
        Publisher publisher = null;
        try
        {
            publisher = getPublisher(configBean);
            ObjectMapper objectMapper = new ObjectMapper();
            PubsubMessage pubsubMessage = getPubsubMessage(objectMapper.writeValueAsString(event));
            logger.info("Before publishing the message: {}", pubsubMessage);
            ApiFuture<String> future = publisher.publish(pubsubMessage);
            logger.debug("future: {} ", future);
            setMessageStatusInLogFiles(future, pagePath);

        } finally
        {
            if (publisher != null)
            {
                publisher.shutdown();
            }
        }

    }

    /**
     * It returns the Publisher object.
     * 
     * @return
     * @throws IOException
     */
    private Publisher getPublisher(PubSubPublisherConfigBean configBean) throws IOException
    {
        logger.debug("In getPublisher method");
        ProjectTopicName topicName = ProjectTopicName.of(configBean.getGcpProjectName(), configBean.getGcpTopicName());

        if (StringUtils.isBlank(configBean.getCredentialsEnvVariableKey()))
        {
            logger.info("Secret credential missing, reachout to devops");
            return Publisher.newBuilder(topicName).build();
        } else
        {
            return Publisher.newBuilder(topicName).setBatchingSettings(getBatchingSettings())
                    .setCredentialsProvider(credentialProvider(configBean)).setExecutorProvider(getExecutorProvider())
                    .setRetrySettings(getRetrySettings()).build();
        }

    }

    /**
     * It returns the BatchingSettings object.
     * 
     * @return
     */
    private BatchingSettings getBatchingSettings()
    {
        logger.debug("In getBatchingSettings method");
        long requestBytesThreshold = 5000L;
        long messageCountBatchSize = 10L;
        Duration publishDelayThreshold = Duration.ofMillis(Constants.DurationofMillis);

        return BatchingSettings.newBuilder().setElementCountThreshold(messageCountBatchSize)
                .setRequestByteThreshold(requestBytesThreshold).setDelayThreshold(publishDelayThreshold).build();
    }

    /**
     * This method reads the GCP credentials from the file and return the object.
     * 
     * @return
     * @throws IOException
     */
    public CredentialsProvider credentialProvider(PubSubPublisherConfigBean configBean) throws IOException
    {
        logger.debug("In credentialProvider method: ");

        List<String> scopeList = new ArrayList<>();
        scopeList.add(Constants.PUBSUB_AUTH);
        scopeList.add(Constants.PUBSUB_SQLSERICE);
        scopeList.add(Constants.PUBSUB_CLOULDPLATFORM);

        logger.debug("this.credentialsFilePath" + configBean.getCredentialsAbsolutePath());
        File credentialsFile = new File(configBean.getCredentialsAbsolutePath());
        InputStream targetStream = new FileInputStream(credentialsFile);
        CredentialsProvider credentialsProvider;
        credentialsProvider = FixedCredentialsProvider
                .create(GoogleCredentials.fromStream(targetStream).createScoped(scopeList));
        logger.debug("credentialsProvider: {} ", credentialsProvider);
        return credentialsProvider;
    }

    /**
     * It returns the ExecutorProvider object.
     * 
     * @return
     */
    private ExecutorProvider getExecutorProvider()
    {
        logger.debug("In getExecutorProvider method");
        return InstantiatingExecutorProvider.newBuilder().setExecutorThreadCount(10).build();
    }

    /**
     * It returns the RetrySettings object.
     * 
     * @return
     */
    private RetrySettings getRetrySettings()
    {
        logger.debug("In getRetrySettings method");
        return RetrySettings.newBuilder().setTotalTimeout(Duration.ofSeconds(Constants.DurationofSeconds))
                .setInitialRetryDelay(Duration.ofMillis(Constants.DurationofMillis))
                .setRetryDelayMultiplier(Constants.DelayMultiplier).setMaxRetryDelay(Duration.ofMillis(Long.MAX_VALUE))
                .setInitialRpcTimeout(Duration.ofSeconds(Constants.DurationofSeconds))
                .setRpcTimeoutMultiplier(Constants.TimeoutMultiplier)
                .setMaxRpcTimeout(Duration.ofSeconds(Constants.DurationofSeconds)).build();
    }

    /**
     * It returns the PubsubMessage object.
     * 
     * @param pagePath
     * @return
     */
    private PubsubMessage getPubsubMessage(String pagePath)
    {
        logger.debug("In getPubsubMessage method");
        ByteString data = ByteString.copyFromUtf8(pagePath);
        return PubsubMessage.newBuilder().setData(data).build();
    }

    /**
     * If message is successfully pushed to Topic It logs the messageID's, else it logs the error messages.
     * 
     * @param future
     * @param pagePath
     */
    private void setMessageStatusInLogFiles(ApiFuture<String> future, final String pagePath)
    {
        logger.debug("Setting message staus in the log files");
        ApiFutures.addCallback(future, new ApiFutureCallback<String>()
        {
            @Override
            public void onFailure(Throwable throwable)
            {
                logger.info(" Throwable : " + throwable);
                if (throwable instanceof ApiException)
                {
                    logger.error("API Exception occurred");
                    ApiException apiException = (ApiException) throwable;
                    logger.info("pagePath : {}, status code : {}", pagePath, apiException.getStatusCode().getCode());
                    logger.debug("apiException.isRetryable() : {}", apiException.isRetryable());
                }
                logger.info("Error publishing message : " + pagePath);
            }

            @Override
            public void onSuccess(String messageId)
            {
                logger.debug("message pushed successfully to the topic");
                logger.info("content path pushed to topic : {} ", pagePath);
                logger.info(" messageId: {} ", messageId);
            }
        });
    }

}
