package com.ftd.pubsub.services;

import com.ftd.pubsub.beans.PubSubPublisherConfigBean;

/**
 * 
 * @author sunkatep
 *
 */
public interface PubSubPublisherService
{

    public void publishMessageToTopic(final String pagePath, PubSubPublisherConfigBean configBean) throws Exception;
    
}
