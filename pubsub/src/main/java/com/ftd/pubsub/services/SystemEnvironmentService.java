package com.ftd.pubsub.services;

/**
 * System environment service.
 * 
 * Utilize it to read system environment variable.
 */
public interface SystemEnvironmentService
{
    String getEnvironmentPropertyValue(final String key);
}
