package com.ftd.pubsub.services.impl;

import java.util.Map;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ftd.pubsub.services.SystemEnvironmentService;

/**
 * System environment service.
 */
@Component(
        name = "SystemEnvironmentService",
        service = SystemEnvironmentService.class,
        immediate = true)
public final class SystemEnvironmentServiceImpl implements SystemEnvironmentService
{
    private static final Logger LOG = LoggerFactory.getLogger(SystemEnvironmentServiceImpl.class);

    private transient Map<String, String> environment;

    /**
     * Activate method.
     */
    @Activate
    public void activate()
    {
        this.environment = System.getenv();
    }

    /**
     * Deactivate method.
     */
    @Deactivate
    public void deactivate()
    {
        this.environment = null;
    }

    @Override
    public String getEnvironmentPropertyValue(String key)
    {
        if (null != this.environment && this.environment.containsKey(key))
        {
            LOG.debug("Environment value :: for key {} is {}", key, this.environment.get(key));
            return this.environment.get(key);
        } else
        {
            LOG.error("System Environment service not available or {} is missing", key);
        }
        return null;
    }

}
