package com.ftd.pubsub.beans;

import java.io.Serializable;

public class MessageResponseBean implements Serializable
{

    private static final long serialVersionUID = -1846822485622841721L;

    private Long tranxTime;
    private String payloadPath;
    private String action;
    private String environment;

    public String getPayloadPath()
    {
        return payloadPath;
    }

    public void setPayloadPath(String payloadPath)
    {
        this.payloadPath = payloadPath;
    }

    public Long getTranxTime()
    {
        return tranxTime;
    }

    public void setTranxTime(Long tranxTime)
    {
        this.tranxTime = tranxTime;
    }

    public String getAction()
    {
        return action;
    }

    public void setAction(String action)
    {
        this.action = action;
    }
    
    public String getEnvironment()
    {
        return environment;
    }

    public void setEnvironment(String environment)
    {
        this.environment = environment;
    }

}
