package com.ftd.pubsub.beans;

import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;

/**
 * Config Bean Class for the Class PubSubPublisherProcess.
 * 
 * @author sunkatep
 *
 */
public class PubSubPublisherConfigBean
{
    private String credentialsEnvVariableKey;
    private String gcpProjectName;
    private String gcpTopicName;
    private Boolean isPushEnabled;
    private Long requestBytesThreshold = (long) 10000;
    private Long messageCountBatchSize = (long) 10000;
    private String[] templatePaths;
    private String credentialsAbsolutePath;
    private String environment;
    private String action;
   
    public String getCredentialsEnvVariableKey()
    {
        return credentialsEnvVariableKey;
    }

    public void setCredentialsEnvVariableKey(String credentialsEnvVariableKey)
    {
        this.credentialsEnvVariableKey = credentialsEnvVariableKey;
    }

    public String getGcpProjectName()
    {
        return gcpProjectName;
    }

    public void setGcpProjectName(String gcpProjectName)
    {
        this.gcpProjectName = gcpProjectName;
    }

    public String getGcpTopicName()
    {
        return gcpTopicName;
    }

    public void setGcpTopicName(String gcpTopicName)
    {
        this.gcpTopicName = gcpTopicName;
    }

    public Long getRequestBytesThreshold()
    {
        return requestBytesThreshold;
    }

    public void setRequestBytesThreshold(Long requestBytesThreshold)
    {
        this.requestBytesThreshold = requestBytesThreshold;
    }

    public Long getMessageCountBatchSize()
    {
        return messageCountBatchSize;
    }

    public void setMessageCountBatchSize(Long messageCountBatchSize)
    {
        this.messageCountBatchSize = messageCountBatchSize;
    }

    public String[] getTemplatePaths()
    {
        return templatePaths;
    }

    public void setTemplatePaths(String[] templatePaths)
    {
        this.templatePaths = templatePaths;
    }

    public Boolean getIsPushEnabled()
    {
        return isPushEnabled;
    }

    public void setIsPushEnabled(Boolean isPushEnabled)
    {
        this.isPushEnabled = isPushEnabled;
    }

    public String getCredentialsAbsolutePath()
    {
        return credentialsAbsolutePath;
    }

    public void setCredentialsAbsolutePath(String credentialsAbsolutePath)
    {

        this.credentialsAbsolutePath = StringUtils.removeStart(credentialsAbsolutePath, "file:");
    }
    
    public String getAction()
    {
        return action;
    }

    public void setAction(String action)
    {
        this.action = action;
    }

    public String getEnvironment()
    {
        return environment;
    }

    public void setEnvironment(String environment)
    {
        this.environment = environment;
    }
    
    @Override
    public String toString()
    {
        return "PubSubPublisherConfigBean [credentialsEnvVariableKey=" + credentialsEnvVariableKey + ", gcpProjectName="
                + gcpProjectName + ", gcpTopicName=" + gcpTopicName + ", isPushEnabled=" + isPushEnabled
                + ", requestBytesThreshold=" + requestBytesThreshold + ", messageCountBatchSize="
                + messageCountBatchSize + ", templatePaths=" + Arrays.toString(templatePaths)
                + ", credentialsAbsolutePath=" + credentialsAbsolutePath + ", environment=" + environment + ", action="
                + action + "]";
    }

}
