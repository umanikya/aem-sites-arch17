package com.ftd.pubsub.workflows;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowData;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.ftd.pubsub.beans.PubSubPublisherConfigBean;
import com.ftd.pubsub.constants.Constants;
import com.ftd.pubsub.services.PubSubPublisherService;
import com.ftd.pubsub.services.SystemEnvironmentService;

/**
 * Custom Workflow process to push a message to a topic in GCP for clearing the cache in redis layer.
 * 
 * @author abhranja1
 * 
 */
@Component(
        service = { WorkflowProcess.class }, property = {
                " service.description=PubSubPublisher service pushes a message to a google pubsub topic",
                " service.vendor=Adobe", " process.label=PubSub Publisher Process" })
public class PubSubPublisherProcess implements WorkflowProcess
{

    private static Logger logger = LoggerFactory.getLogger(PubSubPublisherProcess.class);
    private PubSubPublisherConfigBean configBean;

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    @Reference
    private PubSubPublisherService pubSubPublisherService;

    @Reference
    private SystemEnvironmentService systemEnvironmentService;

    /**
     * This is the activate method for this class. It sets the OSGI properties.
     * 
     * @param properties
     */
    @Activate
    public void activate(Map<String, Object> properties)
    {
        logger.info("[*** AEM ConfigurationService]: activating configuration service");
        setConfigProperties(properties);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.adobe.granite.workflow.exec.WorkflowProcess#execute(com.adobe.granite .workflow.exec.WorkItem,
     * com.adobe.granite.workflow.WorkflowSession, com.adobe.granite.workflow.metadata.MetaDataMap)
     */
    @Override
    public void execute(WorkItem workItem, WorkflowSession workflowSession, MetaDataMap metaDataMap)
            throws WorkflowException
    {
        final WorkflowData workflowData = workItem.getWorkflowData();

        final String payloadType = workflowData.getPayloadType();
        final String payloadPath = workflowData.getPayload().toString();
        logger.debug("payloadPath: {} ", payloadPath);
        logger.debug("configBean: {}", this.configBean);

        if (!StringUtils.equals(payloadType, "JCR_PATH"))
        {
            logger.debug("payloadType type : {}", payloadType);
            return;
        }

        if (configBean.getIsPushEnabled())
        {
            PubSubPublisherConfigBean configBeanModified = configBean;
            String processArgs = metaDataMap.get("PROCESS_ARGS","default");
            logger.debug("Arguments: "+ processArgs);
            String[] proccesArgsVals = StringUtils.split(processArgs, ",");
            
            configBeanModified.setAction(proccesArgsVals[0]);
            configBeanModified.setEnvironment(proccesArgsVals[1]);
            
            try
            {
                if (StringUtils.equals("delete", configBeanModified.getAction()))
                {
                    pubSubPublisherService.publishMessageToTopic(payloadPath, configBeanModified);
                } else
                {
                    String payloadContentTemplateName = getPayloadTemplateName(payloadPath);
                    logger.debug("payloadContentTemplateName: {} configBean.getTemplatePaths(): {} ",
                            payloadContentTemplateName, configBean.getTemplatePaths());
                    if (StringUtils.contains(Arrays.toString(configBean.getTemplatePaths()),
                            payloadContentTemplateName))
                    {
                        pubSubPublisherService.publishMessageToTopic(payloadPath, configBeanModified);
                    }
                }

            } catch (RepositoryException e)
            {
                logger.error("RepositoryException While publishing the message" + e);
            } catch (org.apache.sling.api.resource.LoginException e)
            {
                logger.error("LoginException While publishing the message" + e);
            } catch (Exception e)
            {
                logger.error("Unknown Exception While publishing the message" + e);
            }
        } else
        {
            logger.info(" Push Message Functionality Disabled for the path: {} ", payloadPath);
        }
    }

    /**
     * Gets the template name from the payload jcr content.
     * 
     * @param payloadPath
     * @return
     * @throws LoginException
     * @throws PathNotFoundException
     * @throws RepositoryException
     */
    public String getPayloadTemplateName(String payloadPath) throws LoginException, PathNotFoundException,
            RepositoryException
    {
        Map<String, Object> pubsubUser = new HashMap<>();
        pubsubUser.put(ResourceResolverFactory.SUBSERVICE, Constants.SYSTEM_USER);

        ResourceResolver resourceResolver = null;
        resourceResolver = resourceResolverFactory.getServiceResourceResolver(pubsubUser);
        logger.debug("Resource Type  :{}", resourceResolverFactory.getServiceResourceResolver(pubsubUser));
        logger.debug("payloadPath: {}", payloadPath);
        Resource payloadResource = null;
        Node payloadJcrNode = null;
        String payloadContentTemplateName = null;

        if (resourceResolver != null)
        {
            payloadResource = resourceResolver.getResource(payloadPath + "/jcr:content");
        }

        if (payloadResource != null)
        {
            payloadJcrNode = payloadResource.adaptTo(Node.class);
        }
        if (payloadJcrNode != null && payloadJcrNode.getProperty("cq:template") != null)
        {
            payloadContentTemplateName = payloadJcrNode.getProperty("cq:template").getValue().toString();
        }

        return payloadContentTemplateName;
    }

    /**
     * Reading the Run mode configurations and setting them in the global config object.
     * 
     * @param properties
     */
    protected void setConfigProperties(Map<String, Object> properties)
    {
        logger.debug("SPRING_CLOUD_GCP_CREDENTIALS_LOCATION Env variable value :: {}",
                systemEnvironmentService.getEnvironmentPropertyValue("SPRING_CLOUD_GCP_CREDENTIALS_LOCATION"));

        configBean = new PubSubPublisherConfigBean();
        configBean
                .setCredentialsEnvVariableKey(PropertiesUtil.toString(properties.get("credentialsEnvVariableKey"), ""));
        configBean.setGcpProjectName(PropertiesUtil.toString(properties.get("gcpProjectName"), ""));
        configBean.setGcpTopicName(PropertiesUtil.toString(properties.get("gcpTopicName"), ""));
        configBean.setTemplatePaths(PropertiesUtil.toStringArray(properties.get("templatePath")));
        configBean.setIsPushEnabled(PropertiesUtil.toBoolean(properties.get("isPushEnabled"), false));
        configBean.setCredentialsAbsolutePath(systemEnvironmentService.getEnvironmentPropertyValue(PropertiesUtil
                .toString(properties.get("credentialsEnvVariableKey"), "")));
        logger.debug("configBean: {}", this.configBean);
    }

}
