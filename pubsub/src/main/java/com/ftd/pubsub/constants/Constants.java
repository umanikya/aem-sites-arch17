
package com.ftd.pubsub.constants;


/**This is the class holding the list of constants.
 * @author abhranja1
 *
 */
public final class Constants {

	/**
	 * Private constructor
	 */
	private Constants() {
	}

	public static final String PUBSUB_AUTH = "https://www.googleapis.com/auth/pubsub";
	public static final String PUBSUB_SQLSERICE = "https://www.googleapis.com/auth/sqlservice.admin";
	public static final String PUBSUB_CLOULDPLATFORM = "https://www.googleapis.com/auth/cloud-platform";
	public final static int DurationofMillis = 100;
	public final static int DurationofSeconds = 10000;
	public final static int TimeoutMultiplier = 2;
	public final static int DelayMultiplier = 2;
	public static final String SYSTEM_USER = "ftdi-system-user";

}
