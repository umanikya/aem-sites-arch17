package com.ftd.platform.core.models;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.ftd.platform.core.beans.DeliveryDetailsBean;
import com.ftd.platform.core.constants.Constants;
import com.ftd.platform.core.utils.Utils;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Utils.class)
public class DeliveryDetailsModelTest
{
    @Inject
    private DeliveryDetailsModel deliveryDetailsModel;
    
    List<DeliveryDetailsBean> deliveryDetailslist;
    
    @Mock
    Resource deliveryDetails;
    
    @Before
    public void setup() throws NoSuchFieldException
    {
        deliveryDetailsModel = new DeliveryDetailsModel();
        PowerMockito.mockStatic(Utils.class);
        deliveryDetailslist = mockDeliveryDetailslist();
    }

    /**
     * Test case on the method getAttributes with CTA target as a same window.
     */
    @Test
    public void testGetAttributes()
    {
        when(Utils.getBeanDataFromResource(deliveryDetails, DeliveryDetailsBean.class)).thenReturn(deliveryDetailslist);
        Map<String, Object> attributesMap = deliveryDetailsModel.getAttributes();
        Assert.assertNotNull(attributesMap);
    }
    
    /**
     * Test case on the method getComponent.
     */
    @Test
    public void testGetComponent()
    {
        String componentID = deliveryDetailsModel.getComponent();
        Assert.assertEquals(componentID, Constants.DELIVERY_DETAILS);
        Assert.assertEquals("Delivery Details", deliveryDetailsModel.getComponentTitle());
    }
    
    @Test
    public void testGetComponentTitle()
    {
        Assert.assertEquals("Delivery Details",  deliveryDetailsModel.getComponentTitle());
    }
    
    private List<DeliveryDetailsBean> mockDeliveryDetailslist()
    {
        deliveryDetailslist = new ArrayList<>();
        DeliveryDetailsBean deliveryDetailsBean = new DeliveryDetailsBean();
        deliveryDetailsBean.setLabel("Congratulations");
        deliveryDetailsBean.setValue("Congratulations");
        deliveryDetailslist.add(deliveryDetailsBean);
        return deliveryDetailslist;
    }
}
