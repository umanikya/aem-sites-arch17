package com.ftd.platform.core.workflows;

import static org.mockito.Mockito.when;

import javax.jcr.Node;
import javax.jcr.Property;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.Workflow;
import com.adobe.granite.workflow.exec.WorkflowData;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.adobe.granite.workflow.model.WorkflowModel;
import com.ftd.platform.core.constants.Constants;

import junitx.util.PrivateAccessor;

/**
 * This Test Class is used to test AddPublishDatePropertyProcess Class
 * 
 * @author manpati
 *
 */
public final class AddPublishDatePropertyProcessTest
{

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    private AddPublishDatePropertyProcess addPublishDatePropertyProcess;

    @Mock
    private WorkItem workItem;
    @Mock
    private WorkflowSession workflowSession;
    @Mock
    private MetaDataMap metaDataMap;
    @Mock
    private Workflow workflow;
    @Mock
    private Workflow workflowOne;
    @Mock
    private WorkflowModel workflowModel;
    @Mock
    private ResourceResolverFactory resourceResolverFactory;
    @Mock
    private ResourceResolver resourceResolver;
    @Mock
    private WorkflowData workflowData;
    @Mock
    private Resource jcrResource;
    @Mock
    private Resource childResource;
    @Mock
    private Node mockNode;
    @Mock
    private Property property;

    private static final String path = "/content/experience-fragments" + "/ftd/en-us/bannerfeaturedproducts/512/512";
    private static final String ABSOLUTE_TIME = "absoluteTime";
    Long time = 1530713700L;

    /**
     * This method executes before running all test methods and is used for setting up data for the test cases.
     * 
     * @throws NoSuchFieldException
     */
    @Before
    public void setup() throws NoSuchFieldException
    {
        addPublishDatePropertyProcess = new AddPublishDatePropertyProcess();
        PrivateAccessor.setField(addPublishDatePropertyProcess, "resourceResolverFactory", resourceResolverFactory);
        when(workflow.getId()).thenReturn("Publish Date WorkFlow");
        when(workItem.getWorkflow()).thenReturn(workflow);
        
        try
        {
            when(workflowSession.getWorkflow("Publish Date WorkFlow")).thenReturn(workflowOne);
            when(workflowModel.getTitle()).thenReturn("Scheduled Page/Asset Activation");
            when(workflowOne.getWorkflowModel()).thenReturn(workflowModel);
            when(resourceResolverFactory.getServiceResourceResolver(null)).thenReturn(resourceResolver);
            when(workItem.getWorkflowData()).thenReturn(workflowData);
        } catch (LoginException e)
        {
            e.printStackTrace();
        } catch (WorkflowException e)
        {
            e.printStackTrace();
        }
        when(workflowData.getPayloadType()).thenReturn("JCR_PATH");
        when(workflowData.getPayload()).thenReturn(path);
        when(resourceResolver.getResource(path)).thenReturn(jcrResource);
    }

    public void setupForChildResource()
    {
        when(childResource.adaptTo(Node.class)).thenReturn(mockNode);
        when(metaDataMap.get(ABSOLUTE_TIME, String.class)).thenReturn("1530713700");
        when(workflowData.getMetaDataMap()).thenReturn(metaDataMap);
    }

    /**
     * Test case on the method testExecute for non null params.
     */
    @Test
    public void testExecute()
    {
        when(jcrResource.getChild(Constants.JCR_CONTENT)).thenReturn(childResource);
        setupForChildResource();
        when(metaDataMap.get("PROCESS_ARGS","default")).thenReturn("add");
        when(metaDataMap.get(ABSOLUTE_TIME, Long.class)).thenReturn(time);
        when(resourceResolver.hasChanges()).thenReturn(true);
        try
        {
            addPublishDatePropertyProcess.execute(workItem, workflowSession, metaDataMap);
        } catch (WorkflowException e)
        {
            e.printStackTrace();
        }
    }
    
    /**
     * Test case on the method testExecute for non null params with Reject.
     */
    @Test
    public void testExecuteWithReject()
    {
        when(jcrResource.getChild(Constants.JCR_CONTENT)).thenReturn(childResource);
        setupForChildResource();
        when(metaDataMap.get("PROCESS_ARGS","default")).thenReturn("remove");
        when(resourceResolver.hasChanges()).thenReturn(true);
        try
        {
            addPublishDatePropertyProcess.execute(workItem, workflowSession, metaDataMap);
        } catch (WorkflowException e)
        {
            e.printStackTrace();
        }
    }
    
    /**
     * Test case on the method testExecute for null child resource.
     */
    @Test
    public void testExecuteWithNullChildResource()
    {
        when(jcrResource.getChild(Constants.JCR_CONTENT)).thenReturn(null);
        try
        {
            addPublishDatePropertyProcess.execute(workItem, workflowSession, metaDataMap);
        } catch (WorkflowException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Test case on the method testExecute for null jcr resource.
     */
    @Test
    public void testExecuteWithNullJcrResource()
    {
        when(resourceResolver.getResource(path)).thenReturn(null);
        try
        {
            addPublishDatePropertyProcess.execute(workItem, workflowSession, metaDataMap);
        } catch (WorkflowException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Test case on the method testExecute for resourceResolver has no changes.
     */
    @Test
    public void testExecuteWithResourceRsolverHasNoChanges()
    {
        setupForChildResource();
        when(metaDataMap.get("PROCESS_ARGS","default")).thenReturn("add");
        when(jcrResource.getChild(Constants.JCR_CONTENT)).thenReturn(childResource);
        when(metaDataMap.get(ABSOLUTE_TIME, Long.class)).thenReturn(time);
        when(resourceResolver.hasChanges()).thenReturn(false);
        try
        {
            addPublishDatePropertyProcess.execute(workItem, workflowSession, metaDataMap);
        } catch (WorkflowException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Test case on the method testExecute for returning null GetAbsTime.
     */
    @Test
    public void testExecuteForGetAbsTimeReturnNull()
    {
        setupForChildResource();
        when(jcrResource.getChild(Constants.JCR_CONTENT)).thenReturn(childResource);
        when(resourceResolver.hasChanges()).thenReturn(true);
        when(metaDataMap.get(ABSOLUTE_TIME, String.class)).thenReturn(null);
        when(metaDataMap.get("PROCESS_ARGS","default")).thenReturn("add");
        try
        {
            addPublishDatePropertyProcess.execute(workItem, workflowSession, metaDataMap);
        } catch (WorkflowException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Test case on the method testExecute for invalid getPayloadType.
     */
    @Test
    public void testExecuteForGetPayloadType()
    {
        when(workflowData.getPayloadType()).thenReturn("ASSET");
        try
        {
            addPublishDatePropertyProcess.execute(workItem, workflowSession, metaDataMap);
        } catch (WorkflowException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Test case on the method testExecute for empty workflowModel Title.
     */
    @Test
    public void testExecuteForEmtyWorkFlowModelTitle()
    {
        when(workflowModel.getTitle()).thenReturn("");
        try
        {
            addPublishDatePropertyProcess.execute(workItem, workflowSession, metaDataMap);
        } catch (WorkflowException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Test case on the method testExecute for Deactivation workflowModel Title.
     */
    @Test
    public void testExecuteForAssetDeactivation()
    {
        setupForChildResource();
        when(jcrResource.getChild(Constants.JCR_CONTENT)).thenReturn(childResource);
        when(metaDataMap.get(ABSOLUTE_TIME, Long.class)).thenReturn(time);
        when(metaDataMap.get("PROCESS_ARGS","default")).thenReturn("add");
        when(resourceResolver.hasChanges()).thenReturn(true);
        when(workflowModel.getTitle()).thenReturn("Scheduled Page/Asset Deactivation");
        try
        {
            addPublishDatePropertyProcess.execute(workItem, workflowSession, metaDataMap);
        } catch (WorkflowException e)
        {
            e.printStackTrace();
        }
    }
    
    /**
     * Test case on the method testExecute for Deactivation workflowModel Title with reject.
     */
    @Test
    public void testExecuteForDeactivationReject()
    {
        setupForChildResource();
        when(jcrResource.getChild(Constants.JCR_CONTENT)).thenReturn(childResource);
        when(metaDataMap.get("PROCESS_ARGS","default")).thenReturn("remove");
        when(resourceResolver.hasChanges()).thenReturn(true);
        when(workflowModel.getTitle()).thenReturn("Scheduled Page/Asset Deactivation");
        try
        {
            addPublishDatePropertyProcess.execute(workItem, workflowSession, metaDataMap);
        } catch (WorkflowException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Test case on the method testExecute for invalid workflowModel Title.
     */
    @Test
    public void testExecuteForAssetReactivation()
    {
        setupForChildResource();
        when(jcrResource.getChild(Constants.JCR_CONTENT)).thenReturn(childResource);
        when(metaDataMap.get(ABSOLUTE_TIME, Long.class)).thenReturn(time);
        when(resourceResolver.hasChanges()).thenReturn(true);
        when(workflowModel.getTitle()).thenReturn("Scheduled Page/Asset Reactivation");
        try
        {
            addPublishDatePropertyProcess.execute(workItem, workflowSession, metaDataMap);
        } catch (WorkflowException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Test case on the method testExecute for Empty getPayloadType.
     */
    @Test
    public void testExecuteForGetPayloadTypeEmpty()
    {

        when(workflowData.getPayloadType()).thenReturn("");
        try
        {
            addPublishDatePropertyProcess.execute(workItem, workflowSession, metaDataMap);
        } catch (WorkflowException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Test case on the method testExecute for non null params.
     * 
     * @throws LoginException
     */
    @Test
    public void testExecuteForNullResourceResolver() throws LoginException
    {
        when(resourceResolverFactory.getServiceResourceResolver(null)).thenReturn(null);
        try
        {
            addPublishDatePropertyProcess.execute(workItem, workflowSession, metaDataMap);
        } catch (WorkflowException e)
        {
            e.printStackTrace();
        }
    }
}
