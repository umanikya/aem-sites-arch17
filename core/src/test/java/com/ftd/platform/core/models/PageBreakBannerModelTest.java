package com.ftd.platform.core.models;

import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;

import com.ftd.platform.core.constants.Constants;

import junitx.util.PrivateAccessor;

public class PageBreakBannerModelTest
{
    @InjectMocks
    PageBreakBannerModel pageBreakBannerModel;

    private static final String TITLE = "The Rose Experts";
    private static final String DESCRIPTION = "There is no way to convey emotions this";
    private static final String BUTTON_LABEL = "Shop Roses";
    private static final String BUTTON_LINK = "/products";
    private static final int SHOW_COUNT = 8;
    private static final double SHOW_PERCENTAGE = 55;

    @Before
    public void setup() throws NoSuchFieldException
    {
        pageBreakBannerModel = new PageBreakBannerModel();
        PrivateAccessor.setField(pageBreakBannerModel, "title", TITLE);
        PrivateAccessor.setField(pageBreakBannerModel, "description", DESCRIPTION);
        PrivateAccessor.setField(pageBreakBannerModel, "buttonLabelOne", BUTTON_LABEL);
        PrivateAccessor.setField(pageBreakBannerModel, "buttonLinkOne", BUTTON_LINK);
        PrivateAccessor.setField(pageBreakBannerModel, "buttonLabelTwo", BUTTON_LABEL);
        PrivateAccessor.setField(pageBreakBannerModel, "buttonLinkTwo", BUTTON_LINK);
        PrivateAccessor.setField(pageBreakBannerModel, "showCount", SHOW_COUNT);
        PrivateAccessor.setField(pageBreakBannerModel, "showPercentage", SHOW_PERCENTAGE);

    }

    @Test
    public void testGetAttributes()
    {
        Map<String, Object> result = pageBreakBannerModel.getAttributes();

        Assert.assertEquals(result.size(), 6);
        Assert.assertEquals(result.get(Constants.TITLE_KEY), TITLE);
        Assert.assertEquals(result.get("description"), DESCRIPTION);
        Assert.assertEquals(((Map<?, ?>) result.get(Constants.PRIMARY_CTA_KEY)).get(Constants.TEXT_KEY), BUTTON_LABEL);
        Assert.assertEquals(((Map<?, ?>) result.get(Constants.PRIMARY_CTA_KEY)).get(Constants.URL_KEY), BUTTON_LINK);
        Assert.assertEquals(result.get("showCount"), 8);
    }
}
