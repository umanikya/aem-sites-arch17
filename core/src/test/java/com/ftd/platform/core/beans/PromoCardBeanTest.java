package com.ftd.platform.core.beans;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ftd.platform.core.constants.Constants;
import com.ftd.platform.core.models.SEOMetadataModel;
import com.ftd.platform.core.services.ContentPageDataService;
import com.ftd.platform.core.utils.Utils;

import junitx.util.PrivateAccessor;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Utils.class)
public class PromoCardBeanTest
{
    @InjectMocks
    PromoCardBean promoCardBean;
    
    @Mock
    private ResourceResolver mockResourceResolver;

    @Mock
    private ContentPageDataService mockContentPageDataService;
    
    @Mock
    Resource mockResource;
    
    @Mock
    Resource childResource;
    
    @Mock
    Resource rootResource;
    
    @Mock
    ObjectMapper objectMapper;
    
    @Before
    public void setup() throws NoSuchFieldException
    {
        promoCardBean =  new PromoCardBean();
        PrivateAccessor.setField(promoCardBean, "row", 1);
        PrivateAccessor.setField(promoCardBean, "columnPosition", "right");
        PrivateAccessor.setField(promoCardBean, "promoCardReference", "C123");
        PrivateAccessor.setField(promoCardBean, "contentPageDataService", mockContentPageDataService);
        PrivateAccessor.setField(promoCardBean, "resourceResolver", mockResourceResolver);
        PowerMockito.mockStatic(Utils.class);
    }
    
    @Test
    public void testGetPromoCardListing()
    {
       Map<String, Object> map = new LinkedHashMap<>();
       map.put("row", 1);
       when(mockResourceResolver.getResource("C123" + Constants.ROOT_JCR_CONTENT_PATH)).thenReturn(mockResource);
       List<Resource> childResources;
       childResources = new ArrayList<>();
       childResources.add(childResource);
       Mockito.when(rootResource.listChildren()).thenReturn(childResources.iterator());
       when(mockContentPageDataService.getModelJsonFromResource(mockResource)).thenReturn(rootResource);
       when(objectMapper.convertValue(rootResource, Map.class)).thenReturn(map);
       Map<String, Object> result = promoCardBean.getPromoCardListing();
       Assert.assertEquals(result.size(), 2);
    }

}
