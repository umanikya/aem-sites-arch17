package com.ftd.platform.core.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.sling.api.resource.Resource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.ftd.platform.core.beans.SectionsBean;
import com.ftd.platform.core.constants.Constants;

import junitx.util.PrivateAccessor;

/**
 * Unit Tests for the class EvergreenContent.
 * 
 * @author chakrish1
 *
 */
public class EverGreenModelTest
{
    private EverGreenModel evergreenModel;
    private EverGreenModel evergreenModelSecond;
    private EverGreenModel evergreenModelThird;
    private EverGreenModel evergreenModelFourth;
    private EverGreenModel evergreenModelFifth;
    private LifeStyleContentModel lifestylemodel;

    private Resource resource = mock(Resource.class);
    private Resource resourceOne = mock(Resource.class);
    private SectionsBean sectionData = new SectionsBean();
    private Resource iconsListResource = mock(Resource.class);

    private List<Resource> childResources = new ArrayList<>();

    private static final String ICON = "/Iconsproduction";
    private static final String TEXT = "Heading of Icons";
    private static final String URL = "/Content /Mydam";
    private static final String CONTENT = "Content of icons limitation";
    private static final String LINK_TYPE = "modal";

    private static final String TITLE = "Title";
    private static final String ANCHOR = "/Content /My dam";
    private static final String DETAILS = "Description about Things";
    private static final String LABEL = "Label of the Product";
    private static final String TARGET = "true";
    private static final String VIDEO_ASSET = "/products";
    private static final String SERVER_URL = "/baners";
    private static final String VIDEO_CONTENT_URL = "/contacts";
    private static final String VIDEO_EMAIL_URL = "/emails";
    private static final String VIDEO_SERVER_URL = "/servers";
    private static final String IMAGE_URL = "/image";
    private static final String IMAGE_ALT_TEXT = "/imagealt";
    private static final String MODAL_LINK_LABEL = "Modal Link Label";
    private static final String MODAL_URL = "//s7d5.scene7.com/is/image/ProvideCommerce/FTD_18_MDAY_LX186S_MARCRO_0491_PLPBANNER";
    private static final String videoServerUrl = "/servers";
    private static final String lifeStyleContent = "three";
    /**
     * This method executes before running all test methods and is used for setting up data for the test cases
     * 
     * @throws NoSuchFieldException
     */

    @Before
    public void setup() throws NoSuchFieldException
    {
        evergreenModel = new EverGreenModel();
        evergreenModelSecond = new EverGreenModel();
        evergreenModelThird = new EverGreenModel();
        evergreenModelFourth = new EverGreenModel();
        evergreenModelFifth = new EverGreenModel();
        lifestylemodel = new LifeStyleContentModel();

        PrivateAccessor.setField(evergreenModel, "component", Constants.EVERGREEN_CONTENT_TITLE);
        PrivateAccessor.setField(evergreenModel, "iconsListResource", resource);

        PrivateAccessor.setField(sectionData, "icon", ICON);
        PrivateAccessor.setField(sectionData, "text", TEXT);
        PrivateAccessor.setField(sectionData, "content", CONTENT);
        PrivateAccessor.setField(sectionData, "linkType", LINK_TYPE);

        PrivateAccessor.setField(lifestylemodel, "title", TITLE);
        PrivateAccessor.setField(lifestylemodel, "details", DETAILS);
        PrivateAccessor.setField(lifestylemodel, "anchor", ANCHOR);
        PrivateAccessor.setField(lifestylemodel, "videoAsset", VIDEO_ASSET);
        PrivateAccessor.setField(lifestylemodel, "serverUrl", SERVER_URL);
        PrivateAccessor.setField(lifestylemodel, "videoContentUrl", VIDEO_CONTENT_URL);
        PrivateAccessor.setField(lifestylemodel, "videoEmailUrl", VIDEO_EMAIL_URL);
        PrivateAccessor.setField(lifestylemodel, "imageUrl", IMAGE_URL);
        PrivateAccessor.setField(lifestylemodel, "imageAltText", IMAGE_ALT_TEXT);
        PrivateAccessor.setField(lifestylemodel, "ctaLabel", MODAL_LINK_LABEL);
        PrivateAccessor.setField(lifestylemodel, "ctaUrl", MODAL_URL);
        PrivateAccessor.setField(lifestylemodel, "ctaTarget", TARGET);
        PrivateAccessor.setField(lifestylemodel, "lifeStyleContent", lifeStyleContent);
        PrivateAccessor.setField(lifestylemodel, "videoServerUrl", videoServerUrl);
        PrivateAccessor.setField(evergreenModel, "resource", resource);
    }

    /**
     * Test case on the method getComponent
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testGetComponent() throws NoSuchFieldException
    {

        String componentName = evergreenModel.getComponent();
        assertSame(Constants.EVERGREEN_CONTENT_TITLE, componentName);
    }

    /**
     * 
     * Test case on the method getEverGreen
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testIconsListWithModal() throws NoSuchFieldException
    {
        PrivateAccessor.setField(sectionData, "modalLinkLabel", MODAL_LINK_LABEL);
        PrivateAccessor.setField(sectionData, "modalUrl", MODAL_URL);

        List<SectionsBean> iconsList = new LinkedList<>();
        iconsList.add(sectionData);
        childResources.add(resourceOne);

        when(resource.listChildren()).thenReturn(childResources.iterator());
        when(resourceOne.adaptTo(SectionsBean.class)).thenReturn(sectionData);

        Map<String, Object> getIconsMap = evergreenModel.getEvergreen();
        Map<?, ?> titleMap = iconsList.get(0).getTitle();
        List<?> ctaList = iconsList.get(0).getCta();

        assertEquals(((LinkedList<?>) getIconsMap.get(Constants.SECTIONS_KEY)).toString(), iconsList.get(0).getIcon(),
                ICON);
        assertEquals(((LinkedList<?>) getIconsMap.get(Constants.SECTIONS_KEY)).toString(),
                iconsList.get(0).getContent(), CONTENT);

        assertEquals(titleMap.get(Constants.TEXT_KEY), TEXT);

        Assert.assertEquals(((Map<?, ?>) ctaList.get(0)).size(), 3);

    }

    /**
     * 
     * Test case on the method getEverGreen With Modal
     * 
     */
    @Test
    public void testIconsListWithoutModal()
    {

        List<SectionsBean> iconsList = new LinkedList<>();
        iconsList.add(sectionData);
        childResources.add(resourceOne);

        when(resource.listChildren()).thenReturn(childResources.iterator());
        when(resourceOne.adaptTo(SectionsBean.class)).thenReturn(sectionData);

        List<?> ctaList = iconsList.get(0).getCta();

        Assert.assertEquals(((Map<?, ?>) ctaList.get(0)).size(), 0);
    }

    @Test
    public void testGetComponentTitle()
    {
        assertEquals(evergreenModel.getComponentTitle(), "Evergreen Content");

    }

    @Test
    public void testGetLifeStyleContent() throws NoSuchFieldException
    {
        PrivateAccessor.setField(evergreenModel, "lifeStyleContent", "one");
        assertEquals(evergreenModel.getLifeStyleContent(), 1);
    }
    /*
     * @Test public void testGetprod() throws NoSuchFieldException { childResources.add(resourceOne);
     * when(resource.listChildren()).thenReturn(childResources.iterator());
     * when(resourceOne.adaptTo(LifeStyleContentModel.class)).thenReturn(lifestylemodel);
     * 
     * PrivateAccessor.setField(evergreenModel, lifeStyleContent, "one"); //
     * PrivateAccessor.setField(evergreenModel,Constants.LIFE_STYLE_CONTENT_FIRST, "videorightFirst"); // List<Object>
     * productInfoList = evergreenModel.getprod(); // Assert.assertEquals(productInfoList.size(), 2);
     * 
     * }
     */

}
