package com.ftd.platform.core.models;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import junitx.util.PrivateAccessor;

/**
 * Unit Tests for the class CardListModel class.
 * 
 * @author chakrish1
 *
 */
public class CardListModuleModelTest
{
    private CardListModuleModel cardListModuleModel;

    /**
     * This method executes before running all test methods and is used for setting up data for the test cases.
     * 
     * @throws NoSuchFieldException
     */
    @Before
    public void setup() throws NoSuchFieldException
    {
        cardListModuleModel = new CardListModuleModel();
        PrivateAccessor.setField(cardListModuleModel, "component", "card-list-module");
    }

    /**
     * Test case on the method getAttributes with left image tab
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testGetAttributesOptionleft() throws NoSuchFieldException
    {
        setLeftImageAttributes();
        Map<String, Object> leftImageMap = cardListModuleModel.getAttributes();
        assertThat(leftImageMap.size(), is(3));
        assertEquals("card-list-module", cardListModuleModel.getComponent());
        assertEquals("Card List Module", cardListModuleModel.getComponentTitle());
    }
    /**
     * Test case on the method getAttributes with Middle image tab
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testGetAttributesOptionMiddle() throws NoSuchFieldException
    {
        setMiddleImageAttributes();
        Map<String, Object> middleImageMap = cardListModuleModel.getAttributes();
        assertThat(middleImageMap.size(), is(1));
    }

    /**
     * Test case on the method getAttributes with Right image tab
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testGetAttributesOptionRight() throws NoSuchFieldException
    {
        setRightImageAttributes();
        Map<String, Object> rightImageMap = cardListModuleModel.getAttributes();
        assertThat(rightImageMap.size(), is(1));
    }

    private void setLeftImageAttributes() throws NoSuchFieldException
    {
        PrivateAccessor.setField(cardListModuleModel, "heading", "A Gift for Every Moment");
        PrivateAccessor.setField(cardListModuleModel, "subHeading", "Gift");
        PrivateAccessor.setField(cardListModuleModel, "leftImageLink", "/content/leftimage");
        PrivateAccessor.setField(cardListModuleModel, "leftImageAltText", "Left Image Not configured");
        PrivateAccessor.setField(cardListModuleModel, "leftImageCTALabel", "Left Image CTA Button Lable");
        PrivateAccessor.setField(cardListModuleModel, "leftImageCTALink", "/content/leftbuttonlink");
        PrivateAccessor.setField(cardListModuleModel, "leftImageLinkTarget", "Left newTab");
    }

    private void setMiddleImageAttributes() throws NoSuchFieldException
    {
        PrivateAccessor.setField(cardListModuleModel, "middleImageLink", "/content/middleimage");
        PrivateAccessor.setField(cardListModuleModel, "middleImageAltText", "Middle Image Not configured");
        PrivateAccessor.setField(cardListModuleModel, "middleImageCTALabel", "Middle Image CTA Button Lable");
        PrivateAccessor.setField(cardListModuleModel, "middleImageCTALink", "/content/Middlebuttonlink");
        PrivateAccessor.setField(cardListModuleModel, "middleImageLinkTarget", "Middle newTab");
    }

    private void setRightImageAttributes() throws NoSuchFieldException
    {
        PrivateAccessor.setField(cardListModuleModel, "rightImageLink", "/content/rightimage");
        PrivateAccessor.setField(cardListModuleModel, "rightImageAltText", "Right Image Not configured");
        PrivateAccessor.setField(cardListModuleModel, "rightImageCTALabel", "Right Image CTA Button Lable");
        PrivateAccessor.setField(cardListModuleModel, "rightImageCTALink", "/content/rightbuttonlink");
        PrivateAccessor.setField(cardListModuleModel, "rightImageLinkTarget", "Right newTab");
    }

}
