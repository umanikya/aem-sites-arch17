package com.ftd.platform.core.filters;

import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.ftd.platform.core.beans.ComponentDialogFilterBean;
import com.ftd.platform.core.filters.ComponentDialogFilter.StringHttpServletResponseWrapper;

import junitx.util.PrivateAccessor;

@RunWith(MockitoJUnitRunner.class)
public class ComponentDialogFilterTest
{
    @InjectMocks
    ComponentDialogFilter componentDialogFilter;
    
    @Mock
    SlingHttpServletRequest slingHttpServletRequest;
    
    @Mock
    SlingHttpServletResponse slingHttpServletResponse;
    
    @Mock
    FilterChain mockFilterChain;
    
    @Mock
    PrintWriter printWriter;
    
    @Mock
    ResourceResolver resourceResolver;
    
    @Mock
    Resource resource;
    
    @Mock
    ValueMap valuemap;
    
    @Mock
    RequestDispatcher requestDispatcher;
   
    @Mock
	Map<String,Object> properties;
  
    @Mock
    FilterConfig filterConfig;
    ComponentDialogFilterBean mockComponentDialogFilterBean;
    
    
    @Before
    public void setup() throws NoSuchFieldException,IOException
    {
        componentDialogFilter = new ComponentDialogFilter();
        mockComponentDialogFilterBean = new ComponentDialogFilterBean();
        mockComponentDialogFilterBean.setCcExcludeBodyTagValue("<section class=\"coral-TabPanel-pane coral-FixedColumn\"><div class=\"coral-FixedColumn-column\"><div class=\"coral-Form-fieldwrapper\"><label class=\"coral-Form-fieldlabel\">Variation Id</label><input class=\"coral-Form-field\" type=\"text\" name=\"./variationId\" value=\"\" data-foundation-validation=\"\" data-validation=\"\" is=\"coral-textfield\"><coral-icon class=\"coral-Form-fieldinfo\" icon=\"infoCircle\"></coral-icon><coral-tooltip target=\"_prev\" placement=\"left\"><coral-tooltip-content>Variation where the content need to be replaced</coral-tooltip-content></coral-tooltip></div></div></section>");
        mockComponentDialogFilterBean.setCcBodyHTMLTagValue("<section class=\"coral-TabPanel-pane coral-FixedColumn\"><div class=\"coral-FixedColumn-column\"><div class=\"coral-Form-fieldwrapper\"><label class=\"coral-Form-fieldlabel\">Variation Id</label><input class=\"coral-Form-field\" type=\"text\" name=\"./variationId\" value=\"\" data-foundation-validation=\"\" data-validation=\"\" is=\"coral-textfield\"><coral-icon class=\"coral-Form-fieldinfo\" icon=\"infoCircle\"></coral-icon><coral-tooltip target=\"_prev\" placement=\"left\"><coral-tooltip-content>Variation where the content need to be replaced</coral-tooltip-content></coral-tooltip></div><div class=\"coral-Form-fieldwrapper coral-Form-fieldwrapper--singleline\"><label class=\"coral-Checkbox coral-Form-field\"><input id=\"enable\" class=\"cq-dialog-checkbox-showhide coral-Checkbox-input\" type=\"checkbox\" name=\"./isConditional\" value=\"true\" data-validation=\"\" data-cq-dialog-checkbox-showhide-target=\".button-option-enable-showhide-target\" /><span class=\"coral-Checkbox-checkmark\"></span><span class=\"coral-Checkbox-description\">Is Condition Content ?</span><input type=\"hidden\" name=\"./isConditional@Delete\"></label></div><input type=\"hidden\" name=\"./enable@Delete\" value=\"true\" /><div class=\"hide button-option-enable-showhide-target\" data-showhidetargetvalue=\"true\"><div class=\"coral-Form-fieldwrapper\"><label id=\"label_1516857b-9bc0-4a85-a661-bb78b9cdf661\" class=\"coral-Form-fieldlabel\">Conditional Content Path *</label><foundation-autocomplete class=\"coral-Form-field\" name=\"./conditionalContentPath\" required=\"\" pickersrc=\"/mnt/overlay/granite/ui/content/coral/foundation/form/pathfield/picker.html?_charset_=utf-8&amp;path={value}&amp;root=%2fcontent%2fexperience-fragments&amp;filter=hierarchyNotFile&amp;selectionCount=single\" labelledby=\"label_1516857b-9bc0-4a85-a661-bb78b9cdf661\" data-foundation-validation=\"\"><coral-overlay foundation-autocomplete-suggestion=\"\" class=\"foundation-picker-buttonlist\" data-foundation-picker-buttonlist-src=\"/mnt/overlay/granite/ui/content/coral/foundation/form/pathfield/suggestion{.offset,limit}.html?_charset_=utf-8&amp;root=%2fcontent%2fexperience-fragments&amp;filter=hierarchyNotFile{&amp;query}\"></coral-overlay><coral-taglist foundation-autocomplete-value=\"\" name=\"./conditionalContentPath\"><coral-tag value=\"\"></coral-tag></coral-taglist><input class=\"foundation-field-related\" type=\"hidden\" name=\"./conditionalContentPath@Delete\"></foundation-autocomplete></div></div></div></section>");
        when(slingHttpServletResponse.getWriter()).thenReturn(printWriter);
        PrivateAccessor.setField(componentDialogFilter, "componentDialogFilterBean", mockComponentDialogFilterBean);
    }
    
    
    @Test
	public void testActivate() throws NoSuchFieldException
    {	 
    	
        String expected = "ComponentDialogFilterBean [enabled=true, ccHeadHTMLTagValue=<a class=\"coral-TabPanel-tab\" href=\"#\" data-toggle=\"tab\">cc</a>, ccBodyHTMLTagValue=<section class=\"coral-TabPanel-pane coral-FixedColumn\"><div class=\"coral-FixedColumn-column\"><div class=\"coral-Form-fieldwrapper\"><label class=\"coral-Form-fieldlabel\">Variation Id</label><input class=\"coral-Form-field\" type=\"text\" name=\"./variationId\" value=\"\" data-foundation-validation=\"\" data-validation=\"\" is=\"coral-textfield\"><coral-icon class=\"coral-Form-fieldinfo\" icon=\"infoCircle\"></coral-icon><coral-tooltip target=\"_prev\" placement=\"left\"><coral-tooltip-content>Variation where the content need to be replaced</coral-tooltip-content></coral-tooltip></div><div class=\"coral-Form-fieldwrapper coral-Form-fieldwrapper--singleline\"><label class=\"coral-Checkbox coral-Form-field\"><input id=\"enable\" class=\"cq-dialog-checkbox-showhide coral-Checkbox-input\" type=\"checkbox\" name=\"./isConditional\" value=\"true\" data-validation=\"\" data-cq-dialog-checkbox-showhide-target=\".button-option-enable-showhide-target\" /><span class=\"coral-Checkbox-checkmark\"></span><span class=\"coral-Checkbox-description\">Is Condition Content ?</span><input type=\"hidden\" name=\"./isConditional@Delete\"></label></div><input type=\"hidden\" name=\"./enable@Delete\" value=\"true\" /><div class=\"hide button-option-enable-showhide-target\" data-showhidetargetvalue=\"true\"><div class=\"coral-Form-fieldwrapper\"><label id=\"label_1516857b-9bc0-4a85-a661-bb78b9cdf661\" class=\"coral-Form-fieldlabel\">Conditional Content Path *</label><foundation-autocomplete class=\"coral-Form-field\" name=\"./conditionalContentPath\" required=\"\" pickersrc=\"/mnt/overlay/granite/ui/content/coral/foundation/form/pathfield/picker.html?_charset_=utf-8&amp;path={value}&amp;root=%2fcontent%2fexperience-fragments&amp;filter=hierarchyNotFile&amp;selectionCount=single\" labelledby=\"label_1516857b-9bc0-4a85-a661-bb78b9cdf661\" data-foundation-validation=\"\"><coral-overlay foundation-autocomplete-suggestion=\"\" class=\"foundation-picker-buttonlist\" data-foundation-picker-buttonlist-src=\"/mnt/overlay/granite/ui/content/coral/foundation/form/pathfield/suggestion{.offset,limit}.html?_charset_=utf-8&amp;root=%2fcontent%2fexperience-fragments&amp;filter=hierarchyNotFile{&amp;query}\"></coral-overlay><coral-taglist foundation-autocomplete-value=\"\" name=\"./conditionalContentPath\"><coral-tag value=\"\"></coral-tag></coral-taglist><input class=\"foundation-field-related\" type=\"hidden\" name=\"./conditionalContentPath@Delete\"></foundation-autocomplete></div></div></div></section>, ccExcludeBodyTagValue=<section class=\"coral-TabPanel-pane coral-FixedColumn\"><div class=\"coral-FixedColumn-column\"><div class=\"coral-Form-fieldwrapper\"><label class=\"coral-Form-fieldlabel\">Variation Id</label><input class=\"coral-Form-field\" type=\"text\" name=\"./variationId\" value=\"\" data-foundation-validation=\"\" data-validation=\"\" is=\"coral-textfield\"><coral-icon class=\"coral-Form-fieldinfo\" icon=\"infoCircle\"></coral-icon><coral-tooltip target=\"_prev\" placement=\"left\"><coral-tooltip-content>Variation where the content need to be replaced</coral-tooltip-content></coral-tooltip></div></div></section>, excludePaths=[/content/experience-fragments]]";
	    componentDialogFilter.activate(properties);
	    verify(properties).get("enabled");
	    verify(properties).get("excludePaths");
	    verify(properties).get("ccHeadTagValue");
	    verify(properties).get("ccBodyTagValue");
	    verify(properties).get("ccExcludeBodyTagValue");
	    mockComponentDialogFilterBean = (ComponentDialogFilterBean)PrivateAccessor.getField(componentDialogFilter,"componentDialogFilterBean");
	    assertNotNull(mockComponentDialogFilterBean.getEnabled());
	    assertNotNull(mockComponentDialogFilterBean.getExcludePaths());
	    assertNotNull(mockComponentDialogFilterBean.getCcHeadHTMLTagValue());
	    assertEquals(expected,mockComponentDialogFilterBean.toString());
	 }
    
    @Test
    public void testDoFilterWithNullResponse() throws IOException, ServletException, NoSuchFieldException
    { 
    	
        when(slingHttpServletRequest.getRequestURI()).thenReturn("/content/banner/_cq_dialog.html/content/ftd/en-us/banner_copy");
        when(slingHttpServletRequest.getResourceResolver()).thenReturn(resourceResolver);
        when(resourceResolver.getResource("/content/ftd/en-us/banner_copy")).thenReturn(resource);
        when(resource.getValueMap()).thenReturn(valuemap);
        when(slingHttpServletRequest.getRequestDispatcher("/content/banner/_cq_dialog.html/content/ftd/en-us/banner_copy")).thenReturn(requestDispatcher);
        componentDialogFilter.doFilter(slingHttpServletRequest, slingHttpServletResponse, mockFilterChain);
    }

    @Test
    public void testDoFilterNotHavingExcludePaths() throws IOException, ServletException, NoSuchFieldException
    { 
    	
        when(slingHttpServletRequest.getRequestURI()).thenReturn("/content/banner/_cq_dialog.html/content/ftd/en-us/banner_copy");
        when(slingHttpServletRequest.getResourceResolver()).thenReturn(resourceResolver);
        when(resourceResolver.getResource("/content/ftd/en-us/banner_copy")).thenReturn(resource);
        when(resource.getValueMap()).thenReturn(valuemap);
        when(slingHttpServletRequest.getRequestDispatcher("/content/banner/_cq_dialog.html/content/ftd/en-us/banner_copy")).thenReturn(requestDispatcher);
        when(valuemap.get("isConditional","false")).thenReturn("true");  
        when(valuemap.get("variationId", "")).thenReturn("1");
        doAnswer(new Answer<Object>() {
		    public Object answer(InvocationOnMock invocation) throws Throwable {	
		    	StringHttpServletResponseWrapper arg2 = (StringHttpServletResponseWrapper)invocation.getArguments()[1];
		    	arg2.getWriter().write("Dialog content");
		        return null;  
		    }}).when(requestDispatcher).include(any(SlingHttpServletRequest.class), any(StringHttpServletResponseWrapper.class));

        componentDialogFilter.doFilter(slingHttpServletRequest, slingHttpServletResponse, mockFilterChain);
    }
    

    @Test
    public void testDoFilterHavingExcludePaths() throws IOException, ServletException, NoSuchFieldException
    {
        when(slingHttpServletRequest.getRequestURI()).thenReturn("/content/banner/_cq_dialog.html/content/experience-fragments/ftd/en-us/banner_copy");
        when(slingHttpServletRequest.getResourceResolver()).thenReturn(resourceResolver);
        when(resourceResolver.getResource("/content/experience-fragments/ftd/en-us/banner_copy")).thenReturn(resource);
        when(resource.getValueMap()).thenReturn(valuemap);
        when(slingHttpServletRequest.getRequestDispatcher("/content/banner/_cq_dialog.html/content/experience-fragments/ftd/en-us/banner_copy")).thenReturn(requestDispatcher);
        when(valuemap.get("variationId", "")).thenReturn("2");
        doAnswer(new Answer<Object>() {
		    public Object answer(InvocationOnMock invocation) throws Throwable {	
		    	StringHttpServletResponseWrapper arg2 = (StringHttpServletResponseWrapper)invocation.getArguments()[1];
		    	arg2.getWriter().write("Dialog content");
		        return null;  
		    }}).when(requestDispatcher).include(any(SlingHttpServletRequest.class), any(StringHttpServletResponseWrapper.class));
        
        componentDialogFilter.doFilter(slingHttpServletRequest, slingHttpServletResponse, mockFilterChain);
    }
    
    
	
	@Test
	public void testDoFilterNotASlingInstance() throws IOException, ServletException, NoSuchFieldException
	{
		HttpServletRequest httpServletRequest   = mock(HttpServletRequest.class);
	    HttpServletResponse httpServletResponse = mock(HttpServletResponse.class);
		componentDialogFilter.doFilter(httpServletRequest,httpServletResponse,mockFilterChain);
		verify(mockFilterChain).doFilter(httpServletRequest, httpServletResponse);
    }
	
	@Test
	public void testDoFilterNotDialogRequest() throws IOException, ServletException, NoSuchFieldException
	{
	    when(slingHttpServletRequest.getRequestURI()).thenReturn("/content/banner");
		componentDialogFilter.doFilter(slingHttpServletRequest,slingHttpServletResponse,mockFilterChain);
		verify(mockFilterChain).doFilter(slingHttpServletRequest, slingHttpServletResponse);
	}
	
	@Test
	public void testDeactivate()
	{
		componentDialogFilter.deactivate();
	}
	
	@Test
	public void testInit() throws ServletException
	{
		componentDialogFilter.init(filterConfig);
	}
	@Test
	public void testDestroy()
	{
		componentDialogFilter.destroy();
	}
}
