package com.ftd.platform.core.models;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import junitx.util.PrivateAccessor;

public class PersonalOccasionBannerTest
{
    String COMPONENT_TITLE = "Personal Occasion Banner";
    private PersonalOccasionBanner personalOccasionBanner = new PersonalOccasionBanner();
    private static final String NAME_PRE_TEXT = "Name of the Banner";
    private static final String DEFAULT_NAME = "Personal Occasion Banner";
    private static final String SUMMARY_HEADING =  "Summary";
    private static final String IMAGE_LINK = "//s7d5.scene7.com/is/image/ProvideCommerce/FTD_18_MDAY_LX186S_MARCRO_0491_PLPBANNER";
    private static final String IMAGE_ALT_TEXT = "Alternative text";
    private static final String DEF_PER_IMAGE = "//s7d5.scene7.com/is/image/ProvideCommerce/FTD_18_MDAY_LX186S_MARCRO_0491_PLPBANNER";
    private static final String  DEF_PERIMAGE_ALT= "Alternative text";
    
    
    @Before
    public void setUp() throws NoSuchFieldException{
        PrivateAccessor.setField(personalOccasionBanner, "namePreText",NAME_PRE_TEXT);
        PrivateAccessor.setField(personalOccasionBanner, "defaultName", DEFAULT_NAME);
        PrivateAccessor.setField(personalOccasionBanner, "summaryHeading", SUMMARY_HEADING);
        PrivateAccessor.setField(personalOccasionBanner, "imageLink", IMAGE_LINK);
        PrivateAccessor.setField(personalOccasionBanner, "imgAltTxt", IMAGE_ALT_TEXT);
        PrivateAccessor.setField(personalOccasionBanner, "defaultPersonalImage", DEF_PER_IMAGE);
        PrivateAccessor.setField(personalOccasionBanner, "defaultPersonalImageAlt", DEF_PERIMAGE_ALT);
    }

    @Test
    public void testGetComponent()
    {
        String component = personalOccasionBanner.getComponent();
        assertEquals(component,"personal-occasion-banner");
    }
    
    @Test
    public void TestGetAttributes(){
        Map<String,Object> attributes = new HashMap<String,Object>();
        attributes = personalOccasionBanner.getAttributes();
        assertEquals(attributes.get("namePreText"),NAME_PRE_TEXT);
        assertEquals(attributes.get("defaultName"),DEFAULT_NAME);
        assertEquals(attributes.get("summaryHeading"),SUMMARY_HEADING);
        assertEquals(attributes.size(),6);
    }
    
    @Test
    public void testGetComponentTitle()
    {
        String component = personalOccasionBanner.getComponentTitle();
        assertEquals(component,COMPONENT_TITLE);
    }

}
