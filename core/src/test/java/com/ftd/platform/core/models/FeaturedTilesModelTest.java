package com.ftd.platform.core.models;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;

import java.util.List;
import java.util.Map;

import junitx.util.PrivateAccessor;

import org.junit.Before;
import org.junit.Test;

import com.ftd.platform.core.constants.Constants;

/**
 * Unit Tests for the class CategoryTilesModel class.
 * 
 * @author prabasva
 *
 */
public class FeaturedTilesModelTest
{

    private FeaturedTilesModel categoryTilesModel;

    /**
     * This method executes before running all test methods and is used for setting up data for the test cases.
     * 
     * @throws NoSuchFieldException
     */
    @Before
    public void setup() throws NoSuchFieldException
    {

        categoryTilesModel = new FeaturedTilesModel();

        PrivateAccessor.setField(categoryTilesModel, "tileGroupOneImageURL", "/content");
        PrivateAccessor.setField(categoryTilesModel, "tileGroupOneImageAltText", "tile Group One Image Alt Text");
        PrivateAccessor.setField(categoryTilesModel, "tileGroupOneURL", "tile Group One URL");
        PrivateAccessor.setField(categoryTilesModel, "tileGroupOneLinkLabel", "tile Group One Link Label");
        PrivateAccessor.setField(categoryTilesModel, "tileGroupOneLinkTarget", "tile Group One Link Target");

        PrivateAccessor.setField(categoryTilesModel, "tileGroupTwoImageURL", "tile Group Two ImageURL");
        PrivateAccessor.setField(categoryTilesModel, "tileGroupTwoImageAltText", "tile Group Two Image Alt Text");
        PrivateAccessor.setField(categoryTilesModel, "tileGroupTwoURL", "tile Group Two URL");
        PrivateAccessor.setField(categoryTilesModel, "tileGroupTwoLinkLabel", "tile Group Two Link Label");
        PrivateAccessor.setField(categoryTilesModel, "tileGroupTwoLinkTarget", "tile Group Two Link Target");

        PrivateAccessor.setField(categoryTilesModel, "tileGroupThreeImageURL", "tile Group Three ImageURL");
        PrivateAccessor.setField(categoryTilesModel, "tileGroupThreeImageAltText", "tile Group Three Image Alt Text");
        PrivateAccessor.setField(categoryTilesModel, "tileGroupThreeURL", "tile Group Three URL");
        PrivateAccessor.setField(categoryTilesModel, "tileGroupThreeLinkLabel", "tile Group Three Link Label");
        PrivateAccessor.setField(categoryTilesModel, "tileGroupThreeLinkTarget", "tile Group Three Link Target");

        PrivateAccessor.setField(categoryTilesModel, "tileGroupFourImageURL", "tile Group Four Image URL");
        PrivateAccessor.setField(categoryTilesModel, "tileGroupFourImageAltText", "tile Group Four Image Alt Text");
        PrivateAccessor.setField(categoryTilesModel, "tileGroupFourURL", "tile Group Four URL");
        PrivateAccessor.setField(categoryTilesModel, "tileGroupFourLinkLabel", "tile Group Four Link Label");
        PrivateAccessor.setField(categoryTilesModel, "tileGroupFourLinkTarget", "newtab");

    }

    /**
     * This method is used to test component tiles.
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testGetComponent() throws NoSuchFieldException
    {
        PrivateAccessor.setField(categoryTilesModel, "component", Constants.FEATURED_TILES_TITLE);
        String componentTitle = categoryTilesModel.getComponent();
        assertSame(Constants.FEATURED_TILES_TITLE, componentTitle);
        assertEquals("Category Tiles", categoryTilesModel.getComponentTitle());
    }

    /**
     * This method is used to test four tiles.
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testGetAttributesOptionOne() throws NoSuchFieldException
    {

        PrivateAccessor.setField(categoryTilesModel, "numberOfTiles", "fourth");

        Map<String, Object> tilesAtrributesMap = categoryTilesModel.getAttributes();
        List<?> tilesList = (List<?>) tilesAtrributesMap.get(Constants.CARDS_KEY);
        Map<?, ?> tilesMap = (Map<?, ?>) tilesList.get(0);
        Map<?, ?> imageMap = (Map<?, ?>) tilesMap.get("image");
        Map<?, ?> linkMap = (Map<?, ?>) tilesMap.get("link");
        assertThat(tilesList.size(), is(4));
        assertThat(tilesMap.size(), is(2));
        assertSame("/content", imageMap.get("url"));
        assertSame(false, linkMap.get("newWindow"));
    }

    /**
     * This method is used to test six tiles.
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testGetAttributesOptionTwo() throws NoSuchFieldException
    {

        PrivateAccessor.setField(categoryTilesModel, "tileGroupFiveImageURL", "/conf");
        PrivateAccessor.setField(categoryTilesModel, "tileGroupFiveImageAltText", "tile Group Fifth Image Alt Text");
        PrivateAccessor.setField(categoryTilesModel, "tileGroupFiveURL", "/conf/cat");
        PrivateAccessor.setField(categoryTilesModel, "tileGroupFiveLinkLabel", "tile Group Fifth Link Label");
        PrivateAccessor.setField(categoryTilesModel, "tileGroupFiveLinkTarget", "new");
        PrivateAccessor.setField(categoryTilesModel, "tileGroupSixImageURL", "/app");
        PrivateAccessor.setField(categoryTilesModel, "tileGroupSixImageAltText", "tile Group Sixth Image Alt Text");
        PrivateAccessor.setField(categoryTilesModel, "tileGroupSixURL", "/app");
        PrivateAccessor.setField(categoryTilesModel, "tileGroupSixLinkLabel", "tile Group Sixth Link Label");
        PrivateAccessor.setField(categoryTilesModel, "tileGroupSixLinkTarget", "same");
        PrivateAccessor.setField(categoryTilesModel, "numberOfTiles", "sixth");

        Map<String, Object> tilesAtrributesMap = categoryTilesModel.getAttributes();
        List<?> tilesList = (List<?>) tilesAtrributesMap.get(Constants.CARDS_KEY);
        Map<?, ?> tilesMap = (Map<?, ?>) tilesList.get(4);
        Map<?, ?> imageMap = (Map<?, ?>) tilesMap.get("image");
        Map<?, ?> linkMap = (Map<?, ?>) tilesMap.get("link");
        assertThat(tilesList.size(), is(6));
        assertThat(tilesMap.size(), is(2));
        assertSame(false, linkMap.get("newWindow"));
        assertSame("/conf", imageMap.get("url"));
    }
}
