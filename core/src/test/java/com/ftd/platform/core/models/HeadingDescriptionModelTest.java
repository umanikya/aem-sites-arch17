package com.ftd.platform.core.models;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.ftd.platform.core.constants.Constants;

import junitx.util.PrivateAccessor;

/**
 * Sling Model Test for Heading And Description
 * 
 * @author prabasva
 *
 */
public class HeadingDescriptionModelTest
{

    private static final String COMPONENT_DESCRIPTION = "Component Description";
    private static final String COMPONENT_HEADING = "component heading";
    private static final String COMPONENT_POSITION = "top";
    private static final String DESCRIPTION = "description";
    private static final String TITLE = "title";
    private static final String POSITION = "position";
    private HeadingDescriptionModel headingDescriptionModel;

    /**
     * This method executes before running all test methods and is used for setting up data for the test cases.
     * 
     * @throws NoSuchFieldException
     */
    @Before
    public void setup() throws NoSuchFieldException
    {

        headingDescriptionModel = new HeadingDescriptionModel();
        PrivateAccessor.setField(headingDescriptionModel, "component", Constants.HEADER_AND_DESCRIPTION_TITLE);
    }

    /**
     * This method is used to verify component title
     * 
     */
    @Test
    public void testGetComponent()
    {

        String componentTitle = headingDescriptionModel.getComponent();
        assertSame(Constants.HEADER_AND_DESCRIPTION_TITLE, componentTitle);
    }

    /**
     * This method is used to verify component map attributes
     * 
     */
    @Test
    public void testGetAttributes() throws NoSuchFieldException
    {

        PrivateAccessor.setField(headingDescriptionModel, TITLE, COMPONENT_HEADING);
        PrivateAccessor.setField(headingDescriptionModel, DESCRIPTION, COMPONENT_DESCRIPTION);
        PrivateAccessor.setField(headingDescriptionModel, POSITION, COMPONENT_POSITION);
        Map<String, Object> componentMap = headingDescriptionModel.getAttributes();
        componentMap.get(TITLE);
        assertSame(COMPONENT_HEADING, componentMap.get(TITLE));
        assertSame(COMPONENT_DESCRIPTION, componentMap.get(DESCRIPTION));
        assertSame(COMPONENT_POSITION, componentMap.get(POSITION));
        assertThat(componentMap.size(), is(3));
    }

}
