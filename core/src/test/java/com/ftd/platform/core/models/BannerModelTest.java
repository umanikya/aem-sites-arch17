package com.ftd.platform.core.models;

import java.util.Map;

import junitx.util.PrivateAccessor;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.ftd.platform.core.constants.Constants;

/**
 * Unit Tests for the class BannerModel class.
 * 
 */
public class BannerModelTest
{
    private BannerModel bannerModel;
    private static final String BUTTON_LABEL = "SHOP ALL";
    private static final String BUTTON_LINK = "/products";
    private static final String BUTTON_LINK_TARGET = "true";
    private static final String COLOR_THEME = "light";
    private static final String CONTENT_ALIGNMENT = "left";
    private static final String HEADING = "Mother's Day";
    private static final String IMAGE_LINK = "//s7d5.scene7.com/is/image/ProvideCommerce/FTD_18_MDAY_LX186S_MARCRO_0491_PLPBANNER";
    private static final String IMAGE_LINK_NEW = "//s7img.ftdi.com/is/image/ProvideCommerce/FTD_18_MDAY_LX186S_MARCRO_0491_PLPBANNER";
    private static final String IMAGE_ALT_TEXT = "Image alt";
    private static final String SUB_HEADING = "Make your mom smile with the gift of flowers for FTD.com";

    /**
     * This method executes before running all test methods and is used for setting up data for the test cases.
     * 
     * @throws NoSuchFieldException
     */
    @Before
    public void setup() throws NoSuchFieldException
    {
        bannerModel = new BannerModel();
        PrivateAccessor.setField(bannerModel, "imageLink", IMAGE_LINK);
        PrivateAccessor.setField(bannerModel, "imgAltTxt", IMAGE_ALT_TEXT);
        PrivateAccessor.setField(bannerModel, "heading", HEADING);
        PrivateAccessor.setField(bannerModel, "subHeading", SUB_HEADING);
        PrivateAccessor.setField(bannerModel, "buttonLabel", BUTTON_LABEL);
        PrivateAccessor.setField(bannerModel, "buttonLink", BUTTON_LINK);
        PrivateAccessor.setField(bannerModel, "buttonLinkTarget", BUTTON_LINK_TARGET);
        PrivateAccessor.setField(bannerModel, "contentAlignment", CONTENT_ALIGNMENT);
        PrivateAccessor.setField(bannerModel, "colorTheme", COLOR_THEME);
    }

    /**
     * Test case on the method getAttributes with CTA target as a new tab.
     */
    @Test
    public void testGetAttributesWithCTATarget()
    {

        Map<String, Object> attributesMap = bannerModel.getAttributes();

        Assert.assertEquals(attributesMap.size(), 6);
        Assert.assertEquals(attributesMap.get(Constants.TITLE_KEY), HEADING);
        Assert.assertEquals(attributesMap.get(Constants.DESCRIPTION_KEY), SUB_HEADING);
        Assert.assertEquals(attributesMap.get(Constants.THEME_KEY), COLOR_THEME);
        Assert.assertEquals(attributesMap.get(Constants.ALIGNMENT_KEY), CONTENT_ALIGNMENT);
        Assert.assertEquals(((Map<?, ?>) attributesMap.get(Constants.PRIMARY_CTA_KEY)).get(Constants.TEXT_KEY),
                BUTTON_LABEL);
        Assert.assertEquals(((Map<?, ?>) attributesMap.get(Constants.PRIMARY_CTA_KEY)).get(Constants.URL_KEY),
                BUTTON_LINK);
        Assert.assertEquals(((Map<?, ?>) attributesMap.get(Constants.PRIMARY_CTA_KEY)).get(Constants.NEW_WINDOW_KEY),
                true);
        Assert.assertEquals(((Map<?, ?>) attributesMap.get(Constants.IMAGE_KEY)).get(Constants.URL_KEY), IMAGE_LINK_NEW);
        Assert.assertEquals(((Map<?, ?>) attributesMap.get(Constants.IMAGE_KEY)).get(Constants.ALT_KEY),
                IMAGE_ALT_TEXT);

    }

    /**
     * Test case on the method getAttributes with CTA target as a same window.
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testGetAttributesWithoutCTATarget() throws NoSuchFieldException
    {

        PrivateAccessor.setField(bannerModel, "buttonLinkTarget", null);
        Map<String, Object> attributesMap = bannerModel.getAttributes();
        Assert.assertEquals(attributesMap.size(), 6);
        Assert.assertEquals(((Map<?, ?>) attributesMap.get(Constants.PRIMARY_CTA_KEY)).get(Constants.TEXT_KEY),
                BUTTON_LABEL);
        Assert.assertEquals(((Map<?, ?>) attributesMap.get(Constants.PRIMARY_CTA_KEY)).get(Constants.URL_KEY),
                BUTTON_LINK);
        Assert.assertNull(((Map<?, ?>) attributesMap.get(Constants.PRIMARY_CTA_KEY)).get(Constants.TARGET_KEY));

    }

    /**
     * Test case on the method getComponent.
     */
    @Test
    public void testGetComponent()
    {
        String componentID = bannerModel.getComponent();
        Assert.assertEquals(componentID, Constants.BANNER_COMPONENT_TITLE);
    }
}
