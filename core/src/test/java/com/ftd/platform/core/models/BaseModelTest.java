package com.ftd.platform.core.models;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import org.apache.sling.api.resource.Resource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import junitx.util.PrivateAccessor;

@RunWith(MockitoJUnitRunner.class)
public class BaseModelTest
{

    @InjectMocks
    BaseModel baseModel;

    @Mock
    Resource mockResource;

    @Before
    public void setUp() throws NoSuchFieldException
    {

        PrivateAccessor.setField(baseModel, "resource", mockResource);
        PrivateAccessor.setField(baseModel, "componentTitle", "title of the component");
        PrivateAccessor.setField(baseModel, "variationId", "2");
        PrivateAccessor.setField(baseModel, "isConditional", "true");
        PrivateAccessor.setField(baseModel, "conditionalContentPath",
                "/content/experience-fragments/ftd/en-us/banners/sources/august2018");
    }

    @Test
    public void testGetComponentUniqueId()
    {
        when(mockResource.getPath()).thenReturn("/content/ftd/main/sources");
        assertNotNull(baseModel.getComponentUniqueId());
    }

    @Test
    public void testGetComponentTitle()
    {
        assertEquals(baseModel.getComponentTitle(), "title of the component");
    }

    @Test
    public void testGetVariationId()
    {
        assertEquals(baseModel.getVariationId(), "2");
    }

    @Test
    public void testGetCCpath()
    {
        assertEquals(baseModel.getConditionalContentPath(),
                "/content/experience-fragments/ftd/en-us/banners/sources/august2018");
    }

    @Test
    public void testGetIsCondition()
    {
        String result = baseModel.getIsCondition();
        assertTrue("Have to set Conditional Content Path", result.equals("true"));
    }

}
