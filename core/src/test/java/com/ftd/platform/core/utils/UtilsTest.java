package com.ftd.platform.core.utils;

import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.factory.ModelFactory;
import org.apache.sling.settings.SlingSettingsService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;

import com.ftd.platform.core.beans.CategoryIdBean;
import com.ftd.platform.core.beans.DeliveryDetailsBean;
import com.ftd.platform.core.beans.PageSEOBean;
import com.ftd.platform.core.beans.ProdIdBean;
import com.ftd.platform.core.models.BaseModel;

/**
 * This is the unit test for Utils class
 */

@RunWith(PowerMockRunner.class)
public class UtilsTest
{
    @Mock
    Resource childResource;
    
    @Mock
    BaseModel baseModel;
    
    @Mock
    ModelFactory modelFactory;
    
    @Mock
    Resource rootResource;
    
    @Mock
    SlingSettingsService slingSettingsService;
    
    Boolean isThreeProducts;
    
    ProdIdBean prodIdBean;
    
    List<Resource> childResources;
    
    CategoryIdBean categoryIdBean;
    
    DeliveryDetailsBean deliveryDetailsBean;
    
    PageSEOBean pageSEOBean;
    
    String selector;
    
    String categoryId;
    
    List<String> prodidlist;
    
    Map<String, Object> attributes;
    
    @Before
    public void setup()
    {
        prodIdBean = mockProdIDBean();
        childResources = mockChildResources();
        categoryIdBean = mockCategoryIdBean();
        pageSEOBean = mockPageSEOBean();
        deliveryDetailsBean = mockDeliveryDetailsBean();
    }
    

    /**
     * Unit test for removeParaTagsFromRTE
     */
    @Test
    public void removeParaTagsFromRTE()
    {
        String rteText = "<p>This is testing RTE Text</p>";
        String expected = "This is testing RTE Text";
        Utils.removeParaTagsFromRTE(expected);
        Assert.assertEquals(expected, Utils.removeParaTagsFromRTE(rteText));
        Assert.assertNotEquals(rteText, Utils.removeParaTagsFromRTE(rteText));
        Assert.assertTrue("Cleaned up RTE Text => ", true);
    }
    
    @Test
    public void testGetProductListFromResource()
    {
        isThreeProducts = false; 
        when(rootResource.listChildren()).thenReturn(childResources.iterator());
        when(modelFactory.getModelFromResource(childResource)).thenReturn(baseModel);        
        when(childResource.adaptTo(ProdIdBean.class)).thenReturn(prodIdBean);
        List<String>result = Utils.getProductListFromResource(rootResource, isThreeProducts);
        Assert.assertEquals("[B07]", result.toString());
    }
    
    @Test
    public void testGetThreeProductListFromResource()
    {
        isThreeProducts = true;        
        when(rootResource.listChildren()).thenReturn(childResources.iterator());
        when(modelFactory.getModelFromResource(childResource)).thenReturn(baseModel);        
        when(childResource.adaptTo(ProdIdBean.class)).thenReturn(prodIdBean);
        List<String>result = Utils.getProductListFromResource(rootResource, isThreeProducts);
        Assert.assertEquals("[D04]", result.toString());
    }
    
    @Test
    public void testGetCategoryListFromResource()
    {
        when(rootResource.listChildren()).thenReturn(childResources.iterator());
        when(modelFactory.getModelFromResource(childResource)).thenReturn(baseModel);        
        when(childResource.adaptTo(CategoryIdBean.class)).thenReturn(categoryIdBean);
        List<CategoryIdBean>result = Utils.getBeanDataFromResource(rootResource, CategoryIdBean.class);
        Assert.assertEquals("C01", result.get(0).getCatGiftId());
        Assert.assertEquals("Birthday", result.get(0).getCatGiftLabel());
        Assert.assertEquals("Occasion-Birthday", result.get(0).getSelectValue());
    }
    
    @Test
    public void testGetDeliveryListFromResource()
    {
        when(rootResource.listChildren()).thenReturn(childResources.iterator());
        when(modelFactory.getModelFromResource(childResource)).thenReturn(baseModel);        
        when(childResource.adaptTo(DeliveryDetailsBean.class)).thenReturn(deliveryDetailsBean);
        List<DeliveryDetailsBean>result = Utils.getBeanDataFromResource(rootResource, DeliveryDetailsBean.class);
        Assert.assertEquals("Birthday", result.get(0).getLabel());
        Assert.assertEquals("Birthday", result.get(0).getValue());
    }
    
    
    @Test
    public void testGetSEOListFromResource()
    {
        Map<String, PageSEOBean> actual = new LinkedHashMap<>();
        actual.put(pageSEOBean.getName(), pageSEOBean);
        when(rootResource.listChildren()).thenReturn(childResources.iterator());
        when(modelFactory.getModelFromResource(childResource)).thenReturn(baseModel);        
        when(childResource.adaptTo(PageSEOBean.class)).thenReturn(pageSEOBean);
        Map<String, PageSEOBean>result = Utils.getSEOListFromResource(rootResource);
        result.keySet().stream().forEach((key) -> {
            Assert.assertEquals(result.get(key), actual.get(key));
        });
    }
    
    @Test
    public void testGetFormattedDate() throws ParseException
    {
        LocalDate localDate = LocalDate.of(2018, Month.OCTOBER, 29);
        Date date = new Date(localDate.atStartOfDay(ZoneId.of("UTC")).toEpochSecond() * 1000);
        String result = Utils.getFormattedDate(date);
        Assert.assertEquals("2018-10-29T00:00:00Z", result);
    }
    
    
    @Test
    public void testGetAuthorEnvironment() throws ParseException
    {
        Set<String> runModes = new HashSet<>();
        runModes.add("author");
        when(slingSettingsService.getRunModes()).thenReturn(runModes);
        String result = Utils.getEnvironment(slingSettingsService);
        Assert.assertEquals("aem-author", result);
    }
    
    @Test
    public void testGetPublishEnvironment() throws ParseException
    {
        Set<String> runModes = new HashSet<>();
        runModes.add("publish");
        when(slingSettingsService.getRunModes()).thenReturn(runModes);
        String result = Utils.getEnvironment(slingSettingsService);
        Assert.assertEquals("aem-publish", result);
    }
    
    
    @Test
    public void testSetProductIdInMap() 
    {
        selector = "prod";
        categoryId = "C01";        
        prodidlist = new ArrayList<>();
        prodidlist.add("P01");        
        attributes = new HashMap<>();
        Utils.setProductIdOrCategoryIdInMap(selector, categoryId, prodidlist, attributes);
    }
    
    @Test
    public void testSetCategoryIdInMap() 
    {
        categoryId = "C01";        
        attributes = new HashMap<>();
        Utils.setProductIdOrCategoryIdInMap(null, categoryId, null, attributes);
    }
        
    
    @Test
    public void testGetCategoryIdFromTag() 
    {
       String result = Utils.getCategoryIdFromTag("workflow:wcm");
       Assert.assertEquals("wcm", result);
    }
    
    @Test
    public void testGetValueFromTag() 
    {
       String[] tagArray = {"workflow:wcm"};
       String[] result = Utils.getValueFromTag(tagArray);
       Assert.assertEquals("wcm", result[0]);
    }
    
    private PageSEOBean mockPageSEOBean()
    {
        PageSEOBean pageSEOBean = new PageSEOBean();
        pageSEOBean.setContent("noindex, nofollow");
        pageSEOBean.setName("robots");
        return pageSEOBean;
    }
    
    private CategoryIdBean mockCategoryIdBean()
    {
        CategoryIdBean categoryIdBean = new CategoryIdBean();
        categoryIdBean.setCatGiftId("C01");
        categoryIdBean.setCatGiftLabel("Birthday");
        categoryIdBean.setSelectValue("Occasion-Birthday");
        return categoryIdBean;
    }
    
    
    private DeliveryDetailsBean mockDeliveryDetailsBean()
    {
        DeliveryDetailsBean deliveryDetailsBean = new DeliveryDetailsBean();
        deliveryDetailsBean.setLabel("Birthday");
        deliveryDetailsBean.setValue("Birthday");
        return deliveryDetailsBean;
    }
    
    private List<Resource> mockChildResources()
    {
        List<Resource> childResources = new ArrayList<>();
        childResources.add(childResource);
        return childResources;
    }
    
    private ProdIdBean mockProdIDBean()
    {
        ProdIdBean prodIdBean =  new ProdIdBean();
        prodIdBean.setProductid("B07");
        prodIdBean.setProductidThreeProd("D04");
        return prodIdBean;
    }
}
