package com.ftd.platform.core.models.page;

import static org.junit.Assert.assertEquals;

import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.ftd.platform.core.models.page.PLPLayoutPageModel;

import junitx.util.PrivateAccessor;

public class PLPLayoutPageModelTest
{
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();
    
    @Mock
    ValueMap properties;
    
    PLPLayoutPageModel plpLayoutPageModel;

    @Before
    public void setUp() throws NoSuchFieldException
    {
        plpLayoutPageModel = new PLPLayoutPageModel();
        
        PrivateAccessor.setField(plpLayoutPageModel, "properties", properties);
        
        Mockito.when(properties.get("layout", "plp-layout")).thenReturn("plp-layout");
    }

    @Test
    public void testGetLayout()
    {
        assertEquals("plp-layout", plpLayoutPageModel.getLayout());
    }
}
