package com.ftd.platform.core.utils;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.settings.SlingSettingsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Utils.class)
public class ContentPageListUtilTest
{
    @InjectMocks
    ContentPageListUtil contentPageListUtil;
    
    String pageType; 
    
    @Mock
    SearchResult mockResults;
    
    @Mock
    SlingSettingsService mockSlingSettingsService;
    
    @Mock
    private ValueMap mockValueMap;
    
    @Mock
    private Resource mockResource;
    
    @Mock
    private Hit mockHit;
    
    @Mock
    ResourceResolver mockResourceResolver;
    
    @Mock
    ResourceResolverFactory mockResourceResolverFactory;
    
    @Mock
    QueryBuilder mockQueryBuilder;
    
    @Mock
    private Query mockQuery;
    
    @Mock
    Session mockSession;
    
    Map<String, Object> contentPageListMap;
    
    Map<String, String> querymap;
    
    @Before
    public void setup() throws NoSuchFieldException, ParseException 
    {
        PowerMockito.mockStatic(Utils.class);
        contentPageListMap = MockContentPageListMap();
    }
    

    @Test
    public void testGetActivateTimeLastRep() throws ParseException
    {
        
        Date lastModified = new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.of(2018, 10, 8).toString());
        Date lastReplicated= new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.of(2018, 10, 6).toString());
        Date publishDate = new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.of(2018, 10, 6).toString());
        String lastReplicationAction = "Activate";
        Date result = ContentPageListUtil.getActivateTime(lastModified, lastReplicated, publishDate, null, lastReplicationAction);
        assertEquals(result, lastReplicated);
    }
    
    @Test
    public void testGetActivateTimePub() throws ParseException
    {
        
        Date lastModified = new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.of(2018, 10, 8).toString());
        Date lastReplicated= new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.of(2018, 10, 6).toString());
        Date publishDate = new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.of(2018, 10, 8).toString());
        String lastReplicationAction = "Activate";
        Date result = ContentPageListUtil.getActivateTime(lastModified, lastReplicated, publishDate, null, lastReplicationAction);
        assertEquals(result, publishDate);
    }
    
    @Test
    public void testGetActivateTimePubNull() throws ParseException
    {
        
        Date lastModified = new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.of(2018, 10, 8).toString());
        Date lastReplicated= new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.of(2018, 10, 6).toString());
        Date unPublishDate = new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.of(2018, 10, 8).toString());
        String lastReplicationAction = "Activate";
        Date result = ContentPageListUtil.getActivateTime(lastModified, lastReplicated, null, unPublishDate, lastReplicationAction);
        assertEquals(result, lastReplicated);
    }
    
    @Test
    public void testGetActivateTimeLastRepActNull() throws ParseException
    {
        
        Date publishDate = new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.of(2018, 10, 8).toString());
        Date result = ContentPageListUtil.getActivateTime(null, null, publishDate, null, null);
        assertEquals(result, publishDate);
    }
    
    @Test
    public void testGetActivateTimeLastRepActNotActivate() throws ParseException
    {
        
        Date publishDate = new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.of(2018, 10, 8).toString());
        String lastReplicationAction = "deActivate";
        Date result = ContentPageListUtil.getActivateTime(null, null, publishDate, null, lastReplicationAction);
        assertEquals(result, publishDate);
    }
    
    @Test
    public void testGetActivateTimeLastmod() throws ParseException
    {
        
        Date lastModified = new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.of(2018, 10, 8).toString());
        Date lastReplicated= new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.of(2018, 10, 6).toString());
        Date unPublishDate = new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.of(2018, 10, 8).toString());
        String lastReplicationAction = "deActivate";
        Date result = ContentPageListUtil.getActivateTime(lastModified, lastReplicated, null, unPublishDate, lastReplicationAction);
        assertEquals(result, lastModified);
    }
    
    @Test
    public void testGetDeActivateTimeUnPub() throws ParseException
    {
        
        Date lastReplicated= new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.of(2018, 10, 6).toString());
        Date unPublishDate = new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.of(2018, 10, 8).toString());
        String lastReplicationAction = "Deactivate";
        Date result = ContentPageListUtil.getDeActivateTime(lastReplicated, null, unPublishDate, lastReplicationAction);
        assertEquals(result, unPublishDate);
    }
    
    @Test
    public void testGetDeActivateTimeLastRep() throws ParseException
    {
        
        Date lastReplicated= new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.of(2018, 10, 6).toString());
        Date unPublishDate = new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.of(2018, 10, 5).toString());
        String lastReplicationAction = "Deactivate";
        Date result = ContentPageListUtil.getDeActivateTime(lastReplicated, null, unPublishDate, lastReplicationAction);
        assertEquals(result, lastReplicated);
    }
    
    @Test
    public void testGetDeActivateTimeUnPubNull() throws ParseException
    {
        
        Date lastReplicated= new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.of(2018, 10, 6).toString());
        String lastReplicationAction = "Deactivate";
        Date result = ContentPageListUtil.getDeActivateTime(lastReplicated, null, null, lastReplicationAction);
        assertEquals(result, lastReplicated);
    }
    
    @Test
    public void testGetDeActivateTimeLastRepActNull() throws ParseException
    {
        
        Date unPublishDate = new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.of(2018, 10, 5).toString());
        Date result = ContentPageListUtil.getDeActivateTime(null, null, unPublishDate, null);
        assertEquals(result, unPublishDate);
    }
    
    @Test
    public void testGetDeActivateTimeLastRepActNotDeActivate() throws ParseException
    {
        
        Date unPublishDate = new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.of(2018, 10, 5).toString());
        String lastReplicationAction = "Activate";
        Date result = ContentPageListUtil.getDeActivateTime(null, null, unPublishDate, lastReplicationAction);
        assertEquals(result, unPublishDate);
    }
    
    @Test
    public void testGetContentPageListFromQueryResult() throws ParseException, RepositoryException, LoginException
    {
        when(mockResults.getHits()).thenReturn(Arrays.asList(new Hit[]{mockHit}));
        when(mockHit.getPath()).thenReturn("/content/experience-fragments/FTD/en-us/seometadata/home/123");
        when(mockHit.getResource()).thenReturn(mockResource);        
        when(mockHit.getResource().getResourceResolver()).thenReturn(mockResourceResolver);        
        when(mockResults.getHits()).thenReturn(Collections.singletonList(mockHit));        
        when(mockResourceResolver.getResource(Mockito.any(String.class))).thenReturn(mockResource);        
        when(mockResource.getValueMap()).thenReturn(mockValueMap);        
        when(Utils.getEnvironment(mockSlingSettingsService)).thenReturn("author");
        
        when(mockValueMap.get("cq:lastModified", Date.class)).thenReturn(new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.of(2018, 10, 8).toString()));
        when(mockValueMap.get("cq:publishDate", Date.class)).thenReturn(new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.of(2018, 10, 6).toString()));
        when(mockValueMap.get("cq:unPublishDate", Date.class)).thenReturn(new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.of(2018, 10, 8).toString()));
        when(mockValueMap.get("cq:lastReplicated", Date.class)).thenReturn(new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.of(2018, 10, 6).toString()));
        when(mockValueMap.get("cq:lastReplicationAction", String.class)).thenReturn("Activate");
        SimpleDateFormat sdf  = new SimpleDateFormat("yyyy-MM-dd");
        when(Utils.getFormattedDate(Mockito.any(Date.class))).thenReturn(sdf.format(new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.of(2018, 10, 8).toString())));
        
        Map<String, Object> result = ContentPageListUtil.getContentPageListFromQueryResult("FTD", mockResults, mockSlingSettingsService);
        assertEquals(result.get("maxModifiedTime"), new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.of(2018, 10, 8).toString()));
        assertEquals(result.get("contentPageList").toString(), "[{FTD=/content/experience-fragments/FTD/en-us/seometadata/home/123, start=2018-10-08, end=2018-10-08, template=null}]");
    } 
    
    @Test
    public void testGetJsonFromContentPageListObject() throws ParseException
    {
        when(Utils.getEnvironment(mockSlingSettingsService)).thenReturn("author");
        SimpleDateFormat sdf  = new SimpleDateFormat("yyyy-MM-dd");
        when(Utils.getFormattedDate((Date) contentPageListMap.get("maxModifiedTime"))).thenReturn(sdf.format(new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.of(2018, 10, 8).toString())));
        String result = ContentPageListUtil.getJsonFromContentPageListObject(contentPageListMap, mockSlingSettingsService);
        assertEquals(result, "{\"environment\":\"author\",\"last-update\":\"2018-10-08\",\"data\":[\"\\\"[{FTD\\u003d/content/experience-fragments/FTD/en-us/seometadata/home/123, start\\u003d2018-10-08, end\\u003d2018-10-08, template\\u003dnull, tags\\u003dnull}]\\\"\"]}");
    }
    
    @Test
    public void testGetQueryResult() throws LoginException
    {
        querymap = new HashMap<>();
        querymap.put("test", "test");
        when(mockResourceResolverFactory.getServiceResourceResolver(Mockito.any(Map.class))).thenReturn(mockResourceResolver);
        when(mockResourceResolver.adaptTo(Session.class)).thenReturn(mockSession);
        when(mockQueryBuilder.createQuery(any(PredicateGroup.class), any(Session.class))).thenReturn(mockQuery);
        when(mockQuery.getResult()).thenReturn(mockResults);
        SearchResult result = ContentPageListUtil.getQueryResult(mockResourceResolverFactory, mockQueryBuilder, querymap);
        assertEquals(result, mockResults);
        
    }
    
    @Test
    public void testRepositoryException() throws RepositoryException
    {
        when(mockResults.getHits()).thenReturn(Arrays.asList(new Hit[]{mockHit})); 
        when(mockHit.getResource()).thenThrow(RepositoryException.class);
        Map<String, Object> result = ContentPageListUtil.getContentPageListFromQueryResult("FTD", mockResults, mockSlingSettingsService);
        assertEquals(result.get("maxModifiedTime"),null);
    }
    
    
    private Map<String, Object> MockContentPageListMap() throws ParseException
    {
        Map<String, Object> contentPageListMap = new LinkedHashMap<>();
        List<Object> contentPageList = new ArrayList<>();
        contentPageList.add("\"[{FTD=/content/experience-fragments/FTD/en-us/seometadata/home/123, start=2018-10-08, end=2018-10-08, template=null, tags=null}]\"");
        contentPageListMap.put("maxModifiedTime", new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.of(2018, 10, 8).toString()));
        contentPageListMap.put("contentPageList", contentPageList);
        
        return contentPageListMap;
    }
    
}
