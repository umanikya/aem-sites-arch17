package com.ftd.platform.core.models.page;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.ftd.platform.core.constants.Constants;
import com.ftd.platform.core.models.page.OneColumnLayoutPageModel;

public class OneColumnLayoutPageModelTest
{
    OneColumnLayoutPageModel oneColumnLayoutPageModel;

    @Before
    public void setup()
    {
        oneColumnLayoutPageModel = new OneColumnLayoutPageModel();
    }

    @Test
    public void test()
    {
        assertEquals(Constants.ONE_COLUMN_LAYOUT_TITLE, oneColumnLayoutPageModel.getLayout());
    }

}
