package com.ftd.platform.core.services.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.settings.SlingSettingsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.ftd.platform.core.utils.ContentPageListUtil;
import com.google.gson.Gson;

import junitx.util.PrivateAccessor;

/**
 * @author schauras
 *
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(ContentPageListUtil.class)
public class ContentPageListServiceImplTest
{
    
    @InjectMocks
    ContentPageListServiceImpl contentPageListServiceImpl;
    
    @Mock
    private QueryBuilder mockQueryBuilder;
    
    @Mock
    private SlingSettingsService mockSlingSettingsService;
    
    @Mock
    private ResourceResolverFactory mockResourceResolverFactory;
    
    @Mock
    private Query mockQuery;
    
    @Mock
    private SearchResult mockResults;
    
    private String siteId;
    
    private String[] allowedTemplatePaths;
    
    private Map<String, Object> contentPageListMap;
    
    private List<Object> contentPageList;
    
    private Map<String, Object> rootMap;
    private Map<String, String> intialQueryMap;
    
    /**
     * This method executes before running all test methods and is used for setting up data for the test cases.
     * 
     * @throws RepositoryException
     * @throws NoSuchFieldException
     */
    @Before
    public void setup() throws NoSuchFieldException 
    {
        contentPageListServiceImpl = new ContentPageListServiceImpl();
        PowerMockito.mockStatic(ContentPageListUtil.class);
        siteId = "1234";
        allowedTemplatePaths = mockAllowedTemplatePaths(); 
        intialQueryMap = mockInitialQueryMap();
        contentPageListServiceImpl.activate(mockProperties());
        PrivateAccessor.setField(contentPageListServiceImpl, "queryBuilder", mockQueryBuilder);
        PrivateAccessor.setField(contentPageListServiceImpl, "resourceResolverFactory", mockResourceResolverFactory);
        PrivateAccessor.setField(contentPageListServiceImpl, "slingSettingsService", mockSlingSettingsService);
        rootMap = mockRootMap();
        contentPageList = mockContentPageList();
        contentPageListMap = mockContentPageListMap(); 
        
        
    }
    

    @Test
    public void testGetContentPageList() throws LoginException
    {
        when(mockQueryBuilder.createQuery(any(PredicateGroup.class), any(Session.class))).thenReturn(mockQuery);
        when(mockQuery.getResult()).thenReturn(mockResults);
        when(ContentPageListUtil.getQueryResult(Mockito.any(ResourceResolverFactory.class), Mockito.any(QueryBuilder.class), Mockito.anyMap())).thenReturn(mockResults);
        when(ContentPageListUtil.getContentPageListFromQueryResult(Mockito.anyString(), Mockito.any(SearchResult.class), Mockito.any(SlingSettingsService.class))).thenReturn(contentPageListMap);
        when(ContentPageListUtil.getJsonFromContentPageListObject(Mockito.anyMap(), Mockito.any(SlingSettingsService.class))).thenReturn(new Gson().toJson(rootMap));
        String result = contentPageListServiceImpl.getContentPageList(siteId);
        assertEquals("{\"last-update\":\"Sep 6, 2018 12:00:00 AM\"}", result);
    }
    

    @Test
    public void testGetQuerymap()
    {
        Map<String, String> result =  contentPageListServiceImpl.getQuerymap(intialQueryMap, "");
        assertEquals(result.get("group.1_property.1_value"), "/apps/web/platform/templates/one-column-layout");
        assertEquals(result.get("group.1_property.2_value"), "/apps/web/platform/templates/global-page");
        assertEquals(result.get("group.1_property.3_value"), "/apps/web/platform/templates/pdp-layout");
        assertEquals(result.get("group.1_property.4_value"), "/apps/web/platform/templates/plp-layout");
        assertEquals(result.get("path"), "/content");
    }
    
    @Test
    public void testdeactivate()
    {
        contentPageListServiceImpl.deactivate();
    }

   
    /**
     * @return allowedPath String
     */
    private String[] mockAllowedTemplatePaths()
    {
        String[] allowedPath = { "/apps/web/platform/templates/one-column-layout",
                "/apps/web/platform/templates/global-page", "/apps/web/platform/templates/pdp-layout",
                "/apps/web/platform/templates/plp-layout" };
        return allowedPath;
    }
    
    private Map<String, Object> mockProperties()
    {
        Map<String, Object> properties = new HashMap<>();
        properties.put("allowedTemplatePaths", allowedTemplatePaths);
        return properties;
    }
    
    private Map<String, Object> mockContentPageListMap()
    {
        Map<String, Object> contentPageListMap = new HashMap<>();
        contentPageListMap.put("contentPageList", contentPageList);
        contentPageListMap.put("maxModifiedTime", new Date("09/06/2018"));
        return contentPageListMap;
    }

    private List<Object> mockContentPageList()
    {
        List<Object> contentPageList = new ArrayList<>();
        return contentPageList;
    }

    private Map<String, Object> mockRootMap()
    {
        Map<String, Object> rootMap = new LinkedHashMap<>();
        rootMap.put("last-update", new Date("Sep 6, 2018 12:00:00 AM"));
        return rootMap;
    }
    
    private Map<String, String> mockInitialQueryMap()
    {
        Map<String, String> initialQueryMap = new HashMap<>();
        initialQueryMap.put("type", "cq:PageContent");
        initialQueryMap.put("group.p.or", "true");
        initialQueryMap.put("group.1_property", "cq:template");
        for (int i = 0; i < allowedTemplatePaths.length; i++)
        {
            initialQueryMap.put("group.1_property." + (i + 1) + "_value", allowedTemplatePaths[i]);
        }
        initialQueryMap.put("p.limit", "-1");

        return initialQueryMap;

    }
}
