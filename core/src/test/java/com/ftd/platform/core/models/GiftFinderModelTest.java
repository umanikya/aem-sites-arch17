package com.ftd.platform.core.models;

import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.ftd.platform.core.constants.Constants;

import junitx.util.PrivateAccessor;

/**
 * Unit Tests for the class GiftFinderModel class.
 * 
 */
public class GiftFinderModelTest
{

    private GiftFinderModel giftFinderModel;

    private static final String HEADING_VALUE = "Heading Value";
    private static final String HELP_LABEL_VALUE = "Help Label";
    private static final String HELP_TEXT_VALUE = "help text";
    private static final String BUTTON_LABEL_VALUE = "SHOP ALL";
    private static final String CATEGORY_TAG_VALUE = "web";
    private static final String CATEGORY_VALUE = "web";
    private static final String GIFT_CATEGORY_ID_KEY = "giftCategoryId";

    /**
     * This method executes before running all test methods and is used for setting up data for the test cases.
     */
    @Before
    public void setup()
    {
        giftFinderModel = new GiftFinderModel();
        giftFinderModel.setButtonlabel(BUTTON_LABEL_VALUE);
        giftFinderModel.setCategoryId(CATEGORY_TAG_VALUE);
        giftFinderModel.setHeading(HEADING_VALUE);
        giftFinderModel.setHelpLabel(HELP_LABEL_VALUE);
        giftFinderModel.setHelpText(HELP_TEXT_VALUE);
    }

    /**
     * Test case on the method getAttributes with CTA target as a same window.
     */
    @Test
    public void testGetAttributes()
    {
        Map<String, Object> attributesMap = giftFinderModel.getAttributes();
        Assert.assertEquals(attributesMap.get(Constants.HEADING_KEY), HEADING_VALUE);
        Assert.assertEquals(attributesMap.get(Constants.HELP_LABEL_KEY), HELP_LABEL_VALUE);
        Assert.assertEquals(attributesMap.get(Constants.HELP_TEXT_KEY), HELP_TEXT_VALUE);
        Assert.assertEquals(attributesMap.get(Constants.BUTTON_LABEL_KEY), BUTTON_LABEL_VALUE);
        Assert.assertEquals(attributesMap.get(GIFT_CATEGORY_ID_KEY), CATEGORY_VALUE);
    }
    
    @Test
    public void testGetAttributesWithIdSelector() throws NoSuchFieldException
    {
        PrivateAccessor.setField(giftFinderModel, "idSelectorGift", "yes");
        Map<String, Object> attributesMap = giftFinderModel.getAttributes();
        Assert.assertEquals(8, attributesMap.size());
    }

    
    @Test
    public void testGetComponent() throws NoSuchFieldException
    {
        String componentID = giftFinderModel.getComponent();
        Assert.assertEquals(componentID, Constants.GIFT_FINDER_PLACEHOLDER_TITLE);
    }

}
