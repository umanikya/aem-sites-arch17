package com.ftd.platform.core.models;

import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.ftd.platform.core.constants.Constants;

import junitx.util.PrivateAccessor;

/**
 * Promotion banner
 */
public class PromotionalBannerModelTest
{
    private static final String CTA_URL = "ctaUrl";
    private static final String CTA_LABEL = "ctaLabel";
    private static final String[] HIDE_IN = { "clp", "plp" };
    private static final String PARAM_ONE = "ref=16733";
    private static final String PARAM_TWO = "pid=12345";
    private Resource promotionText = mock(Resource.class);
    private List<Resource> promotionTextResources = new ArrayList<>();
    private PromotionalBannerModel promotionalBannerModel;
    
    private Resource promotionTextResourceOne = mock(Resource.class);
    private Resource promotionTextResourceTwo = mock(Resource.class);
    private ValueMap valueMapResourceOne = mock(ValueMap.class);
    private ValueMap valueMapResourceTwo = mock(ValueMap.class);
    
    /**
     * This method executes before running all test methods and is used for setting up data for the test cases.
     * 
     * @throws NoSuchFieldException
     */
    @Before
    public void setup() throws NoSuchFieldException
    {
        promotionTextResources.add(promotionTextResourceOne);
        promotionTextResources.add(promotionTextResourceTwo);
        
        when(promotionTextResourceOne.getValueMap()).thenReturn(valueMapResourceOne);
        
        when(valueMapResourceOne.get("heading", "")).thenReturn("heading1");
        when(valueMapResourceOne.get("description", "")).thenReturn("description1");
        
        when(promotionTextResourceTwo.getValueMap()).thenReturn(valueMapResourceTwo);
        
        when(valueMapResourceTwo.get("heading", "")).thenReturn("heading2");
        when(valueMapResourceTwo.get("description", "")).thenReturn("description2");

        promotionalBannerModel = new PromotionalBannerModel();
        
        Map<String, String> promotionTextMap = new LinkedHashMap<>();
        
        List<Map<String, String>> promotionTextList = new ArrayList<>();

        promotionTextMap.put("heading", "heading1");
        promotionTextMap.put("description", "description1");
        promotionTextList.add(promotionTextMap);
        
        PrivateAccessor.setField(promotionalBannerModel, "component", "promotion-banner");
        PrivateAccessor.setField(promotionalBannerModel, "promotionText", promotionText);
        PrivateAccessor.setField(promotionalBannerModel, "ctaURL", CTA_URL);
        PrivateAccessor.setField(promotionalBannerModel, "ctaLabel", CTA_LABEL);
        PrivateAccessor.setField(promotionalBannerModel, "hideIn", HIDE_IN);

        PrivateAccessor.setField(promotionalBannerModel, "paramOne", PARAM_ONE);
        PrivateAccessor.setField(promotionalBannerModel, "paramTwo", PARAM_TWO);
        
        PrivateAccessor.setField(promotionalBannerModel, "changeRef", "same-page");
        PrivateAccessor.setField(promotionalBannerModel, "modalId", "test");
        PrivateAccessor.setField(promotionalBannerModel, "termsNCLabel", "Terms and Conditions");
        PrivateAccessor.setField(promotionalBannerModel, "termsNCFontColor", "blue");
        PrivateAccessor.setField(promotionalBannerModel, "tooltipLabel", "terms and condition");
        PrivateAccessor.setField(promotionalBannerModel, "tooltipDescription", "terms and condition description");
        PrivateAccessor.setField(promotionalBannerModel, "backgroundColor", "white");
        PrivateAccessor.setField(promotionalBannerModel, "fontColor", "black");
        PrivateAccessor.setField(promotionalBannerModel, "partnerLogo", "//s7d5.scene7.com/is/image/ProvideCommerce/FTD_18_MDAY_GOLD_HP_LARGE-PROMO-BANNER?");
        PrivateAccessor.setField(promotionalBannerModel, "telephone", "9999999999");
        
        
        when(promotionText.listChildren()).thenReturn(promotionTextResources.iterator());

    }

    /**
     * Test case when ChangeRef value is No and theme is present.
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testCTATagretTypeWithValueNoWithTheme() throws NoSuchFieldException
    {
        PrivateAccessor.setField(promotionalBannerModel, "changeRef", Constants.NO_VALUE);
        Map<String, Object> bannerAttributesMap = promotionalBannerModel.getAttributes();
        Map<?, ?> ctaMap = (Map<?, ?>) bannerAttributesMap.get("primaryCTA");
        
        List<?> promotionTextList = (List<?>)bannerAttributesMap.get("promotionText");
        
        assertSame(CTA_URL, ctaMap.get(Constants.URL_KEY));
        assertSame(CTA_LABEL, ctaMap.get(Constants.TEXT_KEY));
        assertSame(null, ctaMap.get("params"));

        Assert.assertEquals(2, promotionTextList.size());

        Assert.assertEquals(2, ctaMap.size());
        Assert.assertEquals(11, bannerAttributesMap.size());
        Assert.assertEquals("promotion-banner", promotionalBannerModel.getComponent());
        Assert.assertEquals("Promotional Banner", promotionalBannerModel.getComponentTitle());

    }

    /**
     * Test case when ChangeRef value is No and No theme
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testCTATagretTypeWithValueNoWithoutTheme() throws NoSuchFieldException
    {
        PrivateAccessor.setField(promotionalBannerModel, "changeRef", Constants.SAME_PAGE_CONSTANT);
        Map<String, Object> bannerAttributesMap = promotionalBannerModel.getAttributes();
        Map<?, ?> ctaMap = (Map<?, ?>) bannerAttributesMap.get("primaryCTA");
        
        List<?> promotionTextList = (List<?>)bannerAttributesMap.get("promotionText");

        assertSame("#", ctaMap.get(Constants.URL_KEY));
        assertSame(CTA_LABEL, ctaMap.get(Constants.TEXT_KEY));

        Assert.assertEquals(2, promotionTextList.size());
        assertSame(null, bannerAttributesMap.get("theme"));

        Assert.assertEquals(3, ctaMap.size());
        Assert.assertEquals(11, bannerAttributesMap.size());

    }

    /**
     * Test case when ChangeRef value as New Window and With theme
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testCTATagretTypeWithValueNewWindowWithTheme() throws NoSuchFieldException
    {
        List<Object> paramList = new LinkedList<>();
        PrivateAccessor.setField(promotionalBannerModel, "changeRef", Constants.NEW_PAGE_CONSTANT);
        
        Map<String, Object> bannerAttributesMap = promotionalBannerModel.getAttributes();
        Map<?, ?> ctaMap = (Map<?, ?>) bannerAttributesMap.get("primaryCTA");
        List<?> promotionTextList = (List<?>)bannerAttributesMap.get("promotionText");

        assertSame(CTA_URL, ctaMap.get(Constants.URL_KEY));
        assertSame(CTA_LABEL, ctaMap.get(Constants.TEXT_KEY));

        paramList.add(PARAM_ONE);
        paramList.add(PARAM_TWO);
        
        Assert.assertEquals(2, promotionTextList.size());

        Assert.assertEquals(paramList.toString(), ctaMap.get("params").toString());

        Assert.assertEquals(3, ctaMap.size());
        Assert.assertEquals(11, bannerAttributesMap.size());

    }

}
