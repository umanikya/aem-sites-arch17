package com.ftd.platform.core.models;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import junitx.util.PrivateAccessor;

import org.apache.sling.api.resource.Resource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.ftd.platform.core.beans.ProdIdBean;
import com.ftd.platform.core.constants.Constants;

/**
 * Unit Tests for the class BannerWithThreeProductsModel class.
 * 
 * @author nikhil
 * 
 */
public class BannerWithThreeProductsModelTest
{

    private BannerWithThreeProductsModel bannerWithThreeProductsModel;
    private static final String BUTTON_LABEL = "SHOP ALL";
    private static final String BUTTON_LINK = "/products";
    private static final Boolean BUTTON_LINK_TARGET = true;
    private static final String FONT_COLOR = "light";
    private static final String TEXT_ALIGNMENT = "left";
    private static final String HEADING = "Mother's Day";
    private static final String IMAGE_LINK = "//s7d5.scene7.com/is/image/ProvideCommerce/FTD_18_MDAY_LX186S_MARCRO_0491_PLPBANNER";
    private static final String IMAGE_ALT_TEXT = "Image alt";
    private static final String CATEGORY_TAG_VALUE = "tag:variation/web";
    private Resource productListResource = mock(Resource.class);
    private Resource productResourceOne = mock(Resource.class);
    private Resource productResourceTwo = mock(Resource.class);
    private Resource productResourceThree = mock(Resource.class);
    private ProdIdBean productOneBean = new ProdIdBean();
    private ProdIdBean productTwoBean = new ProdIdBean();
    private ProdIdBean productThreeBean = new ProdIdBean();
    private List<Resource> childResources = new ArrayList<>();

    /**
     * This method executes before running all test methods and is used for setting up data for the test cases.
     * 
     * @throws NoSuchFieldException
     */
    @Before
    public void setup() throws NoSuchFieldException
    {
        bannerWithThreeProductsModel = new BannerWithThreeProductsModel();

        PrivateAccessor.setField(bannerWithThreeProductsModel, "heading", HEADING);
        PrivateAccessor.setField(bannerWithThreeProductsModel, "buttonLabel", BUTTON_LABEL);
        PrivateAccessor.setField(bannerWithThreeProductsModel, "buttonLink", BUTTON_LINK);
        PrivateAccessor.setField(bannerWithThreeProductsModel, "buttonLinkTarget", BUTTON_LINK_TARGET.toString());
        PrivateAccessor.setField(bannerWithThreeProductsModel, "fontColor", FONT_COLOR);
        PrivateAccessor.setField(bannerWithThreeProductsModel, "imageLink", IMAGE_LINK);
        PrivateAccessor.setField(bannerWithThreeProductsModel, "imgAltTxt", IMAGE_ALT_TEXT);
        PrivateAccessor.setField(bannerWithThreeProductsModel, "textAlignment", TEXT_ALIGNMENT);
        PrivateAccessor.setField(bannerWithThreeProductsModel, "catId", CATEGORY_TAG_VALUE);
        PrivateAccessor.setField(bannerWithThreeProductsModel, "idSelector", Constants.PRODUCTID_KEY);
        PrivateAccessor.setField(bannerWithThreeProductsModel, "prodId", productListResource);

    }

    /**
     * Test case on the method getAttributes
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testGetAttributes() throws NoSuchFieldException
    {

        productOneBean.setProductid("123");
        productTwoBean.setProductid("456");
        productThreeBean.setProductid("789");

        childResources.add(productResourceOne);
        childResources.add(productResourceTwo);
        childResources.add(productResourceThree);

        when(productListResource.listChildren()).thenReturn(childResources.iterator());
        when(productResourceOne.adaptTo(ProdIdBean.class)).thenReturn(productOneBean);
        when(productResourceTwo.adaptTo(ProdIdBean.class)).thenReturn(productTwoBean);
        when(productResourceThree.adaptTo(ProdIdBean.class)).thenReturn(productThreeBean);

        Map<String, Object> attributesMap = bannerWithThreeProductsModel.getAttributes();
        Map<?, ?> bannerMap = (Map<?, ?>) attributesMap.get("banner");

        Assert.assertEquals(HEADING, bannerMap.get(Constants.TITLE_KEY));
        Assert.assertEquals(BUTTON_LABEL,
                ((Map<?, ?>) bannerMap.get(Constants.PRIMARY_CTA_KEY)).get(Constants.TEXT_KEY));
        Assert.assertEquals(BUTTON_LINK, ((Map<?, ?>) bannerMap.get(Constants.PRIMARY_CTA_KEY)).get(Constants.URL_KEY));
        Assert.assertEquals(BUTTON_LINK_TARGET,
                ((Map<?, ?>) bannerMap.get(Constants.PRIMARY_CTA_KEY)).get(Constants.NEW_WINDOW_KEY));

        Assert.assertEquals(3, ((ArrayList<?>) attributesMap.get(Constants.PRODUCT_KEY)).size());
        Assert.assertEquals("[123, 456, 789]", ((ArrayList<?>) attributesMap.get(Constants.PRODUCT_KEY)).toString());

    }

}
