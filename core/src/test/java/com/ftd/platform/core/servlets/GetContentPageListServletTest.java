package com.ftd.platform.core.servlets; 

import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import org.mockito.junit.MockitoJUnitRunner;
import com.ftd.platform.core.services.ContentPageListService;
import junitx.util.PrivateAccessor;

@RunWith(MockitoJUnitRunner.class)
public class GetContentPageListServletTest {

	@Mock
	SlingHttpServletRequest mockRequest;
	@Mock
	SlingHttpServletResponse mockResponse;
	@Mock
	ContentPageListService mockContentPageListService;
    @Mock
    PrintWriter printWriter;
    
    GetContentPageListServlet  getContentPageListServlet;
    
    
    
	@Before
	public void setup() throws IOException {
		getContentPageListServlet = new GetContentPageListServlet();
	    when(mockResponse.getWriter()).thenReturn(printWriter);
	}

	@Test
	public void testDoGet() throws ServletException, IOException, NoSuchFieldException
	{
		when(mockRequest.getParameter("siteId")).thenReturn("ftd");
		when(mockContentPageListService.getContentPageList("ftd")).thenReturn("fgbs rury");
		PrivateAccessor.setField(getContentPageListServlet,"contentPageListService",mockContentPageListService);
		getContentPageListServlet.doGet(mockRequest, mockResponse);
	}

}
