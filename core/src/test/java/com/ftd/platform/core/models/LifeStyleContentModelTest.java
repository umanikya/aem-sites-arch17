package com.ftd.platform.core.models;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import junitx.util.PrivateAccessor;

public class LifeStyleContentModelTest
{
    private LifeStyleContentModel lifestylemodel;
    private static final String videoServerUrl = "/servers";
    private static final String lifeStyleContent = "three";
    private static final String TITLE = "Title";
    private static final String ANCHOR = "/Content /My dam";
    private static final String DETAILS = "Description about Things";
    private static final String LABEL = "Label of the Product";
    private static final String TARGET = "true";
    private static final String VIDEO_ASSET = "/products";
    private static final String SERVER_URL = "/baners";
    private static final String VIDEO_CONTENT_URL = "/contacts";
    private static final String VIDEO_EMAIL_URL = "/emails";
    private static final String VIDEO_SERVER_URL = "/servers";
    private static final String IMAGE_URL = "/image";
    private static final String IMAGE_ALT_TEXT = "/imagealt";
    private static final String MODAL_LINK_LABEL = "Modal Link Label";
    private static final String MODAL_URL = "//s7d5.scene7.com/is/image/ProvideCommerce/FTD_18_MDAY_LX186S_MARCRO_0491_PLPBANNER";

    @Before
    public void setup() throws NoSuchFieldException
    {
        lifestylemodel = new LifeStyleContentModel();
        PrivateAccessor.setField(lifestylemodel, "title", TITLE);
        PrivateAccessor.setField(lifestylemodel, "details", DETAILS);
        PrivateAccessor.setField(lifestylemodel, "anchor", ANCHOR);
        PrivateAccessor.setField(lifestylemodel, "videoAsset", VIDEO_ASSET);
        PrivateAccessor.setField(lifestylemodel, "serverUrl", SERVER_URL);
        PrivateAccessor.setField(lifestylemodel, "videoContentUrl", VIDEO_CONTENT_URL);
        PrivateAccessor.setField(lifestylemodel, "videoEmailUrl", VIDEO_EMAIL_URL);
        PrivateAccessor.setField(lifestylemodel, "imageUrl", IMAGE_URL);
        PrivateAccessor.setField(lifestylemodel, "imageAltText", IMAGE_ALT_TEXT);
        PrivateAccessor.setField(lifestylemodel, "ctaLabel", MODAL_LINK_LABEL);
        PrivateAccessor.setField(lifestylemodel, "ctaUrl", MODAL_URL);
        PrivateAccessor.setField(lifestylemodel, "ctaTarget", TARGET);
        PrivateAccessor.setField(lifestylemodel, "lifeStyleContent", lifeStyleContent);
        PrivateAccessor.setField(lifestylemodel, "videoServerUrl", videoServerUrl);
    }

    @Test
    public void testGetLifeStyleContent()
    {
        String lifeStyleContent = lifestylemodel.getLifeStyleContent();
        Assert.assertEquals(lifeStyleContent, "three");
    }
    @Test
    public void testGetTitle()
    {
        String title = lifestylemodel.getTitle();
        Assert.assertEquals(title, "Title");
    }
    @Test
    public void testGetDetails()
    {
        String details = lifestylemodel.getDetails();
        Assert.assertEquals(details, "Description about Things");
    }
    @Test
    public void testGetAnchor()
    {
        String anchor = lifestylemodel.getAnchor();
        Assert.assertEquals(anchor, "/Content /My dam");
    }
    @Test
    public void testGetVideoAsset()
    {
        String videoasset = lifestylemodel.getVideoAsset();
        Assert.assertEquals(videoasset, "/products");
    }
    @Test
    public void testGetServerUrl()
    {
        String serverurl = lifestylemodel.getServerUrl();
        Assert.assertEquals(serverurl, "/baners");
    }
    @Test
    public void testGetVideoContentUrl()
    {
        String videocontenturl = lifestylemodel.getVideoContentUrl();
        Assert.assertEquals(videocontenturl, "/contacts");
    }
    @Test
    public void testGetVideoEmailUrl()
    {
        String videoemailurl = lifestylemodel.getVideoEmailUrl();
        Assert.assertEquals(videoemailurl, "/emails");
    }
    @Test
    public void testGetVideoServerUrl()
    {
        String videoserverurl = lifestylemodel.getVideoServerUrl();
        Assert.assertEquals(videoserverurl, "/servers");
    }
    @Test
    public void testGetImageUrl()
    {
        String imageurl = lifestylemodel.getImageUrl();
        Assert.assertEquals(imageurl, "/image");
    }
    @Test
    public void testGetImageAltText()
    {
        String imagealttext = lifestylemodel.getImageAltText();
        Assert.assertEquals(imagealttext, "/imagealt");
    }
    @Test
    public void testGetCtaLabel()
    {
        String ctalabel = lifestylemodel.getCtaLabel();
        Assert.assertEquals(ctalabel, "Modal Link Label");
    }
    @Test
    public void testGetCtaUrl()
    {
        String ctalctaurl = lifestylemodel.getCtaUrl();
        Assert.assertEquals(ctalctaurl,
                "//s7d5.scene7.com/is/image/ProvideCommerce/FTD_18_MDAY_LX186S_MARCRO_0491_PLPBANNER");
    }
    @Test
    public void testGetCtaTarget()
    {
        String ctatarget = lifestylemodel.getCtaTarget();
        Assert.assertEquals(ctatarget, "true");
    }

}
