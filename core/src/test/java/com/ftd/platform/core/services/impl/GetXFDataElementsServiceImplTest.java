package com.ftd.platform.core.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.models.factory.ModelFactory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.tagging.TagConstants;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ftd.platform.core.models.BannerModel;
import com.ftd.platform.core.services.ContentPageDataService;
import com.ftd.platform.core.services.GetXFDataElementsService;

/**
 * This Test Class is used to test GetXFDataELementsSErviceImpl Class
 *
 */
public class GetXFDataElementsServiceImplTest
{
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();
    
    @InjectMocks
    GetXFDataElementsServiceImpl getXFDataElementsServiceImpl;

    @Mock
    private ModelFactory mockModelFactory;

    @Mock
    private ResourceResolver mockResourceResolver;

    @Mock
    private Resource jcrResource;

    @Mock
    private Node mockNode;

    @Mock
    private Node mockNode1;

    @Mock
    private Resource mockChildResource;

    @Mock
    private Iterator<Resource> mockResourceIterator;
    
    @Mock
    ResourceResolverFactory resourceResolverFactory;
    
    @Mock
    private ContentPageDataService contentPageDataService;
    
    @Mock
    Node jcrContentNode;
    
    @Mock
    Property  property;
    
    @Mock
    ObjectMapper mockMapper;
    
    @Mock
    Resource componentResource;
    
    @Mock
    BannerModel bannerModel;
    

    GetXFDataElementsService getXFDataElementsService = null;
    String path = null;
    static final String RELATIVE_ROOT_PATH = "jcr:content/root";

    /**
     * This method executes before running all test methods and is used for setting up data for the test cases.
     *
     * @throws RepositoryException
     * @throws NoSuchFieldException
     * @throws ParseException 
     */
    @Before
    public void setup() throws NoSuchFieldException, ParseException
    {
        getXFDataElementsService = new GetXFDataElementsServiceImpl();
        path = "/content/experience-fragments" + "/ftd/en-us/banners/default/default";
    }

    @Test
    public void testGetComponentModelJson() throws LoginException, RepositoryException, JsonParseException, JsonMappingException, IOException
    {
        LocalDate localDate = LocalDate.of(2018, Month.OCTOBER, 29);
        Date date = new Date(localDate.atStartOfDay(ZoneId.of("UTC")).toEpochSecond() * 1000);
        Calendar calobj = Calendar.getInstance();
        calobj.setTime(date);
        Map<String, String> map = new HashMap<String,String>();
        map.put("f", "d");
        List<Resource> childResources = new ArrayList<>();
        childResources.add(mockChildResource);
        final String ROOT_NODE_PATH = "/content/experience-fragments/ftd/en-us/banners/default/14441";
        when(resourceResolverFactory.getServiceResourceResolver(null)).thenReturn(mockResourceResolver);
        when(mockResourceResolver.getResource(any(String.class))).thenReturn(jcrResource);
        when(jcrResource.adaptTo(Node.class)).thenReturn(mockNode);
        when(mockNode.hasNode(JcrConstants.JCR_CONTENT)).thenReturn(true);
        when(mockNode.getNode(JcrConstants.JCR_CONTENT)).thenReturn(jcrContentNode);
        when(jcrContentNode.hasProperty(TagConstants.PN_TAGS)).thenReturn(true);
        when(jcrContentNode.getProperty(TagConstants.PN_TAGS)).thenReturn(property); 
        
        when(mockNode.hasNode(RELATIVE_ROOT_PATH)).thenReturn(true);
        when(mockNode.getNode(RELATIVE_ROOT_PATH)).thenReturn(mockNode1);
        when(mockNode1.getPath()).thenReturn(ROOT_NODE_PATH + "jcr:content/root");
        when(jcrResource.listChildren()).thenReturn(childResources.iterator());
        
        when(jcrContentNode.hasProperty("cq:publishDate")).thenReturn(true);
        when(jcrContentNode.getProperty("cq:publishDate")).thenReturn(property);
        when(property.getDate()).thenReturn(calobj);
        when(jcrContentNode.hasProperty("cq:unPublishDate")).thenReturn(true);
        when(jcrContentNode.getProperty("cq:unPublishDate")).thenReturn(property);
       
        when(contentPageDataService.getModelFromResource(childResources.iterator().next())).thenReturn(null);
        String result = getXFDataElementsServiceImpl.getComponentModelJson("/content/experience-fragments/ftd/en-us/banners/default/14441");
        assertNotNull(result);
        assertEquals("[{\"start\":\"2018-10-29T00:00:00Z\",\"end\":\"2018-10-29T00:00:00Z\",\"tags\":\"null\"}]", result);
    }
    
    
}
