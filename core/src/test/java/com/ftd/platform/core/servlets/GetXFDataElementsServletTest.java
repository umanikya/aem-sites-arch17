package com.ftd.platform.core.servlets;

import com.ftd.platform.core.services.GetXFDataElementsService;
import junitx.util.PrivateAccessor;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestPathInfo;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.jcr.RepositoryException;
import javax.servlet.ServletException;
import java.io.IOException;
import java.io.PrintWriter;

import static org.mockito.Mockito.when;

public class GetXFDataElementsServletTest
{
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    SlingHttpServletRequest mockRequest;
    @Mock
    SlingHttpServletResponse mockResponse;
    @Mock
    RequestPathInfo requestPathInfo;
    @Mock
    GetXFDataElementsService mockGetXFDataElementsService;
    @Mock
    PrintWriter printWriter;

    GetXFDataElementsServlet getXFDataElementsServlet;
    String path = null;

    /**
     * This method executes before running all test methods and is used for setting up data for the test cases.
     *
     * @throws RepositoryException
     * @throws IOException
     */
    @Before
    public void setup() throws IOException,NoSuchFieldException
    {
        path = "/content" + "/experience-fragments/ftd/en-us/banners/default/default";
        getXFDataElementsServlet = new GetXFDataElementsServlet();
        PrivateAccessor.setField(getXFDataElementsServlet, "getXFDataElementsService", mockGetXFDataElementsService);
        when(mockRequest.getRequestPathInfo()).thenReturn(requestPathInfo);
        when(requestPathInfo.getResourcePath()).thenReturn(path);
    }

    /**
     * Test case on the method dGet for Non Null Response.
     *
     * @throws ServletException
     * @throws IOException
     * @throws NoSuchFieldException
     */
    @Test
    public void testDoGetWithNonNullResponse() throws ServletException, IOException, NoSuchFieldException
    {
        when(mockGetXFDataElementsService.getComponentModelJson(path)).thenReturn("Hello Servlet");
        when(mockResponse.getWriter()).thenReturn(printWriter);
        getXFDataElementsServlet.doGet(mockRequest, mockResponse);
    }

    /**
     * Test case on the method dGet for Null Response.
     *
     * @throws ServletException
     * @throws IOException
     * @throws NoSuchFieldException
     */
    @Test
    public void testDoGetWithNullResponse() throws ServletException, IOException, NoSuchFieldException
    {
        String str = StringUtils.EMPTY;
        when(mockGetXFDataElementsService.getComponentModelJson(path)).thenReturn(str);
        getXFDataElementsServlet.doGet(mockRequest, mockResponse);
    }

}
