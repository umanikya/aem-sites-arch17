package com.ftd.platform.core.models;

import javax.jcr.RepositoryException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.ftd.platform.core.constants.Constants;

import junitx.util.PrivateAccessor;

/**
 * Unit Tests for the class ExperienceFragmentModel class.
 * 
 */
public class ExperienceFragmentModelTest
{
    private static final String VARIATION_ID = "var1234";
    private ExperienceFragmentModel experienceFragmentModel;
    
    /**
     * This method executes before running all test methods and is used for setting up data for the test cases.
     * 
     * @throws NoSuchFieldException
     * @throws RepositoryException
     */
    @Before
    public void setup() throws NoSuchFieldException
    {
        experienceFragmentModel = new ExperienceFragmentModel();

        PrivateAccessor.setField(experienceFragmentModel, "variantId", VARIATION_ID);
    }
    
    /**
     * Test case on the method variantionId.
     */
    @Test
    public void testGetVariantionId()
    {
        String variationId = experienceFragmentModel.getVariationId();
        Assert.assertEquals(variationId, VARIATION_ID);
        Assert.assertEquals(Constants.EXPERIENCE_FRAGMENT_TITLE, experienceFragmentModel.getComponent());
        Assert.assertEquals("Experience Fragment", experienceFragmentModel.getComponentTitle());
    }

}