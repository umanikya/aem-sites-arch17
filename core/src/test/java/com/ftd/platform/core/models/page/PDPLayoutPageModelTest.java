package com.ftd.platform.core.models.page;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.ftd.platform.core.models.page.PDPLayoutPageModel;

public class PDPLayoutPageModelTest
{

    PDPLayoutPageModel pdpLayoutPageModel;

    @Before
    public void setup()
    {
        pdpLayoutPageModel = new PDPLayoutPageModel();
    }
    @Test
    public void test()
    {
        assertEquals("pdp-layout", pdpLayoutPageModel.getLayout());
    }
}
