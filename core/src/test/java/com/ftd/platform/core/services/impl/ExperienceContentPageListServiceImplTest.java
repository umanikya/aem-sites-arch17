package com.ftd.platform.core.services.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.settings.SlingSettingsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.ftd.platform.core.utils.ContentPageListUtil;
import com.google.gson.Gson;

import junitx.util.PrivateAccessor;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ContentPageListUtil.class)
public class ExperienceContentPageListServiceImplTest
{
    @InjectMocks
    ExperienceContentPageListServiceImpl experienceContentPageListServiceImpl;
    
    @Mock
    private QueryBuilder mockQueryBuilder;
    
    @Mock
    private SlingSettingsService mockSlingSettingsService;
    
    @Mock
    private ResourceResolverFactory mockResourceResolverFactory;
    
    @Mock
    private Query mockQuery;
    
    @Mock
    private SearchResult mockResults;
    
    private List<Object> contentPageList;
    
    private Map<String, Object> contentPageListMap;
    
    private Map<String, Object> rootMap;
    
    String siteId;
    
    
    /**
     * This method executes before running all test methods and is used for setting up data for the test cases.
     * 
     * @throws RepositoryException
     * @throws NoSuchFieldException
     */
    @Before
    public void setup() throws NoSuchFieldException 
    {
        experienceContentPageListServiceImpl = new ExperienceContentPageListServiceImpl();
        PowerMockito.mockStatic(ContentPageListUtil.class);
        siteId = "1234";       
        PrivateAccessor.setField(experienceContentPageListServiceImpl, "queryBuilder", mockQueryBuilder);
        PrivateAccessor.setField(experienceContentPageListServiceImpl, "resourceResolverFactory", mockResourceResolverFactory);
        PrivateAccessor.setField(experienceContentPageListServiceImpl, "slingSettingsService", mockSlingSettingsService);
        contentPageList = mockContentPageList();
        contentPageListMap = mockContentPageListMap();
        rootMap = mockRootMap();
    }
    
    @Test
    public void testGetExperienceContentPageList() throws LoginException
    {
        when(mockQueryBuilder.createQuery(any(PredicateGroup.class), any(Session.class))).thenReturn(mockQuery);
        when(mockQuery.getResult()).thenReturn(mockResults);
        when(ContentPageListUtil.getQueryResult(Mockito.any(ResourceResolverFactory.class), Mockito.any(QueryBuilder.class), Mockito.anyMap())).thenReturn(mockResults);
        when(ContentPageListUtil.getContentPageListFromQueryResult(Mockito.anyString(), Mockito.any(SearchResult.class), Mockito.any(SlingSettingsService.class))).thenReturn(contentPageListMap);
        when(ContentPageListUtil.getJsonFromContentPageListObject(Mockito.anyMap(), Mockito.any(SlingSettingsService.class))).thenReturn(new Gson().toJson(rootMap));
        String result = experienceContentPageListServiceImpl.getExperienceContentPageList(siteId);
        assertEquals("{\"last-update\":\"Sep 6, 2018 12:00:00 AM\"}", result);
    }
    
    @Test
    public void testGetQuerymap()
    {
        Map<String, String> result =  experienceContentPageListServiceImpl.getQuerymap("");
        assertEquals(result.get("group.1_property.1_value"), "/conf/web/platform/settings/wcm/templates/experience-fragment-template");
        assertEquals(result.get("type"), "cq:PageContent");
        assertEquals(result.get("group.p.or"), "true");
        assertEquals(result.get("group.1_property"), "cq:template");
        assertEquals(result.get("p.limit"), "-1");
        assertEquals(result.get("path"), "/content/experience-fragments");
    }
    
    private Map<String, Object> mockContentPageListMap()
    {
        Map<String, Object> contentPageListMap = new HashMap<>();
        contentPageListMap.put("contentPageList", contentPageList);
        contentPageListMap.put("maxModifiedTime", new Date("09/06/2018"));
        return contentPageListMap;
    }
    
    private List<Object> mockContentPageList()
    {
        List<Object> contentPageList = new ArrayList<>();
        return contentPageList;
    }
    
    private Map<String, Object> mockRootMap()
    {
        Map<String, Object> rootMap = new LinkedHashMap<>();
        rootMap.put("last-update", new Date("Sep 6, 2018 12:00:00 AM"));
        return rootMap;
    }
    
}
