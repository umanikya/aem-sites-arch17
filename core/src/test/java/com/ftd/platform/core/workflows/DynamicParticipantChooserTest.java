package com.ftd.platform.core.workflows;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import javax.jcr.RepositoryException;

import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.junit.Before;
import org.junit.Test;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.Workflow;
import com.adobe.granite.workflow.exec.WorkflowData;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.ftd.platform.core.constants.Constants;

import junitx.util.PrivateAccessor;

/**
 * This Test Class is used to test DynamicParticipantChooser Class
 * 
 * @author prabasva
 *
 */
public class DynamicParticipantChooserTest
{

    private static final String ADMIN_USER = "admin";

    private static final String PUBLISHER_GORUP_ID = "ftd-publisher";

    private static final String AUTHOR_GROUP_ID = "ftd-author";

    private DynamicParticipantChooser dynamicParticipantChooser;

    private WorkItem workItem;

    private WorkflowSession workflowSession;

    private MetaDataMap metaDataMap;

    private UserManager userManager;

    private WorkflowData workflowData;
    
    private Workflow workflow;
    
    ResourceResolverFactory resourceResolverFactory;
    Map<String, Object> param = new HashMap<>();

    /**
     * This method executes before running all test methods and is used for setting up data for the test cases.
     * 
     * @throws LoginException
     * @throws RepositoryException
     * @throws NoSuchFieldException
     */
    @Before
    public void setup() throws NoSuchFieldException
    {

        dynamicParticipantChooser = new DynamicParticipantChooser();
        workItem = mock(WorkItem.class);
        metaDataMap = mock(MetaDataMap.class);
        workflowSession = mock(WorkflowSession.class);
        userManager = mock(UserManager.class);
        workflowData = mock(WorkflowData.class);
        workflow = mock(Workflow.class);
        ResourceResolver resourceResolver = mock(ResourceResolver.class);
        resourceResolverFactory = mock(ResourceResolverFactory.class);
        PrivateAccessor.setField(dynamicParticipantChooser, "resolverFactory", resourceResolverFactory);
        when(workItem.getWorkflowData()).thenReturn(workflowData);
        param.put(ResourceResolverFactory.SUBSERVICE, Constants.SYSTEM_USER);
        when(resourceResolver.adaptTo(UserManager.class)).thenReturn(userManager);
        try
        {
            when(resourceResolverFactory.getServiceResourceResolver(param)).thenReturn(resourceResolver);
        } catch (LoginException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Test case is used to test Author Group as participant.
     * 
     * @throws WorkflowException
     * @throws RepositoryException
     */
    @Test
    public void testGetParticipantWithAuthorGroup()
    {
        Authorizable authorizable = mock(Authorizable.class);
        when(metaDataMap.get(Constants.PROCESS_ARGS, String.class)).thenReturn("author");
        when(workflowData.getPayload()).thenReturn("/content/ftd/platform");
        String authorGroup = null;
        try
        {
            when(userManager.getAuthorizable(AUTHOR_GROUP_ID)).thenReturn(authorizable);
            when(authorizable.isGroup()).thenReturn(true);
            authorGroup = dynamicParticipantChooser.getParticipant(workItem, workflowSession, metaDataMap);
        } catch (WorkflowException | RepositoryException e)
        {
            e.printStackTrace();
        }

        assertEquals(AUTHOR_GROUP_ID, authorGroup);
    }
    
    @Test
    public void testGetParticipantWithAuthorGroupThrowException() throws LoginException,WorkflowException
    {
        when(metaDataMap.get(Constants.PROCESS_ARGS, String.class)).thenReturn("author");
        when(workflowData.getPayload()).thenReturn("/content/ftd/platform");
        when(resourceResolverFactory.getServiceResourceResolver(param)).thenThrow(new LoginException());
        when(workItem.getWorkflow()).thenReturn(workflow);
        when(workItem.getWorkflow().getInitiator()).thenReturn(ADMIN_USER);
        String authorGroup = dynamicParticipantChooser.getParticipant(workItem, workflowSession, metaDataMap);
        assertEquals(ADMIN_USER, authorGroup);
    }


    /**
     * Test case is used to test Publisher Group as participant.
     * 
     * @throws WorkflowException
     */
    @Test
    public void testGetParticipantWithPublisherGroup()
    {
        Authorizable authorizableOne = mock(Authorizable.class);
        when(metaDataMap.get(Constants.PROCESS_ARGS, String.class)).thenReturn("publisher");
        when(workflowData.getPayload()).thenReturn("/content/ftd/platform");
        String authorGroup = null;
        try
        {
            when(userManager.getAuthorizable(PUBLISHER_GORUP_ID)).thenReturn(authorizableOne);
            when(authorizableOne.isGroup()).thenReturn(true);
            authorGroup = dynamicParticipantChooser.getParticipant(workItem, workflowSession, metaDataMap);
        } catch (WorkflowException | RepositoryException e)
        {
            e.printStackTrace();
        }

        assertEquals(PUBLISHER_GORUP_ID, authorGroup);
    }

    /**
     * Test case is used to test invalid user as participant.
     * 
     * @throws WorkflowException
     */
    @Test
    public void testGetParticipantWithInvalidGroup()
    {
        Authorizable authorizableOne = mock(Authorizable.class);
        Workflow workflow = mock(Workflow.class);
        when(workItem.getWorkflow()).thenReturn(workflow);
        when(workflow.getInitiator()).thenReturn(ADMIN_USER);
        when(workflowData.getPayload()).thenReturn("/content/experience-fragments/ftd/platform");
        when(metaDataMap.get(Constants.PROCESS_ARGS, String.class)).thenReturn("publisher");
        String authorGroup = null;
        try
        {
            when(userManager.getAuthorizable(PUBLISHER_GORUP_ID)).thenReturn(authorizableOne);
            when(authorizableOne.isGroup()).thenReturn(false);
            authorGroup = dynamicParticipantChooser.getParticipant(workItem, workflowSession, metaDataMap);
        } catch (WorkflowException | RepositoryException e)
        {
            e.printStackTrace();
        }

        assertEquals(ADMIN_USER, authorGroup);
    }
}
