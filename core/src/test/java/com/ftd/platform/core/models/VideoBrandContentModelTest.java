package com.ftd.platform.core.models;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.sling.api.resource.Resource;

import org.junit.Before;
import org.junit.Test;

import com.ftd.platform.core.beans.IconBean;
import com.ftd.platform.core.constants.Constants;

import junitx.util.PrivateAccessor;

/**
 * @author chakrish1
 *
 */
public class VideoBrandContentModelTest
{

    private VideoBrandContentModel videoBrandContentModel;
    private Resource resource = mock(Resource.class);
    private Resource resourceOne = mock(Resource.class);
    private List<Resource> childResources = new LinkedList<>();
    private IconBean iconData = new IconBean();

    private static final String TEXT = "TEXT";
    private static final String VIDEO_ASSET = "/products";
    private static final String SERVER_URL = "/baners";
    private static final String VIDEO_CONTENT_URL = "/contacts";
    private static final String VIDEO_EMAIL_URL = "/emails";
    private static final String VIDEO_SERVER_URL = "/servers";
    private static final String VIDEO_POSTER_IMAGE_URL = "/images";
    private static final String TITLE = "text heading";
    private static final String DISCRIPTION = "Make your mom smile with the gift of flowers for FTD.com";
    private static final String ICON_ALT_TEXT = "Icon alternative text";
    private static final String ICON_LINK = "/icons";

    /**
     * This method executes before running all test methods and is used for setting up data for the test cases.
     * 
     * @throws NoSuchFieldException
     */
    @Before
    public void setup() throws NoSuchFieldException
    {

        videoBrandContentModel = new VideoBrandContentModel();
        iconData = new IconBean();
        PrivateAccessor.setField(videoBrandContentModel, "videoAsset", VIDEO_ASSET);
        PrivateAccessor.setField(videoBrandContentModel, "videContentUrl", VIDEO_CONTENT_URL);
        PrivateAccessor.setField(videoBrandContentModel, "videoEmailUrl", VIDEO_EMAIL_URL);
        PrivateAccessor.setField(videoBrandContentModel, "serverUrl", SERVER_URL);
        PrivateAccessor.setField(videoBrandContentModel, "videoServerUrl", VIDEO_SERVER_URL);
        PrivateAccessor.setField(videoBrandContentModel, "videoPosterImage", VIDEO_POSTER_IMAGE_URL);
        PrivateAccessor.setField(videoBrandContentModel, "title", TITLE);
        PrivateAccessor.setField(videoBrandContentModel, "description", DISCRIPTION);
        PrivateAccessor.setField(videoBrandContentModel, "imageResource", resource);
        PrivateAccessor.setField(videoBrandContentModel, "buttonLabel", "JOIN NOW");
        PrivateAccessor.setField(videoBrandContentModel, "buttonLink", "https://www.ftd.com/ftd-gold-ctg/joingold-alt4/?intcid=goldmday_SubTile1&fs_trigger=1&isLegacy=1");
        PrivateAccessor.setField(videoBrandContentModel, "iconHeading", "Delivered by Hand");
        PrivateAccessor.setField(videoBrandContentModel, "iconDescription", "When flowers are delivered, they create a special moment. So, we strive for perfection with every bouquet.");
        iconData.setIconAltText(ICON_ALT_TEXT);
        iconData.setIconLink(ICON_LINK);
        iconData.setText(TEXT);
        iconData.setTitle(TITLE);
        Map<String, String> imageMap = iconData.getImage();
        imageMap.put(Constants.ALT_KEY, ICON_ALT_TEXT);
        imageMap.put(Constants.URL_KEY, ICON_LINK);

    }

    /**
     * Test case on the method getAttributes
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testGetAttributes() throws NoSuchFieldException
    {

        List<IconBean> iconsList = new LinkedList<>();
        iconsList.add(iconData);
        childResources.add(resourceOne);

        when(resource.listChildren()).thenReturn(childResources.iterator());
        when(resourceOne.adaptTo(IconBean.class)).thenReturn(iconData);

        Map<String, Object> attributesMap = videoBrandContentModel.getAttributes();

        assertEquals(8, attributesMap.size());
        assertEquals(attributesMap.get(Constants.TITLE_KEY), TITLE);
        assertEquals(attributesMap.get(Constants.TEXT_KEY), DISCRIPTION);
        assertEquals(((Map<?, ?>) attributesMap.get(Constants.VIDEO_KEY)).get(Constants.ASSET_KEY), VIDEO_ASSET);
        assertEquals(((Map<?, ?>) attributesMap.get(Constants.VIDEO_KEY)).get(Constants.SERVER_URL_KEY), SERVER_URL);
        assertEquals(((Map<?, ?>) attributesMap.get(Constants.VIDEO_KEY)).get(Constants.CONTENT_URL_KEY),
                VIDEO_CONTENT_URL);
        assertEquals(((Map<?, ?>) attributesMap.get(Constants.VIDEO_KEY)).get(Constants.EMAIL_URL_KEY),
                VIDEO_EMAIL_URL);
        assertEquals(((Map<?, ?>) attributesMap.get(Constants.VIDEO_KEY)).get(Constants.VIDEO_SERVER_URL_KEY),
                VIDEO_SERVER_URL);
        assertEquals(((Map<?, ?>) attributesMap.get(Constants.VIDEO_KEY)).get(Constants.POSTER_IMAGE_KEY),
                VIDEO_POSTER_IMAGE_URL);
        assertEquals((List<?>) attributesMap.get(Constants.BLOCK_KEY), iconsList);
        assertEquals(((LinkedList<?>) attributesMap.get(Constants.BLOCK_KEY)).toString(), iconsList.get(0).getText(),
                TEXT);
        assertEquals(((LinkedList<?>) attributesMap.get(Constants.BLOCK_KEY)).toString(), iconsList.get(0).getTitle(),
                TITLE);
        assertEquals(((LinkedList<?>) attributesMap.get(Constants.BLOCK_KEY)).toString(),
                iconsList.get(0).getImage().get(Constants.ALT_KEY), ICON_ALT_TEXT);
        assertEquals(((LinkedList<?>) attributesMap.get(Constants.BLOCK_KEY)).toString(),
                iconsList.get(0).getImage().get(Constants.URL_KEY), ICON_LINK);
    }
    

    /**
     * Test case on the method getAttributes without video content
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testGetAttributesWithoutVideoContent() throws NoSuchFieldException
    {
        List<IconBean> iconsList = new LinkedList<>();
        iconsList.add(iconData);
        childResources.add(resourceOne);
        
        PrivateAccessor.setField(videoBrandContentModel, "videoAsset", "   ");

        when(resource.listChildren()).thenReturn(childResources.iterator());
        when(resourceOne.adaptTo(IconBean.class)).thenReturn(iconData);

        Map<String, Object> attributesMap = videoBrandContentModel.getAttributes();
        
        assertEquals(7, attributesMap.size());
    }


    /**
     * Test case on the method getComponent.
     */
    @Test
    public void testGetComponent()
    {
        String componentID = videoBrandContentModel.getComponent();
        assertEquals(componentID, Constants.VIDEO_BRAND_CONTENT_TITLE);
        assertEquals("Video Brand Content", videoBrandContentModel.getComponentTitle());
    }

}
