package com.ftd.platform.core.servlets;

import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.PrintWriter;

import javax.jcr.RepositoryException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestPathInfo;
import org.junit.Before;
import org.junit.Rule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.slf4j.Logger;

import com.ftd.platform.core.constants.Constants;

public class ConditionalContentSearchServletTest
{
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    SlingHttpServletRequest mockRequest;
    @Mock
    SlingHttpServletResponse mockResponse;
    @Mock
    Logger mockLog;
    @Mock
    RequestPathInfo requestPathInfo;
    @Mock
    PrintWriter printWriter;

    String path = null;
    String variant = null;
    String tag = null;
    String servlet = null;
    String forDateTime = null;

    /**
     * This method executes before running all test methods and is used for setting up data for the test cases.
     * 
     * @throws RepositoryException
     * @throws IOException
     */
    @Before
    public void setup() throws RepositoryException, IOException
    {
        path = "/content" + "/experience-fragments/ftd/en-us/banners/sources";
        variant = "top";
        tag = "ftd/sources/512";
        servlet = "Hello Servlet";
        when(mockRequest.getRequestPathInfo()).thenReturn(requestPathInfo);
        when(requestPathInfo.getResourcePath()).thenReturn(path);
        when(mockRequest.getParameter(Constants.VARIANT_ID)).thenReturn(variant);
        when(mockRequest.getParameter(Constants.TAGS)).thenReturn(tag);
        when(mockResponse.getWriter()).thenReturn(printWriter);
    }



}
