package com.ftd.platform.core.models;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import junitx.util.PrivateAccessor;

import org.apache.sling.api.resource.Resource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.ftd.platform.core.beans.ProdIdBean;
import com.ftd.platform.core.constants.Constants;

/**
 * TestCases for the Class ProductGridsModel
 * 
 * @author sunkatep
 *
 */
public class ProductGridsTest
{

    private static final String TITLE = "The Rose Experts";
    private static final String DESCRIPTION = "There is no way to convey emotions this";
    private static final String BUTTON_LABEL = "Shop Roses";
    private static final String BUTTON_LINK = "/products";
    private static final Boolean BUTTON_TARGET = true;
    private static final String CATEGORY_ID = "web:categoryId/C04";
    private static final String PRODUCT_ID_SELECTOR = "prodSizeSelector";

    private ProductGridsModel productGridsModel;
    private Resource resource = mock(Resource.class);
    private Resource resourceOne = mock(Resource.class);
    private Resource resourceTwo = mock(Resource.class);
    private Resource resourceThree = mock(Resource.class);
    private ProdIdBean productOne = new ProdIdBean();
    private ProdIdBean productTwo = new ProdIdBean();
    private ProdIdBean productThree = new ProdIdBean();

    private List<Resource> childResources = new ArrayList<>();

    /**
     * This method executes before running all test methods and is used for setting up data for the test cases.
     */
    @Before
    public void setup() throws NoSuchFieldException
    {
        productGridsModel = new ProductGridsModel();
        PrivateAccessor.setField(productGridsModel, "titleTwo", TITLE);
        PrivateAccessor.setField(productGridsModel, "description", DESCRIPTION);
        PrivateAccessor.setField(productGridsModel, "buttonLabelTwo", BUTTON_LABEL);
        PrivateAccessor.setField(productGridsModel, "buttonLinkTwo", BUTTON_LINK);
        PrivateAccessor.setField(productGridsModel, "buttonLinkTargetTwo", BUTTON_TARGET.toString());
        PrivateAccessor.setField(productGridsModel, "catIdTwo", CATEGORY_ID);

        PrivateAccessor.setField(productGridsModel, "titleThree", TITLE);
        PrivateAccessor.setField(productGridsModel, "buttonLabelThree", BUTTON_LABEL);
        PrivateAccessor.setField(productGridsModel, "buttonLinkThree", BUTTON_LINK);
        PrivateAccessor.setField(productGridsModel, "buttonLinkTargetThree", BUTTON_TARGET.toString());
        PrivateAccessor.setField(productGridsModel, "catIdThree", CATEGORY_ID);

        PrivateAccessor.setField(productGridsModel, "idSelectorGrid", Constants.PRODUCTID_KEY);
        PrivateAccessor.setField(productGridsModel, "prodIdTwo", resource);
        PrivateAccessor.setField(productGridsModel, "prodIdThree", resource);

    }

    /**
     * Test case on the method getComponent.
     */
    @Test
    public void testGetComponent()
    {
        String componentID = productGridsModel.getComponent();
        Assert.assertEquals(componentID, Constants.THREE_PRODUCT_GRIDS_TITLE);
    }

    /**
     * Test case on the method getPrimaryCta
     */
    @Test
    public void testGetPrimaryCtaWithTarget()
    {
        Map<String, Object> primaryCtaMap = productGridsModel.getPrimaryCta(BUTTON_LABEL, BUTTON_LINK,
                BUTTON_TARGET.toString());
        Assert.assertEquals(BUTTON_LABEL, primaryCtaMap.get(Constants.TEXT_KEY));
        Assert.assertEquals(BUTTON_LINK, primaryCtaMap.get(Constants.URL_KEY));
        Assert.assertEquals(BUTTON_TARGET, primaryCtaMap.get(Constants.NEW_WINDOW_KEY));
    }

    /**
     * Test case on the method getPrimaryCta
     */
    @Test
    public void testGetPrimaryCtaWithoutTarget()
    {
        Map<String, Object> primaryCtaMap = productGridsModel.getPrimaryCta(BUTTON_LABEL, BUTTON_LINK, "");
        Assert.assertEquals(BUTTON_LABEL, primaryCtaMap.get(Constants.TEXT_KEY));
        Assert.assertEquals(BUTTON_LINK, primaryCtaMap.get(Constants.URL_KEY));
        Assert.assertNull(primaryCtaMap.get(Constants.TARGET_KEY));

    }

    /**
     * Test case on the method getAttributes
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testGetAttributesWithTwoProdSelector() throws NoSuchFieldException
    {
        PrivateAccessor.setField(productGridsModel, PRODUCT_ID_SELECTOR, Constants.TWO_VALUE);

        productOne.setProductid("123");
        productTwo.setProductid("456");
        childResources.add(resourceOne);
        childResources.add(resourceTwo);

        when(resource.listChildren()).thenReturn(childResources.iterator());
        when(resourceOne.adaptTo(ProdIdBean.class)).thenReturn(productOne);
        when(resourceTwo.adaptTo(ProdIdBean.class)).thenReturn(productTwo);

        Map<String, Object> attributesMap = productGridsModel.getAttributes();

        Assert.assertEquals(TITLE,
                ((Map<?, ?>) attributesMap.get(Constants.BANNER_COMPONENT_TITLE)).get(Constants.TITLE_KEY));
        Assert.assertEquals(DESCRIPTION,
                ((Map<?, ?>) attributesMap.get(Constants.BANNER_COMPONENT_TITLE)).get(Constants.DESCRIPTION_KEY));

        Assert.assertEquals(2, ((ArrayList<?>) attributesMap.get(Constants.PRODUCT_KEY)).size());
        Assert.assertEquals("[123, 456]", ((ArrayList<?>) attributesMap.get(Constants.PRODUCT_KEY)).toString());

        Assert.assertEquals(BUTTON_LABEL,
                (((Map<?, ?>) ((Map<?, ?>) attributesMap.get(Constants.BANNER_COMPONENT_TITLE))
                        .get(Constants.PRIMARY_CTA_KEY)).get(Constants.TEXT_KEY)));

        Assert.assertEquals(BUTTON_LINK, (((Map<?, ?>) ((Map<?, ?>) attributesMap.get(Constants.BANNER_COMPONENT_TITLE))
                .get(Constants.PRIMARY_CTA_KEY)).get(Constants.URL_KEY)));

        Assert.assertEquals(BUTTON_TARGET,
                (((Map<?, ?>) ((Map<?, ?>) attributesMap.get(Constants.BANNER_COMPONENT_TITLE))
                        .get(Constants.PRIMARY_CTA_KEY)).get(Constants.NEW_WINDOW_KEY)));

    }

    /**
     * Test case on the method getAttributes
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testGetAttributesWithThreeProdSelector() throws NoSuchFieldException
    {
        PrivateAccessor.setField(productGridsModel, PRODUCT_ID_SELECTOR, Constants.THREE_OPTIONS_IN_MENU);

        productOne.setProductidThreeProd("123");
        productTwo.setProductidThreeProd("456");
        productThree.setProductidThreeProd("789");

        childResources.add(resourceOne);
        childResources.add(resourceTwo);
        childResources.add(resourceThree);
        when(resource.listChildren()).thenReturn(childResources.iterator());
        when(resourceOne.adaptTo(ProdIdBean.class)).thenReturn(productOne);
        when(resourceTwo.adaptTo(ProdIdBean.class)).thenReturn(productTwo);
        when(resourceThree.adaptTo(ProdIdBean.class)).thenReturn(productThree);

        Map<String, Object> attributesMap = productGridsModel.getAttributes();

        Assert.assertEquals(TITLE, attributesMap.get(Constants.TITLE_KEY));
        Assert.assertNull(attributesMap.get(Constants.DESCRIPTION_KEY));
        Assert.assertEquals(3, ((ArrayList<?>) attributesMap.get(Constants.PRODUCT_KEY)).size());
        Assert.assertEquals("[123, 456, 789]", ((ArrayList<?>) attributesMap.get(Constants.PRODUCT_KEY)).toString());
        Assert.assertEquals(BUTTON_LABEL,
                ((Map<?, ?>) attributesMap.get(Constants.PRIMARY_CTA_KEY)).get(Constants.TEXT_KEY));
        Assert.assertEquals(BUTTON_LINK,
                ((Map<?, ?>) attributesMap.get(Constants.PRIMARY_CTA_KEY)).get(Constants.URL_KEY));
        Assert.assertEquals(BUTTON_TARGET,
                ((Map<?, ?>) attributesMap.get(Constants.PRIMARY_CTA_KEY)).get(Constants.NEW_WINDOW_KEY));

    }

}
