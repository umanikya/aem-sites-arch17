package com.ftd.platform.core.models;

import java.util.Map;

import junitx.util.PrivateAccessor;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.ftd.platform.core.constants.Constants;

/**
 * TestCases for the Class ProductCardBannerModel
 * 
 * @author nikhil
 *
 */
public class PromoCardModelTest
{

    private static final String TITLE = "The Rose Experts";
    private static final String DESCRIPTION = "There is no way to convey emotions this";
    private static final String PRICE = "25";
    private static final String POSITION = "23";
    private static final String LABEL = "SHOP ALL";

    private static final String URL = "/content/image";
    private static final String ALT_TEXT = "altText";
    private static final String BUTTON_LABEL = "Shop Roses";
    private static final String BUTTON_LINK = "/products";
    private static final Boolean BUTTON_TARGET_TRUE = true;
    private static final Boolean BUTTON_TARGET_FALSE = false;
    private static final String SIZE = "double";
    private static final String THEME = "light";

    private PromoCardModel productCardBannerModel;

    /**
     * This method executes before running all test methods and is used for setting up data for the test cases.
     */
    @Before
    public void setup() throws NoSuchFieldException
    {
        productCardBannerModel = new PromoCardModel();
        PrivateAccessor.setField(productCardBannerModel, "badgePosition", POSITION);
        PrivateAccessor.setField(productCardBannerModel, "badgelabel", LABEL);
        PrivateAccessor.setField(productCardBannerModel, "heading", TITLE);
        PrivateAccessor.setField(productCardBannerModel, "description", DESCRIPTION);
        PrivateAccessor.setField(productCardBannerModel, "price", PRICE);
        PrivateAccessor.setField(productCardBannerModel, "ctaLabel", BUTTON_LABEL);
        PrivateAccessor.setField(productCardBannerModel, "ctaUrl", BUTTON_LINK);

        PrivateAccessor.setField(productCardBannerModel, "imageRef", URL);
        PrivateAccessor.setField(productCardBannerModel, "imageAltText", ALT_TEXT);
        PrivateAccessor.setField(productCardBannerModel, "theme", THEME);
    }

    /**
     * Test case on the method getComponent.
     */
    @Test
    public void testGetComponent()
    {
        String componentID = productCardBannerModel.getComponent();
        Assert.assertEquals(componentID, Constants.PROMO_CARD_COMPONENT_TITLE);
    }

    /**
     * Test case on the method getAttributes with CTA target as a new tab.
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testGetAttributesWithCTATarget() throws NoSuchFieldException
    {

        PrivateAccessor.setField(productCardBannerModel, "ctaTarget", BUTTON_TARGET_TRUE.toString());
        PrivateAccessor.setField(productCardBannerModel, Constants.SIZE_KEY, SIZE);

        Map<String, Object> attributesMap = productCardBannerModel.getAttributes();

        Assert.assertEquals(attributesMap.size(), 8);
        Assert.assertEquals(attributesMap.get(Constants.TITLE_KEY), TITLE);
        Assert.assertEquals(attributesMap.get(Constants.DESCRIPTION_KEY), DESCRIPTION);
        Assert.assertEquals(attributesMap.get(Constants.PRICE_KEY), PRICE);
        Assert.assertEquals(attributesMap.get(Constants.THEME_KEY), THEME);

        Assert.assertEquals(((Map<?, ?>) attributesMap.get(Constants.PRIMARY_CTA_KEY)).get(Constants.TEXT_KEY),
                BUTTON_LABEL);
        Assert.assertEquals(((Map<?, ?>) attributesMap.get(Constants.PRIMARY_CTA_KEY)).get(Constants.URL_KEY),
                BUTTON_LINK);
        Assert.assertEquals(((Map<?, ?>) attributesMap.get(Constants.PRIMARY_CTA_KEY)).get(Constants.NEW_WINDOW_KEY),
                true);
        Assert.assertEquals(((Map<?, ?>) attributesMap.get(Constants.BADGE_KEY)).get(Constants.POSITION_KEY), POSITION);
        Assert.assertEquals(((Map<?, ?>) attributesMap.get(Constants.BADGE_KEY)).get(Constants.LABEL_KEY), LABEL);
    }

    /**
     * Test case on the method getAttributes with CTA target as a same window.
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testGetAttributesWithCTATargetSameWindow() throws NoSuchFieldException
    {

        PrivateAccessor.setField(productCardBannerModel, Constants.SIZE_KEY, SIZE);
        Map<String, Object> attributesMap = productCardBannerModel.getAttributes();

        PrivateAccessor.setField(productCardBannerModel, "ctaTarget", BUTTON_TARGET_FALSE.toString());
        Assert.assertEquals(attributesMap.size(), 8);
        Assert.assertEquals(((Map<?, ?>) attributesMap.get(Constants.PRIMARY_CTA_KEY)).get(Constants.TEXT_KEY),
                BUTTON_LABEL);
        Assert.assertEquals(((Map<?, ?>) attributesMap.get(Constants.PRIMARY_CTA_KEY)).get(Constants.URL_KEY),
                BUTTON_LINK);
        Assert.assertEquals(((Map<?, ?>) attributesMap.get(Constants.PRIMARY_CTA_KEY)).get(Constants.NEW_WINDOW_KEY),
                false);
        Assert.assertEquals(((Map<?, ?>) attributesMap.get(Constants.BADGE_KEY)).get(Constants.POSITION_KEY), POSITION);
        Assert.assertEquals(((Map<?, ?>) attributesMap.get(Constants.BADGE_KEY)).get(Constants.LABEL_KEY), LABEL);
    }

    @Test
    public void testGetAttributesWithSingleTile() throws NoSuchFieldException
    {

        PrivateAccessor.setField(productCardBannerModel, Constants.SIZE_KEY, "single");

        Map<String, Object> attributesMap = productCardBannerModel.getAttributes();

        PrivateAccessor.setField(productCardBannerModel, "ctaTarget", BUTTON_TARGET_FALSE.toString());
        Assert.assertEquals(7,attributesMap.size());
        Assert.assertEquals(((Map<?, ?>) attributesMap.get(Constants.PRIMARY_CTA_KEY)).get(Constants.TEXT_KEY),
                BUTTON_LABEL);
        Assert.assertEquals(((Map<?, ?>) attributesMap.get(Constants.PRIMARY_CTA_KEY)).get(Constants.URL_KEY),
                BUTTON_LINK);
        Assert.assertEquals(((Map<?, ?>) attributesMap.get(Constants.PRIMARY_CTA_KEY)).get(Constants.NEW_WINDOW_KEY),
                false);
    }
}
