package com.ftd.platform.core.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.sling.api.resource.Resource;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.ftd.platform.core.beans.PromoCardBean;
import com.ftd.platform.core.constants.Constants;

import junitx.util.PrivateAccessor;

public class ProductListModelTest
{
    private static final int THIRTYSIX = 36;

    private ProductListModel productListModel;

    private PromoCardBean procutListPromoCardBean;

    @Mock
    private Resource promocardResource;

    /**
     * This method executes before running all test methods and is used for setting up data for the test cases.
     * 
     * @throws NoSuchFieldException
     */

    @Before
    public void setup() throws NoSuchFieldException
    {
        procutListPromoCardBean = Mockito.mock(PromoCardBean.class);
        Resource promocardResource = Mockito.mock(Resource.class);
        Resource promocardResourceChildOne = Mockito.mock(Resource.class);
        List<Resource> categoriesChildResoucelist = new ArrayList<>();
        List<Resource> promoCardChildResoucelist = new ArrayList<>();
        promoCardChildResoucelist.add(promocardResourceChildOne);
        productListModel = new ProductListModel();
        PrivateAccessor.setField(productListModel, Constants.COMPONENT_KEY, Constants.PRODUCT_LIST_TITLE);
        PrivateAccessor.setField(productListModel, Constants.PAGINATION_KEY, THIRTYSIX);
        PrivateAccessor.setField(productListModel, Constants.DISPLAY_FILTERS_KEY, true);
        PrivateAccessor.setField(productListModel, Constants.DISPLAY_SORT_OPTIONS_KEY, false);
        PrivateAccessor.setField(productListModel, Constants.DISPLAY_GIFT_FINDER_KEY, true);
        PrivateAccessor.setField(productListModel, Constants.SPECIFY_CATEGORIES_KEY, Constants.FALSE_KEY);
        PrivateAccessor.setField(productListModel, Constants.PROMOCARD, promocardResource);
        Mockito.when(promocardResource.listChildren()).thenReturn(promoCardChildResoucelist.iterator());
        Mockito.when(promocardResourceChildOne.getPath()).thenReturn("/content/promocard");
        Mockito.when(promocardResourceChildOne.adaptTo(PromoCardBean.class)).thenReturn(procutListPromoCardBean);
    }

    /**
     * This method is used to verify component identifier
     */
    @Test
    public void testGetComponent()
    {

        String componentTitle = productListModel.getComponent();
        assertSame(Constants.PRODUCT_LIST_TITLE, componentTitle);
    }

    /**
     * This method is used to test the product list model attributes
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testGetProductListMap() throws NoSuchFieldException
    {
        PrivateAccessor.setField(productListModel, Constants.SPECIFY_CATEGORIES_KEY, Constants.TRUE_KEY);
        Map<String, Object> componentTitle = productListModel.getAttributes();
        List<?> categoriesList = (List<?>) componentTitle.get(Constants.CATEGORIES_KEY);
        assertSame(5, componentTitle.size());
        assertSame(THIRTYSIX, componentTitle.get(Constants.PAGINATION_KEY));
        assertSame(true, componentTitle.get(Constants.DISPLAY_FILTERS_KEY));
        assertSame(false, componentTitle.get(Constants.DISPLAY_SORT_OPTIONS_KEY));
        assertSame(true, componentTitle.get(Constants.DISPLAY_GIFT_FINDER_KEY));
    }

}
