package com.ftd.platform.core.servlets;

import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.junit.Before;
import org.junit.Test;

import org.junit.runner.RunWith;

import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.ftd.platform.core.services.ExperienceContentPageListService;

import junitx.util.PrivateAccessor;

@RunWith(MockitoJUnitRunner.class)
public class GetExperienceContentPageListServletTest {
	
	@Mock
	SlingHttpServletRequest mockRequest;
	@Mock
	SlingHttpServletResponse mockResponse;
	@Mock
	ExperienceContentPageListService mockExperienceContentPageListService;
	@Mock
    PrintWriter printWriter;
	
	GetExperienceContentPageListServlet getExperienceContentPageListServlet;
	
	@Before
	public void setup() throws IOException {
		getExperienceContentPageListServlet = new GetExperienceContentPageListServlet();
	    when(mockResponse.getWriter()).thenReturn(printWriter);
	}
	
	@Test
	public void testDoGet()  throws ServletException, IOException,  NoSuchFieldException
	{
		when(mockRequest.getParameter("siteId")).thenReturn("ftd");
		when(mockExperienceContentPageListService.getExperienceContentPageList("ftd")).thenReturn("value");
		PrivateAccessor.setField(getExperienceContentPageListServlet,"experienceContentPageListService",mockExperienceContentPageListService);
		
		getExperienceContentPageListServlet.doGet(mockRequest, mockResponse);
		
  
	}

}
