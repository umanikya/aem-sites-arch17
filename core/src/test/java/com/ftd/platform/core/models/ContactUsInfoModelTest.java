package com.ftd.platform.core.models;

import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import junitx.util.PrivateAccessor;

public class ContactUsInfoModelTest
{
    private ContactUsInfoModel cntusinfomodel; 
    private static final String CONTACT_HEADING = "CONTACT US AEM Test";
    private static final String CONTACT_DESCRIPTION = "We're here for you 24 hours a day, 7 days a week.";
    private static final String TRACK_ORDER = "Track Your Order";
    private static final String TRACK_ORDER_URL = "/order-status";
    private static final String PHONE = "1-800-SEND-FTD (1-800-736-3383)";
    private static final String EMAIL = "Send Us a Message";
    private static final String CHAT_CLASS_NAME = "chat-liveagent-link";
    private static final String PHONE_URL = "18007363383";
    private static final String EMAIL_URL = "custserv@ftdi.com";
    Map<String, Object> attributes;
    Map<String, Object> contact;
    
    @Before
    public void setup() throws NoSuchFieldException
    {
        cntusinfomodel = new ContactUsInfoModel();
        PrivateAccessor.setField(cntusinfomodel,"contactHeading",CONTACT_HEADING);
        PrivateAccessor.setField(cntusinfomodel,"contactDescription",CONTACT_DESCRIPTION);
        PrivateAccessor.setField(cntusinfomodel,"trackOrder",TRACK_ORDER);
        PrivateAccessor.setField(cntusinfomodel,"trackOrderUrl",TRACK_ORDER_URL);
        PrivateAccessor.setField(cntusinfomodel,"phone",PHONE);
        PrivateAccessor.setField(cntusinfomodel,"email",EMAIL);
        PrivateAccessor.setField(cntusinfomodel,"chatClassName",CHAT_CLASS_NAME);
        PrivateAccessor.setField(cntusinfomodel,"phoneUrl",PHONE_URL);
        PrivateAccessor.setField(cntusinfomodel,"emailUrl",EMAIL_URL);
        attributes = new LinkedHashMap<String, Object>();
    }
    
    @Test
    public void testGetComponent()
    {
        String componentID =cntusinfomodel.getComponent();
        Assert.assertEquals(componentID,"contact-us-info");
    }
    
    @Test
    public void testGetComponentTitle()
    {
        String componentTitle=cntusinfomodel.getComponentTitle();
        Assert.assertEquals(componentTitle,"Contact Info content");
    }
    
    @Test
    public void testGetAttributes()
    {
        attributes = cntusinfomodel.getAttributes();
        contact =(Map<String, Object>)attributes.get("contact");
        Assert.assertEquals(CONTACT_HEADING,contact.get("heading"));
        Assert.assertEquals(attributes.size(),1);
        Assert.assertEquals(CONTACT_DESCRIPTION,contact.get("description"));
    }
    
}
