package com.ftd.platform.core.models.page;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.RepositoryException;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.settings.SlingSettingsService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.day.cq.wcm.api.Page;
import com.ftd.platform.core.constants.Constants;
import com.ftd.platform.core.models.BaseModel;
import com.ftd.platform.core.services.ContentPageDataService;

import junitx.util.PrivateAccessor;

/**
 * Test file to test PageBaseModel.class
 * 
 * @author manpati
 * 
 */
public class PageBaseModelTest
{
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @InjectMocks
    private PageBaseModel pageBaseModel;

    @Mock
    ResourceResolver resourceResolver;

    @Mock
    Resource resource;

    @Mock
    Resource resourcePath;

    @Mock
    Resource childResourceOne;

    @Mock
    Resource childResourceTwo;

    @Mock
    Resource childResourceThree;

    @Mock
    BaseModel baseModel;

    @Mock
    SlingSettingsService slingSettingsService;
    
    @Mock
    Page currentPage;

    @Mock
    ContentPageDataService contentPageDataService;


    @Before
    public void setUp() throws NoSuchFieldException
    {
        PrivateAccessor.setField(pageBaseModel, "resourceResolver", resourceResolver);
        PrivateAccessor.setField(pageBaseModel, "rootResource", resource);
        PrivateAccessor.setField(pageBaseModel, "slingSettingsService", slingSettingsService);
        List<Resource> childResources = new ArrayList<>();
        childResources.add(childResourceOne);
        childResources.add(childResourceTwo);
        childResources.add(childResourceThree);
    }

    void setupForInit()
    {
        List<Resource> childResources = new ArrayList<>();
        childResources.add(childResourceOne);
        Mockito.when(resource.listChildren()).thenReturn(childResources.iterator());
    }

    /**
     * This method is used to test Init method
     * 
     * @throws RepositoryException
     */
    @Test
    public void testComponentsMap() throws RepositoryException
    {
        setupForInit();
        pageBaseModel.init();
        assertEquals(Constants.BASE_PAGE_LAYOUT_TITLE, pageBaseModel.getLayout());
    }

    /**
     * This method is used to test Init method
     * 
     * @throws RepositoryException
     */
    @Test
    public void testComponentsList() throws RepositoryException
    {
        setupForInit();
        pageBaseModel.init();
        List<Object> componentsList = pageBaseModel.getComponents();
        assertEquals(Constants.BASE_PAGE_LAYOUT_TITLE, pageBaseModel.getLayout());
    }
    
    @Test
    public void testHeaderComponent()
    {
        setupForInit();      
        Mockito.when(contentPageDataService.getModelFromResource(childResourceOne)).thenReturn(baseModel);
        Mockito.when(baseModel.getComponent()).thenReturn("HeaderComponents");
        pageBaseModel.processComponent(resource, "header");
        
    }
    
    @Test
    public void testFooterComponent()
    {
        setupForInit();      
        Mockito.when(contentPageDataService.getModelFromResource(childResourceOne)).thenReturn(baseModel);
        Mockito.when(baseModel.getComponent()).thenReturn("FooterComponents");
        pageBaseModel.processComponent(resource, "footer");
    }
    
    @Test
    public void testModelComponent()
    {
        setupForInit();      
        Mockito.when(contentPageDataService.getModelFromResource(childResourceOne)).thenReturn(baseModel);
        Mockito.when(baseModel.getComponent()).thenReturn("ModelComponents");
        pageBaseModel.processComponent(resource, "modal");
    }
    
    @Test
    public void testComponent()
    {
        setupForInit();      
        Mockito.when(contentPageDataService.getModelFromResource(childResourceOne)).thenReturn(baseModel);
        Mockito.when(baseModel.getComponent()).thenReturn("Components");
        pageBaseModel.processComponent(resource, null);
        List<Object> componentsList = pageBaseModel.getComponents();
        assertEquals(baseModel, componentsList.get(0));        
    }

}
