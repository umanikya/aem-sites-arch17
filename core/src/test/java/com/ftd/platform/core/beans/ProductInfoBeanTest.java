package com.ftd.platform.core.beans;

import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;

import junitx.util.PrivateAccessor;

public class ProductInfoBeanTest
{
    @InjectMocks
    ProductInfoBean productInfoBean;
    
    
    @Before
    public void setup() throws NoSuchFieldException
    {
        productInfoBean =  new ProductInfoBean();
        PrivateAccessor.setField(productInfoBean, "title", "The Floral Experts");
        PrivateAccessor.setField(productInfoBean, "anchor", "right");
        PrivateAccessor.setField(productInfoBean, "details", "Delivered by Hand");
        PrivateAccessor.setField(productInfoBean, "label", "The Floral");
        PrivateAccessor.setField(productInfoBean, "url", "https://s7d5.scene7.com/is/image/");
        PrivateAccessor.setField(productInfoBean, "target", "The Floral Experts");
        PrivateAccessor.setField(productInfoBean, "serverUrl", "https://s7d5.scene7.com/is/image/");
        PrivateAccessor.setField(productInfoBean, "videoAsset", "ProvideCommerce/FTD_18_Mday_Video_TheFlowerExperts");
        PrivateAccessor.setField(productInfoBean, "videContentUrl", "https://s7d5.scene7.com/skins/");
        PrivateAccessor.setField(productInfoBean, "videoEmailUrl", "https://s7d5.scene7.com/s7/emailFriend");
        PrivateAccessor.setField(productInfoBean, "videoServerUrl", "https://s7d5.scene7.com/is/content/");
    }
    
    @Test
    public void testGetContent()
    {
       Map<String, Object> result = productInfoBean.getContent();
       Assert.assertEquals(result.size(), 2);
    }
    
    @Test
    public void testGetVideo()
    {
       Map<String, Object> result = productInfoBean.getVideo();
       Assert.assertEquals(result.size(), 2);
    }

}
