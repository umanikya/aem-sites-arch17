package com.ftd.platform.core.models;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.sling.api.resource.Resource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.ftd.platform.core.beans.LinkUrlTargetBean;
import com.ftd.platform.core.constants.Constants;

import junitx.util.PrivateAccessor;

@RunWith(MockitoJUnitRunner.class)
public class LinkListModelTest
{
    LinkListModel linkListModel = new LinkListModel();
    private String Heading = "Linklist heading";
    private String  Theme  = "brand-discount";
    private String COMPONENT_TITLE = "Link List";
    
    private LinkUrlTargetBean linkUrlTargetBean = new LinkUrlTargetBean();
    List<Resource> childResources;
    
    @Mock
    Resource childResource;
    
    @Mock
    Resource mockLinksResource;
    
    @Before
    public void setup() throws NoSuchFieldException
    {
       
       PrivateAccessor.setField(linkListModel, "heading", Heading);
       PrivateAccessor.setField(linkListModel, "theme", Theme);
       PrivateAccessor.setField(linkListModel, "linksResource", mockLinksResource); 
       childResources = mockChildResources();
       PrivateAccessor.setField(linkUrlTargetBean, "text", "Sympathy & Funeral"); 
       PrivateAccessor.setField(linkUrlTargetBean, "link", "/category/occasion_sympathy?intcid=card-module-2-sympathy"); 
       PrivateAccessor.setField(linkUrlTargetBean, "target", "_blank"); 
    }

    @Test
    public void testGetComponent()
    {
        String componentId = linkListModel.getComponent();
        assertEquals(componentId,Constants.LINK_LIST_KEY);
    }
    @Test
    public void testGetAttributes()
    {
        when(mockLinksResource.listChildren()).thenReturn(childResources.iterator());
        when(childResource.adaptTo(LinkUrlTargetBean.class)).thenReturn(linkUrlTargetBean);
        Map<String, Object> attributesMap = linkListModel.getAttributes();
        assertEquals(attributesMap.size(),3);
        assertEquals(attributesMap.get("heading"),Heading);
        assertEquals(attributesMap.get("componentTheme"),Theme);
    }

    @Test
    public void testGetComponentTitle()
    {
        String componentTitle = linkListModel.getComponentTitle();
        assertEquals(componentTitle,COMPONENT_TITLE);
    }
    
    private List<Resource> mockChildResources()
    {
        childResources = new ArrayList<Resource>();
        childResources.add(childResource);
        return childResources;
    }


}
