package com.ftd.platform.core.models;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.scripting.sightly.compiler.expression.nodes.PropertyAccess;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.ftd.platform.core.beans.IconBean;
import com.ftd.platform.core.beans.LinksHelperModel;
import com.ftd.platform.core.beans.MainMenuBean;
import com.ftd.platform.core.beans.NavigationColumnBean;
import com.ftd.platform.core.constants.Constants;

import junitx.util.PrivateAccessor;

public class PrimaryNavigationModelTest
{

    private PrimaryNavigationModel primaryNavigationModel;

    private Resource resourceOne = mock(Resource.class);

    @Mock
    Resource mockRootResource;

    NavigationColumnBean navigationColumnBean;

    MainMenuBean mainMenuBean;

    private Resource columnOneResource = mock(Resource.class);
    private Resource columnFourResource = mock(Resource.class);
    private Resource columnTwoResource = mock(Resource.class);
    private Resource columnThreeResource = mock(Resource.class);
    Resource mockChildResourceOne = mock(Resource.class);
    Resource mockChildResourceTwo = mock(Resource.class);
    private LinksHelperModel linksHelperModel = new LinksHelperModel();

    private static final String URL = "/Iconsproduction";
    private static final String LABEL = "Alternatie text";

    private static final String MENU_LABEL = "Shop Now for Everyone";
    private static final String COLUMN_ONE_HEADING = "Mens for Everyone";
    private static final String COLUMN_TWO_HEADING = "Womens for Everyone";
    private static final String COLUMN_THREE_HEADING = "Girls for Everyone";
    private static final String COLUMN_FOUR_HEADING = "Boys for Everyone";

    /**
     * This method executes before running all test methods and is used for setting up data for the test cases.
     */
    @Before
    public void setup() throws NoSuchFieldException
    {
        PrimaryNavigationModel primaryNavigationModelTwo = new PrimaryNavigationModel();
        primaryNavigationModel = new PrimaryNavigationModel();

        PrivateAccessor.setField(primaryNavigationModel, "menuOneLabel", "Shop Now for One");
        PrivateAccessor.setField(primaryNavigationModel, "menuOneColOneHeading", "Mens for One");
        PrivateAccessor.setField(primaryNavigationModel, "menuOneColTwoHeading", "Womens for One");
        PrivateAccessor.setField(primaryNavigationModel, "menuOneColThreeHeading", "Girls for One");
        PrivateAccessor.setField(primaryNavigationModel, "menuOneColFourHeading", "Boys for One");
        PrivateAccessor.setField(primaryNavigationModel, "menuOneColOneResource", resourceOne);
        PrivateAccessor.setField(primaryNavigationModel, "menuOneColTwoResource", resourceOne);
        PrivateAccessor.setField(primaryNavigationModel, "menuOneColThreeResource", resourceOne);

        PrivateAccessor.setField(primaryNavigationModelTwo, "menuTwoLabel", "Shop Now for Two");
        PrivateAccessor.setField(primaryNavigationModelTwo, "menuTwoColOneHeading", "Mens for Two");
        PrivateAccessor.setField(primaryNavigationModelTwo, "menuTwoColTwoHeading", "Womens for Two");
        PrivateAccessor.setField(primaryNavigationModelTwo, "menuTwoColThreeHeading", "Girls for Two");
        PrivateAccessor.setField(primaryNavigationModelTwo, "menuTwoColFourHeading", "Boys for Two");
        PrivateAccessor.setField(primaryNavigationModelTwo, "menuTwoColOneResource", resourceOne);
        PrivateAccessor.setField(primaryNavigationModelTwo, "menuTwoColTwoResource", resourceOne);
        PrivateAccessor.setField(primaryNavigationModelTwo, "menuTwoColThreeResource", resourceOne);
        PrivateAccessor.setField(primaryNavigationModelTwo, "menuTwoColFourResource", resourceOne);

        PrivateAccessor.setField(primaryNavigationModel, "menuThreeLabel", "Shop Now for Three");
        PrivateAccessor.setField(primaryNavigationModel, "menuThreeColOneHeading", "Mens for Three");
        PrivateAccessor.setField(primaryNavigationModel, "menuThreeColTwoHeading", "Womens for Three");
        PrivateAccessor.setField(primaryNavigationModel, "menuThreeColThreeHeading", "Girls for Three");
        PrivateAccessor.setField(primaryNavigationModel, "menuThreeColFourHeading", "Boys for Three");
        PrivateAccessor.setField(primaryNavigationModel, "menuThirdColOneResource", resourceOne);
        PrivateAccessor.setField(primaryNavigationModel, "menuThirdColTwoResource", resourceOne);
        PrivateAccessor.setField(primaryNavigationModel, "menuThirdColThreeResource", resourceOne);
        PrivateAccessor.setField(primaryNavigationModel, "menuThirdColFourResource", resourceOne);

        PrivateAccessor.setField(primaryNavigationModel, "menuFourLabel", "Shop Now for four");
        PrivateAccessor.setField(primaryNavigationModel, "menuFourColOneHeading", "Mens for four");
        PrivateAccessor.setField(primaryNavigationModel, "menuFourColTwoHeading", "Womens for four");
        PrivateAccessor.setField(primaryNavigationModel, "menuFourColThreeHeading", "Girls for four");
        PrivateAccessor.setField(primaryNavigationModel, "menuFourColFourHeading", "Boys for four");
        PrivateAccessor.setField(primaryNavigationModel, "menuFourColOneResource", resourceOne);
        PrivateAccessor.setField(primaryNavigationModel, "menuFourColTwoResource", resourceOne);
        PrivateAccessor.setField(primaryNavigationModel, "menuFourColThreeResource", resourceOne);
        PrivateAccessor.setField(primaryNavigationModel, "menuFourColFourResource", resourceOne);

        PrivateAccessor.setField(primaryNavigationModel, "menuFiveLabel", "Shop Now for five");
        PrivateAccessor.setField(primaryNavigationModel, "menuFiveColOneHeading", "Mens for five");
        PrivateAccessor.setField(primaryNavigationModel, "menuFiveColTwoHeading", "Womens for five");
        PrivateAccessor.setField(primaryNavigationModel, "menuFiveColThreeHeading", "Girls for five");
        PrivateAccessor.setField(primaryNavigationModel, "menuFiveColFourHeading", "Boys for five");
        PrivateAccessor.setField(primaryNavigationModel, "menuFiveColOneResource", resourceOne);
        PrivateAccessor.setField(primaryNavigationModel, "menuFiveColTwoResource", resourceOne);
        PrivateAccessor.setField(primaryNavigationModel, "menuFiveColThreeResource", resourceOne);
        PrivateAccessor.setField(primaryNavigationModel, "menuFiveColFourResource", resourceOne);

        PrivateAccessor.setField(linksHelperModel, "url", URL);
        PrivateAccessor.setField(linksHelperModel, "label", LABEL);
        PrivateAccessor.setField(linksHelperModel, "target", "_blank");
        linksHelperModel.setLabel("label");
        linksHelperModel.setUrl("url");
        linksHelperModel.getLabel();
        linksHelperModel.getUrl();
    }

    /**
     * Test case on the Two Options in Menu
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testTwoOptionsInMenu() throws NoSuchFieldException
    {
        PrivateAccessor.setField(primaryNavigationModel, Constants.COLUMN_SELECTOR_KEY, Constants.TWO_OPTIONS_IN_MENU);
        List<Resource> childResources = new ArrayList<>();
        childResources.add(mockChildResourceOne);
        childResources.add(mockChildResourceTwo);
        when(resourceOne.listChildren()).thenReturn(childResources.iterator());
        primaryNavigationModel.primaryNavInit();
        primaryNavigationModel.getAttributes();

        List<MainMenuBean> mainMenuListTest = primaryNavigationModel.mainMenuList;
        Assert.assertEquals(2, (mainMenuListTest.size()));
        Assert.assertEquals("navigation", primaryNavigationModel.getComponent());
        Assert.assertEquals("Primary Navigation", primaryNavigationModel.getComponentTitle());
    }

    /**
     * Test case on the Three Options in Menu
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testThreeOptionsInMenu() throws NoSuchFieldException
    {
        PrivateAccessor.setField(primaryNavigationModel, Constants.COLUMN_SELECTOR_KEY,
                Constants.THREE_OPTIONS_IN_MENU);
        List<Resource> childResources = new ArrayList<>();
        childResources.add(mockChildResourceOne);
        childResources.add(mockChildResourceTwo);
        when(resourceOne.listChildren()).thenReturn(childResources.iterator());
        primaryNavigationModel.primaryNavInit();
        primaryNavigationModel.getAttributes();

        List<MainMenuBean> mainMenuListTest = primaryNavigationModel.mainMenuList;
        Assert.assertEquals(3, (mainMenuListTest.size()));
    }

    /**
     * Test case on the Four Options in Menu
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testFourOptionsInMenu() throws NoSuchFieldException
    {
        PrivateAccessor.setField(primaryNavigationModel, Constants.COLUMN_SELECTOR_KEY, Constants.FOUR_OPTIONS_IN_MENU);
        List<Resource> childResources = new ArrayList<>();
        childResources.add(mockChildResourceOne);
        childResources.add(mockChildResourceTwo);
        when(resourceOne.listChildren()).thenReturn(childResources.iterator());
        primaryNavigationModel.primaryNavInit();
        primaryNavigationModel.getAttributes();

        List<MainMenuBean> mainMenuListTest = primaryNavigationModel.mainMenuList;
        Assert.assertEquals(4, (mainMenuListTest.size()));
    }

    /**
     * Test case on the Five Options in Menu
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testFiveOptionsInMenu() throws NoSuchFieldException
    {
        PrivateAccessor.setField(primaryNavigationModel, Constants.COLUMN_SELECTOR_KEY, Constants.FIVE_OPTIONS_IN_MENU);
        List<Resource> childResources = new ArrayList<>();
        childResources.add(mockChildResourceOne);
        childResources.add(mockChildResourceTwo);
        when(resourceOne.listChildren()).thenReturn(childResources.iterator());
        primaryNavigationModel.primaryNavInit();
        primaryNavigationModel.getAttributes();

        List<MainMenuBean> mainMenuListTest = primaryNavigationModel.mainMenuList;
        Assert.assertEquals(5, (mainMenuListTest.size()));
    }

    /**
     * Test case on the No Options in Menu
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testNoOptionInMenu() throws NoSuchFieldException
    {
        PrivateAccessor.setField(primaryNavigationModel, Constants.COLUMN_SELECTOR_KEY, null);
        List<Resource> childResources = new ArrayList<>();
        childResources.add(mockChildResourceOne);
        childResources.add(mockChildResourceTwo);
        when(resourceOne.listChildren()).thenReturn(childResources.iterator());
        primaryNavigationModel.primaryNavInit();
        primaryNavigationModel.getAttributes();

        List<MainMenuBean> mainMenuListTest = primaryNavigationModel.mainMenuList;
        Assert.assertEquals(1, (mainMenuListTest.size()));
    }

    /**
     * Test case on the getNavigationColumnList
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testGetNavigationColumnList() throws NoSuchFieldException
    {

        Long num = 1L;
        PrivateAccessor.setField(primaryNavigationModel, Constants.COLUMN_SELECTOR_KEY, Constants.TWO_OPTIONS_IN_MENU);
        primaryNavigationModel = new PrimaryNavigationModel();
        List<LinksHelperModel> colList = new ArrayList<>();
        List<NavigationColumnBean> navigationColumnList = new ArrayList<>();
        navigationColumnBean = new NavigationColumnBean(COLUMN_ONE_HEADING, colList);
        colList.add(linksHelperModel);
        colList.add(linksHelperModel);

        mainMenuBean = new MainMenuBean(null, null, null);
        mainMenuBean.setLabel(MENU_LABEL);
        mainMenuBean.setTopmenulevel(navigationColumnList);
        List<Resource> childResources = new ArrayList<>();
        childResources.add(mockChildResourceOne);
        childResources.add(mockChildResourceTwo);

        when(columnOneResource.listChildren()).thenReturn(childResources.iterator());
        when(columnTwoResource.listChildren()).thenReturn(childResources.iterator());
        when(columnThreeResource.listChildren()).thenReturn(childResources.iterator());
        when(columnFourResource.listChildren()).thenReturn(childResources.iterator());
        when(mockChildResourceOne.adaptTo(LinksHelperModel.class)).thenReturn(linksHelperModel);
        primaryNavigationModel.primaryNavInit();
        primaryNavigationModel.getAttributes();
        mainMenuBean = primaryNavigationModel.getMainMenuBean(MENU_LABEL, COLUMN_ONE_HEADING, COLUMN_TWO_HEADING,
                COLUMN_THREE_HEADING, COLUMN_FOUR_HEADING, columnOneResource, columnTwoResource, columnThreeResource,
                columnFourResource, "1");

        navigationColumnBean.setHeading(COLUMN_ONE_HEADING);
        navigationColumnBean.setNavheading(colList);
        navigationColumnList.add(navigationColumnBean);

        Assert.assertEquals("_blank", linksHelperModel.getTarget());
        Assert.assertEquals(MENU_LABEL, mainMenuBean.getLabel());
        Assert.assertEquals(num, mainMenuBean.getMenuPosition());
        Assert.assertEquals(navigationColumnList.get(0).getHeading(), mainMenuBean.getTopmenulevel().get(0).heading);
        Assert.assertNotNull(mainMenuBean.toString());

    }

}
