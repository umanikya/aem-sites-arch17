package com.ftd.platform.core.services.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;

import javax.jcr.RepositoryException;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.ftd.platform.core.utils.ContentPageListUtil;

import junitx.util.PrivateAccessor;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ContentPageListUtil.class)
public class SeoMetadataServiceImplTest
{
    @InjectMocks
    SeoMetadataServiceImpl seoMetadataServiceImpl;
    
    @Mock
    private QueryBuilder mockQueryBuilder;
    
    @Mock
    private ResourceResolverFactory mockResourceResolverFactory;
    
    @Mock
    private Query mockQuery;
    
    @Mock
    private Resource mockResource;
    
    @Mock
    private SearchResult mockResults;
    
    @Mock
    private Hit mockHit;
    
    @Mock
    private ValueMap mockValueMap;
    
    String siteId;
    String pageType;
    String pageId;
    
    
    /**
     * This method executes before running all test methods and is used for setting up data for the test cases.
     * 
     * @throws RepositoryException
     * @throws NoSuchFieldException
     */
    @Before
    public void setup() throws NoSuchFieldException 
    {
        seoMetadataServiceImpl = new SeoMetadataServiceImpl();
        PowerMockito.mockStatic(ContentPageListUtil.class);
        PrivateAccessor.setField(seoMetadataServiceImpl, "queryBuilder", mockQueryBuilder);
        PrivateAccessor.setField(seoMetadataServiceImpl, "resourceResolverFactory", mockResourceResolverFactory);
        siteId = "FTD";
        pageType = "home";
        pageId = "123";
        
    }
    
    @Test
    public void testGetSeoMetadata() throws LoginException, RepositoryException
    {
        when(mockResults.getHits()).thenReturn(Arrays.asList(new Hit[]{mockHit}));
        when(mockHit.getPath()).thenReturn("/content/experience-fragments/FTD/en-us/seometadata/home/123");
        when(mockHit.getResource()).thenReturn(mockResource);
        when(mockHit.getResource().getValueMap()).thenReturn(mockValueMap);
        when(mockResults.getHits()).thenReturn(Collections.singletonList(mockHit));
        when(ContentPageListUtil.getQueryResult(Mockito.any(ResourceResolverFactory.class), Mockito.any(QueryBuilder.class), Mockito.anyMap())).thenReturn(mockResults);
        String result = seoMetadataServiceImpl.getSeoMetadata(siteId, pageType, pageId);
        assertEquals("{\"resultCount\":1,\"seoMetadata\":[{\"metaTags\":{},\"experienceFragmentPath\":\"/content/experience-fragments/FTD/en-us/seometadata/home/123\",\"pageId\":\"123\",\"siteId\":\"FTD\",\"pageType\":\"home\"}]}", result);
    }
    
    @Test
    public void testGetSeoMetadataWithoutSiteid() throws LoginException, RepositoryException
    {
        when(mockResults.getHits()).thenReturn(Arrays.asList(new Hit[]{mockHit}));
        when(mockHit.getPath()).thenReturn("/content/experience-fragments/proflowers/en-us/seometadata/home/123");
        when(mockHit.getResource()).thenReturn(mockResource);
        when(mockHit.getResource().getValueMap()).thenReturn(mockValueMap);
        when(mockResults.getHits()).thenReturn(Collections.singletonList(mockHit));
        when(ContentPageListUtil.getQueryResult(Mockito.any(ResourceResolverFactory.class), Mockito.any(QueryBuilder.class), Mockito.anyMap())).thenReturn(mockResults);
        when(ContentPageListUtil.getQueryResult(Mockito.any(ResourceResolverFactory.class), Mockito.any(QueryBuilder.class), Mockito.anyMap())).thenReturn(mockResults);
        String result = seoMetadataServiceImpl.getSeoMetadata("", pageType, pageId);
        assertEquals("{\"resultCount\":1,\"seoMetadata\":[{\"metaTags\":{},\"experienceFragmentPath\":\"/content/experience-fragments/proflowers/en-us/seometadata/home/123\",\"pageId\":\"123\",\"siteId\":\"proflowers\",\"pageType\":\"home\"}]}", result);
    }
    
    @Test
    public void testLoginException() throws LoginException
    {
    when(ContentPageListUtil.getQueryResult(Mockito.any(ResourceResolverFactory.class), Mockito.any(QueryBuilder.class), Mockito.anyMap())).thenThrow(LoginException.class);
    String result = seoMetadataServiceImpl.getSeoMetadata(siteId, pageType, pageId);
    assertEquals("{}",result);
    }
    
    @Test
    public void testRepositoryException() throws LoginException, RepositoryException
    {
        when(mockResults.getHits()).thenReturn(Arrays.asList(new Hit[]{mockHit}));
        when(mockHit.getPath()).thenThrow(RepositoryException.class);
        when(ContentPageListUtil.getQueryResult(Mockito.any(ResourceResolverFactory.class), Mockito.any(QueryBuilder.class), Mockito.anyMap())).thenReturn(mockResults);
        String result = seoMetadataServiceImpl.getSeoMetadata(siteId, pageType, pageId);
        assertEquals("{\"resultCount\":1,\"seoMetadata\":[]}", result);
    }
    
}
