package com.ftd.platform.core.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.factory.ModelFactory;
import org.apache.sling.settings.SlingSettingsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.ftd.platform.core.beans.ReplicationDataBean;
import com.ftd.platform.core.models.BaseModel;
import com.ftd.platform.core.utils.ContentPageListUtil;
import com.ftd.platform.core.utils.Utils;

import junitx.util.PrivateAccessor;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ContentPageListUtil.class, Utils.class })
public class ContentPageDataServiceImplTest
{
    @InjectMocks
    ContentPageDataServiceImpl contentPageDataServiceImpl;

    @Mock
    SlingSettingsService mockSlingSettingsService;

    @Mock
    ModelFactory modelFactory;

    @Mock
    Resource rootResource;

    @Mock
    Resource componentResource;

    @Mock
    Resource childResource;

    @Mock
    Resource parentResource;

    @Mock
    BaseModel baseModel;

    @Mock
    ResourceResolver resourceResolver;

    @Mock
    private ValueMap mockValueMap;

    Date date;
    ReplicationDataBean replicationDataBean;

    @Before
    public void setup() throws NoSuchFieldException
    {
        contentPageDataServiceImpl = new ContentPageDataServiceImpl();
        PowerMockito.mockStatic(ContentPageListUtil.class);
        PowerMockito.mockStatic(Utils.class);
        PrivateAccessor.setField(contentPageDataServiceImpl, "slingSettingsService", mockSlingSettingsService);
        PrivateAccessor.setField(contentPageDataServiceImpl, "modelFactory", modelFactory);
        replicationDataBean = mockReplicationDataBean();
        LocalDate localDate = LocalDate.of(2018, Month.OCTOBER, 04);
        date = new Date(localDate.atStartOfDay(ZoneId.of("UTC")).toEpochSecond() * 1000);

    }

    @Test
    public void testGetModelJsonFromResource()
    {
        List<Resource> childResources;
        childResources = new ArrayList<>();
        childResources.add(childResource);
        Mockito.when(rootResource.listChildren()).thenReturn(childResources.iterator());
        Mockito.when(modelFactory.getModelFromResource(childResource)).thenReturn(baseModel);
        Object result = contentPageDataServiceImpl.getModelJsonFromResource(rootResource);
        assertEquals(result, baseModel);
        assertNotNull(replicationDataBean.toString());
    }

    @Test
    public void testGetStartDate()
    {
        when(Utils.getEnvironment(mockSlingSettingsService)).thenReturn("author");
        when(ContentPageListUtil.getActivateTime(replicationDataBean.getLastModified(),
                replicationDataBean.getLastReplicated(), replicationDataBean.getPublishDate(),
                replicationDataBean.getUnPublishDate(), replicationDataBean.getLastReplicationAction()))
                        .thenReturn(date);
        when(Utils.getFormattedDate(date)).thenReturn("Thu Oct 04 00:00:00 IST 2018");
        String result = contentPageDataServiceImpl.getStartDate(replicationDataBean);
        assertEquals("Thu Oct 04 00:00:00 IST 2018", result);
    }

    @Test
    public void testGetStartDateWithoutAuthor()
    {
        when(Utils.getEnvironment(mockSlingSettingsService)).thenReturn("");
        when(Utils.getFormattedDate(replicationDataBean.getLastModified())).thenReturn("Thu Oct 04 00:00:00 IST 2018");
        String result = contentPageDataServiceImpl.getStartDate(replicationDataBean);
        assertEquals("Thu Oct 04 00:00:00 IST 2018", result);
    }

    @Test
    public void testGetStartDateforNull()
    {
        when(Utils.getEnvironment(mockSlingSettingsService)).thenReturn("author");
        String result = contentPageDataServiceImpl.getStartDate(replicationDataBean);
        assertEquals(null, result);
    }

    @Test
    public void testGetEndDate()
    {
        when(Utils.getEnvironment(mockSlingSettingsService)).thenReturn("author");
        when(ContentPageListUtil.getDeActivateTime(replicationDataBean.getLastReplicated(),
                replicationDataBean.getPublishDate(), replicationDataBean.getUnPublishDate(),
                replicationDataBean.getLastReplicationAction())).thenReturn(date);
        when(Utils.getFormattedDate(date)).thenReturn("Thu Oct 04 00:00:00 IST 2018");
        String result = contentPageDataServiceImpl.getEndDate(replicationDataBean);
        assertEquals("Thu Oct 04 00:00:00 IST 2018", result);
    }

    @Test
    public void testGetEndDateWithoutAuthor()
    {
        when(Utils.getEnvironment(mockSlingSettingsService)).thenReturn("");
        when(Utils.getFormattedDate(replicationDataBean.getUnPublishDate())).thenReturn("Thu Oct 04 00:00:00 IST 2018");
        String result = contentPageDataServiceImpl.getEndDate(replicationDataBean);
        assertEquals("Thu Oct 04 00:00:00 IST 2018", result);
    }

    @Test
    public void testGetEndDateforNull()
    {
        when(Utils.getEnvironment(mockSlingSettingsService)).thenReturn("author");
        String result = contentPageDataServiceImpl.getEndDate(replicationDataBean);
        assertEquals(null, result);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testGetReplicationDataFromResource()
    {
        when(rootResource.adaptTo(ValueMap.class)).thenReturn(mockValueMap);

        when(mockValueMap.get("cq:lastModified", Date.class)).thenReturn(new Date("10/04/2018"));
        when(mockValueMap.get("cq:publishDate", Date.class)).thenReturn(new Date("06/04/2018"));
        when(mockValueMap.get("cq:unPublishDate", Date.class)).thenReturn(new Date("10/04/2018"));
        when(mockValueMap.get("cq:lastReplicated", Date.class)).thenReturn(new Date("06/04/2018"));
        when(mockValueMap.get("cq:lastReplicationAction", String.class)).thenReturn("Active");

        when(rootResource.getParent()).thenReturn(parentResource);
        when(rootResource.getParent().getValueMap()).thenReturn(mockValueMap);

        ReplicationDataBean result = contentPageDataServiceImpl.getReplicationDataFromResource(mockValueMap);

        assertEquals(new Date("10/04/2018"), result.getLastModified());
        assertEquals(new Date("06/04/2018"), result.getPublishDate());
        assertEquals(new Date("10/04/2018"), result.getUnPublishDate());
        assertEquals(new Date("06/04/2018"), result.getLastReplicated());
        assertEquals("Active", result.getLastReplicationAction());

    }

    @SuppressWarnings("deprecation")
    private ReplicationDataBean mockReplicationDataBean()
    {
        replicationDataBean = new ReplicationDataBean(date, date, date, date, "Activate");
        replicationDataBean.setLastModified(new Date("10/04/2018"));
        replicationDataBean.setLastReplicated(new Date("06/04/2018"));
        replicationDataBean.setPublishDate(new Date("06/04/2018"));
        replicationDataBean.setUnPublishDate(new Date("10/04/2018"));
        replicationDataBean.setLastReplicationAction("Activate");
        return replicationDataBean;
    }

}
