package com.ftd.platform.core.models;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.sling.api.resource.Resource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.ftd.platform.core.beans.LinkUrlTargetBean;
import com.ftd.platform.core.constants.Constants;

import junitx.util.PrivateAccessor;

/**
 * TestCases for the Class Footer Model
 * 
 * @author nikhil
 * 
 */
public class FooterModelTest
{

    private FooterModel footerModel;
    private Resource resource = mock(Resource.class);
    private Resource resourceOne = mock(Resource.class);
    private List<Resource> childResources = new ArrayList<>();
    private LinkUrlTargetBean columnData = new LinkUrlTargetBean("Text", "Link", "Icon");
    private LinkUrlTargetBean contactDataPhone = new LinkUrlTargetBean("Phone", "9999777888", Constants.CHAT_ICON_VALUE);
    private LinkUrlTargetBean contactDataEmail = new LinkUrlTargetBean("Email", "abc@gmail.com",
            Constants.EMAIL_ICON_VALUE);
    private LinkUrlTargetBean contactDataChat = new LinkUrlTargetBean("chat-liveagent-link");

    /**
     * This method executes before running all test methods and is used for setting up data for the test cases.
     */
    @Before
    public void setup() throws NoSuchFieldException
    {
        footerModel = new FooterModel();
        PrivateAccessor.setField(footerModel, "firstColumHeading", "The Rose Experts");
        PrivateAccessor.setField(footerModel, "firstColumResource", resource);
        PrivateAccessor.setField(footerModel, "secondColumHeading", "The Chocolates Experts");
        PrivateAccessor.setField(footerModel, "secondColumResource", resource);
        PrivateAccessor.setField(footerModel, "thirdColumHeading", "Flower Experts");
        PrivateAccessor.setField(footerModel, "thirdColumResource", resource);
        PrivateAccessor.setField(footerModel, "socialHeading", "Media Experts");
        PrivateAccessor.setField(footerModel, "socialResource", resource);
        PrivateAccessor.setField(footerModel, "richText", "Data Experts");
        PrivateAccessor.setField(footerModel, "termsResource", resource);
        PrivateAccessor.setField(footerModel, "contactHeading", "Contacts");
        PrivateAccessor.setField(footerModel, "contactDescription", "Contacts Description");
        PrivateAccessor.setField(footerModel, "phone", "Phone");
        PrivateAccessor.setField(footerModel, "phoneUrl", "9999777888");
        PrivateAccessor.setField(footerModel, "email", "Email");
        PrivateAccessor.setField(footerModel, "emailUrl", "abc@gmail.com");
        PrivateAccessor.setField(footerModel, "chatClassName", "chat-liveagent-link");
        PrivateAccessor.setField(footerModel, "chatClassName", "chat-liveagent-link");
        PrivateAccessor.setField(footerModel, "signuptitle", "subscribe for email offers");
        PrivateAccessor.setField(footerModel, "placeholdertext", "yourmail@example.com");
        PrivateAccessor.setField(footerModel, "ctalabel", "sign up");
        PrivateAccessor.setField(columnData, "target","_blank");
        PrivateAccessor.setField(columnData, "areaLabel","area");
    }

    
    @Test
    public void testGetAttributes()
    {
        List<LinkUrlTargetBean> columnOneList = new ArrayList<>();

        columnOneList.add(columnData);

        childResources.add(resourceOne);

        when(resource.listChildren()).thenReturn(childResources.iterator());
        when(resourceOne.adaptTo(LinkUrlTargetBean.class)).thenReturn(columnData);
        Map<String, Object> result = footerModel.getAttributes();
        Assert.assertEquals(7, result.size());
        Assert.assertEquals("footer", footerModel.getComponent());
        Assert.assertEquals("Footer", footerModel.getComponentTitle());
    }
    /**
     * Test case on the method getColumn1
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testColumnOne() throws NoSuchFieldException
    {
        List<LinkUrlTargetBean> columnOneList = new ArrayList<>();

        columnOneList.add(columnData);

        childResources.add(resourceOne);

        when(resource.listChildren()).thenReturn(childResources.iterator());
        when(resourceOne.adaptTo(LinkUrlTargetBean.class)).thenReturn(columnData);

        Map<String, Object> getColumnMapOne = footerModel.getColumn1();

        Assert.assertEquals("The Rose Experts", getColumnMapOne.get(Constants.HEADING_KEY));
        Assert.assertEquals(columnOneList.toString(),
                ((ArrayList<?>) getColumnMapOne.get(Constants.LINKS_KEY)).toString());
        Assert.assertEquals("Text",columnData.getText());
        Assert.assertEquals("Link",columnData.getLink());
        Assert.assertEquals("Icon",columnData.getIcon());
        Assert.assertEquals("_blank",columnData.getTarget());
        Assert.assertEquals("area",columnData.getAreaLabel());
    }

    /**
     * Test case on the method getColumn2
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testColumnTwo() throws NoSuchFieldException
    {
        List<LinkUrlTargetBean> columnTwoList = new ArrayList<>();

        columnTwoList.add(columnData);

        childResources.add(resourceOne);

        when(resource.listChildren()).thenReturn(childResources.iterator());
        when(resourceOne.adaptTo(LinkUrlTargetBean.class)).thenReturn(columnData);

        Map<String, Object> getColumnMapTwo = footerModel.getColumn2();

        Assert.assertEquals("The Chocolates Experts", getColumnMapTwo.get(Constants.HEADING_KEY));
        Assert.assertEquals(columnTwoList.toString(),
                ((ArrayList<?>) getColumnMapTwo.get(Constants.LINKS_KEY)).toString());
    }

    /**
     * Test case on the method getColumn3
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testColumnThree() throws NoSuchFieldException
    {
        List<LinkUrlTargetBean> columnThreeList = new ArrayList<>();

        columnThreeList.add(columnData);

        childResources.add(resourceOne);

        when(resource.listChildren()).thenReturn(childResources.iterator());
        when(resourceOne.adaptTo(LinkUrlTargetBean.class)).thenReturn(columnData);

        Map<String, Object> getColumnMapThree = footerModel.getColumn3();

        Assert.assertEquals("Flower Experts", getColumnMapThree.get(Constants.HEADING_KEY));
        Assert.assertEquals(columnThreeList.toString(),
                ((ArrayList<?>) getColumnMapThree.get(Constants.LINKS_KEY)).toString());
    }

    /**
     * Test case on the method getSocial
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testSocial() throws NoSuchFieldException
    {
        List<LinkUrlTargetBean> socialList = new ArrayList<>();

        socialList.add(columnData);

        childResources.add(resourceOne);

        when(resource.listChildren()).thenReturn(childResources.iterator());
        when(resourceOne.adaptTo(LinkUrlTargetBean.class)).thenReturn(columnData);

        Map<String, Object> getSocial = footerModel.getSocial();

        Assert.assertEquals("Media Experts", getSocial.get(Constants.HEADING_KEY));
        Assert.assertEquals(socialList.toString(), ((ArrayList<?>) getSocial.get(Constants.LINKS_KEY)).toString());
    }

    /**
     * Test case on the method getCopyRight
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testCopyRight() throws NoSuchFieldException
    {
        List<LinkUrlTargetBean> copyRightList = new ArrayList<>();

        copyRightList.add(columnData);

        childResources.add(resourceOne);

        when(resource.listChildren()).thenReturn(childResources.iterator());
        when(resourceOne.adaptTo(LinkUrlTargetBean.class)).thenReturn(columnData);

        Map<String, Object> getCopyRight = footerModel.getCopyRight();

        Assert.assertEquals("Data Experts", getCopyRight.get(Constants.DISCLAIMERS_KEY));
        Assert.assertEquals(copyRightList.toString(), ((ArrayList<?>) getCopyRight.get(Constants.LINKS_KEY)).toString());
    }

    /**
     * Test case on the method getContact
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testContact() throws NoSuchFieldException
    {
        List<LinkUrlTargetBean> contactList = new ArrayList<>();
        contactList.add(contactDataPhone);
        contactList.add(contactDataEmail);
        contactList.add(contactDataChat);

        Map<String, Object> getContact = footerModel.getContact();

        Assert.assertEquals("Contacts", getContact.get(Constants.HEADING_KEY));
        Assert.assertEquals("Contacts Description", getContact.get(Constants.DESCRIPTION_KEY));
        Assert.assertEquals("chat-liveagent-link",contactDataChat.getClassName());
    }
    
    @Test
    public void testSignup()
    {
        Map<String, Object> getsignUp = footerModel.getSignUp();
        Assert.assertEquals("subscribe for email offers", getsignUp.get("signuptitle"));
        Assert.assertEquals("yourmail@example.com", getsignUp.get("placeholdertext"));
        Assert.assertEquals("sign up", getsignUp.get("ctalabel"));
        
    }


}
