package com.ftd.platform.core.models;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.ftd.platform.core.constants.Constants;
import org.junit.Assert;
import junitx.util.PrivateAccessor;

/**
 * Unit Tests for the class RichText.
 * 
 * @author prabasva
 *
 */
public class ModalModelTest
{
    private static final String VARIENT_ID_VALUE = "Varient id value";
    private static final String MODAL_TITLE = "MODAL TITLE";
    private static final String MODAL_CONTENT = "Modal Content";
    private static final String MODAL_ID = "Model Id";

    private ModalModel modalModel;

    /**
     * This method executes before running all test methods and is used for setting up data for the test cases.
     * 
     * @throws NoSuchFieldException
     */
    @Before
    public void setup() throws NoSuchFieldException
    {
        modalModel = new ModalModel();
        
        PrivateAccessor.setField(modalModel, Constants.VARIATION_ID, VARIENT_ID_VALUE);
        PrivateAccessor.setField(modalModel, "modalTitle", MODAL_TITLE);
        PrivateAccessor.setField(modalModel, "modalContent", MODAL_CONTENT);
        PrivateAccessor.setField(modalModel, "modalId", MODAL_ID);
    }

    /**
     * Test case on the method getRichTextFieldsMap based on Drop down values
     * 
     * 
     * @throws NoSuchFieldException
     */
    @Test
    public void testgetRichTextFields()
    {
        Map<String, Object> modalResultMap = modalModel.getModalFields();
        
        @SuppressWarnings("unchecked")
        Map<String, Object> attributesMap = (Map<String, Object>) modalResultMap.get(Constants.ATTRIBUTES_KEY);

        Assert.assertEquals(Constants.MODAL_CONTRACT_KEY, modalResultMap.get(Constants.COMPONENT_KEY));
        Assert.assertEquals(VARIENT_ID_VALUE, modalResultMap.get(Constants.VARIATION_ID));
        Assert.assertEquals(MODAL_TITLE, attributesMap.get(Constants.MODAL_TITLE_KEY));
        Assert.assertEquals(MODAL_ID, attributesMap.get(Constants.MODAL_ID_KEY));
    }

}
