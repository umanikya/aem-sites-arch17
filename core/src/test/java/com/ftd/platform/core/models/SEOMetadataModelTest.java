package com.ftd.platform.core.models;

import static org.mockito.Mockito.when;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.sling.api.resource.Resource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.ftd.platform.core.beans.PageSEOBean;
import com.ftd.platform.core.utils.Utils;

import junitx.util.PrivateAccessor;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Utils.class)
public class SEOMetadataModelTest
{
    @InjectMocks
    SEOMetadataModel seoMetadataModel;
    
    @Mock
    private Resource seoList;
    
    PageSEOBean pageSEOBean;
    
    private static final String PAGE_TITLE = "Online Flowers - Flower Delivery - Send FTD Flowers, Plants & Gift";
    private static final String DESCRIPTION = "Order fresh flowers online with same day delivery or visit local FTD florists. Shop for flowers, sweets, gifts and gift baskets by occasion & season.";

    @Before
    public void setup() throws NoSuchFieldException
    {
        seoMetadataModel =  new SEOMetadataModel();
        PrivateAccessor.setField(seoMetadataModel, "pageTitle", PAGE_TITLE);
        PrivateAccessor.setField(seoMetadataModel, "description", DESCRIPTION);
        PrivateAccessor.setField(seoMetadataModel, "seoList", seoList);
        PowerMockito.mockStatic(Utils.class);
        pageSEOBean = mockPageSEOBean();
    }
    
    @Test
    public void testGetAttributes()
    {
       Map<String, PageSEOBean> map = new LinkedHashMap<>();
       map.put(pageSEOBean.getName(), pageSEOBean);
       when(Utils.getSEOListFromResource(seoList)).thenReturn(map);
       Map<String, Object> result = seoMetadataModel.getAttributes();
       Assert.assertEquals(result.size(), 3);
       Assert.assertEquals(result.get("pageTitle"), PAGE_TITLE);
       Assert.assertEquals(result.get("description"), DESCRIPTION);
       Assert.assertEquals(result.get("metaTags"), map);
    }
    
    private PageSEOBean mockPageSEOBean()
    {
        PageSEOBean pageSEOBean = new PageSEOBean();
        pageSEOBean.setContent("noindex, nofollow");
        pageSEOBean.setName("robots");
        return pageSEOBean;
    }
    
}
