package com.ftd.platform.core.servlets;

import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import static org.mockito.Mockito.verify;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.ftd.platform.core.services.SeoMetadataService;

import junitx.util.PrivateAccessor;

@RunWith(MockitoJUnitRunner.class)
public class GetSeoMetadataServletTest {
	@Mock
	SlingHttpServletRequest mockRequest;
	@Mock
	SlingHttpServletResponse mockResponse;
	@Mock
	PrintWriter printWriter;
	@Mock
	SeoMetadataService mockSeoMetadataService;
	
	GetSeoMetadataServlet getSeoMetadataServlet;
	
	@Before
	public void setup() throws IOException {
		getSeoMetadataServlet = new GetSeoMetadataServlet();
	    when(mockResponse.getWriter()).thenReturn(printWriter);
	}

	@Test
	public void testDoGet() throws ServletException, IOException,  NoSuchFieldException
	{
		String siteId = "ftd";
		String pageType = "";
		String pageId = "";
		when(mockRequest.getParameter("siteId")).thenReturn("ftd");
		when(mockRequest.getParameter("pageType")).thenReturn("");
		when(mockRequest.getParameter("pageId")).thenReturn("");
		PrivateAccessor.setField(getSeoMetadataServlet, "seoMetadataService", mockSeoMetadataService);
		when(mockSeoMetadataService.getSeoMetadata(siteId, pageType, pageId)).thenReturn("value");
		getSeoMetadataServlet.doGet(mockRequest, mockResponse);
		verify(mockSeoMetadataService).getSeoMetadata(siteId, pageType, pageId);
	}
}
