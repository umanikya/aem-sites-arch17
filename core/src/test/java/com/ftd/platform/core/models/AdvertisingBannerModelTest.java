package com.ftd.platform.core.models;

import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.ftd.platform.core.constants.Constants;

public class AdvertisingBannerModelTest
{

    private AdvertisingBannerModel advbannerModel;
    private static final String BUTTON_LABEL = "SHOP NOW";
    private static final String BUTTON_LINK = "/products";
    private static final String BUTTON_LINK_TARGET = "true";
    private static final String COLOR_THEME = "light";
    private static final String GOLD_COLOR_THEME = "gold";
    private static final String HEADING = "Mother's Day";
    private static final String IMAGE_LINK = "//s7d5.scene7.com/is/image/ProvideCommerce/FTD_18_MDAY_LX186S_MARCRO_0491_PLPBANNER";
    private static final String SUB_HEADING = "Make your mom smile with the gift of flowers for FTD.com";
    private static final String SAME_LINK_TARGET = "_self";

    /**
     * This method executes before running all test methods and is used for setting up data for the test cases.
     */
    @Before
    public void setup()
    {
        advbannerModel = new AdvertisingBannerModel();
        advbannerModel.setHeading(HEADING);
        advbannerModel.setSubHeading(SUB_HEADING);
        advbannerModel.setButtonLabel(BUTTON_LABEL);
        advbannerModel.setButtonLink(BUTTON_LINK);
        advbannerModel.setLinkTarget(BUTTON_LINK_TARGET);
        advbannerModel.setColor(COLOR_THEME);
        advbannerModel.setImage(IMAGE_LINK);
    }

    /**
     * Test case on the method getAttributes with CTA target as a new tab.
     */
    @Test
    public void testGetAttributesWithCTATarget()
    {

        Map<String, Object> attributesMap = advbannerModel.getAttributes();

        Assert.assertEquals(attributesMap.size(), 5);
        Assert.assertEquals(attributesMap.get(Constants.TITLE_KEY), HEADING);
        Assert.assertEquals(attributesMap.get(Constants.DESCRIPTIONS_KEY), SUB_HEADING);
        Assert.assertEquals(attributesMap.get(Constants.THEME_KEY), COLOR_THEME);
        Assert.assertEquals(((Map<?, ?>) attributesMap.get(Constants.PRIMARY_CTA_KEY)).get(Constants.TEXT_KEY),
                BUTTON_LABEL);
        Assert.assertEquals(((Map<?, ?>) attributesMap.get(Constants.PRIMARY_CTA_KEY)).get(Constants.URL_KEY),
                BUTTON_LINK);
        Assert.assertEquals(((Map<?, ?>) attributesMap.get(Constants.PRIMARY_CTA_KEY)).get(Constants.NEW_WINDOW_KEY),
                true);
    }

    /**
     * Test case on the method getAttributes with CTA target as a same window.
     */
    @Test
    public void testGetAttributesWithCTATargetSameWindow()
    {

        advbannerModel.setLinkTarget(SAME_LINK_TARGET);
        Map<String, Object> attributesMap = advbannerModel.getAttributes();
        Assert.assertEquals(attributesMap.size(), 5);
        Assert.assertEquals(((Map<?, ?>) attributesMap.get(Constants.PRIMARY_CTA_KEY)).get(Constants.TEXT_KEY),
                BUTTON_LABEL);
        Assert.assertEquals(((Map<?, ?>) attributesMap.get(Constants.PRIMARY_CTA_KEY)).get(Constants.URL_KEY),
                BUTTON_LINK);
        Assert.assertNull(((Map<?, ?>) attributesMap.get(Constants.PRIMARY_CTA_KEY)).get(Constants.TARGET_KEY));
    }

    /**
     * Test case on the method getComponent.
     */
    @Test
    public void testGetComponent()
    {
        String componentID = advbannerModel.getComponent();
        Assert.assertEquals(componentID, Constants.ADVERTISING_BANNER_COMPONENT_TITLE);
    }
    
    /**
     * Test case on the method get component with gold theme.
     */
    @Test
    public void testGetComponentWithGoldTheme()
    {
        advbannerModel.setColor(GOLD_COLOR_THEME);

        String componentID = advbannerModel.getComponent();
        Assert.assertEquals(componentID, Constants.BANNER_COMPONENT_TITLE);
    }
    
    
    @Test
    public void testGetComponentTitle()
    {
        String componentTitle = advbannerModel.getComponentTitle();
        Assert.assertEquals(componentTitle, "Advertising Banner");
    }
}
