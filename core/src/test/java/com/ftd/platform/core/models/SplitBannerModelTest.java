/**
 * 
 */
package com.ftd.platform.core.models;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import junitx.util.PrivateAccessor;

/**
 * @author prabasva
 *
 */
public class SplitBannerModelTest
{

    private static final String DISPLAY_OPTIONS = "displayOptions";
    private SplitBannerModel splitBannerModel;

    /**
     * This method executes before running all test methods and is used for setting up data for the test cases.
     * 
     * @throws NoSuchFieldException
     */
    @Before
    public void setup() throws NoSuchFieldException
    {

        splitBannerModel = new SplitBannerModel();
        PrivateAccessor.setField(splitBannerModel, "component", "split-video-banner");

    }

    @Test
    public void testGetComponent() throws NoSuchFieldException
    {

        String bannerName = splitBannerModel.getComponent();
        assertSame("split-video-banner", bannerName);
    }

    @Test
    public void testGetAttributesOptionOne() throws NoSuchFieldException
    {

        setRightVideoAttributes();
        setLeftImageAttributes();
        PrivateAccessor.setField(splitBannerModel, DISPLAY_OPTIONS, "video-right");
        Map<String, Object> bannerVideoMap = splitBannerModel.getAttributes();
        assertThat(bannerVideoMap.size(), is(1));
    }

    @Test
    public void testGetAttributesOptionTwo() throws NoSuchFieldException
    {

        setLeftVideoAttributes();
        setRightImageAttributes();
        PrivateAccessor.setField(splitBannerModel, DISPLAY_OPTIONS, "video-left");
        Map<String, Object> bannerVideoMap = splitBannerModel.getAttributes();
        assertThat(bannerVideoMap.size(), is(1));
    }

    @Test
    public void testGetAttributesOptionThree() throws NoSuchFieldException
    {

        setLeftImageAttributes();
        setRightImageAttributes();

        PrivateAccessor.setField(splitBannerModel, DISPLAY_OPTIONS, "no-video");
        Map<String, Object> bannerVideoMap = splitBannerModel.getAttributes();
        assertThat(bannerVideoMap.size(), is(1));
    }

    private void setRightVideoAttributes() throws NoSuchFieldException
    {
        PrivateAccessor.setField(splitBannerModel, "rightVideoText", "Right Video Text");
        PrivateAccessor.setField(splitBannerModel, "rightVideoAsset", "Right Video Asset");
        PrivateAccessor.setField(splitBannerModel, "rightServerURL", "/content/rightVideourl");
        PrivateAccessor.setField(splitBannerModel, "rightVideoContentURL", "/content/rightcontenturl");
        PrivateAccessor.setField(splitBannerModel, "rightVideoEmailURL", "/content/rightemailurl");
        PrivateAccessor.setField(splitBannerModel, "rightVideoServerURL", "/content/rightserverurl");
        PrivateAccessor.setField(splitBannerModel, "rightVideoPosterImage", "/content/rightposterimage");
    }

    private void setLeftVideoAttributes() throws NoSuchFieldException
    {
        PrivateAccessor.setField(splitBannerModel, "leftVideoText", "Left Video Text");
        PrivateAccessor.setField(splitBannerModel, "leftVideoAsset", "Left Video Asset");
        PrivateAccessor.setField(splitBannerModel, "leftServerURL", "/content/leftVideourl");
        PrivateAccessor.setField(splitBannerModel, "leftVideoContentURL", "/content/leftcontenturl");
        PrivateAccessor.setField(splitBannerModel, "leftVideoEmailURL", "/content/leftemailurl");
        PrivateAccessor.setField(splitBannerModel, "leftVideoServerURL", "/content/leftserverurl");
        PrivateAccessor.setField(splitBannerModel, "leftVideoPosterImage", "/content/leftposterimage");
    }

    private void setRightImageAttributes() throws NoSuchFieldException
    {
        PrivateAccessor.setField(splitBannerModel, "rightImageURLRef", "/content/rightvideo");
        PrivateAccessor.setField(splitBannerModel, "rightImageAltText", "Right Image Not configured");
        PrivateAccessor.setField(splitBannerModel, "rightImageHeading", "Right Image Heading");
        PrivateAccessor.setField(splitBannerModel, "rightImageButtonLabel", "Right Image Button Lable");
        PrivateAccessor.setField(splitBannerModel, "rightImageButtonLink", "/content/rightbuttonlink");
    }

    private void setLeftImageAttributes() throws NoSuchFieldException
    {
        PrivateAccessor.setField(splitBannerModel, "leftImageURLRef", "/content/leftvideo");
        PrivateAccessor.setField(splitBannerModel, "leftImageAltText", "Left Image Not configured");
        PrivateAccessor.setField(splitBannerModel, "leftImageHeading", "Left Image Heading");
        PrivateAccessor.setField(splitBannerModel, "leftImageButtonLabel", "Left Image Button Lable");
        PrivateAccessor.setField(splitBannerModel, "leftImageButtonLink", "/content/leftbuttonlink");
    }

}
