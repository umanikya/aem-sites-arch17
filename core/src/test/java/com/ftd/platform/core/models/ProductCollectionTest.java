package com.ftd.platform.core.models;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.sling.api.resource.Resource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import static org.mockito.Mockito.when;

import com.ftd.platform.core.beans.ProdIdBean;
import com.ftd.platform.core.beans.PromoCardBean;
import com.ftd.platform.core.constants.Constants;

import junitx.util.PrivateAccessor;

@RunWith(MockitoJUnitRunner.class)
public class ProductCollectionTest
{
   @Mock
   Resource mockProdId;
   
   @Mock 
   Resource mockPromocard;
   
   @Mock
   Resource productResourceOne;
   
   @Mock
   Resource productResourceTwo;
   
   @Mock
   Resource promoResourceOne;
   
   List<Resource> childResources = new ArrayList<Resource>();
   List<Resource> childResourcesPromo = new ArrayList<Resource>();
   private ProdIdBean productOneBean = new ProdIdBean();
   private ProdIdBean productTwoBean = new ProdIdBean();
   private PromoCardBean promoCardBean = new PromoCardBean();
        
   ProductCollection productCollection;
   @Before
   public void setup() throws NoSuchFieldException{
       productCollection = new ProductCollection();
       PrivateAccessor.setField(productCollection, "heading", "Heading");
       PrivateAccessor.setField(productCollection, "subHeading", "SubHeading");
       PrivateAccessor.setField(productCollection, "prodId", mockProdId);
       PrivateAccessor.setField(productCollection, "promocard", mockPromocard);
       PrivateAccessor.setField(productCollection, "component", Constants.PRODUCT_COLLECTION_TITLE);
   }
   
    @Test
    public void testGetComponent()
    {
        String str = productCollection.getComponent();
        assertEquals(str,Constants.PRODUCT_COLLECTION_TITLE);
    }
    
    @Test
    public void testGetAttributes(){
        childResources.add(productResourceOne);
        childResources.add(productResourceTwo);
        childResourcesPromo.add(promoResourceOne);
        productOneBean.setProductid("123");
        productTwoBean.setProductid("456");
        when(mockProdId.listChildren()).thenReturn(childResources.iterator());
        when(productResourceOne.adaptTo(ProdIdBean.class)).thenReturn(productOneBean);
        when(productResourceTwo.adaptTo(ProdIdBean.class)).thenReturn(productTwoBean);
        when(mockPromocard.listChildren()).thenReturn(childResourcesPromo.iterator());
        when(promoResourceOne.getPath()).thenReturn("content/experience-fragments/ftd/en-us/promocard/plp/occasion_getwell");
        when(promoResourceOne.adaptTo(PromoCardBean.class)).thenReturn(promoCardBean);
        
        Map<String, Object> attribute = productCollection.getAttributes();
        assertEquals("Heading", attribute.get("title"));
        assertEquals("SubHeading", attribute.get("description"));
        assertEquals(2,((ArrayList<?>)attribute.get("productIds")).size());
        assertEquals(4,attribute.size());
    }
    
    @Test
    public void testGetComponentTitle()
    {
        assertEquals(productCollection.getComponentTitle(),"Product Collection");
    }

}
