package com.ftd.platform.core.models;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.factory.ModelFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.ftd.platform.core.constants.Constants;

import junitx.util.PrivateAccessor;

@RunWith(MockitoJUnitRunner.class)
public class MembershipSliderModelTest
{
    
    @InjectMocks
    MembershipSliderModel membershipSliderModel;
    
    @Mock
    private Resource benefits;
    
    @Mock
    private Resource termsAndConditions;
    
    @Mock
    private Resource termsAndConditionsDetails;
    
    @Mock
    Resource childResourceOne;
    
    @Mock
    Resource childResourceTwo;
    
    @Mock
    Resource childResourceThree;
    
    @Mock
    ModelFactory modelFactory;
    
    @Mock
    Resource rootResource;
    
    @Mock
    private ValueMap mockValueMapOne;
    
    @Mock
    private ValueMap mockValueMapTwo;
    
    @Mock
    private ValueMap mockValueMapThree;
    
    List<Resource> childResourcesOne;
    List<Resource> childResourcesTwo;
    List<Resource> childResourcesThree;
    
    private static final String HEADER = "FTD Gold Membership";
    private static final String BANNER_DESCRIPTION = "Want FREE standard shipping for a year and no service fees?";
    private static final String TERMS_AND_CONDITIONS_DETAILS_HEADER = "FTD Gold Membership Terms & Conditions";
    private static final String MEMBERSHIP_PID = "GM02";
    private static final String THEME = "Information";
    private static final String BENEFITS = "[Free Standard Shipping for a year. Pay only <b>$30</b> today]";
    private static final String TERMS_AND_CONDITIONS = "[{header=Continue the savings with automatic renewal, description=For your convenience, your FTD Gold Membership renews automatically each year by charging the credit card you used to purchase your FTD Gold Membership}]";
    private static final String TERMS_AND_CONDITIONS_DETAILS = "[{header=Free Standard Shipping/No Service Fees:, description=FTD Gold Membership members pay no standard shipping charges on FTD flowers, plants and gifts shipped direct in a gift box.}]";
    
    
    @Before
    public void setup() throws NoSuchFieldException
    {
        membershipSliderModel =  new MembershipSliderModel();
        membershipSliderModel.setHeader(HEADER);
        membershipSliderModel.setBannerDescription(BANNER_DESCRIPTION);
        membershipSliderModel.setTermsAndConditionsDetailsHeader(TERMS_AND_CONDITIONS_DETAILS_HEADER);
        membershipSliderModel.setMembershipPid(MEMBERSHIP_PID);
        membershipSliderModel.setTheme(THEME);
        PrivateAccessor.setField(membershipSliderModel, "benefits", benefits);
        PrivateAccessor.setField(membershipSliderModel, "termsAndConditions", termsAndConditions);
        PrivateAccessor.setField(membershipSliderModel, "termsAndConditionsDetails", termsAndConditionsDetails);
        childResourcesOne = mockChildResourcesOne();
        childResourcesTwo = mockChildResourcesTwo();
        childResourcesThree = mockChildResourcesThree();
    }
    
    @Test
    public void testGetAttributes()
    {
       prepareBenefitsObject();
       prepareTermsAndConditions();
       prepareTermsAndConditionsDetails();  
       
       Map<String, Object> result = membershipSliderModel.getAttributes();
       
       Assert.assertEquals(result.size(), 8);
       Assert.assertEquals(result.get("header"), HEADER);
       Assert.assertEquals(result.get("bannerDescription"), BANNER_DESCRIPTION);
       Assert.assertEquals(result.get("termsAndConditionsDetailsHeader"), TERMS_AND_CONDITIONS_DETAILS_HEADER);
       Assert.assertEquals(result.get("membershipPid"), MEMBERSHIP_PID);
       Assert.assertEquals(result.get("theme"), THEME);
       Assert.assertEquals(result.get("benefits").toString(), BENEFITS);
       Assert.assertEquals(result.get("termsAndConditions").toString(), TERMS_AND_CONDITIONS);
       Assert.assertEquals(result.get("termsAndConditionsDetails").toString(), TERMS_AND_CONDITIONS_DETAILS);
       
    }
    
    private void prepareTermsAndConditionsDetails()
    {
        when(termsAndConditionsDetails.listChildren()).thenReturn(childResourcesThree.iterator());
        when(childResourceThree.getValueMap()).thenReturn(mockValueMapThree);
        when(mockValueMapThree.get("header", "")).thenReturn("Free Standard Shipping/No Service Fees:");
        when(mockValueMapThree.get("description", "")).thenReturn("FTD Gold Membership members pay no standard shipping charges on FTD flowers, plants and gifts shipped direct in a gift box.");
        
    }

    private void prepareTermsAndConditions()
    {
        when(termsAndConditions.listChildren()).thenReturn(childResourcesTwo.iterator());
        when(childResourceTwo.getValueMap()).thenReturn(mockValueMapTwo);
        when(mockValueMapTwo.get("header", "")).thenReturn("Continue the savings with automatic renewal");
        when(mockValueMapTwo.get("description", "")).thenReturn("For your convenience, your FTD Gold Membership renews automatically each year by charging the credit card you used to purchase your FTD Gold Membership");
        
    }

    private void prepareBenefitsObject()
    {
        when(benefits.listChildren()).thenReturn(childResourcesOne.iterator());
        when(childResourceOne.getValueMap()).thenReturn(mockValueMapOne);
        when(mockValueMapOne.get("benefit", "")).thenReturn("Free Standard Shipping for a year. Pay only <b>$30</b> today");
        
    }

    private List<Resource> mockChildResourcesOne()
    {
        List<Resource> childResources = new ArrayList<>();
        childResources.add(childResourceOne);
        return childResources;
    }
    
    private List<Resource> mockChildResourcesTwo()
    {
        List<Resource> childResources = new ArrayList<>();
        childResources.add(childResourceTwo);
        return childResources;
    }
    
    private List<Resource> mockChildResourcesThree()
    {
        List<Resource> childResources = new ArrayList<>();
        childResources.add(childResourceThree);
        return childResources;
    }

}
