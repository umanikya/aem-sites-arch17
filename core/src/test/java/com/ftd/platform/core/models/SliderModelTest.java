package com.ftd.platform.core.models;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.sling.api.resource.Resource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.ftd.platform.core.beans.LinkUrlTargetBean;
import com.ftd.platform.core.beans.ProdIdBean;
import com.ftd.platform.core.constants.Constants;

import junitx.util.PrivateAccessor;

@RunWith(MockitoJUnitRunner.class)
public class SliderModelTest
{
    private SliderModel sliderModel;
    private static final String TITLE = "Join gold member";
    private static final String PRICING_TEXT = "Pricing section text";
    private static final String PRODUCT_SECTION_TITLE = "";
    private static final String SHORT_DESCRIPTION = "<p><b>FTD glod member</b></p>\\r\\n";
    private static final String LONG_DESCRIPTION = "<p><b>FTD glod member long</b></p>\\r\\n";
    private static final String AFTER_SANITY_SHORT_DESCRIPTION = "<b>FTD glod member</b>\\r\\n";
    private static final String AFTER_SANITY_LONG_DESCRIPTION = "<b>FTD glod member long</b>\\r\\n";
    private static final String PRODUCT_ID = "";
    private static final String BUTTON_LABEL = "SHOP NOW";
    private static final String BUTTON_LINK = "/products";
    private static final String BUTTON_LINK_TARGET = "true";
    private static final String SAME_LINK_TARGET = "_self";
    
    @Mock
    private Resource linksResource;
    
    @Mock
    Resource childResource;
    
    @Mock
    private LinkUrlTargetBean linkUrlTargetBean;
    
    List<Resource> childResources;
    

    /**
     * This method executes before running all test methods and is used for setting up data for the test cases.
     * @throws NoSuchFieldException 
     */
    @Before
    public void setup() throws NoSuchFieldException
    {
        sliderModel = new SliderModel();
        sliderModel.setTitle(TITLE);
        sliderModel.setPricingText(PRICING_TEXT);
        sliderModel.setProductSectionTitle(PRODUCT_SECTION_TITLE);
        sliderModel.setShortDescription(SHORT_DESCRIPTION);
        sliderModel.setLongDescription(LONG_DESCRIPTION);
        sliderModel.setProductId(PRODUCT_ID);
        sliderModel.setButtonLabel(BUTTON_LABEL);
        sliderModel.setButtonLink(BUTTON_LINK);
        sliderModel.setLinkTarget(BUTTON_LINK_TARGET);
        childResources = mockChildResources();
        
        PrivateAccessor.setField(sliderModel, "additionalLinks", linksResource);
        
        PrivateAccessor.setField(linkUrlTargetBean, "text", "Sympathy & Funeral");
        PrivateAccessor.setField(linkUrlTargetBean, "target", "//s7d5.scene7.com/is/image/ProvideCommerce/FTD_18_OCCASIONS_SYMPATHYFUNERAL_HP_CARDLISTMODULE?");
        PrivateAccessor.setField(linkUrlTargetBean, "icon", "Image");
        PrivateAccessor.setField(linkUrlTargetBean, "areaLabel", "Test");
        PrivateAccessor.setField(linkUrlTargetBean, "link", "/category/occasion_sympathy?intcid=card-module-2-sympathy");
        PrivateAccessor.setField(linkUrlTargetBean, "className", "SliderModel");
    }

    /**
     * Test case on the method getAttributes with CTA target as a new tab.
     */
    @Test
    public void testGetAttributesWithCTATarget()
    {
        when(linksResource.listChildren()).thenReturn(childResources.iterator());
        when(childResource.adaptTo(LinkUrlTargetBean.class)).thenReturn(linkUrlTargetBean);
        Map<String, Object> attributesMap = sliderModel.getAttributes();

        Assert.assertEquals(attributesMap.size(), 8);
        Assert.assertEquals(attributesMap.get(Constants.TITLE_KEY), TITLE);
        Assert.assertEquals(attributesMap.get("pricingText"), PRICING_TEXT);
        Assert.assertEquals(attributesMap.get("productSectionTitle"), PRODUCT_SECTION_TITLE);
        Assert.assertEquals(attributesMap.get("shortDescription"), AFTER_SANITY_SHORT_DESCRIPTION);
        Assert.assertEquals(attributesMap.get("longDescription"), AFTER_SANITY_LONG_DESCRIPTION);
    }

    /**
     * Test case on the method getAttributes with CTA target as a same window.
     */
    @Test
    public void testGetAttributesWithCTATargetSameWindow()
    {
        sliderModel.setLinkTarget(SAME_LINK_TARGET);
        when(linksResource.listChildren()).thenReturn(childResources.iterator());
        when(childResource.adaptTo(LinkUrlTargetBean.class)).thenReturn(linkUrlTargetBean);
        Map<String, Object> attributesMap = sliderModel.getAttributes();
        Assert.assertEquals(attributesMap.size(), 8);
        Assert.assertEquals(((Map<?, ?>) attributesMap.get(Constants.PRIMARY_CTA_KEY)).get(Constants.TEXT_KEY),
                BUTTON_LABEL);
        Assert.assertEquals(((Map<?, ?>) attributesMap.get(Constants.PRIMARY_CTA_KEY)).get(Constants.URL_KEY),
                BUTTON_LINK);
        Assert.assertNull(((Map<?, ?>) attributesMap.get(Constants.PRIMARY_CTA_KEY)).get(Constants.TARGET_KEY));
    }

    /**
     * Test case on the method getComponent.
     */
    @Test
    public void testGetComponent()
    {
        String componentID = sliderModel.getComponent();

        Assert.assertEquals(componentID, Constants.SLIDER_COMPONENT_TITLE);
        Assert.assertEquals("Slider", sliderModel.getComponentTitle());
    }
    
    private List<Resource> mockChildResources()
    {
        List<Resource> childResources = new ArrayList<>();
        childResources.add(childResource);
        return childResources;
    }
}
