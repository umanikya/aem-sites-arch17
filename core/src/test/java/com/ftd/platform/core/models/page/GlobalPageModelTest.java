package com.ftd.platform.core.models.page;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;


import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.factory.ModelFactory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.ftd.platform.core.constants.Constants;
import com.ftd.platform.core.models.BaseModel;
import com.ftd.platform.core.models.ExperienceFragmentModel;
import com.ftd.platform.core.models.FooterModel;
import com.ftd.platform.core.models.ModalModel;
import com.ftd.platform.core.models.PrimaryNavigationModel;
import com.ftd.platform.core.models.PromotionalBannerModel;

import junitx.util.PrivateAccessor;

public class GlobalPageModelTest
{
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @InjectMocks
    GlobalPageModel globalPageModel;
    
    @Mock
    GlobalPageModel globalPageModelone;
    
    @Mock
    PrimaryNavigationModel primaryNavModel;

    @Mock
    FooterModel footerModel;
    
    @Mock
    ModalModel modalModel;

    @Mock
    PromotionalBannerModel promotionalBannerModel;
    
    @Mock
    Resource headerResourc;
    
    @Mock
    Resource footerResource;
    
    @Mock
    Resource modalResource;
    

    /**
     * This method executes before running all test methods and is used for setting up data for the test cases.
     * 
     * @throws NoSuchFieldException
     */
    @Before
    public void setup() throws NoSuchFieldException
    {
        globalPageModel = new GlobalPageModel();
        List<Object> headerComponents = new ArrayList<>();
        List<Object> footerComponents = new ArrayList<>();
        List<Object> modalComponents = new ArrayList<>();
        
        headerComponents.add(primaryNavModel);
        headerComponents.add(promotionalBannerModel);
        
        footerComponents.add(footerModel);
        
        modalComponents.add(modalModel);
        
        PrivateAccessor.setField(globalPageModel, "headerComponents", headerComponents);
        PrivateAccessor.setField(globalPageModel, "footerComponents", footerComponents);
        PrivateAccessor.setField(globalPageModel, "modalComponents", modalComponents);
        PrivateAccessor.setField(globalPageModel, "modalResource", modalResource);
    }
    
    /**
     * Test init.
     */
    @Test
    public void testInit()
    {
        List<Object> headerComponentList = globalPageModel.getHeaderComponents();
        List<Object> footerComponentList = globalPageModel.getFooterComponents();
        List<Object> modalComponentList = globalPageModel.getModalComponents();
        List<Resource> childResources = new ArrayList<>();
        when(modalResource.listChildren()).thenReturn(childResources.iterator());
        globalPageModel.init();
        
        assertEquals(2, headerComponentList.size());
        assertEquals(1, footerComponentList.size());
        assertEquals(1, modalComponentList.size());
    }
   
    /**
     * It is used to test component list from page base model
     */
    @Test
    public void testGetComponents()
    {
        List<Object> componentList = globalPageModel.getComponents();
        assertNull(componentList);
    }

    /**
     * It is used to test layout
     */
    @Test
    public void testGetLayout()
    {
        String layout = globalPageModel.getLayout();
        assertEquals(Constants.GLOBAL_LAYOUT_TITLE, layout);
    }
    
}
