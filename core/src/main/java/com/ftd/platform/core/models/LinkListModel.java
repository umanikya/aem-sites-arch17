package com.ftd.platform.core.models;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ftd.platform.core.beans.LinkUrlTargetBean;
import com.ftd.platform.core.constants.Constants;
import com.ftd.platform.core.utils.Utils;

/**
 * Sling Model for Link List
 * 
 */
@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = Constants.LINK_LIST_RESOURCE_TYPE)
@Exporter(
        name = Constants.JACKSON_KEY,
        extensions = Constants.JSON_KEY)
public class LinkListModel extends BaseModel
{
    @Inject
    private String heading;
    
    @Inject
    private String theme;
    
    @Inject
    @JsonIgnore
    public Resource linksResource;

    private static final String COMPONENT_TITLE = "Link List";

    /**
     * This method returns the component identifier.
     * 
     * @return string for component identifier
     */
    @Override
    public String getComponent()
    {
        return Constants.LINK_LIST_KEY;
    }

    /**
     * Gets attributes
     * 
     * @return
     */
    public Map<String, Object> getAttributes()
    {
        Map<String, Object> attributes = new LinkedHashMap<>();

        List<LinkUrlTargetBean> linkList = Utils.getBeanDataFromResource(linksResource, LinkUrlTargetBean.class);
        if (StringUtils.isNotBlank(this.heading))
        {
            attributes.put("heading", this.heading);
        }
        if (StringUtils.isNotBlank(this.theme))
        {
            attributes.put("componentTheme", this.theme);
        }
        attributes.put("links", linkList);

        return attributes;
    }

    /**
     * This is to get component title
     * 
     * @return
     */
    @Override
    public String getComponentTitle()
    {
        return COMPONENT_TITLE;
    }
}
