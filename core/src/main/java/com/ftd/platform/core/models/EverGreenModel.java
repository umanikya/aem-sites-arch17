package com.ftd.platform.core.models;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ftd.platform.core.beans.SectionsBean;
import com.ftd.platform.core.constants.Constants;

/**
 * Sling Model for Ever Green component
 * 
 * @author chakrish1
 *
 */
@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = Constants.EVERGREEN_RESOURCE_TYPE)
@Exporter(
        name = Constants.JACKSON_KEY,
        extensions = Constants.JSON_KEY)
public class EverGreenModel extends BaseModel
{
    @Inject
    @JsonIgnore
    public Resource iconsListResource;

    @Inject
    private String lifeStyleContent;

    List<Object> productInfoList = new LinkedList<>();

    @Inject
    @Default(
            values = Constants.EVERGREEN_CONTENT_TITLE)
    private String component;
    private static final String COMPONENT_TITLE = "Evergreen Content";

    /**
     * This method returns the component identifier.
     * 
     * @return
     */
    @Override
    public String getComponent()
    {
        return component;
    }

    /**
     * This method constructs the everGreen map object and returns the same.
     * 
     * @return
     */

    public Map<String, Object> getEvergreen()
    {
        List<Object> iconList = new LinkedList<>();
        Map<String, Object> everGreenMap = new LinkedHashMap<>();
        if (Objects.nonNull(iconsListResource))
        {
            Iterator<Resource> iterator = iconsListResource.listChildren();
            while (iterator.hasNext())
            {
                Resource childResource = iterator.next();
                SectionsBean sectionsBean = childResource.adaptTo(SectionsBean.class);
                Map<String, Object> sectionsMap = new LinkedHashMap<>();
                if (Objects.nonNull(childResource) && Objects.nonNull(sectionsBean))
                {
                    sectionsMap.put(Constants.TYPE_KEY, sectionsBean.getIcon());
                    sectionsMap.put(Constants.TITLE_KEY, sectionsBean.getTitle());
                    sectionsMap.put(Constants.CONTENT_KEY, sectionsBean.getContent());
                    sectionsMap.put(Constants.CTA_KEY, sectionsBean.getCta());
                    if (((LinkedHashMap<?, ?>) sectionsBean.getCta().get(0)).size() < 3)
                    {
                        sectionsMap.remove(Constants.CTA_KEY);
                    }
                    iconList.add(sectionsMap);
                }
            }
        }
        everGreenMap.put(Constants.SECTIONS_KEY, iconList);
        return everGreenMap;
    }

    /**
     * This method constructs the List of Product Information map objects and returns the same.
     * 
     * @return
     */
    @JsonProperty("product-info")
    public List<Object> getprod()
    {
        if (null != resource)
        {
            int LifeStyleContentCount =getLifeStyleContent();
            for(int count=0; count< LifeStyleContentCount; count++)
            {
                Resource lifeStyleContentResouce  = resource.getChild("item" + count);
                System.out.print("item"+count);
                if (null != lifeStyleContentResouce )
                {
                    setDataForLifeStyle(lifeStyleContentResouce .adaptTo(LifeStyleContentModel.class));
                }
            }
        }
        return productInfoList;
    }
    /**
     * This method constructs the Content map object and returns the same.
     * 
     * @return
     */

    public Map<String, Object> getContent(String title, String anchor, String details, String label, String url,
            String target)
    {
        Map<String, Object> content = new LinkedHashMap<>();
        Map<String, Object> contentMap = new LinkedHashMap<>();
        Map<String, Object> ctaMap = new LinkedHashMap<>();

        ctaMap.put(Constants.LABEL_KEY, label);
        ctaMap.put(Constants.URL_KEY, url);
        setTargetKeyInPrimaryCta(ctaMap, target);

        contentMap.put(Constants.TITLE_KEY, title);
        contentMap.put(Constants.ANCHOR_KEY, anchor);
        contentMap.put(Constants.DETAILS_KEY, details);
        contentMap.put(Constants.CTA_KEY, ctaMap);

        content.put(Constants.DISPLAY_TYPE_KEY, Constants.CONTENT_KEY);
        content.put(Constants.CONTENT_KEY, contentMap);

        return content;
    }

    /**
     * This method constructs the Content map object and returns the same.
     * 
     * @return
     */
    public Map<String, Object> getVideo(String videoAsset, String serverUrl, String videContentUrl,
            String videoEmailUrl, String videoServerUrl)
    {
        Map<String, Object> videoContent = new LinkedHashMap<>();
        Map<String, Object> videoMap = new LinkedHashMap<>();

        videoMap.put(Constants.ASSET_KEY, videoAsset);
        videoMap.put(Constants.SERVER_URL_KEY, serverUrl);
        videoMap.put(Constants.CONTENT_URL_KEY, videContentUrl);
        videoMap.put(Constants.EMAIL_URL_KEY, videoEmailUrl);
        videoMap.put(Constants.VIDEO_SERVER_URL_KEY, videoServerUrl);
        videoContent.put(Constants.DISPLAY_TYPE_KEY, Constants.VIDEO_KEY);
        videoContent.put(Constants.CONTENT_KEY, videoMap);

        return videoContent;
    }

    /**
     * This method constructs the Image map object and returns the same.
     * 
     * @return
     */
    public Map<String, Object> getImage(String imageUrl, String imageAltText)
    {
        Map<String, Object> imageContent = new LinkedHashMap<>();
        Map<String, Object> imageMap = new LinkedHashMap<>();

        imageMap.put(Constants.IMAGE_KEY, imageUrl);
        imageMap.put(Constants.ALT_KEY, imageAltText);

        imageContent.put(Constants.DISPLAY_TYPE_KEY, Constants.PHOTO_KEY);
        imageContent.put(Constants.CONTENT_KEY, imageMap);

        return imageContent;
    }

    public void setDataForLifeStyle(LifeStyleContentModel lifecyclemodel)
    {

        if (lifecyclemodel.getLifeStyleContent().equals("videoleft"))
        {
            productInfoList.add(getVideo(lifecyclemodel.getVideoAsset(), lifecyclemodel.getServerUrl(),
                    lifecyclemodel.getVideoContentUrl(), lifecyclemodel.getVideoEmailUrl(),
                    lifecyclemodel.getVideoServerUrl()));
            productInfoList
                    .add(getContent(lifecyclemodel.getTitle(), lifecyclemodel.getAnchor(), lifecyclemodel.getDetails(),
                            lifecyclemodel.getCtaLabel(), lifecyclemodel.getCtaUrl(), lifecyclemodel.getCtaTarget()));
        }
        if (lifecyclemodel.getLifeStyleContent().equals("videoright"))
        {
            productInfoList
                    .add(getContent(lifecyclemodel.getTitle(), lifecyclemodel.getAnchor(), lifecyclemodel.getDetails(),
                            lifecyclemodel.getCtaLabel(), lifecyclemodel.getCtaUrl(), lifecyclemodel.getCtaTarget()));
            productInfoList.add(getVideo(lifecyclemodel.getVideoAsset(), lifecyclemodel.getServerUrl(),
                    lifecyclemodel.getVideoContentUrl(), lifecyclemodel.getVideoEmailUrl(),
                    lifecyclemodel.getVideoServerUrl()));
        }

        if (lifecyclemodel.getLifeStyleContent().equals("imageleft"))
        {
            productInfoList.add(getImage(lifecyclemodel.getImageUrl(), lifecyclemodel.getImageAltText()));
            productInfoList
                    .add(getContent(lifecyclemodel.getTitle(), lifecyclemodel.getAnchor(), lifecyclemodel.getDetails(),
                            lifecyclemodel.getCtaLabel(), lifecyclemodel.getCtaUrl(), lifecyclemodel.getCtaTarget()));
        }
        if (lifecyclemodel.getLifeStyleContent().equals("imageright"))
        {
            productInfoList
                    .add(getContent(lifecyclemodel.getTitle(), lifecyclemodel.getAnchor(), lifecyclemodel.getDetails(),
                            lifecyclemodel.getCtaLabel(), lifecyclemodel.getCtaUrl(), lifecyclemodel.getCtaTarget()));
            productInfoList.add(getImage(lifecyclemodel.getImageUrl(), lifecyclemodel.getImageAltText()));
        }
    }
    /**
     * This is to get component title
     * 
     * @return
     */
    @Override
    public String getComponentTitle()
    {
        return COMPONENT_TITLE;
    }

    @JsonIgnore
    public int getLifeStyleContent()
    {
        if (null != lifeStyleContent)
            switch (lifeStyleContent) {
                case "zero" :
                    return 0;
                case "one" :
                    return 1;
                case "two" :
                    return 2;
                case "three" :
                    return 3;
                case "four" :
                    return 4;
                case "five" :
                    return 5;
            }

        return 0;

    }

}
