package com.ftd.platform.core.beans;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class CategoryIdBean
{

    @Inject
    @JsonProperty("displayName")
    private String catGiftLabel;

    @Inject
    @JsonProperty("giftCategoryId")
    private String catGiftId;

    @Inject
    @JsonProperty("selected")
    private String selectValue;
    
    @Inject
    private String pagetype;

    public String getCatGiftLabel()
    {
        return catGiftLabel;
    }

    public void setCatGiftLabel(String catGiftLabel)
    {
        this.catGiftLabel = catGiftLabel;
    }

    public String getCatGiftId()
    {
        return catGiftId;
    }

    public void setCatGiftId(String catGiftId)
    {
        this.catGiftId = catGiftId;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getSelectValue()
    {
        return selectValue;
    }

    public void setSelectValue(String selectValue)
    {
        this.selectValue = selectValue;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getPagetype()
    {
        return pagetype;
    }

    public void setPagetype(String pagetype)
    {
        this.pagetype = pagetype;
    }
    
    

}
