package com.ftd.platform.core.models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import com.ftd.platform.core.utils.Utils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ftd.platform.core.beans.LinkUrlTargetBean;
import com.ftd.platform.core.constants.Constants;

/**
 * Sling Model for Footer Component
 * 
 * @author chakrish1
 * 
 */
@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = Constants.FOOTER_RESOURCE_TYPE)
@Exporter(
        name = "jackson",
        extensions = "json")
public class FooterModel extends BaseModel
{
    @Inject
    private String firstColumHeading;

    @Inject
    @JsonIgnore
    public Resource firstColumResource;

    @Inject
    @JsonIgnore
    private String secondColumHeading;

    @Inject
    @JsonIgnore
    public Resource secondColumResource;

    @Inject
    @JsonIgnore
    private String thirdColumHeading;

    @Inject
    @JsonIgnore
    public Resource thirdColumResource;

    @Inject
    @JsonIgnore
    public Resource socialResource;

    @Inject
    private String contactHeading;

    @Inject
    private String contactDescription;

    @Inject
    private String phone;

    @Inject
    private String email;

    @Inject
    private String chatClassName;

    @Inject
    private String phoneUrl;

    @Inject
    private String emailUrl;

    @Inject
    private String socialHeading;

    @Inject
    @JsonIgnore
    public Resource termsResource;

    @Inject
    private String richText;

    @Inject
    private String signuptitle;

    @Inject
    private String placeholdertext;

    @Inject
    private String ctalabel;

    private static final String COMPONENT_TITLE = "Footer";

    /**
     * This is to get component title
     * 
     * @return
     */
    @Override
    public String getComponentTitle()
    {
        return COMPONENT_TITLE;
    }

    /**
     * It returns the Attributes Map containing the footer component data.
     * 
     * @return
     */
    public Map<String, Object> getAttributes()
    {
        Map<String, Object> attributesMap = new LinkedHashMap<>();
        attributesMap.put("column1", getColumn1());
        attributesMap.put("column2", getColumn2());
        attributesMap.put("column3", getColumn3());
        attributesMap.put("contact", getContact());
        attributesMap.put("social", getSocial());
        attributesMap.put("copyright", getCopyRight());
        attributesMap.put("signup", getSignUp());
        return attributesMap;
    }

    /**
     * This method returns the Map object holding the information required for column 1 section.
     * 
     * @return
     */

    @JsonIgnore
    public Map<String, Object> getColumn1()
    {

        Map<String, Object> firstColumMap = new LinkedHashMap<>();
        firstColumMap.put(Constants.HEADING_KEY, this.firstColumHeading);
        firstColumMap.put(Constants.LINKS_KEY, getMenuLinksResource(firstColumResource));
        return firstColumMap;
    }

    /**
     * This method returns the Map object holding the information required for column 2 section.
     * 
     * @return
     */

    @JsonIgnore
    public Map<String, Object> getColumn2()
    {

        Map<String, Object> secondColumMap = new LinkedHashMap<>();

        secondColumMap.put(Constants.HEADING_KEY, this.secondColumHeading);
        secondColumMap.put(Constants.LINKS_KEY, getMenuLinksResource(secondColumResource));
        return secondColumMap;
    }

    /**
     * This method returns the Map object holding the information required for column 3 section.
     * 
     * @return
     */

    @JsonIgnore
    public Map<String, Object> getColumn3()
    {

        Map<String, Object> thirdColumMap = new LinkedHashMap<>();

        thirdColumMap.put(Constants.HEADING_KEY, this.thirdColumHeading);
        thirdColumMap.put(Constants.LINKS_KEY, getMenuLinksResource(thirdColumResource));
        return thirdColumMap;
    }

    /**
     * This method returns the Map object holding the information required for contacts Us section.
     * 
     * @return
     */

    @JsonIgnore
    public Map<String, Object> getContact()
    {

        Map<String, Object> contactUsMap = new LinkedHashMap<>();

        contactUsMap.put(Constants.HEADING_KEY, this.contactHeading);
        contactUsMap.put(Constants.DESCRIPTION_KEY, this.contactDescription);
        List<LinkUrlTargetBean> contactList = new ArrayList<>();
        LinkUrlTargetBean linkBean = new LinkUrlTargetBean(this.phone, Constants.PHONE_SUFFIX + this.phoneUrl,
                Constants.PHONE_ICON_VALUE);
        contactList.add(linkBean);
        linkBean = new LinkUrlTargetBean(this.email, Constants.EMAIL_SUFFIX + this.emailUrl,
                Constants.EMAIL_ICON_VALUE);
        contactList.add(linkBean);
        linkBean = new LinkUrlTargetBean(chatClassName);
        contactList.add(linkBean);

        contactUsMap.put(Constants.LINKS_KEY, contactList);
        return contactUsMap;
    }

    @JsonIgnore
    public Map<String, Object> getSignUp()
    {
        Map<String, Object> signupMap = new LinkedHashMap<>();
        signupMap.put("signuptitle", this.signuptitle);
        signupMap.put("placeholdertext", this.placeholdertext);
        signupMap.put("ctalabel", this.ctalabel);
        return signupMap;

    }

    /**
     * This method returns the Map object holding the information required for Social section.
     * 
     * @return
     */

    @JsonIgnore
    public Map<String, Object> getSocial()
    {

        Map<String, Object> socialLinksMap = new LinkedHashMap<>();

        socialLinksMap.put(Constants.HEADING_KEY, this.socialHeading);
        socialLinksMap.put(Constants.LINKS_KEY, getMenuLinksResource(socialResource));
        return socialLinksMap;
    }

    /**
     * This method returns the Map object holding the information required for Terms and Condition section.
     * 
     * @return
     */

    @JsonIgnore
    public Map<String, Object> getCopyRight()
    {

        Map<String, Object> copyRightsMap = new LinkedHashMap<>();

        copyRightsMap.put(Constants.LINKS_KEY, getMenuLinksResource(termsResource));
        copyRightsMap.put(Constants.DISCLAIMERS_KEY, Utils.removeParaTagsFromRTE(this.richText));
        return copyRightsMap;
    }

    /**
     * Iterating the Link ,label ,Target from LinkUrlTarget bean class by creating a ArrayList
     * 
     * @param menuLinksResource
     * @return
     */
    @JsonIgnore
    public List<LinkUrlTargetBean> getMenuLinksResource(Resource menuLinksResource)
    {
        List<LinkUrlTargetBean> menuLinksResourceList = new ArrayList<>();
        if (menuLinksResource != null)
        {
            Iterator<Resource> iterator = menuLinksResource.listChildren();
            while (iterator.hasNext())
            {
                Resource childResource = iterator.next();
                if (childResource != null)
                    menuLinksResourceList.add(childResource.adaptTo(LinkUrlTargetBean.class));
            }
        }
        return menuLinksResourceList;
    }

    /**
     * This method returns the component identifier.
     * 
     * @return string for component identifier
     */
    @Override
    public String getComponent()
    {
        return Constants.FOOTER_TITLE;
    }

}
