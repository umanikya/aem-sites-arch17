package com.ftd.platform.core.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.RepositoryException;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.ftd.platform.core.services.SeoMetadataService;
import com.ftd.platform.core.utils.ContentPageListUtil;
import com.ftd.platform.core.utils.Utils;
import com.google.gson.Gson;

/**
 * Service implementation class of the service SeoMetadataService.
 * 
 */
@Component(
        name = "SeoMetadataServiceImplementaion",
        service = SeoMetadataService.class,
        immediate = true)
public class SeoMetadataServiceImpl implements SeoMetadataService
{
    private static final Logger LOG = LoggerFactory.getLogger(SeoMetadataService.class);
    
    @Reference
    QueryBuilder queryBuilder;

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    private static String SEO_METADATA_PATH_PATTERN = "/content/experience-fragments/{siteId}/en-us/seometadata";
    private static String [] siteIds = {"ftd", "proflowers"};

    @Override
    public String getSeoMetadata(String siteId, String pageType, String pageId)
    {
        if (StringUtils.isNotBlank(siteId))
        {
            String seoMetadataPath = StringUtils.replace(SEO_METADATA_PATH_PATTERN, "{siteId}", siteId);

            if (StringUtils.isNotBlank(pageType))
            {
                seoMetadataPath += "/" + pageType;

                if (StringUtils.isNotBlank(pageId))
                {
                    seoMetadataPath += "/" + pageId;
                }
            }

            return getSeoMetadataJson(new String[] { seoMetadataPath });
        } else
        {
            String[] seoMetadataPaths = new String[siteIds.length];

            for (int i = 0; i < siteIds.length; i++)
            {
                seoMetadataPaths[i] = StringUtils.replace(SEO_METADATA_PATH_PATTERN, "{siteId}", siteIds[i]);
            }
            return getSeoMetadataJson(seoMetadataPaths);
        }
    }

    private String getSeoMetadataJson(String [] seoMetadataPaths)
    {
        SearchResult queryResult;
        try
        {
            queryResult = ContentPageListUtil.getQueryResult(resourceResolverFactory, queryBuilder,
                    getQuerymap(seoMetadataPaths));
        } catch (LoginException e)
        {
            LOG.error("login Exception occurred :: {}", e);
            return "{}";
        } catch (Exception e)
        {
            LOG.error("Unknown Exception occurred :: {}", e);
            return "{}";
        }
        return getSeoMetadata(queryResult);
    }
    
    private Map<String, String> getQuerymap(String[] seoMetadataPaths)
    {
        Map<String, String> queryMap = new HashMap<>();

        queryMap.put("group.p.or", "true");

        for (int i = 0; i < seoMetadataPaths.length; i++)
        {
            queryMap.put("group." + (i + 1) + "_path", seoMetadataPaths[i]);
        }
        queryMap.put("type", "nt:unstructured");
        queryMap.put("property", "sling:resourceType");
        queryMap.put("property.value", "web/platform/components/content/seoMetadata");
        queryMap.put("p.limit", "-1");

        return queryMap;
    }
    
    private static String getSeoMetadata(SearchResult queryResult)
    {
        Map<String, Object> seoMetadata = new HashMap<>();
        List<Object> seoMetadataList = new ArrayList<>();

        for (final Hit hit : queryResult.getHits())
        {
            try
            {
                Map<String, Object> seoMetadataElement = new LinkedHashMap<>();
                String[] splitPaths = hit.getPath().split("/");
                ValueMap properties = hit.getResource().getValueMap();
                seoMetadataElement.put("pageTitle", properties.get("pageTitle", ""));
                seoMetadataElement.put("description", properties.get("description", ""));
                seoMetadataElement.put("metaTags", Utils.getSEOListFromResource(hit.getResource().getChild("seoList")));
                seoMetadataElement.put("experienceFragmentPath",
                        StringUtils.substringBefore(hit.getPath(), "/jcr:content"));
                seoMetadataElement.put("pageId",
                        StringUtils.substringAfterLast((String) seoMetadataElement.get("experienceFragmentPath"), "/"));
                seoMetadataElement.put("siteId", splitPaths[3]);
                seoMetadataElement.put("pageType", splitPaths[6]);
                seoMetadataList.add(seoMetadataElement);
            } catch (RepositoryException e)
            {
                LOG.error("Unable to process SEO node");
            }

        }
        seoMetadata.put("seoMetadata", seoMetadataList);
        seoMetadata.put("resultCount", queryResult.getHits().size());
        
        return new Gson().toJson(seoMetadata);
    }

}
