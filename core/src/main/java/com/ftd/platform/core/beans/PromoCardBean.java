package com.ftd.platform.core.beans;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.factory.ModelFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ftd.platform.core.constants.Constants;
import com.ftd.platform.core.services.ContentPageDataService;
import com.ftd.platform.core.utils.Utils;

/**
 * Bean Class for holding the promo card or advertising banner details for product list component.
 * 
 * @author prabasva
 * 
 */
@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class PromoCardBean
{
    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Inject
    private int row;

    @Inject
    private String columnPosition;

    @Inject
    private String promoCardReference;

    @Inject
    private ContentPageDataService contentPageDataService;
    
    @SlingObject
    private ResourceResolver resourceResolver;

    @SuppressWarnings("unchecked")
    @JsonAnyGetter
    public Map<String, Object> getPromoCardListing()
    {
        Map<String, Object> promocardMap = new LinkedHashMap<>();
        promocardMap.put(Constants.ROWNUMBER, row);
        promocardMap.put(Constants.COLPOSITION, columnPosition);
        Resource resource = resourceResolver.getResource(promoCardReference + Constants.ROOT_JCR_CONTENT_PATH);
        if (Objects.nonNull(resource))
        {
            ObjectMapper objectMapper = new ObjectMapper();
            Object modelObject = contentPageDataService.getModelJsonFromResource(resource);
            try
            {
                if (null != modelObject)
                {
                    promocardMap.putAll(objectMapper.convertValue(modelObject, Map.class));
                }
            } catch (Exception e)
            {
                log.debug("error occured during object conversion {}", e);
            }
        }
        return promocardMap;
    }
}
