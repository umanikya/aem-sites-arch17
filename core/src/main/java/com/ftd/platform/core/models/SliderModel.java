package com.ftd.platform.core.models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ftd.platform.core.beans.LinkUrlTargetBean;
import com.ftd.platform.core.constants.Constants;
import com.ftd.platform.core.utils.Utils;

/**
 * Sling Model for Slider component
 * 
 */
@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = "web/platform/components/content/slider")
@Exporter(
        name = "jackson",
        extensions = "json")
public class SliderModel extends BaseModel
{

    @Inject
    @JsonIgnore
    public Resource additionalLinks;

    @Inject
    private String title;

    @Inject
    private String pricingText;

    @Inject
    private String productSectionTitle;

    @Inject
    private String buttonLabel;

    @Inject
    private String buttonLink;

    @Inject
    private String linkTarget;

    @Inject
    private String shortDescription;

    @Inject
    private String longDescription;

    @Inject
    private String productId;

    String component;

    Map<String, Object> attributes = new LinkedHashMap<>();

    private static final String COMPONENT_TITLE = "Slider";

    /**
     * This method returns the component identifier.
     * 
     * @return
     */
    @Override
    public String getComponent()
    {
        return Constants.SLIDER_COMPONENT_TITLE;
    }

    /**
     * This method constructs the attributes map object and returns the same.
     * 
     * @return
     */
    public Map<String, Object> getAttributes()
    {
        Map<String, Object> primarycta = new LinkedHashMap<>();
        List<String> prodidlist = new ArrayList<>();

        primarycta.put(Constants.TEXT_KEY, buttonLabel);
        primarycta.put(Constants.URL_KEY, buttonLink);

        setTargetKeyInPrimaryCta(primarycta, linkTarget);

        attributes.put(Constants.TITLE_KEY, title);
        attributes.put("pricingText", pricingText);
        attributes.put("productSectionTitle", productSectionTitle);
        attributes.put("shortDescription", Utils.removeParaTagsFromRTE(shortDescription));
        attributes.put("longDescription", Utils.removeParaTagsFromRTE(longDescription));
        attributes.put("links", getLinks(additionalLinks));

        prodidlist.add(productId);
        attributes.put(Constants.PRODUCT_KEY, prodidlist);

        attributes.put(Constants.PRIMARY_CTA_KEY, primarycta);

        return attributes;
    }

    /**
     * Setter method for title variable
     * 
     * @param heading
     */
    public void setTitle(String title)
    {
        this.title = title;
    }

    /**
     * Setter method for productSectionTitle variable
     * 
     * @param productSectionTitle
     */
    public void setProductSectionTitle(String productSectionTitle)
    {
        this.productSectionTitle = productSectionTitle;
    }

    /**
     * Setter method for pricingText variable
     * 
     * @param pricingText
     */
    public void setPricingText(String pricingText)
    {
        this.pricingText = pricingText;
    }

    /**
     * Setter method for buttonLabel variable
     * 
     * @param buttonLabel
     */
    public void setButtonLabel(String buttonLabel)
    {
        this.buttonLabel = buttonLabel;
    }

    /**
     * Setter method for buttonLink variable
     * 
     * @param buttonLink
     */
    public void setButtonLink(String buttonLink)
    {
        this.buttonLink = buttonLink;
    }

    /**
     * Setter method for linkTarget variable
     * 
     * @param linkTarget
     */
    public void setLinkTarget(String linkTarget)
    {
        this.linkTarget = linkTarget;
    }

    /**
     * Setter method for shortDescription variable
     * 
     * @param shortDescription
     */
    public void setShortDescription(String shortDescription)
    {
        this.shortDescription = shortDescription;
    }

    /**
     * Setter method for longDescription variable
     * 
     * @param longDescription
     */
    public void setLongDescription(String longDescription)
    {
        this.longDescription = longDescription;
    }

    /**
     * Setter method for productId variable
     * 
     * @param productId
     */
    public void setProductId(String productId)
    {
        this.productId = productId;
    }

    /**
     * This is to get component title
     * 
     * @return
     */
    @Override
    public String getComponentTitle()
    {
        return COMPONENT_TITLE;
    }

    /**
     * Iterating the Link ,label ,Target from LinkUrlTarget bean class by creating a ArrayList
     * 
     * @param menuLinksResource
     * @return
     */
    @JsonIgnore
    public List<LinkUrlTargetBean> getLinks(Resource linksResource)
    {
        List<LinkUrlTargetBean> linksResourceList = new ArrayList<>();
        if (linksResource != null)
        {
            Iterator<Resource> iterator = linksResource.listChildren();
            while (iterator.hasNext())
            {
                Resource childResource = iterator.next();
                if (childResource != null)
                    linksResourceList.add(childResource.adaptTo(LinkUrlTargetBean.class));
            }
        }
        return linksResourceList;
    }

}
