package com.ftd.platform.core.filters;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.apache.sling.engine.EngineConstants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Deactivate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ftd.platform.core.beans.ComponentDialogFilterBean;

@Component(
        service = { Filter.class },
        immediate = true,
        configurationPolicy = ConfigurationPolicy.REQUIRE,
        property = { EngineConstants.SLING_FILTER_SCOPE + "=" + EngineConstants.FILTER_SCOPE_REQUEST,
                "label=Component Dialog Filter", "description=Filter to add conditional configuration tab to a component dialog", "service.ranking=-3000" })
public class ComponentDialogFilter implements Filter
{
    private Logger LOG = LoggerFactory.getLogger(ComponentDialogFilter.class);
    
    private ComponentDialogFilterBean componentDialogFilterBean;
    
    //Default values of OSGI service configuration
    private static String [] CC_EXCLUDE_PATHS = {"/content/experience-fragments"};
    private static String CC_HEAD_HTML_TAG_VALUE = "<a class=\"coral-TabPanel-tab\" href=\"#\" data-toggle=\"tab\">cc</a>";
    private static String CC_BODY_HTML_TAG_VALUE = "<section class=\"coral-TabPanel-pane coral-FixedColumn\"><div class=\"coral-FixedColumn-column\"><div class=\"coral-Form-fieldwrapper\"><label class=\"coral-Form-fieldlabel\">Variation Id</label><input class=\"coral-Form-field\" type=\"text\" name=\"./variationId\" value=\"\" data-foundation-validation=\"\" data-validation=\"\" is=\"coral-textfield\"><coral-icon class=\"coral-Form-fieldinfo\" icon=\"infoCircle\"></coral-icon><coral-tooltip target=\"_prev\" placement=\"left\"><coral-tooltip-content>Variation where the content need to be replaced</coral-tooltip-content></coral-tooltip></div><div class=\"coral-Form-fieldwrapper coral-Form-fieldwrapper--singleline\"><label class=\"coral-Checkbox coral-Form-field\"><input id=\"enable\" class=\"cq-dialog-checkbox-showhide coral-Checkbox-input\" type=\"checkbox\" name=\"./isConditional\" value=\"true\" data-validation=\"\" data-cq-dialog-checkbox-showhide-target=\".button-option-enable-showhide-target\" /><span class=\"coral-Checkbox-checkmark\"></span><span class=\"coral-Checkbox-description\">Is Condition Content ?</span><input type=\"hidden\" name=\"./isConditional@Delete\"></label></div><input type=\"hidden\" name=\"./enable@Delete\" value=\"true\" /><div class=\"hide button-option-enable-showhide-target\" data-showhidetargetvalue=\"true\"><div class=\"coral-Form-fieldwrapper\"><label id=\"label_1516857b-9bc0-4a85-a661-bb78b9cdf661\" class=\"coral-Form-fieldlabel\">Conditional Content Path *</label><foundation-autocomplete class=\"coral-Form-field\" name=\"./conditionalContentPath\" required=\"\" pickersrc=\"/mnt/overlay/granite/ui/content/coral/foundation/form/pathfield/picker.html?_charset_=utf-8&amp;path={value}&amp;root=%2fcontent%2fexperience-fragments&amp;filter=hierarchyNotFile&amp;selectionCount=single\" labelledby=\"label_1516857b-9bc0-4a85-a661-bb78b9cdf661\" data-foundation-validation=\"\"><coral-overlay foundation-autocomplete-suggestion=\"\" class=\"foundation-picker-buttonlist\" data-foundation-picker-buttonlist-src=\"/mnt/overlay/granite/ui/content/coral/foundation/form/pathfield/suggestion{.offset,limit}.html?_charset_=utf-8&amp;root=%2fcontent%2fexperience-fragments&amp;filter=hierarchyNotFile{&amp;query}\"></coral-overlay><coral-taglist foundation-autocomplete-value=\"\" name=\"./conditionalContentPath\"><coral-tag value=\"\"></coral-tag></coral-taglist><input class=\"foundation-field-related\" type=\"hidden\" name=\"./conditionalContentPath@Delete\"></foundation-autocomplete></div></div></div></section>";
    private static String CC_EXCLUDE_BODY_HTML_TAG_VALUE = "<section class=\"coral-TabPanel-pane coral-FixedColumn\"><div class=\"coral-FixedColumn-column\"><div class=\"coral-Form-fieldwrapper\"><label class=\"coral-Form-fieldlabel\">Variation Id</label><input class=\"coral-Form-field\" type=\"text\" name=\"./variationId\" value=\"\" data-foundation-validation=\"\" data-validation=\"\" is=\"coral-textfield\"><coral-icon class=\"coral-Form-fieldinfo\" icon=\"infoCircle\"></coral-icon><coral-tooltip target=\"_prev\" placement=\"left\"><coral-tooltip-content>Variation where the content need to be replaced</coral-tooltip-content></coral-tooltip></div></div></section>";
    private static String IS_CONDITIONAL_ATTRIBUTE = "isConditional";
    
    private static String IS_CONDITIONAL_CHECKBOX_HTML = "name=\"./isConditional\" value=\"true\" data-validation=\"\"";
    private static String VARIATION_ID_TEXTFIELD_HTML = "name=\"./variationId\" value=\"";
    
    private static String DIALOG_REQUEST_IDENTIFIER = "/_cq_dialog.html";
    @Override
    public final void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException
    {
        if (request instanceof SlingHttpServletRequest && response instanceof SlingHttpServletResponse)
        {
            final SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) request;

            if (slingRequest.getRequestURI().contains(DIALOG_REQUEST_IDENTIFIER))
            {
                final String ResourcePath = slingRequest.getRequestURI().split(DIALOG_REQUEST_IDENTIFIER)[1];

                final Resource contentResource = slingRequest.getResourceResolver().getResource(ResourcePath);
                final ValueMap properties = contentResource.getValueMap();

                final HttpServletResponse servletResponse = (HttpServletResponse) response;
                final StringHttpServletResponseWrapper responseWrapper = new StringHttpServletResponseWrapper(
                        servletResponse);
                String dialogHTML = getDialogHTML(slingRequest, responseWrapper);

                response.setContentType("text/html");

                response.getWriter().write(addTabtoDialog(dialogHTML, properties,
                        StringUtils.startsWith(ResourcePath, CC_EXCLUDE_PATHS[0])));
            } else
            {
                // Not a request related to dialog continue without any modification
                chain.doFilter(request, response);
            }
        } else
        {
            // Not a SlingHttpServletRequest/Response, so ignore.
            chain.doFilter(request, response);
        }
    }

    /**
     * Gets the dialog JSON.
     * 
     * @param slingRequest The sling request.
     * @param responseWrapper The response wrapper.
     * @return The dialog JSON.
     * @throws IOException Error while adapting to a page.
     * @throws ServletException Error while adapting to a page.
     */
    private String getDialogHTML(final SlingHttpServletRequest slingRequest,
            final StringHttpServletResponseWrapper responseWrapper) throws ServletException, IOException
    {
        slingRequest.getRequestDispatcher(slingRequest.getRequestURI()).include(slingRequest, responseWrapper);
        return responseWrapper.toString();
    }

    /**
     * Add a new tab item to dialog HTML.
     * 
     * @param dialogHTML The dialog HTML.
     * @param properties The dialog properties.
     * @param excludeFields The flag to exclude some fields.
     * @return The merged JSON.
     */
    private String addTabtoDialog(final String dialogHTML, final ValueMap properties, final boolean excludeFields)
    {
        if (StringUtils.isNotEmpty(dialogHTML))
        {
            String newDialogHTML = dialogHTML;
            String ccBodyHTMLTagValue = excludeFields
                    ? componentDialogFilterBean.getCcExcludeBodyTagValue()
                    : componentDialogFilterBean.getCcBodyHTMLTagValue();

            newDialogHTML = insertHTMLTag(newDialogHTML, componentDialogFilterBean.getCcHeadHTMLTagValue(), "</nav>",
                    1);
            newDialogHTML = insertHTMLTag(newDialogHTML, ccBodyHTMLTagValue, "</section>", 2);

            final String variationId = properties.get("variationId", "");

            newDialogHTML = newDialogHTML.replace(VARIATION_ID_TEXTFIELD_HTML + "\"",
                    VARIATION_ID_TEXTFIELD_HTML + variationId + "\"");

            // Check for exclude path
            if (!excludeFields)
            {
                final String conditionalContentPath = properties.get("conditionalContentPath", "");
                newDialogHTML = newDialogHTML.replace("<coral-tag value=\"\"></coral-tag>", "<coral-tag value=\""
                        + conditionalContentPath + "\">" + conditionalContentPath + "</coral-tag>");

                if (properties.get(IS_CONDITIONAL_ATTRIBUTE, "false").equals("true"))
                {
                    newDialogHTML = newDialogHTML.replace(IS_CONDITIONAL_CHECKBOX_HTML,
                            IS_CONDITIONAL_CHECKBOX_HTML + "checked=\"\"");
                }
            }

            LOG.trace("Final modified dialog output *** " + newDialogHTML);

            return newDialogHTML;
        }

        // In case of failure return original dialog, need to do more error handling
        return dialogHTML;
    }

    /**
     * Inserts tag in the correct location.
     * 
     * @param dialogHTML The dialog HTML.
     * @param insertedHTML The inserted HTML.
     * @param delimiterTag The delimiter tag.
     * @param order The order.
     * 
     * @return dialog output HTML.
     */
    private String insertHTMLTag(final String dialogHTML, final String insertedHTML, final String delimiterTag,
            int order)
    {
        StringBuilder output = new StringBuilder();
        String beforeOutput = StringUtils.substringBeforeLast(dialogHTML, delimiterTag);
        String afterOutput = StringUtils.substringAfterLast(dialogHTML, delimiterTag);

        // If tag is inserted inside the delimiterTag
        if (order == 1)
        {
            return output.append(beforeOutput).append(insertedHTML).append(delimiterTag).append(afterOutput).toString();
        } else
        {
            return output.append(beforeOutput).append(delimiterTag).append(insertedHTML).append(afterOutput).toString();
        }
    }

    /**
     * Activate.
     *
     * @param properties The OSGi configuration properties.
     */
    @Activate
    protected final void activate(final Map<String, Object> properties)
    {
        // Initialize and keep the configuration in memory on start of component
        setConfigProperties(properties);
        LOG.debug("Component dialog filter is enabled ***");
    }

    /**
     * Deactivate method.
     */
    @Deactivate
    public void deactivate()
    {
        this.componentDialogFilterBean = null;
        LOG.debug("Component dialog filter is disabled ***");
    }

    /**
     * Setting service configuration.
     * 
     * @param properties
     */
    private void setConfigProperties(Map<String, Object> properties)
    {
        this.componentDialogFilterBean = new ComponentDialogFilterBean();
        this.componentDialogFilterBean.setEnabled(PropertiesUtil.toBoolean(properties.get("enabled"), true));
        this.componentDialogFilterBean
                .setExcludePaths(PropertiesUtil.toStringArray(properties.get("excludePaths"), CC_EXCLUDE_PATHS));
        this.componentDialogFilterBean.setCcHeadHTMLTagValue(
                PropertiesUtil.toString(properties.get("ccHeadTagValue"), CC_HEAD_HTML_TAG_VALUE));
        this.componentDialogFilterBean.setCcBodyHTMLTagValue(
                PropertiesUtil.toString(properties.get("ccBodyTagValue"), CC_BODY_HTML_TAG_VALUE));
        this.componentDialogFilterBean.setCcExcludeBodyTagValue(
                PropertiesUtil.toString(properties.get("ccExcludeBodyTagValue"), CC_EXCLUDE_BODY_HTML_TAG_VALUE));

        LOG.trace("ComponentDialogFilter: {}", this.componentDialogFilterBean);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException
    {
        LOG.debug("Component dialog filter is initialized ***");
    }

    @Override
    public void destroy()
    {
        LOG.debug("Component dialog filter is Destroyed *** ");
    }

    /**
     * Response wrapper class.
     */
    class StringHttpServletResponseWrapper extends HttpServletResponseWrapper
    {
        private final transient StringWriter stringWriter = new StringWriter();

        /**
         * Constructor.
         * 
         * @param response The response.
         */
        public StringHttpServletResponseWrapper(final HttpServletResponse response)
        {
            super(response);
        }

        /**
         * Gets the writer.
         * 
         * @return The print writer.
         * @throws IOException Error while adapting to a page.
         */
        @Override
        public final PrintWriter getWriter() throws IOException
        {
            return new PrintWriter(stringWriter);
        }

        /**
         * Gets the output stream.
         * 
         * @return The output stream.
         * @throws IOException Error while adapting to a page.
         */
        @Override
        public final ServletOutputStream getOutputStream() throws IOException
        {
            return super.getOutputStream();
        }

        /**
         * Gets the string representation of response.
         * 
         * @return The response string.
         */
        public final String toString()
        {
            return stringWriter.toString();
        }
    }

}
