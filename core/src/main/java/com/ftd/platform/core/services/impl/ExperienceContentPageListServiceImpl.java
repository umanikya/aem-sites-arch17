package com.ftd.platform.core.services.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.ftd.platform.core.services.ExperienceContentPageListService;
import com.ftd.platform.core.utils.ContentPageListUtil;

/**
 * This is a Service implementation class of the service ExperienceContentPageListService.
 * 
 * @author abhranja1
 * 
 */
@Component(
        name = "ExperienceContentPageListServiceImplementaion",
        service = ExperienceContentPageListService.class,
        immediate = true)
public class ExperienceContentPageListServiceImpl implements ExperienceContentPageListService
{
    @Reference
    QueryBuilder queryBuilder;

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    @Reference
    private SlingSettingsService slingSettingsService;

    private static final Logger logger = LoggerFactory.getLogger(ExperienceContentPageListService.class);

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.ftd.platform.core.services.ExperienceContentPageListService#getExperienceContentPageList(java.lang.String)
     */
    public String getExperienceContentPageList(String siteId)
    {
        logger.info("In getExperienceContentPageList method");
        Map<String, Object> contentPageListMap = null;
        SearchResult queryResult = null;
        try
        {
            Map<String, String> queryMap = getQuerymap(siteId);
            queryResult = ContentPageListUtil.getQueryResult(resourceResolverFactory, queryBuilder, queryMap);
            logger.info("Total number of pages::{}", queryResult.getHits().size());
            contentPageListMap = ContentPageListUtil.getContentPageListFromQueryResult("fragment", queryResult,
                    slingSettingsService);
        } catch (LoginException e)
        {
            logger.error("login Exception occurred :: {}", e);
            return "{}";
        } catch (Exception e)
        {
            logger.error("Unknown Exception occurred :: {}", e);
            return "{}";
        }
        return ContentPageListUtil.getJsonFromContentPageListObject(contentPageListMap, slingSettingsService);
    }

    /**
     * It constructs and returns the query Map.
     * 
     * @param siteId
     * @return
     */
    public Map<String, String> getQuerymap(String siteId)
    {
        logger.debug("In the method getQuerymap ");
        Map<String, String> queryMap = new HashMap<>();
        queryMap.put("type", "cq:PageContent");
        if (siteId.length() == 0)
        {
            queryMap.put("path", "/content/experience-fragments");
        } else
        {
            queryMap.put("path", "/content/experience-fragments/" + siteId);
        }
        queryMap.put("group.p.or", "true");
        queryMap.put("group.1_property", "cq:template");
        queryMap.put("group.1_property.1_value",
                "/conf/web/platform/settings/wcm/templates/experience-fragment-template");
        queryMap.put("p.limit", "-1");

        return queryMap;
    }

}
