package com.ftd.platform.core.models;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.inject.Inject;

import com.ftd.platform.core.utils.Utils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ftd.platform.core.constants.Constants;

/**
 * Sling Model for Modal
 * 
 * @author prabasva
 *
 */
@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = Constants.MODAL_RESOURCE_TYPE)
@Exporter(
        name = Constants.JACKSON_KEY,
        extensions = Constants.JSON_KEY)
public class ModalModel extends BaseModel
{
    @Inject
    private String modalTitle;

    @Inject
    private String modalId;
    
    @Inject
    private String modalContent;
    
    private static final String COMPONENT_TITLE = "Modal";

    /**
     * This method used to set modal map
     * 
     * @return map
     */
    @JsonAnyGetter
    public Map<String, Object> getModalFields()
    {
        Map<String, Object> attributes = new LinkedHashMap<>();
        Map<String, Object> selectionMap = new LinkedHashMap<>();
        selectionMap.put(Constants.COMPONENT_KEY, getComponent());
        selectionMap.put(Constants.VARIATION_ID, variationId);
        attributes.put(Constants.MODAL_TITLE_KEY, modalTitle);
        attributes.put(Constants.MODAL_ID_KEY, this.modalId);
        attributes.put(Constants.MODAL_CONTENT_KEY, Utils.removeParaTagsFromRTE(modalContent));
        
        selectionMap.put(Constants.ATTRIBUTES_KEY, attributes);

        return selectionMap;
    }

    /**
     * This method returns the component identifier.
     * 
     * @return string for component identifier
     */
    @Override
    @JsonIgnore
    public String getComponent()
    {
        return Constants.MODAL_KEY;
    }
    /**
     * This is to get component title
     * 
     * @return
     */
    @Override
    public String getComponentTitle()
    {
        return COMPONENT_TITLE;
    }
}
