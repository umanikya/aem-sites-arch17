package com.ftd.platform.core.beans;

import javax.inject.Inject;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.ftd.platform.core.constants.Constants;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * LinkUrlbean Class is used for Multifield resource from AEM dialog.
 *
 *
 */
@Model(
        adaptables = Resource.class)

public class LinksHelperModel
{

    @Inject
    private String url;

    @Inject
    @JsonProperty("DisplayName")
    private String label;

    @Inject
    @Default(values = "")
    @JsonProperty("newWindow")
    private String target;

    /**
     * @return Getter for URL
     */
    public String getUrl()
    {
        return url;
    }

    /**
     * @param url for url
     */
    public void setUrl(String url)
    {
        this.url = url;
    }

    /**
     * Get method for Label
     * 
     * @return
     */
    public String getLabel()
    {
        return label;
    }

    /**
     * Set method for label
     * 
     * @param label
     */
    public void setLabel(String label)
    {
        this.label = label;
    }

    /**
     * Gets Target for a link (Same or New window)
     *
     * @return target _blank or _same
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getTarget()
    {
        if (StringUtils.isNotBlank(target) && target.equals(Constants.TARGET_KEY_VALUE))
        {
            return target;
        } else
        {
            return null;
        }
    }

}
