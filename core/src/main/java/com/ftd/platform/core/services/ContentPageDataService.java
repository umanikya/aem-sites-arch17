package com.ftd.platform.core.services;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;

import com.ftd.platform.core.beans.ReplicationDataBean;

public interface ContentPageDataService
{
    public String getStartDate(ReplicationDataBean replicationDataBean);

    public String getEndDate(ReplicationDataBean replicationDataBean);

    public ReplicationDataBean getReplicationDataFromResource(ValueMap pageProperties);
    
    public Object getModelFromResource(Resource componentResource);
    
    public Object getModelJsonFromResource(Resource componentResource);

}
