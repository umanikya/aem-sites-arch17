package com.ftd.platform.core.models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ftd.platform.core.beans.PromoCardBean;
import com.ftd.platform.core.constants.Constants;
import com.ftd.platform.core.utils.Utils;

/**
 * Sling Model for Product Collection Component.
 * 
 */
@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = Constants.PRODUCT_COLLECTION_RESOURCE_TYPE)
@Exporter(
        name = Constants.JACKSON_KEY,
        extensions = Constants.JSON_KEY)
public class ProductCollection extends BaseModel
{
    @Inject
    String heading;

    @Inject
    String subHeading;

    @Inject
    String analyticsTrackingId;

    @Inject
    private Resource prodId;

    @Inject
    private Resource promocard;

    @Inject
    @Default(
            values = Constants.PRODUCT_COLLECTION_TITLE)
    private String component;
    private static final String COMPONENT_TITLE = "Product Collection";
    /**
     *
     * @return - it returns the component identifier.
     */
    @Override
    public String getComponent()
    {
        return component;
    }

    /**
     * @return - It returns the Attributes Map containing the product list component data.
     */
    public Map<String, Object> getAttributes()
    {
        Map<String, Object> attributes = new LinkedHashMap<>();

        if (StringUtils.isNotBlank(this.heading))
        {
            attributes.put("title", this.heading);
        }

        if (StringUtils.isNotBlank(this.subHeading))
        {
            attributes.put("description", this.subHeading);
        }

        if (StringUtils.isNotBlank(this.analyticsTrackingId))
        {
            attributes.put("analyticsTrackingId", this.analyticsTrackingId);
        }
        List<String> prodidlist = Utils.getProductListFromResource(prodId, false);
        Utils.setProductIdOrCategoryIdInMap("prod", null, prodidlist, attributes);

        attributes.put(Constants.PROMO_CARD_LISTING, getChildResourceList(promocard));
        return attributes;
    }

    /**
     * This method is used to generate list of maps by adapting resource
     * 
     * @param - resource
     * @return - list of maps
     */
    @JsonIgnore
    public List<Object> getChildResourceList(Resource resource)
    {
        List<Object> resourceList = new ArrayList<>();
        if (Objects.nonNull(resource))
        {
            Iterator<Resource> iterator = resource.listChildren();
            while (iterator.hasNext())
            {
                Resource childResource = iterator.next();
                if (Objects.nonNull(childResource))
                {
                    if (childResource.getPath().contains(Constants.PROMOCARD))
                    {
                        resourceList.add(childResource.adaptTo(PromoCardBean.class));
                    }
                }
            }
        }
        return resourceList;
    }

    /**
     * This is to get component title
     * 
     * @return
     */
    @Override
    public String getComponentTitle()
    {
        return COMPONENT_TITLE;
    }
}
