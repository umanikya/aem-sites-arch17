package com.ftd.platform.core.models.page;

import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;

import com.ftd.platform.core.constants.Constants;

/**
 * Sling Model for PLP Layout Page Component.
 * 
 * @author manpati
 *
 */
@Model(
        adaptables = SlingHttpServletRequest.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = Constants.PLP_LAYOUT_PAGE_MODEL_RESOURCE_TYPE)
@Exporter(
        name = Constants.JACKSON_KEY,
        extensions = Constants.JSON_KEY)
public class PLPLayoutPageModel extends PageBaseModel
{
    public static final String PLP_LAYOUT_TITLE = "plp-layout";
    
    @Inject
    private ValueMap properties;

    /**
     * It returns the plp layout title in the page level json.
     * 
     * @return
     */
    @Override
    public String getLayout()
    {
        return properties.get("layout", PLP_LAYOUT_TITLE);
    }

}
