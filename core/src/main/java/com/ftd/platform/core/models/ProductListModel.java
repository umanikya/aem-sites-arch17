package com.ftd.platform.core.models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ftd.platform.core.beans.CategoryBean;
import com.ftd.platform.core.beans.PromoCardBean;
import com.ftd.platform.core.constants.Constants;

/**
 * Sling Model for Product List Component.
 * 
 * @author prabasva
 * 
 */
@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = Constants.PRODUCT_LIST_RESOURCE_TYPE)
@Exporter(
        name = Constants.JACKSON_KEY,
        extensions = Constants.JSON_KEY)
public class ProductListModel extends BaseModel
{

    @Inject
    private int pagination;

    @Inject
    private boolean displayFilters;

    @Inject
    private boolean displaySortOptions;

    @Inject
    private boolean displayGiftFinder;

    @Inject
    @Default(
            values = Constants.FALSE_KEY)
    private String specifyCategories;

    @Inject
    private Resource promocard;
    
    @Inject
    private String pageId;

    @Inject
    @Default(
            values = Constants.PRODUCT_LIST_TITLE)
    private String component;
    private static final String COMPONENT_TITLE = "Product List";
    /**
     *
     * @return - it returns the component identifier.
     */
    @Override
    public String getComponent()
    {
        return component;
    }

    /**
     * @return - It returns the Attributes Map containing the product list component data.
     */
    public Map<String, Object> getAttributes()
    {
        Map<String, Object> productListPropertiesMap = new LinkedHashMap<>();

        if (null != pageId)
        {
            productListPropertiesMap.put("pageId", pageId);
        }
        productListPropertiesMap.put(Constants.PAGINATION_KEY, pagination);
        productListPropertiesMap.put(Constants.DISPLAY_FILTERS_KEY, displayFilters);
        productListPropertiesMap.put(Constants.DISPLAY_SORT_OPTIONS_KEY, displaySortOptions);
        productListPropertiesMap.put(Constants.DISPLAY_GIFT_FINDER_KEY, displayGiftFinder);
        productListPropertiesMap.put(Constants.PROMO_CARD_LISTING, getChildResourceList(promocard));

        return productListPropertiesMap;
    }

    /**
     * This method is used to generate list of maps by adapting resource
     * 
     * @param - resource
     * @return - list of maps
     */
    @JsonIgnore
    public List<Object> getChildResourceList(Resource resource)
    {
        List<Object> resourceList = new ArrayList<>();
        if (Objects.nonNull(resource))
        {
            Iterator<Resource> iterator = resource.listChildren();
            while (iterator.hasNext())
            {
                Resource childResource = iterator.next();
                if (Objects.nonNull(childResource))
                {

                    if (childResource.getPath().contains(Constants.CATEGORIES_KEY))
                    {
                        resourceList.add(childResource.adaptTo(CategoryBean.class));
                    }

                    if (childResource.getPath().contains(Constants.PROMOCARD))
                    {
                        resourceList.add(childResource.adaptTo(PromoCardBean.class));
                    }
                }
            }
        }
        return resourceList;
    }

    /**
     * This is to get component title
     * 
     * @return
     */
    @Override
    public String getComponentTitle()
    {
        return COMPONENT_TITLE;
    }
}
