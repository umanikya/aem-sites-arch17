package com.ftd.platform.core.models;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.inject.Inject;

import com.ftd.platform.core.utils.Utils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.ExporterOption;
import org.apache.sling.models.annotations.Model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.ftd.platform.core.constants.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Sling Model for Heading And Description
 * 
 * @author prabasva
 *
 */
@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = Constants.HEADING_DESCRIPTION_RESOURCE_TYPE)
@Exporter(
        name = Constants.JACKSON_KEY,
        extensions = Constants.JSON_KEY,
        options = { @ExporterOption(
                name = Constants.SLING_MODEL_DEFAULT_ORDER,
                value = Constants.FALSE_KEY) })
public class HeadingDescriptionModel extends BaseModel
{

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Inject
    private String title;

    @Inject
    private String description;

    @Inject
    private String position;

    @Inject
    @Default(
            values = Constants.HEADER_AND_DESCRIPTION_TITLE)
    private String component;

    private static final String COMPONENT_TITLE = "Heading And Description";
    private static final String TITLE_KEY = "title";
    private static final String DESCRIPTION_KEY = "description";
    private static final String POSITION_KEY = "position";


    /**
     * This method returns the component identifier.
     * 
     * @return
     */
    @Override
    public String getComponent()
    {
        return component;
    }

    /**
     * This method constructs the attributes map object for header and description
     * 
     * @return
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Map<String, Object> getAttributes()
    {

        Map<String, Object> componentAttributes = new LinkedHashMap<>();
        componentAttributes.put(TITLE_KEY, title);
        componentAttributes.put(DESCRIPTION_KEY, Utils.removeParaTagsFromRTE(description));
        componentAttributes.put(POSITION_KEY, position);
        return componentAttributes;
    }
    /**
     * This is to get component title
     * 
     * @return
     */
    @Override
    public String getComponentTitle()
    {
        return COMPONENT_TITLE;
    }
}
