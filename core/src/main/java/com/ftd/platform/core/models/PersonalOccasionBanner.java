package com.ftd.platform.core.models;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.ExporterOption;
import org.apache.sling.models.annotations.Model;

import com.ftd.platform.core.constants.Constants;

/**
 * Sling Model for Personal Occasion Banner component
 * 
 */
@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = "web/platform/components/content/personaloccasionbanner")
@Exporter(
        name = "jackson",
        extensions = "json",
        options = { @ExporterOption(
                name = "SerializationFeature.WRITE_DATES_AS_TIMESTAMPS",
                value = "true") })
public class PersonalOccasionBanner extends BaseModel
{
    private static final String COMPONENT_TITLE = "Personal Occasion Banner";

    @Inject
    private String namePreText;

    @Inject
    private String defaultName;

    @Inject
    private String summaryHeading;

    @Inject
    private String imageLink;

    @Inject
    private String imgAltTxt;

    @Inject
    private String defaultPersonalImage;

    @Inject
    private String defaultPersonalImageAlt;
    
    @Inject
    private String serviceEndpointUrl;

    /**
     * This method returns the component identifier.
     * 
     * @return
     */
    @Override
    public String getComponent()
    {
        return "personal-occasion-banner";
    }

    /**
     * This method constructs the attributes map object and returns the same.
     * 
     * @return
     */
    public Map<String, Object> getAttributes()
    {
        Map<String, Object> attributes = new LinkedHashMap<>();

        attributes.put("namePreText", this.namePreText);
        attributes.put("defaultName", this.defaultName);
        attributes.put("summaryHeading", this.summaryHeading);

        if (StringUtils.isNotBlank(this.serviceEndpointUrl))
        {
            attributes.put("serviceEndpointUrl", this.serviceEndpointUrl);
        } else
        {
            attributes.put("serviceEndpointUrl", "/legacy");
        }

        attributes.put(Constants.IMAGE_KEY, getImageMap(this.imageLink, this.imgAltTxt));
        attributes.put("defaultPersonalImage", getImageMap(this.defaultPersonalImage, this.defaultPersonalImageAlt));

        return attributes;
    }

    /**
     * This is to get component title
     * 
     * @return
     */
    @Override
    public String getComponentTitle()
    {
        return COMPONENT_TITLE;
    }

}
