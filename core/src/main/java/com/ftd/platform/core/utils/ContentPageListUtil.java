package com.ftd.platform.core.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.settings.SlingSettingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.ftd.platform.core.constants.Constants;
import com.google.gson.Gson;

/**
 * Utility class for the Content Page List Services.
 * 
 * @author sunkatep
 *
 */
public class ContentPageListUtil
{

    private static final Logger logger = LoggerFactory.getLogger(ContentPageListUtil.class);

    /**
     * It derives the activate time.
     * 
     * @param lastModified
     * @param lastReplicated
     * @param publishDate
     * @param unPublishDate
     * @param lastReplicationAction
     * @return
     */
    public static Date getActivateTime(Date lastModified, Date lastReplicated, Date publishDate, Date unPublishDate,
            String lastReplicationAction)
    {
        logger.debug("In getActivateTime Method");
        Date activateDate = null;

        logger.debug("lastReplicatedDate: {}", lastReplicated);
        logger.debug("publishDate: {}", publishDate);
        logger.debug("lastModifiedDate: {}", lastModified);
        logger.debug("lastReplicationAction: {}", lastReplicationAction);

        if (lastReplicationAction != null)
        {
            if (lastReplicationAction.equals("Activate"))
            {
                if (publishDate == null)
                {
                    activateDate = lastReplicated;
                } else
                {
                    if (publishDate.after(lastReplicated))
                    {
                        activateDate = publishDate;
                    } else
                    {
                        activateDate = lastReplicated;
                    }
                }
            } else
            {
                if (publishDate != null)
                {
                    activateDate = publishDate;
                } else
                {
                    activateDate = lastModified;
                }
            }

        } else
        {
            if (publishDate != null)
            {
                activateDate = publishDate;
            }
        }
        logger.debug("activateDate: {} ", activateDate);
        return activateDate;

    }

    /**
     * It derives the deactivate time.
     * 
     * @param lastReplicated
     * @param publishDate
     * @param unPublishDate
     * @param lastReplicationAction
     * @return
     */
    public static Date getDeActivateTime(Date lastReplicated, Date publishDate, Date unPublishDate,
            String lastReplicationAction)
    {
        logger.debug("In getDeActivateTime Method");
        Date deActivateDate = null;

        logger.debug("lastReplicatedDate: {}", lastReplicated);
        logger.debug("publishDate: {}", publishDate);
        logger.debug("lastReplicationAction: {}", lastReplicationAction);

        if (lastReplicationAction != null)
        {
            if (lastReplicationAction.equals("Deactivate"))
            {
                if (unPublishDate == null)
                {
                    deActivateDate = lastReplicated;
                } else
                {
                    if (unPublishDate.after(lastReplicated))
                    {
                        deActivateDate = unPublishDate;
                    } else
                    {
                        deActivateDate = lastReplicated;
                    }
                }
            } else
            {
                if (unPublishDate != null)
                {
                    deActivateDate = unPublishDate;
                }
            }
        } else
        {
            if (unPublishDate != null)
            {
                deActivateDate = unPublishDate;
            }
        }
        logger.debug("deActivateDate: {} ", deActivateDate);
        return deActivateDate;

    }

    /**
     * It returns the ResourceResolver from the factory object.
     * 
     * @param resourceResolverFactory
     * @return
     * @throws LoginException
     */
    public static SearchResult getQueryResult(ResourceResolverFactory resourceResolverFactory,
            QueryBuilder queryBuilder, Map<String, String> querymap) throws LoginException

    {
        Map<String, Object> userMap = new HashMap<>();
        userMap.put(ResourceResolverFactory.SUBSERVICE, Constants.SYSTEM_USER);
        ResourceResolver resourceResolver = resourceResolverFactory.getServiceResourceResolver(userMap);
        Session session = resourceResolver.adaptTo(Session.class);
        logger.debug("Session from ResoourceResolver", session);
        Query query = queryBuilder.createQuery(PredicateGroup.create(querymap), session);
        return query.getResult();
    }

    /**
     * It compares the 2 prams and returns the latest date.
     * 
     * @param maxModifiedTime
     * @param lastModifiedDate
     * @return
     */
    public static Date getMaxModifiedTime(Date maxModifiedTime, Date lastModifiedDate)
    {
        if (maxModifiedTime == null)
            maxModifiedTime = lastModifiedDate;
        else if (maxModifiedTime.before(lastModifiedDate))
            maxModifiedTime = lastModifiedDate;
        return maxModifiedTime;
    }

    /**
     * @param queryResult
     * @return
     */
    public static Map<String, Object> getContentPageListFromQueryResult(String pageType, SearchResult queryResult,
            SlingSettingsService slingSettingsService)
    {
        logger.debug("In the method getContentPageListFromQueryResult ");
        Map<String, Object> contentPageListMap = new HashMap<>();
        List<Object> contentPageList = new ArrayList<>();
        ResourceResolver resourceResolver = null;
        Date maxModifiedTime = null;

        for (final Hit hit : queryResult.getHits())
        {
            Map<String, Object> contentPageDataMap = new LinkedHashMap<>();
            try
            {
                resourceResolver = hit.getResource().getResourceResolver();
                String jcrContentPath = hit.getPath();
                ValueMap properties = resourceResolver.getResource(jcrContentPath).getValueMap();
                logger.debug("Path of the page::{}", jcrContentPath);
                contentPageDataMap.put(pageType, getFormattedPagePath(hit.getPath()));
                setContentPageDataMap(properties, contentPageDataMap, slingSettingsService);
                logger.debug("lastmodified :: {} , maxmodified :: {} ", properties.get("cq:lastModified", Date.class),
                        maxModifiedTime);
                maxModifiedTime = getMaxModifiedTime(maxModifiedTime, properties.get("cq:lastModified", Date.class));

                if ("fragment".equals(pageType))
                {
                    String[] tags = { "" };
                    tags = properties.get("cq:tags", String[].class);
                    contentPageDataMap.put("tags", tags);

                    // Get variation id in case of fragment
                    Resource rootResource = hit.getResource().getChild("root");

                    if (null != rootResource)
                    {
                        Iterator<Resource> iterator = rootResource.listChildren();
                        while (iterator.hasNext())
                        {
                            ValueMap xFComponentProperties = iterator.next().getValueMap();

                            if (xFComponentProperties.containsKey(Constants.VARIATION_ID))
                            {
                                contentPageDataMap.put(Constants.VARIATION_ID,
                                        xFComponentProperties.get(Constants.VARIATION_ID, String.class));
                            }

                            break;
                        }
                    }
                }
                contentPageList.add(contentPageDataMap);

            } catch (RepositoryException e)
            {
                logger.debug("RepositoryException during the hits on JCR repo :: {}", e);
            }
        }
        contentPageListMap.put("contentPageList", contentPageList);
        contentPageListMap.put("maxModifiedTime", maxModifiedTime);

        return contentPageListMap;
    }

    /**
     * @param pagePath
     * @return
     */
    public static String getFormattedPagePath(String pagePath)
    {
        return StringUtils.removeEnd(pagePath, "/jcr:content");
    }

    /**
     * @param pagePath
     * @return
     */
    private static String getFormattedTemplatePath(String templatePath)
    {
        String formattedTemplatePath = StringUtils.removeStart(templatePath, "/apps/web/platform/templates/");
        formattedTemplatePath = StringUtils.removeStart(formattedTemplatePath,
                "/conf/web/platform/settings/wcm/templates/");
        return formattedTemplatePath;
    }

    /**
     * @param contentPageList
     * @return
     */
    public static String getJsonFromContentPageListObject(Map<String, Object> contentPageListMap,
            SlingSettingsService slingSettingsService)
    {
        Map<String, Object> rootMap = new LinkedHashMap<>();
        logger.debug("Max Modified Time :: {}", contentPageListMap.get("maxModifiedTime"));
        rootMap.put("environment", Utils.getEnvironment(slingSettingsService));
        if (contentPageListMap.get("maxModifiedTime") != null)
        {
            rootMap.put("last-update", Utils.getFormattedDate((Date) contentPageListMap.get("maxModifiedTime")));
        }
        rootMap.put("data", contentPageListMap.get("contentPageList"));

        return new Gson().toJson(rootMap);
    }

    /**
     * It fetches the data from the properties object and sets the content page data in the Map object
     * contentPageDataMap.
     * 
     * @param properties
     * @param contentPageDataMap
     * @param slingSettingsService
     * @throws RepositoryException
     */
    public static void setContentPageDataMap(ValueMap properties, Map<String, Object> contentPageDataMap,
            SlingSettingsService slingSettingsService) throws RepositoryException
    {
        Date lastModified = properties.get("cq:lastModified", Date.class);

        if (Utils.getEnvironment(slingSettingsService).contains("author"))
        {
            Date publishDate = properties.get("cq:publishDate", Date.class);
            Date unPublishDate = properties.get("cq:unPublishDate", Date.class);
            Date lastReplicated = properties.get("cq:lastReplicated", Date.class);
            String lastReplicationAction = properties.get("cq:lastReplicationAction", String.class);

            Date activateTime = getActivateTime(lastModified, lastReplicated, publishDate, unPublishDate,
                    lastReplicationAction);
            Date deActivateTime = getDeActivateTime(lastReplicated, publishDate, unPublishDate, lastReplicationAction);
            logger.debug("activateTime: {} , deActivateTime: {}", activateTime, deActivateTime);

            if (activateTime != null)
                contentPageDataMap.put("start", Utils.getFormattedDate(activateTime));

            if (deActivateTime != null)
                contentPageDataMap.put("end", Utils.getFormattedDate(deActivateTime));
        } else
        { // For publish env
            if (lastModified != null)
                contentPageDataMap.put("start", Utils.getFormattedDate(lastModified));
        }
        
        
        // Handle plp cases
        if (StringUtils.equals("plp-multi-category", properties.get("layout", String.class)))
        {
            contentPageDataMap.put("template", "plp-multi-category");
        } else
        {
            contentPageDataMap.put("template", getFormattedTemplatePath(properties.get("cq:template", String.class)));
        }
    }

}
