package com.ftd.platform.core.servlets;

import java.io.IOException;
import java.util.Objects;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ftd.platform.core.services.ContentPageListService;

/**
 * @author abhranja1
 * 
 */
@Component(
        service = { Servlet.class },
        property = { "service.description=Get Content Page List Servlet", "sling.servlet.paths=/bin/contentPageList",
                "sling.servlet.methods = GET", "sling.servlet.extensions=json",
                "sling.auth.requirements = -{/bin/contentPageList}" })
public class GetContentPageListServlet extends SlingSafeMethodsServlet
{

    private static final Logger logger = LoggerFactory.getLogger(GetContentPageListServlet.class);
    private static final long serialVersionUID = 9179895523313781530L;

    @Reference
    ContentPageListService contentPageListService;

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
     * org.apache.sling.api.SlingHttpServletResponse) doGet method will get the JSON Response from Service
     */
    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServletException, IOException
    {
        logger.debug("GetContentPageServlet : In doGetmethod ()");
        
        String siteId = Objects.isNull(request.getParameter("siteId")) ? "" : request.getParameter("siteId");
        logger.debug("Site Id: {} ", siteId);
        
        response.setContentType("application/json");
        response.getWriter().write(contentPageListService.getContentPageList(siteId));
    }

}
