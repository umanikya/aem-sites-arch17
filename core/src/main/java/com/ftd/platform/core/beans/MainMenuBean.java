package com.ftd.platform.core.beans;

import java.util.List;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Bean Class for holding Main menu details.
 * 
 * @author sunkatep
 * 
 */
@Model(
        adaptables = Resource.class)
public class MainMenuBean implements Comparable<MainMenuBean>
{

    @Inject
    @JsonProperty("DisplayName")
    private String label;

    @Inject
    @JsonProperty("links")
    public List<NavigationColumnBean> topmenulevel;

    public Long menuPosition;

    /**
     * Constructor Method
     * 
     * @param label
     * @param topmenulevel
     */
    public MainMenuBean(String label, String menuPosition, List<NavigationColumnBean> topmenulevel)
    {
        super();
        this.label = label;
        try
        {
            this.menuPosition = Long.parseLong(menuPosition);
        } catch (NumberFormatException e)
        {
            this.menuPosition = 0L;
        }
        this.topmenulevel = topmenulevel;
    }
    /**
     * This method gets the label String.
     * 
     * @return
     */
    public String getLabel()
    {
        return label;
    }

    /**
     * Setter method for Label
     * 
     * @param label
     */

    public void setLabel(String label)
    {
        this.label = label;
    }

    /**
     * Getter method for menu list
     * 
     * @return
     */
    public List<NavigationColumnBean> getTopmenulevel()
    {
        return topmenulevel;
    }

    /**
     * Setter method for menu list
     * 
     * @param topmenulevel
     */
    public void setTopmenulevel(List<NavigationColumnBean> topmenulevel)
    {
        this.topmenulevel = topmenulevel;
    }

    /**
     * Getter method for menu position
     * 
     * @return
     */
    @JsonIgnore
    public Long getMenuPosition()
    {
        return menuPosition;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(MainMenuBean compareObj)
    {
        if (this.menuPosition == compareObj.menuPosition)
            return 0;
        else
            return this.menuPosition > compareObj.menuPosition ? 1 : -1;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return "MainMenuBean [label=" + label + ", topmenulevel=" + topmenulevel + ", menuPosition=" + menuPosition
                + ", getLabel()=" + getLabel() + ", getTopmenulevel()=" + getTopmenulevel() + ", getMenuPosition()="
                + getMenuPosition() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
                + super.toString() + "]";
    }

}
