package com.ftd.platform.core.services.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.ftd.platform.core.services.ContentPageListService;
import com.ftd.platform.core.utils.ContentPageListUtil;

/**
 * This is a Service implementation class of the service ContentPageListService.
 * 
 */
@Component(
        service = ContentPageListService.class,
        immediate = true)
public class ContentPageListServiceImpl implements ContentPageListService
{
    @Reference
    QueryBuilder queryBuilder;

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    @Reference
    private SlingSettingsService slingSettingsService;

    // In memory initial query
    private Map<String, String> initialQueryMap;

    private static final Logger logger = LoggerFactory.getLogger(ContentPageListService.class);

    // Default values of OSGI service configuration
    private static String[] ALLOWED_TEMPLATE_PATHS = { "/apps/web/platform/templates/one-column-layout",
            "/apps/web/platform/templates/global-page", "/apps/web/platform/templates/pdp-layout",
            "/apps/web/platform/templates/plp-layout" };

    /*
     * (non-Javadoc)
     * 
     * @see com.ftd.platform.core.services.ContentPageListService#getContentPageList(java.lang.String)
     */
    public String getContentPageList(String siteId)
    {
        Map<String, Object> contentPageListMap = null;
        SearchResult queryResult = null;
        try
        {
            Map<String, String> queryMap = getQuerymap(initialQueryMap, siteId);
            queryResult = ContentPageListUtil.getQueryResult(resourceResolverFactory, queryBuilder, queryMap);
            logger.info("Total number of pages::{}", queryResult.getHits().size());
            contentPageListMap = ContentPageListUtil.getContentPageListFromQueryResult("page", queryResult,
                    slingSettingsService);
        } catch (LoginException e)
        {
            logger.error("login Exception occurred :: {}", e);
            return "{}";
        } catch (Exception e)
        {
            logger.error("Unknown Exception occurred :: {}", e);
            return "{}";
        }
        return ContentPageListUtil.getJsonFromContentPageListObject(contentPageListMap, slingSettingsService);
    }

    /**
     * It constructs and returns the query Map.
     * 
     * @param groupPropertTemplatePath
     * 
     * @param siteId
     * @return
     */
    public Map<String, String> getQuerymap(Map<String, String> intialQueryMap, String siteId)
    {
        Map<String, String> queryMap = intialQueryMap;
        if (siteId.length() == 0)
        {
            queryMap.put("path", "/content");
        } else
        {
            queryMap.put("path", "/content/" + siteId);
        }

        return queryMap;
    }

    /**
     * Activate.
     *
     * @param properties The OSGi configuration properties.
     */
    @Activate
    protected final void activate(final Map<String, Object> properties)
    {
        String[] allowedTemplatePaths = PropertiesUtil.toStringArray(properties.get("allowedTemplatePaths"),
                ALLOWED_TEMPLATE_PATHS);
        
        initialQueryMap = new HashMap<>();
        initialQueryMap.put("type", "cq:PageContent");
        initialQueryMap.put("group.p.or", "true");
        initialQueryMap.put("group.1_property", "cq:template");
        for (int i = 0; i < allowedTemplatePaths.length; i++)
        {
            initialQueryMap.put("group.1_property." + (i + 1) + "_value", allowedTemplatePaths[i]);
        }
        initialQueryMap.put("p.limit", "-1");
    }
    
    /**
     * Deactivate method.
     */
    @Deactivate
    public void deactivate()
    {
        this.initialQueryMap = null;
    }
}
