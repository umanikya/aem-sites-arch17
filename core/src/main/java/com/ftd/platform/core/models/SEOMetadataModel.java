package com.ftd.platform.core.models;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.ExporterOption;
import org.apache.sling.models.annotations.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ftd.platform.core.utils.Utils;

/**
 * Sling Model for SEO metadata component
 * 
 */
@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = "web/platform/components/content/seoMetadata")
@Exporter(
        name = "jackson",
        extensions = "json",
        options = { @ExporterOption(
                name = "SerializationFeature.WRITE_DATES_AS_TIMESTAMPS",
                value = "true") })
public class SEOMetadataModel extends BaseModel
{
    private static final String COMPONENT_TITLE = "seometadata-head";
    
    @Inject
    private String pageTitle;
    
    @Inject
    private String description;
    
    @Inject
    @JsonIgnore
    private Resource seoList;
    
    /**
     * This method constructs the attributes map object and returns the same.
     * 
     * @return
     */
    public Map<String, Object> getAttributes()
    {
        Map<String, Object> attributes = new LinkedHashMap<>();
        
        attributes.put("pageTitle", this.pageTitle);
        attributes.put("description", this.description);
        attributes.put("metaTags", Utils.getSEOListFromResource(seoList));
        
        return attributes;
    }
    
    /**
     * This method returns the component identifier.
     * 
     * @return
     */
    @Override
    public String getComponent()
    {
        return COMPONENT_TITLE;
    }
}
