package com.ftd.platform.core.models;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.ExporterOption;
import org.apache.sling.models.annotations.Model;

import com.ftd.platform.core.constants.Constants;

/**
 * Sling Model for Banner component
 * 
 */
/**
 * @author manpati
 *
 */
@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = "web/platform/components/content/banner")
@Exporter(
        name = "jackson",
        extensions = "json",
        options = { @ExporterOption(
                name = "SerializationFeature.WRITE_DATES_AS_TIMESTAMPS",
                value = "true") })
public class BannerModel extends BaseModel
{
    private static final String COMPONENT_TITLE = "Banner";
    
    @Inject
    private String imageLink;

    @Inject
    private String imgAltTxt;

    @Inject
    private String heading;

    @Inject
    private String subHeading;

    @Inject
    private String buttonLabel;

    @Inject
    private String buttonLink;

    @Inject
    private String buttonLinkTarget;

    @Inject
    private String contentAlignment;

    @Inject
    private String colorTheme;

    /**
     * This method returns the component identifier.
     * 
     * @return
     */
    @Override
    public String getComponent()
    {
        return Constants.BANNER_COMPONENT_TITLE;
    }

    /**
     * This method constructs the attributes map object and returns the same.
     * 
     * @return
     */
    public Map<String, Object> getAttributes()
    {

        Map<String, Object> attributes = new LinkedHashMap<>();
        Map<String, Object> primaryCTA = new LinkedHashMap<>();

        attributes.put(Constants.TITLE_KEY, this.heading);

        if (subHeading != null)
        {
            attributes.put(Constants.DESCRIPTION_KEY, this.subHeading);
        }

        primaryCTA.put(Constants.TEXT_KEY, this.buttonLabel);
        primaryCTA.put(Constants.URL_KEY, this.buttonLink);

        setTargetKeyInPrimaryCta(primaryCTA, buttonLinkTarget);

        if (buttonLink != null && buttonLabel != null)
        {
            attributes.put(Constants.PRIMARY_CTA_KEY, primaryCTA);
        }
        attributes.put(Constants.THEME_KEY, this.colorTheme);
        attributes.put(Constants.ALIGNMENT_KEY, this.contentAlignment);

        attributes.put(Constants.IMAGE_KEY, getImageMap(this.imageLink, this.imgAltTxt));

        return attributes;
    }

    /**
     * This is to get component title
     * 
     * @return
     */
    @Override
    public String getComponentTitle()
    {
        return COMPONENT_TITLE;
    }

}
