package com.ftd.platform.core.services.impl;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Value;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.tagging.TagConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ftd.platform.core.services.ContentPageDataService;
import com.ftd.platform.core.services.GetXFDataElementsService;
import com.ftd.platform.core.utils.Utils;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * This service will get the Experience Fragment Model JSON for a component using the resource type. Along with the
 * component model, it also includes the tags that are being defined at the experience fragment variant for conditional
 * content.
 */
@Component(
        name = "GetXFDataElementsService",
        service = GetXFDataElementsService.class,
        immediate = true)
public class GetXFDataElementsServiceImpl implements GetXFDataElementsService
{

    private final Logger LOG = LoggerFactory.getLogger(getClass());
    private static final String RELATIVE_ROOT_PATH = "jcr:content/root";

    @Reference
    private ContentPageDataService contentPageDataService;
 
    @Reference
    ResourceResolverFactory resourceResolverFactory;

    /**
     * This method will iterate through each result and find the root path "jcr:content/root" and will get the component
     * under the root resource Node. A JSON reponse will be returned as part of the component sling Model.
     *
     * @param resourcePath The Experience Fragment content path.
     * @return Result component JSON.
     */
    public String getComponentModelJson(String resourcePath)
    {
        ResourceResolver resourceResolver = null;
        try
        {
            resourcePath = StringUtils.removeEnd(resourcePath, ".data.json");
            LOG.debug("Resource Path => " + resourcePath);
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
            String futureStartDate = null;
            String futureEndDate = null;

            if (resourceResolver != null)
            {
                Resource jcrResource = resourceResolver.getResource(resourcePath);
                if (null != jcrResource)
                {
                    Node node = jcrResource.adaptTo(Node.class);
                    Value[] tags = null;

                    if (node != null)
                    {
                        // this is to get the tags for experience fragment variant
                        if (node.hasNode(JcrConstants.JCR_CONTENT))
                        {
                            Node jcrContentNode = node.getNode(JcrConstants.JCR_CONTENT);
                            if (jcrContentNode.hasProperty(TagConstants.PN_TAGS))
                            {
                                tags = jcrContentNode.getProperty(TagConstants.PN_TAGS).getValues();
                            }
                            if (jcrContentNode.hasProperty("cq:publishDate"))
                            {
                                Date laterDate = jcrContentNode.getProperty("cq:publishDate").getDate().getTime();
                                futureStartDate = Utils.getFormattedDate(laterDate);
                            }
                            if (jcrContentNode.hasProperty("cq:unPublishDate"))
                            {
                                Date laterDate = jcrContentNode.getProperty("cq:unPublishDate").getDate().getTime();
                                futureEndDate = Utils.getFormattedDate(laterDate);
                            }
                        }

                        Object mf = null;
                        // This is to retrieve component model json which XF holds
                        if (node.hasNode(RELATIVE_ROOT_PATH))
                        {
                            JsonArray jsonArray = new JsonArray();
                            String componentJson;

                            Iterator<Resource> iterator = resourceResolver
                                    .getResource(node.getNode(RELATIVE_ROOT_PATH).getPath()).listChildren();
                            while (iterator.hasNext())
                            {
                                mf = contentPageDataService.getModelFromResource(iterator.next());

                                componentJson = modifyComponentJson(mf, tags, futureStartDate, futureEndDate);

                                JsonParser parser = new JsonParser();
                                JsonObject o = parser.parse(componentJson).getAsJsonObject();

                                jsonArray.add(o);
                            }

                            return jsonArray.toString();
                        }

                    }
                } else {
                    LOG.debug("No exp fragment found at path :: " + resourcePath);
                }
            }
        } catch (RepositoryException re)
        {
            LOG.error("Exception while accessing the Repository", re);
        } catch (LoginException le)
        {
            LOG.error("Exception while Login to the Repository", le);
        } finally
        {
            if ((resourceResolver != null) && (resourceResolver.isLive()))
            {
                resourceResolver.close();
            }
        }
        return null;
    }

    /**
     * This method will add the Tags as part of the component JSON that is being rendered from the Resource. Adds
     * "tags": [] of tag values.
     *
     * @param componentJson Component Json from resource
     * @param tags Value array of tags of that particular experience fragment
     * @return Modified component json including tags attribute
     */
    private String modifyComponentJson(Object componentJson, Value[] tags, String futureStartDate, String futureEndDate)
    {
        String modifiedComponentJson = null;
        try
        {
            ObjectMapper mapper = new ObjectMapper();
            Map<String, String> map = new HashMap<>();
            if (null != componentJson)
            {
                String strComponentJson = mapper.writeValueAsString(componentJson);
                map = (Map<String, String>) mapper.readValue(strComponentJson, Map.class);
            }
            map.put("tags", Arrays.toString(tags));
            if (null != futureStartDate)
                map.put("start", futureStartDate);
            if (null != futureEndDate)
                map.put("end", futureEndDate);
            modifiedComponentJson = mapper.writeValueAsString(map);
        } catch (IOException ioe)
        {
            LOG.error("IO Exception", ioe);
        }
        LOG.debug("Modified Component Json with Tags Attribute : {}", modifiedComponentJson);
        return modifiedComponentJson;
    }
}
