package com.ftd.platform.core.models.page;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.settings.SlingSettingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.ftd.platform.core.beans.ReplicationDataBean;
import com.ftd.platform.core.constants.Constants;
import com.ftd.platform.core.models.BaseModel;
import com.ftd.platform.core.models.ExperienceFragmentModel;
import com.ftd.platform.core.services.ContentPageDataService;
import com.ftd.platform.core.utils.Utils;

@Model(
        adaptables = SlingHttpServletRequest.class)
public class PageBaseModel
{
    private final Logger log = LoggerFactory.getLogger(getClass());

    @Inject
    private Page currentPage;
    
    @Inject
    private SlingSettingsService slingSettingsService;

    @ChildResource(
            via = Constants.RESOURCE)
    protected Resource rootResource;

    @SlingObject
    ResourceResolver resourceResolver;

    @Inject
    private ContentPageDataService contentPageDataService;

    protected List<Object> components = new ArrayList<>();
    protected List<Object> headerComponents = new ArrayList<>();
    protected List<Object> footerComponents = new ArrayList<>();
    protected List<Object> modalComponents = new ArrayList<>();
    ReplicationDataBean replicationDataBean = null;

    /**
     * This method initializes the components list object
     */
    @PostConstruct
    public void init()
    {
        processComponent(rootResource, null);
    }

    protected void processComponent(final Resource componentResource, final String locationContext)
    {
        if (null != currentPage)
        {
            replicationDataBean = contentPageDataService
                    .getReplicationDataFromResource(currentPage.getProperties());
        }

        if (Objects.nonNull(componentResource))
        {
            Iterator<Resource> iterator = componentResource.listChildren();
            while (iterator.hasNext())
            {
                Resource childResource = iterator.next();
                setComponentModelJsonData(childResource, false, null, locationContext);
            }
        }
    }

    /**
     * Sets the component JSON from its resource
     * 
     * @param componentResource -- The component resource.
     */
    private void setComponentModelJsonData(Resource componentResource, boolean isExperienceFragment,
            ExperienceFragmentModel exp, String locationContext)
    {
        Object modelObject = contentPageDataService.getModelFromResource(componentResource);

        if (null != modelObject)
        {
            log.debug("Adding component to the list: {}", modelObject);
            BaseModel componentModel = (BaseModel) modelObject;

            String componentName = componentModel.getComponent();
            if (Objects.nonNull(componentName))
            {
                setComponentLocation(componentModel, locationContext);
            }
        }
    }

    /**
     * This method will add "aem-author" or "aem-publish" to the JSON output at page level.
     * 
     * @return environment -- Will return the environment details.
     */
    public String getEnvironment()
    {
        return Utils.getEnvironment(slingSettingsService);
    }

    /**
     * It returns the base layout title in the page level json.
     * 
     * @return
     */
    public String getLayout()
    {
        return Constants.BASE_PAGE_LAYOUT_TITLE;
    }

    /**
     * This method gets the list of all components which are part of home page.
     * 
     * @return
     */
    public List<Object> getComponents()
    {
        return components;
    }

    /**
     * This method gets the activation time in the String format.
     * 
     * @return
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getStart()
    {
        return contentPageDataService.getStartDate(replicationDataBean);
    }

    /**
     * This method gets the deactivation time in the String format
     * 
     * @return
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getEnd()
    {
        return contentPageDataService.getEndDate(replicationDataBean);
    }
    
    /**
     * Sets the component JSON on the basis of location
     * 
     * @param componentModel The component model.
     * @param locationContext The location context.
     */
    private void setComponentLocation(BaseModel componentModel, String locationContext)
    {
        if (StringUtils.isNotBlank(locationContext))
        {
            if (StringUtils.equals(locationContext, Constants.HEADER_LOCATION))
            {
                headerComponents.add(componentModel);
            } else if (StringUtils.equals(locationContext, Constants.FOOTER_LOCATION))
            {
                footerComponents.add(componentModel);
            } else if (StringUtils.equals(locationContext, Constants.MODAL_LOCATION))
            {
                modalComponents.add(componentModel);
            }
        } else
        {
            components.add(componentModel);
        }
    }
}
