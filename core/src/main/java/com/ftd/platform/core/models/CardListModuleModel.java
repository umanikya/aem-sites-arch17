package com.ftd.platform.core.models;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;

import org.apache.sling.models.annotations.Model;
import com.ftd.platform.core.constants.Constants;

/**
 * Sling Model for Sling Model for Card List Module component
 * 
 * @author chakrish1
 *
 */
@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = Constants.CARD_LIST_MODULE_RESOURCE_TYPE)
@Exporter(
        name = Constants.JACKSON_KEY,
        extensions = Constants.JSON_KEY)
public class CardListModuleModel extends BaseModel
{

    @Inject
    private String heading;

    @Inject
    private String subHeading;

    @Inject
    private String leftImageLink;

    @Inject
    private String leftImageAltText;

    @Inject
    private String leftImageCTALabel;

    @Inject
    private String leftImageCTALink;

    @Inject
    private String leftImageLinkTarget;

    @Inject
    private String middleImageLink;

    @Inject
    private String middleImageAltText;

    @Inject
    private String middleImageCTALabel;

    @Inject
    private String middleImageCTALink;

    @Inject
    private String middleImageLinkTarget;

    @Inject
    private String rightImageLink;

    @Inject
    private String rightImageAltText;

    @Inject
    private String rightImageCTALabel;

    @Inject
    private String rightImageCTALink;

    @Inject
    private String rightImageLinkTarget;

    @Inject
    @Default(
            values = Constants.CARD_LIST_MODULE_TITLE)
    private String component;

    private static final String COMPONENT_TITLE = "Card List Module";
    /**
     * This method returns the component identifier.
     * 
     * @return
     */
    @Override
    public String getComponent()
    {
        return component;
    }
    Map<String, Object> attributes = new LinkedHashMap<>();

    /**
     * This method constructs the attributes map object for image and links display based on condition and returns the
     * same.
     * 
     * @return
     */
    public Map<String, Object> getAttributes()
    {
        List<Object> cards = new ArrayList<>();
        if(StringUtils.isNotBlank(this.heading))
        {
           attributes.put(Constants.TITLE_KEY, this.heading);
        }
        
        if(StringUtils.isNotBlank(this.subHeading))
        {
            attributes.put(Constants.TEXT_KEY, this.subHeading);
        }        

        cards.add(getImageLinksMap(leftImageLink, leftImageAltText, leftImageCTALabel, leftImageCTALink,
                leftImageLinkTarget));

        cards.add(getImageLinksMap(middleImageLink, middleImageAltText, middleImageCTALabel, middleImageCTALink,
                middleImageLinkTarget));

        cards.add(getImageLinksMap(rightImageLink, rightImageAltText, rightImageCTALabel, rightImageCTALink,
                rightImageLinkTarget));

        attributes.put(Constants.CARDS_KEY, cards);
        return attributes;
    }

    /**
     * This method constructs the attributes map object for image and links returns the same
     * 
     * @param imageLink
     * @param imageAltText
     * @param buttonLabel
     * @param buttonLink
     * @param targetLink
     * @return
     */
    private Map<String, Object> getImageLinksMap(String imageLink, String imageAltText, String buttonLabel,
            String buttonLink, String targetLink)
    {
        Map<String, Object> primaryCTAMap = new LinkedHashMap<>();
        Map<String, Object> imageWithCTAMap = new LinkedHashMap<>();

        primaryCTAMap.put(Constants.TEXT_KEY, buttonLabel);
        primaryCTAMap.put(Constants.URL_KEY, buttonLink);
        
        setTargetKeyInPrimaryCta(primaryCTAMap, targetLink);
        
        imageWithCTAMap.put(Constants.IMAGE_KEY, getImageMap(imageLink, imageAltText));
        imageWithCTAMap.put(Constants.LINK_KEY, primaryCTAMap);

        return imageWithCTAMap;
    }
    
    /**
     * This is to get component title
     * 
     * @return
     */
    @Override
    public String getComponentTitle()
    {
        return COMPONENT_TITLE;
    }
}
