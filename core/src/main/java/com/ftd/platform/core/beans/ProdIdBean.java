package com.ftd.platform.core.beans;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

/**
 * Bean Class for holding product details
 * 
 * @author sunkatep
 *
 */
@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ProdIdBean
{

    @Inject
    private String productid;

    @Inject
    private String productidThreeProd;

    /**
     * Getter method for Product Id
     * 
     * @return
     */
    public String getProductid()
    {
        return productid;
    }

    /**
     * Setter method for Product Id
     * 
     * @param productid
     */
    public void setProductid(String productid)
    {
        this.productid = productid;
    }

    /**
     * Getter method for Product Id
     * 
     * @return
     */
    public String getProductidThreeProd()
    {
        return productidThreeProd;
    }

    /**
     * Setter method for Product Id
     * 
     * @param productid
     */
    public void setProductidThreeProd(String productidThreeProd)
    {
        this.productidThreeProd = productidThreeProd;
    }

}
