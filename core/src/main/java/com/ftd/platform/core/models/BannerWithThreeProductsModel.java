package com.ftd.platform.core.models;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;

import com.ftd.platform.core.constants.Constants;
import com.ftd.platform.core.utils.Utils;

/**
 * Sling Model for Banner With Three Products component
 * 
 * @author nikhil
 *
 */
@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = "web/platform/components/content/bannerwiththreeproducts")
@Exporter(
        name = "jackson",
        extensions = "json")
public class BannerWithThreeProductsModel extends BaseModel
{

    @Inject
    private String idSelector;

    @Inject
    private String imageLink;

    @Inject
    private String imgAltTxt;

    @Inject
    private String heading;

    @Inject
    private String buttonLabel;

    @Inject
    private String buttonLink;

    @Inject
    private String buttonLinkTarget;

    @Inject
    private String textAlignment;

    @Inject
    private String fontColor;

    @Inject
    private String catId;

    @Inject
    private Resource prodId;
    
    @Inject
    String analyticsTrackingId;
    private static final String COMPONENT_TITLE = "Banner with three Products";
    /**
     * This method returns the component identifier.
     * 
     * @return
     */
    @Override
    public String getComponent()
    {
        return Constants.BANNER_THREE_PRODUCTS_COMPONENT_TITLE;
    }

    /**
     * This method constructs the attributes map object and returns the same.
     * 
     * @return
     */
    public Map<String, Object> getAttributes()
    {

        Map<String, Object> attributes = new LinkedHashMap<>();
        Map<String, Object> primaryCTA = new LinkedHashMap<>();
        Map<String, Object> banner = new LinkedHashMap<>();
        List<String> prodidlist;

        banner.put(Constants.TITLE_KEY, this.heading);

        primaryCTA.put(Constants.TEXT_KEY, this.buttonLabel);
        primaryCTA.put(Constants.URL_KEY, this.buttonLink);

        setTargetKeyInPrimaryCta(primaryCTA, buttonLinkTarget);

        banner.put(Constants.PRIMARY_CTA_KEY, primaryCTA);
        banner.put(Constants.THEME_KEY, this.fontColor);
        banner.put(Constants.IMAGE_KEY, getImageMap(this.imageLink, this.imgAltTxt));
        banner.put(Constants.ALIGNMENT_KEY, this.textAlignment);
        attributes.put("productCount", 3);
        
        if (StringUtils.isNotBlank(this.analyticsTrackingId))
        {
            attributes.put("analyticsTrackingId", this.analyticsTrackingId);
        }

        attributes.put(Constants.BANNER_COMPONENT_TITLE, banner);
        prodidlist = Utils.getProductListFromResource(prodId, false);
        Utils.setProductIdOrCategoryIdInMap(idSelector, Utils.getCategoryIdFromTag(catId), prodidlist, attributes);

        return attributes;
    }
    /**
     * This is to get component title
     * 
     * @return
     */
    @Override
    public String getComponentTitle()
    {
        return COMPONENT_TITLE;
    }
}
