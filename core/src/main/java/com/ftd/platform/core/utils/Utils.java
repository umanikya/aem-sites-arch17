package com.ftd.platform.core.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TimeZone;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.settings.SlingSettingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ftd.platform.core.beans.PageSEOBean;
import com.ftd.platform.core.beans.ProdIdBean;
import com.ftd.platform.core.constants.Constants;

/**
 * Utility Class having common methods.
 *
 */
public class Utils
{

    private static final Logger log = LoggerFactory.getLogger(Utils.class);
    /**
     * Private constructor
     */
    private Utils()
    {
    }

    /**
     * It returns the list of productId's from the multi-field resource node.
     * 
     * @param resource
     * @param isThreeProducts
     * @return
     */
    public static List<String> getProductListFromResource(Resource resource, Boolean isThreeProducts)
    {
        List<String> prodidlist = new ArrayList<>();
        if (resource != null)
        {
            Iterator<Resource> iterator = resource.listChildren();
            while (iterator.hasNext())
            {
                Resource childResource = iterator.next();
                if (childResource != null)
                {
                    if (!isThreeProducts)
                    {
                        prodidlist.add(childResource.adaptTo(ProdIdBean.class).getProductid());
                    } else
                    {
                        prodidlist.add(childResource.adaptTo(ProdIdBean.class).getProductidThreeProd());
                    }
                }
            }
        }
        return prodidlist;
    }

    /**
     * It sets either productId or categoryId in the attributes Map object.
     * 
     * @param selector
     * @param categoryId
     * @param prodidlist
     * @param attributes
     */
    public static void setProductIdOrCategoryIdInMap(String selector, String categoryId, List<String> prodidlist,
            Map<String, Object> attributes)
    {
        if (selector != null && selector.equals(Constants.PRODUCTID_KEY))
        {
            attributes.put(Constants.PRODUCT_KEY, prodidlist);

        } else
        {
            attributes.put(Constants.CATEGORY_ID_KEY, categoryId);
        }
    }

    /**
     * It returns the categoryId value by performing string operations in tag value.
     * 
     * @param tagValue
     * @return
     */
    public static String getCategoryIdFromTag(String tagValue)
    {
        if (tagValue != null)
        {
            tagValue = tagValue.split("[\\:/]+")[tagValue.split("[\\:/]+").length - 1];
        }
        return tagValue;
    }

    /**
     * It returns the Sting array of Tags by performing string operations in tag value.
     * 
     * @param tagArray
     * @return
     */
    public static String[] getValueFromTag(String[] tagArray)
    {

        if (Objects.nonNull(tagArray))
            for (int i = 0; i < tagArray.length; i++)
            {
                tagArray[i] = tagArray[i].split("[\\:/]+")[tagArray[i].split("[\\:/]+").length - 1];
            }
        return tagArray;

    }

    /**
     * This method will add "aem-author" or "aem-publish" to the JSON output at page level.
     * 
     * @return environment -- Will return the environment details.
     */
    public static String getEnvironment(SlingSettingsService slingSettingsService)
    {
        StringBuilder environment = new StringBuilder("aem-");
        Set<String> runModes = slingSettingsService.getRunModes();
        for (String runMode : runModes)
        {
            if ((runMode.equals("author")) || (runMode.equals("publish")))
            {
                environment.append(runMode);
                break;
            }
        }
        return environment.toString();
    }

    /**
     * It returns the String format for the Data object that passed as a param.
     * 
     * @param date - Date Object
     * @return
     */
    public static String getFormattedDate(Date date)
    {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        String dateStr = formatter.format(date);
        return dateStr;
    }

    /**
     * This method adds the child resources of a given Resource to the bean where the bean class iterates through the
     * multifield and stores the values
     * 
     * @return List of child resources with the values
     */
    public static Map<String, PageSEOBean> getSEOListFromResource(Resource resource)
    {
        Map<String, PageSEOBean> seoList = new LinkedHashMap<>();
        if (resource != null)
        {
            Iterator<Resource> iterator = resource.listChildren();
            while (iterator.hasNext())
            {
                Resource childResource = iterator.next();
                if (childResource != null)
                {
                    PageSEOBean seoBean = childResource.adaptTo(PageSEOBean.class);
                    seoList.put(seoBean.getName(), seoBean);
                }
            }
        }
        log.debug("SEO Size is => " + seoList.size() + "\nSEO =>" + seoList);
        return seoList;
    }

    /**
     * This method will extract any content/text in between html
     * <p>
     * </p>
     * tags.
     * 
     * @param rteText -- RTE entered text
     * @return cleanRTEText -- Removed
     *         <p>
     *         </p>
     *         tags text
     */
    public static String removeParaTagsFromRTE(String rteText)
    {
        String cleanRTEText = null;
        String pattern = "<[/]?p[^>]*>";
        log.debug("Text is => " + rteText);
        if (StringUtils.isNotEmpty(rteText))
            cleanRTEText = rteText.replaceAll(pattern, "");
        log.debug("Cleaned up RTE Text => " + cleanRTEText);
        return cleanRTEText;
    }

    /**
     * It returns the list of bean type from the multi-field resource node.
     * 
     * @param resource
     * @return
     */
    public static <T extends Object> List<T> getBeanDataFromResource(Resource resource, Class<T> beanType)
    {
        List<T> beanList = new ArrayList<>();
        if (resource != null)
        {
            Iterator<Resource> iterator = resource.listChildren();
            while (iterator.hasNext())
            {
                Resource childResource = iterator.next();
                if (childResource != null)
                {
                    beanList.add(childResource.adaptTo(beanType));
                }
            }
        }
        return beanList;
    }

}
