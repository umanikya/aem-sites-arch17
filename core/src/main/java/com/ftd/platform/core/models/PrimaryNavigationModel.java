package com.ftd.platform.core.models;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ftd.platform.core.beans.LinkUrlTargetBean;
import com.ftd.platform.core.beans.LinksHelperModel;
import com.ftd.platform.core.beans.MainMenuBean;
import com.ftd.platform.core.beans.NavigationColumnBean;
import com.ftd.platform.core.constants.Constants;

/**
 * Navigation Heading is Used for generating Sling Models.
 * 
 * @author abhranja1
 * 
 */
@Model(
        adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = "/apps/web/platform/components/content/primarynav")
@Exporter(
        name = "jackson", extensions = "json")
public class PrimaryNavigationModel extends BaseModel
{
    private final Logger logger = LoggerFactory.getLogger(PrimaryNavigationModel.class);

    @Inject
    private String columnSelector;

    @Inject
    private String menuOnePosition;
    @Inject
    private String menuOneLabel;
    @Inject
    private String menuOneColOneHeading;
    @Inject
    private String menuOneColTwoHeading;
    @Inject
    private String menuOneColThreeHeading;
    @Inject
    private String menuOneColFourHeading;
    @Inject
    @JsonIgnore
    private Resource menuOneColOneResource;
    @Inject
    @JsonIgnore
    public Resource menuOneColTwoResource;
    @Inject
    @JsonIgnore
    public Resource menuOneColThreeResource;
    @Inject
    @JsonIgnore
    public Resource menuOneColFourResource;

    @Inject
    private String menuTwoPosition;
    @Inject
    private String menuTwoLabel;
    @Inject
    private String menuTwoColOneHeading;
    @Inject
    private String menuTwoColTwoHeading;
    @Inject
    private String menuTwoColThreeHeading;
    @Inject
    private String menuTwoColFourHeading;
    @Inject
    @JsonIgnore
    public Resource menuTwoColOneResource;
    @Inject
    @JsonIgnore
    public Resource menuTwoColTwoResource;
    @Inject
    @JsonIgnore
    public Resource menuTwoColThreeResource;
    @Inject
    @JsonIgnore
    public Resource menuTwoColFourResource;

    @Inject
    private String menuThreePosition;
    @Inject
    private String menuThreeLabel;
    @Inject
    private String menuThreeColOneHeading;
    @Inject
    private String menuThreeColTwoHeading;
    @Inject
    private String menuThreeColThreeHeading;
    @Inject
    private String menuThreeColFourHeading;
    @Inject
    @JsonIgnore
    public Resource menuThirdColOneResource;
    @Inject
    @JsonIgnore
    public Resource menuThirdColTwoResource;
    @Inject
    @JsonIgnore
    public Resource menuThirdColThreeResource;
    @Inject
    @JsonIgnore
    public Resource menuThirdColFourResource;

    @Inject
    private String menuFourPosition;
    @Inject
    private String menuFourLabel;
    @Inject
    private String menuFourColOneHeading;
    @Inject
    private String menuFourColTwoHeading;
    @Inject
    private String menuFourColThreeHeading;
    @Inject
    private String menuFourColFourHeading;
    @Inject
    @JsonIgnore
    public Resource menuFourColOneResource;
    @Inject
    @JsonIgnore
    public Resource menuFourColTwoResource;
    @Inject
    @JsonIgnore
    public Resource menuFourColThreeResource;
    @Inject
    @JsonIgnore
    public Resource menuFourColFourResource;

    @Inject
    private String menuFivePosition;
    @Inject
    private String menuFiveLabel;
    @Inject
    private String menuFiveColOneHeading;
    @Inject
    private String menuFiveColTwoHeading;
    @Inject
    private String menuFiveColThreeHeading;
    @Inject
    private String menuFiveColFourHeading;
    @Inject
    @JsonIgnore
    public Resource menuFiveColOneResource;
    @Inject
    @JsonIgnore
    public Resource menuFiveColTwoResource;
    @Inject
    @JsonIgnore
    public Resource menuFiveColThreeResource;
    @Inject
    @JsonIgnore
    public Resource menuFiveColFourResource;
    
 
    
    private static final String COMPONENT_TITLE = "Primary Navigation";

    @JsonIgnore
    public List<MainMenuBean> mainMenuList = new ArrayList<>();

    /**
     * In init() method is used to handle the Tab based on selection value for displaying the menuLabel,ColumnHeading
     * for all five different Tabs.
     * 
     */
    @PostConstruct
    protected void primaryNavInit()
    {
        MainMenuBean mainMenu = null;

        mainMenu = getMainMenuBean(menuOneLabel, menuOneColOneHeading, menuOneColTwoHeading, menuOneColThreeHeading,
                menuOneColFourHeading, menuOneColOneResource, menuOneColTwoResource, menuOneColThreeResource,
                menuOneColFourResource, menuOnePosition);
        mainMenuList.add(mainMenu);

        if (columnSelector != null)
        {
            if (columnSelector.equals(Constants.TWO_OPTIONS_IN_MENU)
                    || columnSelector.equals(Constants.THREE_OPTIONS_IN_MENU)
                    || columnSelector.equals(Constants.FOUR_OPTIONS_IN_MENU)
                    || columnSelector.equals(Constants.FIVE_OPTIONS_IN_MENU))
            {
                mainMenu = getMainMenuBean(menuTwoLabel, menuTwoColOneHeading, menuTwoColTwoHeading,
                        menuTwoColThreeHeading, menuTwoColFourHeading, menuTwoColOneResource, menuTwoColTwoResource,
                        menuTwoColThreeResource, menuTwoColFourResource, menuTwoPosition);
                mainMenuList.add(mainMenu);
            }

            if (columnSelector.equals(Constants.THREE_OPTIONS_IN_MENU)
                    || columnSelector.equals(Constants.FOUR_OPTIONS_IN_MENU)
                    || columnSelector.equals(Constants.FIVE_OPTIONS_IN_MENU))
            {

                mainMenu = getMainMenuBean(menuThreeLabel, menuThreeColOneHeading, menuThreeColTwoHeading,
                        menuThreeColThreeHeading, menuThreeColFourHeading, menuThirdColOneResource,
                        menuThirdColTwoResource, menuThirdColThreeResource, menuThirdColFourResource, menuThreePosition);
                mainMenuList.add(mainMenu);
            }

            if (columnSelector.equals(Constants.FOUR_OPTIONS_IN_MENU)
                    || columnSelector.equals(Constants.FIVE_OPTIONS_IN_MENU))
            {
                mainMenu = getMainMenuBean(menuFourLabel, menuFourColOneHeading, menuFourColTwoHeading,
                        menuFourColThreeHeading, menuFourColFourHeading, menuFourColOneResource,
                        menuFourColTwoResource, menuFourColThreeResource, menuFourColFourResource, menuFourPosition);
                mainMenuList.add(mainMenu);
            }

            if (columnSelector.equals(Constants.FIVE_OPTIONS_IN_MENU))
            {
                mainMenu = getMainMenuBean(menuFiveLabel, menuFiveColOneHeading, menuFiveColTwoHeading,
                        menuFiveColThreeHeading, menuFiveColFourHeading, menuFiveColOneResource,
                        menuFiveColTwoResource, menuFiveColThreeResource, menuFiveColFourResource, menuFivePosition);
                mainMenuList.add(mainMenu);
            }
        }
        logger.debug("BEFORE sorting :: {} ", mainMenuList);
        java.util.Collections.sort(mainMenuList);
        logger.debug("AFTER sorting :: {} ", mainMenuList);

    }

    /**
     * This method returns the component identifier.
     * 
     * @return The component identifier
     */
    @Override
    public String getComponent()
    {
        return Constants.PRIMARY_NAVIGATION_TITLE;
    }
    
    /**
     * It returns the Map object with entire primary navigation details in it.
     * 
     * @return
     */
    public Map<String, Object> getAttributes()
    {

        Map<String, Object> contentConfig = new LinkedHashMap<>();
        Map<String, Object> navigationContentConfigurationID = new LinkedHashMap<>();
        navigationContentConfigurationID.put(Constants.LINKS_KEY, mainMenuList);
        contentConfig.put(Constants.NAVIGATION_CONTENT_CONFIG_ID_KEY, navigationContentConfigurationID);

        return contentConfig;
    }

    /**
     * Created object for the Navigation heading and handling five different tabs.
     * 
     * @param menuLabel
     * @param columnOneHeading
     * @param columnTwoHeading
     * @param columnThreeHeading
     * @param columnFourHeading
     * @param columnOneResource
     * @param columnTwoResource
     * @param columnThreeResource
     * @param columnFourResource
     * @return
     */
    public MainMenuBean getMainMenuBean(String menuLabel, String columnOneHeading, String columnTwoHeading,
            String columnThreeHeading, String columnFourHeading, Resource columnOneResource,
            Resource columnTwoResource, Resource columnThreeResource, Resource columnFourResource, String menuPosition)
    {

        List<NavigationColumnBean> navigationColumnList = new ArrayList<>();
        NavigationColumnBean navheading = null;
        if (columnOneResource != null)
        {
            navheading = new NavigationColumnBean(columnOneHeading, getNavigationColumnList(columnOneResource));
            navigationColumnList.add(navheading);
        }
        if (columnTwoResource != null)
        {
            navheading = new NavigationColumnBean(columnTwoHeading, getNavigationColumnList(columnTwoResource));
            navigationColumnList.add(navheading);
        }
        if (columnThreeResource != null)
        {
            navheading = new NavigationColumnBean(columnThreeHeading, getNavigationColumnList(columnThreeResource));
            navigationColumnList.add(navheading);
        }
        if (columnFourResource != null)
        {
            navheading = new NavigationColumnBean(columnFourHeading, getNavigationColumnList(columnFourResource));
            navigationColumnList.add(navheading);
        }
        return new MainMenuBean(menuLabel, menuPosition, navigationColumnList);
    }
    /**
     * Iterating the Link and label from LinkUrl bean class by creating a ArrayList
     * 
     * @param navigationColumnResource
     * @return
     */
    @JsonIgnore
    public List<LinksHelperModel> getNavigationColumnList(Resource navigationColumnResource)
    {
        List<LinksHelperModel> columnList = new ArrayList<>();
        if (navigationColumnResource != null)
        {
            Iterator<Resource> iterator = navigationColumnResource.listChildren();
            while (iterator.hasNext())
            {
                Resource childResource = iterator.next();
                if (childResource != null)
                    columnList.add(childResource.adaptTo(LinksHelperModel.class));
                columnList.remove(null);
            }
        }

        return columnList;
    }

    /**
     * This is to get component title
     * 
     * @return
     */
    @Override
    public String getComponentTitle()
    {
        return COMPONENT_TITLE;
    }
    
    /**
     * This method returns the Map object holding the information required for contacts Us section.
     * 
     * @return
     */
}
