package com.ftd.platform.core.servlets;

import java.io.IOException;
import java.util.Objects;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ftd.platform.core.services.ExperienceContentPageListService;

/**
 * @author abhranja1
 * 
 */
@Component(
        service = { Servlet.class },
        property = { "service.description=Get Experience Content Page List Servlet",
                "sling.servlet.paths=/bin/experienceFragmentsList", "sling.servlet.methods = GET",
                "sling.auth.requirements = -{/bin/experienceFragmentsList}" })
public class GetExperienceContentPageListServlet extends SlingSafeMethodsServlet
{

    private static final Logger logger = LoggerFactory.getLogger(GetExperienceContentPageListServlet.class);
    private static final long serialVersionUID = 9179895523313781530L;

    @Reference
    ExperienceContentPageListService experienceContentPageListService;

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
     * org.apache.sling.api.SlingHttpServletResponse) doGet method will get the JSON Response from Service
     */
    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServletException, IOException
    {
        logger.debug("GetExperienceContentPageListServlet : In doGetmethod ()");
        
        String siteId = Objects.isNull(request.getParameter("siteId")) ? "" : request.getParameter("siteId");
        logger.debug("Site Id: {}", siteId);
        
        response.setContentType("application/json");
        response.getWriter().write(experienceContentPageListService.getExperienceContentPageList(siteId));
    }

}
