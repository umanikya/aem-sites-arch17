package com.ftd.platform.core.models;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;

import com.ftd.platform.core.constants.Constants;

@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = "web/platform/components/content/pagebreakbanner")
@Exporter(
        name = "jackson",
        extensions = "json")
public class PageBreakBannerModel extends BaseModel
{
    
    private static final String COMPONENT_TITLE = "page-break-banner";
    
    @Inject
    private String title;
    
    @Inject
    private String description;
    
    @Inject
    private String buttonLabelOne;
    
    @Inject
    private String buttonLinkOne;
    
    @Inject
    private String buttonLabelTwo;
    
    @Inject
    private String buttonLinkTwo;    
    
    @Inject
    private int showCount;
    
    @Inject
    private double showPercentage;
    
    /**
     * This method constructs the attributes map object and returns the same.
     * 
     * @return
     */
    public Map<String, Object> getAttributes()
    {
        Map<String, Object> attributes = new LinkedHashMap<>();
        
        attributes.put("title", this.title);
        attributes.put("description", this.description);
        
        attributes.put(Constants.PRIMARY_CTA_KEY,
                getCategory(this.buttonLabelOne, this.buttonLinkOne));
        
        attributes.put(Constants.SECONDARY_CTA_KEY,
                getCategory(this.buttonLabelTwo, this.buttonLinkTwo));
        
        attributes.put("showCount", this.showCount);
        attributes.put("showPercentage", this.showPercentage);
        
        return attributes;
    }

    private Object getCategory(String buttonLabel, String buttonLink)
    {
        Map<String, Object> cta = new LinkedHashMap<>();

        cta.put(Constants.TEXT_KEY, buttonLabel);
        cta.put(Constants.URL_KEY, buttonLink);

        return cta;
    }
    
    /**
     * This method returns the component identifier.
     * 
     * @return
     */
    @Override
    public String getComponent()
    {
        return COMPONENT_TITLE;
    }
}
