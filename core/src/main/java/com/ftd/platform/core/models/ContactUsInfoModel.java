package com.ftd.platform.core.models;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.ftd.platform.core.beans.LinkUrlTargetBean;
import com.ftd.platform.core.constants.Constants;

@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = "/apps/web/platform/components/content/contactusinfo")
@Exporter(
        name = "jackson",
        extensions = "json")
@JsonPropertyOrder({Constants.COMPONENT_KEY,Constants.ATTRIBUTES_KEY })
public class ContactUsInfoModel extends BaseModel
{
    private static final String COMPONENT_TITLE = "Contact Info content";

    @Inject
    private String contactHeading;

    @Inject
    private String contactDescription;

    @Inject
    private String trackOrder;

    @Inject
    private String trackOrderUrl;

    @Inject
    private String phone;

    @Inject
    private String email;

    @Inject
    private String chatClassName;

    @Inject
    private String phoneUrl;

    @Inject
    private String emailUrl;

    @Override
    public String getComponentTitle()
    {
        return COMPONENT_TITLE;
    }
    
    public Map<String, Object> getAttributes()
    {
        Map<String, Object> attributes = new LinkedHashMap<>();
        attributes.put("contact", getContact());
        return attributes;
    }

    @JsonIgnore
    private Map<String, Object> getContact()
    {
        Map<String, Object> attributes = new LinkedHashMap<>();

        attributes.put(Constants.HEADING_KEY, this.contactHeading);
        if (StringUtils.isNotBlank(this.contactDescription))
        {
            attributes.put(Constants.DESCRIPTION_KEY, this.contactDescription);
        }
        List<LinkUrlTargetBean> contactList = new ArrayList<>();

        LinkUrlTargetBean linkBean = null;

        if (StringUtils.isNotBlank(this.trackOrder) && StringUtils.isNotBlank(this.trackOrderUrl))
        {
            linkBean = new LinkUrlTargetBean(this.trackOrder, this.trackOrderUrl, "PinSvg");
            contactList.add(linkBean);
        }

        if (StringUtils.isNotBlank(this.phone) && StringUtils.isNotBlank(this.phoneUrl))
        {
            linkBean = new LinkUrlTargetBean(this.phone, Constants.PHONE_SUFFIX + this.phoneUrl,
                    Constants.PHONE_ICON_VALUE);
            contactList.add(linkBean);
        }

        if (StringUtils.isNotBlank(this.email) && StringUtils.isNotBlank(this.emailUrl))
        {
            linkBean = new LinkUrlTargetBean(this.email, Constants.EMAIL_SUFFIX + this.emailUrl,
                    Constants.EMAIL_ICON_VALUE);
            contactList.add(linkBean);
        }

        if (StringUtils.isNotBlank(this.chatClassName))
        {
            linkBean = new LinkUrlTargetBean(this.chatClassName);
            contactList.add(linkBean);
        }

        if (null != linkBean)
        {
            attributes.put(Constants.LINKS_KEY, contactList);
        }

        return attributes;
    }

    @Override
    @JsonProperty(Constants.COMPONENT_KEY)
    public String getComponent()
    {
        return "contact-us-info";
    }

}
