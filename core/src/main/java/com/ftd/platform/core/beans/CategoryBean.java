package com.ftd.platform.core.beans;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import com.ftd.platform.core.utils.Utils;

/**
 * Category bean
 * 
 */
@Model(
        adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class CategoryBean
{

    @Inject
    private String category;

    @Inject
    private int numberOfProducts;

    @Inject
    private int startIndex;

    /**
     * @return - it returns the category
     */
    public String getCategory()
    {
        return Utils.getCategoryIdFromTag(category);
    }

    /**
     * @return - it returns the number of products
     */
    public int getNumberOfProducts()
    {
        return numberOfProducts;
    }

    /**
     * @return - it return the sort index
     */
    public int getStartIndex()
    {
        return startIndex;
    }

}
