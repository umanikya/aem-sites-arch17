package com.ftd.platform.core.models;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.ftd.platform.core.constants.Constants;

/**
 * Model Class for ExperienceFragmentModel Class
 * 
 */
@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = Constants.EXPERIENCE_FRAGMENT_RESOURCE_PATH)
@Exporter(
        name = Constants.JACKSON_KEY,
        extensions = Constants.JSON_KEY)
public class ExperienceFragmentModel extends BaseModel
{
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    private static final String COMPONENT_TITLE = "Experience Fragment";
    
    @Inject
    protected String variantId;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getVariationId()
    {
        return variantId;
    }

    /**
     * This method returns the component identifier.
     *
     * @return
     */
    @Override
    public String getComponent()
    {
        return Constants.EXPERIENCE_FRAGMENT_TITLE;
    }

    /**
     * This is to get component title
     * 
     * @return
     */
    @Override
    public String getComponentTitle()
    {
        return COMPONENT_TITLE;
    }
}
