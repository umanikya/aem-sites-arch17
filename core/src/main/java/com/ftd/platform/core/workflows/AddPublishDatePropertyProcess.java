package com.ftd.platform.core.workflows;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.Workflow;
import com.adobe.granite.workflow.exec.WorkflowData;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.ftd.platform.core.constants.Constants;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.jcr.Node;
import javax.jcr.PropertyType;
import javax.jcr.RepositoryException;
import javax.jcr.Value;

/**
 * This workflow process step will add new property called "cq:publishDate" and "cq:unPublishDate" properties to the
 * experience fragment. This process step will be part of - Schedule Page/Activate Asset - Schedule Page/Deactivate
 * Asset After the creation of version for particular asset on activation/deactivation later, this step will be
 * executed. This workflow step will - Add a property "cq:publishDate" to the payload path on Publish Later
 * functionality - Add a property "cq:unPublishDate" to the payload path on Unpublish Later functionality
 * 
 * @author manpati
 * 
 */
@Component(
        service = { WorkflowProcess.class },
        property = { Constants.SERVICE_DESCRIPTION + "=Add/Remove Future Publish/UnPublish Date Property",
                Constants.SERVICE_VENDOR + "=Add/Remove Publish/UnPublish Date Property Process",
                Constants.PROCESS_LABEL + "=Add/Remove PublishLater/UnPublishLater Date Property Process" })
public class AddPublishDatePropertyProcess implements WorkflowProcess
{

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    ResourceResolver resourceResolver = null;
    Workflow workflow = null;

    @Override
    public void execute(WorkItem workItem, WorkflowSession workflowSession, MetaDataMap metaDataMap)
            throws WorkflowException
    {
        try
        {
            workflow = workflowSession.getWorkflow(workItem.getWorkflow().getId());
            String workflowModel = workflow.getWorkflowModel().getTitle();
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
            WorkflowData workflowData = workItem.getWorkflowData();
            log.debug("Workflow PayLoad Type => " + workflowData.getPayloadType());
            

            if (StringUtils.isNotEmpty(workflowData.getPayloadType())
                    && (workflowData.getPayloadType().equals("JCR_PATH")))
            {
                log.debug("workflowData.getPayloadType() = " + workflowData.getPayloadType());
                String path = workflowData.getPayload().toString();
                log.info("Path is => " + path);
                if (resourceResolver != null)
                {
                    Resource jcrResource = resourceResolver.getResource(path);
                    

                    if (StringUtils.isNotEmpty(workflowModel))
                    {

                        if (jcrResource != null && (jcrResource.getChild("jcr:content") != null))
                        {
                            log.debug("Resource Path:{}", jcrResource.getPath());
                            Resource child = jcrResource.getChild("jcr:content");
                            Node node = child.adaptTo(Node.class);
                            String processArgs = metaDataMap.get("PROCESS_ARGS","default");
                            log.debug("workflowModel=" + workflowModel);
                            if (workflowModel.equals("Scheduled Page/Asset Activation"))
                            {
                                if(StringUtils.equals("add", processArgs))
                                {
                                    node.setProperty("cq:publishDate", getAbsTime(workItem), PropertyType.DATE);
                                }
                                else {
                                    node.setProperty("cq:publishDate", (Value)null);
                                }
                            } else if (workflowModel.equals("Scheduled Page/Asset Deactivation"))
                            {
                                if(StringUtils.equals("add", processArgs))
                                {
                                    node.setProperty("cq:unPublishDate", getAbsTime(workItem), PropertyType.DATE);
                                }
                                else {
                                      node.setProperty("cq:unPublishDate", (Value)null);
                                }
                            }

                            if (resourceResolver.hasChanges())
                            {
                                try
                                {
                                    resourceResolver.commit();
                                } catch (PersistenceException e)
                                {
                                    log.error("Exception While Saving Changes:{}", e);
                                }
                            }
                        }
                    }
                }
            }

        } catch (LoginException le)
        {
            log.error("Exception while Login", le);

        } catch (RepositoryException e)
        {
            log.error("Exception while setting the property.", e);
        } finally
        {
            if (resourceResolver != null)
                resourceResolver.close();
        }
    }

    /**
     * Get Absolute Time will return the formatted time to be saved in JCR in property cq:publishDate
     * 
     * @param workItem
     * 
     * @return String datetime in the proper format pattern.
     */
    private String getAbsTime(WorkItem workItem)
    {
        if (workItem.getWorkflowData().getMetaDataMap().get("absoluteTime", String.class) != null)
        {
            Calendar cal = getTime(workItem);
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:Ss.SSSXXX");// yyyy-MM-dd'T'HH:mm:Ss.SSS'Z'
            log.debug("Absolute Time => " + formatter.format(cal.getTime()));// yyyy-MM-dd'T'HH:mm:ss.SSSXXX
            return formatter.format(cal.getTime());
        }
        return null;
    }

    /**
     * Get absoluteTime property value from metaData node which is of type Long and convert it into calendar time.
     * 
     * @param workItem
     * @return Calendar time
     */
    private Calendar getTime(WorkItem workItem)
    {
        Long time = (Long) workItem.getWorkflowData().getMetaDataMap().get("absoluteTime", Long.class);
        log.debug("time =" + time);
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(time.longValue());
        return cal;
    }

}
