package com.ftd.platform.core.beans;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ftd.platform.core.constants.Constants;

/**
 * LinkUrlTargetbean Class is used for MultiField resource from AEM dialog.
 *
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class LinkUrlTargetBean
{
    @Inject
    private String text;

    @Inject
    @JsonProperty("newWindow")
    private String target;

    @Inject
    private String icon;

    @Inject
    private String areaLabel;

    @Inject
    private String link;

    @Inject
    private String className;
    
    /**
     * Constructor Method
     *
     * @param text
     * @param link
     * @param icon
     */
    public LinkUrlTargetBean(String text, String link, String icon)
    {
        this.text = text;
        this.link = link;
        this.icon = icon;
    }

    /**
     * Constructor Method
     *
     * @param className
     */
    public LinkUrlTargetBean(String className)
    {
        this.className = className;
    }

    /**
     * Default Constructor
     */
    public LinkUrlTargetBean()
    {
        super();
    }

    /**
     * Getter method for Text
     *
     * @return
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getText()
    {
        return text;
    }

    /**
     * Getter method for Target
     *
     * @return
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getTarget()
    {
        if (target != null && target.equals(Constants.TARGET_KEY_VALUE))
        {
            return target;
        } else
        {
            return null;
        }
    }
    
    /**
     * Getter method for Icon
     *
     * @return
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getIcon()
    {
        return icon;
    }

    /**
     * Getter method for AreaLabel
     *
     * @return
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getAreaLabel()
    {
        return areaLabel;
    }

    /**
     * Getter method for Links
     *
     * @return
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getLink()
    {
        return link;
    }

    /**
     * Getter method for class name
     *
     * @return
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getClassName()
    {
        return className;
    }

}
