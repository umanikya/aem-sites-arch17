package com.ftd.platform.core.beans;

import java.util.List;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;

import com.fasterxml.jackson.annotation.JsonProperty;

@Model(
        adaptables = Resource.class)
public class NavigationColumnBean
{

    /**
     * Constructor for NavigationColumnBean class
     * 
     * @param text
     * @param heading
     * @param navheading
     */
    public NavigationColumnBean(String heading, List<LinksHelperModel> navheading)
    {
        this.heading = heading;
        this.navheading = navheading;
    }

    @Inject
    public String heading;

    @JsonProperty("links")
    public List<LinksHelperModel> navheading;

    public String getHeading()
    {
        return heading;
    }

    /**
     * @param heading
     */
    public void setHeading(String heading)
    {
        this.heading = heading;
    }

    /**
     * @return
     */
    public List<LinksHelperModel> getNavheading()
    {
        return navheading;
    }

    /**
     * Setting Linkurlbean Inside navheading
     * 
     * @param navheading
     */
    public void setNavheading(List<LinksHelperModel> navheading)
    {
        this.navheading = navheading;
    }

}
