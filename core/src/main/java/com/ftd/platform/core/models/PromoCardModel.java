package com.ftd.platform.core.models;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;

import com.ftd.platform.core.constants.Constants;

/**
 * Sling Model for Product Card Banner component
 *
 *
 */
@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = Constants.PROMO_CARD_RESOURCE_TYPE)
@Exporter(
        name = "jackson",
        extensions = "json")
public class PromoCardModel extends BaseModel
{

    @Inject
    private String size;

    @Inject
    private String badgePosition;

    @Inject
    private String badgelabel;

    @Inject
    private String heading;

    @Inject
    private String description;

    @Inject
    private String price;

    @Inject
    private String ctaLabel;

    @Inject
    private String ctaUrl;

    @Inject
    private String ctaTarget;

    @Inject
    private String imageRef;

    @Inject
    private String imageAltText;
    
    @Inject
    private String theme;

    private static final String COMPONENT_TITLE = "Promo Card";
    /**
     * This method returns the component identifier.
     * 
     * @return
     */
    @Override
    public String getComponent()
    {
        return Constants.PROMO_CARD_COMPONENT_TITLE;
    }

    /**
     * This method constructs the attributes map object and returns the same.
     * 
     * @return
     */
    public Map<String, Object> getAttributes()
    {
        Map<String, String> badgeMap = new LinkedHashMap<>();
        Map<String, Object> attributes = new LinkedHashMap<>();

        if (StringUtils.isNotBlank(this.ctaLabel) && StringUtils.isNotBlank(this.ctaUrl))
        {
            Map<String, Object> primarycta = new LinkedHashMap<>();
            primarycta.put(Constants.TEXT_KEY, ctaLabel);
            primarycta.put(Constants.URL_KEY, ctaUrl);

            setTargetKeyInPrimaryCta(primarycta, ctaTarget);
            attributes.put(Constants.PRIMARY_CTA_KEY, primarycta);
        }

        attributes.put(Constants.TYPE_KEY, size);
        attributes.put(Constants.TITLE_KEY, heading);
        attributes.put(Constants.DESCRIPTION_KEY, description);
        attributes.put(Constants.PRICE_KEY, price);

        badgeMap.put(Constants.POSITION_KEY, badgePosition);
        badgeMap.put(Constants.LABEL_KEY, badgelabel);

        attributes.put(Constants.BADGE_KEY, badgeMap);
        attributes.put(Constants.IMAGE_KEY, getImageMap(this.imageRef, this.imageAltText));
        attributes.put(Constants.THEME_KEY, theme);
        if (StringUtils.equals(size, "single"))
        {
            attributes.remove(Constants.IMAGE_KEY);
        }

        return attributes;
    }
    /**
     * This is to get component title
     * 
     * @return
     */
    @Override
    public String getComponentTitle()
    {
        return COMPONENT_TITLE;
    }
}
