package com.ftd.platform.core.workflows;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.jcr.RepositoryException;

import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.ParticipantStepChooser;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowData;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.ftd.platform.core.constants.Constants;

/**
 * This class is used to choose participant group dynamically based on the payload
 * 
 * @author prabasva
 *
 */
@Component(
        service = { ParticipantStepChooser.class },
        property = { Constants.SERVICE_DESCRIPTION_PROPERTY, Constants.SERVICE_VENDOR_PROPERTY,
                Constants.CHOOSER_LABEL_PROPERTY })
public class DynamicParticipantChooser implements ParticipantStepChooser
{

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Reference
    private ResourceResolverFactory resolverFactory;

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.adobe.granite.workflow.exec.ParticipantStepChooser#getParticipant(com.adobe.granite.workflow.exec.WorkItem,
     * com.adobe.granite.workflow.WorkflowSession, com.adobe.granite.workflow.metadata.MetaDataMap)
     */
    @Override
    public String getParticipant(WorkItem workItem, WorkflowSession session, MetaDataMap metaDataMap)
            throws WorkflowException
    {

        WorkflowData workflowdata = workItem.getWorkflowData();
        String pagePath = workflowdata.getPayload().toString();
        String[] pagePathTokens = pagePath.split(Constants.FORWARD_SLASH_DELIMETER);
        String workflowProcessArguments = metaDataMap.get(Constants.PROCESS_ARGS, String.class);
        String brand = pagePath.contains(Constants.EXPERIENCE_FRAGMENTS_CONSTANT)
                ? pagePathTokens[3]
                : pagePathTokens[2];
        String approverGroupId = brand.concat(Constants.HYPHEN_CONSTANT).concat(workflowProcessArguments);
        String groupName;
        log.debug("Process Arguments :: {} ", workflowProcessArguments);

        if (isGroupIdExist(approverGroupId))
        {
            groupName = approverGroupId;
        } else
        {
            groupName = workItem.getWorkflow().getInitiator();
        }
        log.debug("Platform User Group Info :: {}", groupName);

        return groupName;
    }

    /**
     * This method return true If group id exists
     * 
     * @param approverGroupId - contains user group id
     * @return Boolean
     */
    private Boolean isGroupIdExist(String approverGroupId)
    {
        ResourceResolver resourceResolver;
        Boolean isGroupIdExist = false;

        Map<String, Object> userMapper = new HashMap<>();
        userMapper.put(ResourceResolverFactory.SUBSERVICE, Constants.SYSTEM_USER);

        try
        {
            resourceResolver = resolverFactory.getServiceResourceResolver(userMapper);
            final UserManager userManager = resourceResolver.adaptTo(UserManager.class);
            if (Objects.nonNull(userManager))
            {
                isGroupIdExist = userManager.getAuthorizable(approverGroupId).isGroup();
            }
        } catch (LoginException | RepositoryException e)
        {
            log.debug("Exception occurs :: Due to systemuser/usergroup not exists : {}", e);
        }

        return isGroupIdExist;
    }

}
