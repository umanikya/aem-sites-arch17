package com.ftd.platform.core.models;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.ExporterOption;
import org.apache.sling.models.annotations.Model;

import com.ftd.platform.core.beans.DeliveryDetailsBean;
import com.ftd.platform.core.constants.Constants;
import com.ftd.platform.core.utils.Utils;

@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = "/apps/web/platform/components/content/deliverydetails")
@Exporter(
        name = "jackson",
        extensions = "json",
        options = { @ExporterOption(
                name = "SerializationFeature.WRITE_DATES_AS_TIMESTAMPS",
                value = "true") })
public class DeliveryDetailsModel extends BaseModel
{
    @Inject
    private Resource deliveryDetails;
    
    private static final String COMPONENT_TITLE = "Delivery Details";

    /**
     * This method returns the component identifier.
     * 
     * @return
     */
    @Override
    public String getComponent()
    {
        return Constants.DELIVERY_DETAILS;
    }

    /**
     * This method constructs the attributes map object and returns the same.
     * 
     * @return
     */
    public Map<String, Object> getAttributes()
    {
        Map<String, Object> attributes = new LinkedHashMap<>();
        attributes.put(Constants.OCCASIONS, Utils.getBeanDataFromResource(deliveryDetails, DeliveryDetailsBean.class));
        return attributes;
    }

    /**
     * This is to get component title
     * 
     * @return
     */
    @Override
    public String getComponentTitle()
    {
        return COMPONENT_TITLE;
    }
    
}
