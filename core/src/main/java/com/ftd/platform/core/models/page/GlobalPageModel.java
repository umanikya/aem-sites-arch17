package com.ftd.platform.core.models.page;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ftd.platform.core.beans.PageSEOBean;
import com.ftd.platform.core.constants.Constants;

/**
 * Model class for Global Page Component
 *
 */
@Model(
        adaptables = SlingHttpServletRequest.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = Constants.GLOBAL_PAGE_MODEL_RESOURCE_TYPE)
@Exporter(
        name = Constants.JACKSON_KEY,
        extensions = Constants.JSON_KEY)
public class GlobalPageModel extends PageBaseModel
{
    @ChildResource(
            via = Constants.RESOURCE)
    protected Resource headerResource;

    @ChildResource(
            via = Constants.RESOURCE)
    protected Resource footerResource;
    
    @ChildResource(
            via = Constants.RESOURCE)
    protected Resource modalResource;

    /**
     * This method initializes the components list object
     */
    @PostConstruct
    public void init()
    {
        processComponent(headerResource, Constants.HEADER_LOCATION);
        processComponent(footerResource, Constants.FOOTER_LOCATION);
        if (null != modalResource)
        {
            processComponent(modalResource, Constants.MODAL_LOCATION);
        }
    }

    /**
     * Gets components for header section.
     * 
     * @return
     */
    public List<Object> getHeaderComponents()
    {
        return headerComponents;
    }
    
    /**
     * Gets components for modal section.
     * 
     * @return
     */
    public List<Object> getModalComponents()
    {
        return modalComponents;
    }

    /**
     * Gets components for footer section.
     * 
     * @return
     */
    public List<Object> getFooterComponents()
    {
        return footerComponents;
    }

    /**
     * Gets the layout.
     * 
     * @return
     */
    @Override
    public String getLayout()
    {
        return Constants.GLOBAL_LAYOUT_TITLE;
    }

    /**
     * Gets the generic body component, overridden to hide this attribute.
     * 
     * @return
     */
    @JsonIgnore
    public List<Object> getComponents()
    {
        return null;
    }

    /**
     * Gets SEO mapping elements from page properties, overridden to hide this attribute.
     * 
     * @return
     */
    @JsonIgnore
    public List<PageSEOBean> getMetaTags()
    {
        return null;
    }
}
