package com.ftd.platform.core.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.ExporterOption;
import org.apache.sling.models.annotations.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ftd.platform.core.constants.Constants;
import com.ftd.platform.core.utils.Utils;

/**
 * Sling Model for Promotional Banner
 * 
 */
@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = Constants.PROMOTIONAL_BANNER_RESOURCE_TYPE)
@Exporter(
        name = Constants.JACKSON_KEY,
        extensions = Constants.JSON_KEY,
        options = { @ExporterOption(
                name = Constants.SLING_MODEL_DEFAULT_ORDER,
                value = Constants.FALSE_KEY) })
public class PromotionalBannerModel extends BaseModel
{
    @Inject
    @JsonIgnore
    public Resource promotionText;

    @Inject
    private String changeRef;

    @Inject
    private String ctaURL = "";

    @Inject
    private String ctaLabel = "";

    @Inject
    private String[] hideIn = {};

    @Inject
    private String paramOne;

    @Inject
    private String paramTwo;
    
    @Inject
    private String theme;
    
    @Inject
    private String modalId;
    
    @Inject
    private String termsNCLabel;
    
    @Inject
    private String termsNCFontColor;
    
    @Inject
    private String tooltipLabel;
    
    @Inject 
    private String tooltipDescription;
    
    @Inject
    private String backgroundColor;
    
    @Inject
    private String fontColor;
    
    @Inject
    private String partnerLogo;
    
    @Inject
    private String telephone;

    @Inject
    @Default(
            values = Constants.PROMOTION_BANNER_TITLE)
    private String component;
    private static final String COMPONENT_TITLE = "Promotional Banner";
    
    /**
     * This method returns the component identifier.
     * 
     * @return string for component identifier
     */
    @Override
    public String getComponent()
    {
        return Constants.PROMOTION_BANNER_TITLE;
    }

    /**
     * This method constructs the attributes map object for Promotional banner
     * 
     * @return
     */
    public Map<String, Object> getAttributes()
    {
        Map<String, Object> attributes = new LinkedHashMap<>();
        Map<String, Object> primaryCtaAttributesMap = new LinkedHashMap<>();
        List<Object> paramList = new LinkedList<>();
        paramList.add(paramOne);
        paramList.add(paramTwo);

        attributes.put("hideIn", Utils.getValueFromTag(hideIn));
        if (null != promotionText)
        {
            attributes.put("promotionText", getPromotionText(promotionText));
        }
        attributes.put("componentTheme", this.theme);
        attributes.put("partnerLogo", partnerLogo);
        attributes.put("telephone", telephone);
        if (StringUtils.isNotBlank(this.modalId))
        {
            Map<String, Object> termsAndCondition = new HashMap<>();
            termsAndCondition.put("modalId", this.modalId);
            termsAndCondition.put("label", this.termsNCLabel);
            termsAndCondition.put("color", this.termsNCFontColor);
            
            attributes.put("termsAndCondition", termsAndCondition);
        }

        if (StringUtils.isNotBlank(this.tooltipLabel) && StringUtils.isNotBlank(this.tooltipDescription))
        {
            Map<String, Object> tooltip = new HashMap<>();
            tooltip.put("label", this.tooltipLabel);
            tooltip.put("description", this.tooltipDescription);
            attributes.put("tooltip", tooltip);
        }
        if (StringUtils.isNotBlank(this.backgroundColor))
        {
            attributes.put("backgroundColor", this.backgroundColor);
        }
        if (StringUtils.isNotBlank(this.fontColor))
        {
            attributes.put("fontColor", this.fontColor);
        }

        if (changeRef != null && changeRef.equals(Constants.SAME_PAGE_CONSTANT))
        {
            primaryCtaAttributesMap.put(Constants.URL_KEY, "#");
            attributes.put("changeRef", false);
        } else
        {
            primaryCtaAttributesMap.put(Constants.URL_KEY, ctaURL);
            attributes.put("changeRef", true);
        }

        primaryCtaAttributesMap.put(Constants.TEXT_KEY, ctaLabel);

        if (changeRef != null && changeRef.equals(Constants.NO_VALUE))
        {
            primaryCtaAttributesMap.remove("params");
        } else
        {
            primaryCtaAttributesMap.put("params", paramList);
        }

        attributes.put(Constants.PRIMARY_CTA_KEY, primaryCtaAttributesMap);

        return attributes;
    }

    /**
     * Gets promotion text.
     * 
     * @return
     */
    private List<Map<String, String>> getPromotionText(Resource promotionText)
    {
        List<Map<String, String>> promotionTextList = new ArrayList<>();

        Iterator<Resource> iterator = promotionText.listChildren();

        while (iterator.hasNext())
        {
            Resource childResource = iterator.next();
            if (childResource != null)
            {
                Map<String, String> promotionTextMap = new LinkedHashMap<>();

                String title = childResource.getValueMap().get("heading", "");
                if (StringUtils.isNotBlank(title))
                {
                    promotionTextMap.put("title", title);
                }

                String description = childResource.getValueMap().get("description", "");
                if (StringUtils.isNotBlank(description))
                {
                    promotionTextMap.put("description", description);
                }
                promotionTextList.add(promotionTextMap);
            }
        }

        return promotionTextList;
    }
    
    /**
     * This is to get component title
     * 
     * @return
     */
    @Override
    public String getComponentTitle()
    {
        return COMPONENT_TITLE;
    }
}
