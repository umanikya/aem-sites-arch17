package com.ftd.platform.core.beans;

import java.util.Arrays;

/**
 * Component dialog filter Felix configuration bean.
 * 
 */
public class ComponentDialogFilterBean
{
    private Boolean enabled;

    private String ccHeadHTMLTagValue;
    private String ccBodyHTMLTagValue;
    private String ccExcludeBodyTagValue;

    private String[] excludePaths;

    public final Boolean getEnabled()
    {
        return enabled;
    }

    public final void setEnabled(Boolean enabled)
    {
        this.enabled = enabled;
    }

    public final String getCcHeadHTMLTagValue()
    {
        return ccHeadHTMLTagValue;
    }

    public final void setCcHeadHTMLTagValue(String ccHeadHTMLTagValue)
    {
        this.ccHeadHTMLTagValue = ccHeadHTMLTagValue;
    }

    public final String getCcBodyHTMLTagValue()
    {
        return ccBodyHTMLTagValue;
    }

    public final void setCcBodyHTMLTagValue(String ccBodyHTMLTagValue)
    {
        this.ccBodyHTMLTagValue = ccBodyHTMLTagValue;
    }

    public final String[] getExcludePaths()
    {
        return excludePaths;
    }

    public final void setExcludePaths(String[] excludePaths)
    {
        this.excludePaths = excludePaths;
    }

    public String getCcExcludeBodyTagValue()
    {
        return ccExcludeBodyTagValue;
    }

    public void setCcExcludeBodyTagValue(String ccExcludeBodyTagValue)
    {
        this.ccExcludeBodyTagValue = ccExcludeBodyTagValue;
    }

    @Override
    public String toString()
    {
        return "ComponentDialogFilterBean [enabled=" + enabled + ", ccHeadHTMLTagValue=" + ccHeadHTMLTagValue
                + ", ccBodyHTMLTagValue=" + ccBodyHTMLTagValue + ", ccExcludeBodyTagValue=" + ccExcludeBodyTagValue
                + ", excludePaths=" + Arrays.toString(excludePaths) + "]";
    }

}
