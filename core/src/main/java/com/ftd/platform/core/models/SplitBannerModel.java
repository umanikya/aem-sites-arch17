package com.ftd.platform.core.models;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.ExporterOption;
import org.apache.sling.models.annotations.Model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.ftd.platform.core.constants.Constants;

/**
 * Sling Model for Split Banner
 * 
 * @author prabasva
 *
 */
@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = Constants.SPLIT_VIDEO_BANNER_RESOURCE_TYPE)
@Exporter(
        name = Constants.JACKSON_KEY,
        extensions = Constants.JSON_KEY,
        options = { @ExporterOption(
                name = Constants.SLING_MODEL_DEFAULT_ORDER,
                value = Constants.FALSE_KEY) })
public class SplitBannerModel extends BaseModel
{

    @Inject
    @Default(
            values = StringUtils.EMPTY)
    private String displayOptions;

    @Inject
    private String leftImageURLRef;

    @Inject
    private String leftImageAltText;

    @Inject
    private String leftImageHeading;

    @Inject
    private String leftImageButtonLabel;

    @Inject
    private String leftImageButtonLink;

    @Inject
    private String leftImageLinkTarget;

    @Inject
    private String rightImageURLRef;

    @Inject
    private String rightImageAltText;

    @Inject
    private String rightImageHeading;

    @Inject
    private String rightImageButtonLabel;

    @Inject
    private String rightImageButtonLink;

    @Inject
    private String rightImageLinkTarget;

    @Inject
    private String leftVideoText;

    @Inject
    private String leftVideoAsset;

    @Inject
    private String leftServerURL;

    @Inject
    private String leftVideoContentURL;

    @Inject
    private String leftVideoEmailURL;

    @Inject
    private String leftVideoServerURL;

    @Inject
    private String leftVideoPosterImage;

    @Inject
    private String rightVideoText;

    @Inject
    private String rightVideoAsset;

    @Inject
    private String rightServerURL;

    @Inject
    private String rightVideoContentURL;

    @Inject
    private String rightVideoEmailURL;

    @Inject
    private String rightVideoServerURL;

    @Inject
    private String rightVideoPosterImage;

    @Inject
    @Default(
            values = Constants.SPLIT_VIDEO_BANNER_KEY)
    private String component;
    private static final String COMPONENT_TITLE = "Split Banner";
    /**
     * This method returns the component identifier.
     * 
     * @return
     */
    @Override
    public String getComponent()
    {
        return component;
    }

    /**
     * This method constructs the attributes map object for image and video display based on condition and returns the
     * same.
     * 
     * @return
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Map<String, Object> getAttributes()
    {

        Map<String, Object> componentAttributes = null;
        componentAttributes = new LinkedHashMap<>();
        List<Object> bannerList = new ArrayList<>();

        if (displayOptions.equals(Constants.VIDEO_RIGHT_KEY))
        {

            bannerList.add(getImageAttributesMap(leftImageHeading, leftImageAltText, leftImageURLRef,
                    leftImageButtonLabel, leftImageButtonLink, leftImageLinkTarget));
            bannerList.add(getVideoAttributesMap(rightVideoText, rightVideoAsset, rightServerURL, rightVideoContentURL,
                    rightVideoEmailURL, rightVideoServerURL, rightVideoPosterImage));

        } else if (displayOptions.equals(Constants.VIDEO_LEFT_KEY))
        {

            bannerList.add(getVideoAttributesMap(leftVideoText, leftVideoAsset, leftServerURL, leftVideoContentURL,
                    leftVideoEmailURL, leftVideoServerURL, leftVideoPosterImage));
            bannerList.add(getImageAttributesMap(rightImageHeading, rightImageAltText, rightImageURLRef,
                    rightImageButtonLabel, rightImageButtonLink, rightImageLinkTarget));

        } else
        {

            bannerList.add(getImageAttributesMap(leftImageHeading, leftImageAltText, leftImageURLRef,
                    leftImageButtonLabel, leftImageButtonLink, leftImageLinkTarget));
            bannerList.add(getImageAttributesMap(rightImageHeading, rightImageAltText, rightImageURLRef,
                    rightImageButtonLabel, rightImageButtonLink, rightImageLinkTarget));

        }

        componentAttributes.put(Constants.COMPONENTS_KEY, bannerList);
        return componentAttributes;
    }

    /**
     * This method constructs the attributes map object for image and returns the same.
     * 
     * @return
     */
    private Map<String, Object> getImageAttributesMap(String imageHeading, String imageAltText, String imageUrl,
            String imageButtonLabel, String imageButtonLink, String imageLinkTarget)
    {

        Map<String, Object> imageComponentMap = new LinkedHashMap<>();
        Map<String, Object> imagePropertiesMap = new LinkedHashMap<>();
        Map<String, Object> primaryCtaAttributesMap = new LinkedHashMap<>();
        primaryCtaAttributesMap.put(Constants.TEXT_KEY, imageButtonLabel);
        primaryCtaAttributesMap.put(Constants.URL_KEY, imageButtonLink);

        setTargetKeyInPrimaryCta(primaryCtaAttributesMap, imageLinkTarget);

        imagePropertiesMap.put(Constants.TITLE_KEY, imageHeading);
        imagePropertiesMap.put(Constants.IMAGE_KEY, getImageMap(imageUrl, imageAltText));
        imagePropertiesMap.put(Constants.PRIMARY_CTA_KEY, primaryCtaAttributesMap);

        imageComponentMap.put(Constants.COMPONENT_KEY, Constants.BANNER_COMPONENT_TITLE);
        imageComponentMap.put(Constants.ATTRIBUTES_KEY, imagePropertiesMap);

        return imageComponentMap;
    }

    /**
     * This method constructs the attributes map object for video and returns the same.
     * 
     * @return
     */
    private Map<String, Object> getVideoAttributesMap(String videoText, String videoAsset, String serverURL,
            String videoContentURL, String videoEmailURL, String videoServerURL, String videoPosterImage)
    {

        Map<String, Object> videoComponentMap = new LinkedHashMap<>();
        Map<String, Object> videoPropertiesMap = new LinkedHashMap<>();
        videoPropertiesMap.put(Constants.TEXT_KEY, videoText);
        videoPropertiesMap.put(Constants.ASSET_KEY, videoAsset);
        videoPropertiesMap.put(Constants.SERVER_URL_KEY, serverURL);
        videoPropertiesMap.put(Constants.CONTENT_URL_KEY, videoContentURL);
        videoPropertiesMap.put(Constants.EMAIL_URL_KEY, videoEmailURL);
        videoPropertiesMap.put(Constants.VIDEO_SERVER_URL_KEY, videoServerURL);
        videoPropertiesMap.put(Constants.POSTER_IMAGE_KEY, videoPosterImage);

        videoComponentMap.put(Constants.COMPONENT_KEY, Constants.VIDEO_KEY);
        videoComponentMap.put(Constants.ATTRIBUTES_KEY, videoPropertiesMap);
        return videoComponentMap;
    }
    /**
     * This is to get component title
     * 
     * @return
     */
    @Override
    public String getComponentTitle()
    {
        return COMPONENT_TITLE;
    }
}
