package com.ftd.platform.core.models;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;

import com.ftd.platform.core.constants.Constants;
import com.ftd.platform.core.utils.Utils;

/**
 * Sling Model for Product Grids component
 * 
 * @author nikhil
 *
 */
@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = "web/platform/components/content/productgrids")
@Exporter(
        name = "jackson",
        extensions = "json")
public class ProductGridsModel extends BaseModel
{

    @Inject
    private String prodSizeSelector;

    @Inject
    private String idSelectorGrid;

    @Inject
    private String titleTwo;

    @Inject
    private String titleThree;

    @Inject
    private String description;

    @Inject
    private String buttonLabelTwo;

    @Inject
    private String buttonLinkTwo;

    @Inject
    private String buttonLinkTargetTwo;

    @Inject
    private String buttonLabelThree;

    @Inject
    private String buttonLinkThree;

    @Inject
    private String buttonLinkTargetThree;

    @Inject
    private String catIdTwo;

    @Inject
    private Resource prodIdTwo;

    @Inject
    private String catIdThree;

    @Inject
    private Resource prodIdThree;
    
    @Inject
    String analyticsTrackingId;
    
    private static final String COMPONENT_TITLE = "Product Grids";

    /**
     * This method returns the component identifier.
     * 
     * @return
     */
    @Override
    public String getComponent()
    {
        if (prodSizeSelector != null && prodSizeSelector.equals(Constants.TWO_VALUE))
            return Constants.TWO_PRODUCT_GRIDS_TITLE;
        else
            return Constants.THREE_PRODUCT_GRIDS_TITLE;

    }

    /**
     * This method constructs the attributes map object and returns the same.
     * 
     * @return
     */

    public Map<String, Object> getAttributes()
    {

        Map<String, Object> attributes = new LinkedHashMap<>();
        Map<String, Object> banner = new LinkedHashMap<>();
        List<String> prodIdlist = null;
        String categoryId = null;
        
        if (StringUtils.isNotBlank(this.analyticsTrackingId))
        {
            attributes.put("analyticsTrackingId", this.analyticsTrackingId);
        }

        if (prodSizeSelector != null && prodSizeSelector.equals(Constants.TWO_VALUE))
        {

            banner.put(Constants.TITLE_KEY, this.titleTwo);
            attributes.put("productCount", 2);
            banner.put(Constants.DESCRIPTION_KEY, this.description);
            
            // Only set CTA when both button label and link are present
            if (StringUtils.isNotBlank(this.buttonLabelTwo) && StringUtils.isNotBlank(this.buttonLinkTwo))
            {
                banner.put(Constants.PRIMARY_CTA_KEY,
                        getPrimaryCta(this.buttonLabelTwo, this.buttonLinkTwo, this.buttonLinkTargetTwo));
            }
            attributes.put(Constants.BANNER_COMPONENT_TITLE, banner);
            prodIdlist = Utils.getProductListFromResource(this.prodIdTwo, false);
            categoryId = Utils.getCategoryIdFromTag(catIdTwo);
            Utils.setProductIdOrCategoryIdInMap(idSelectorGrid, categoryId, prodIdlist, attributes);

            return attributes;

        } else
        {
            attributes.put(Constants.TITLE_KEY, this.titleThree);
            attributes.put("productCount", 3);
            prodIdlist = Utils.getProductListFromResource(this.prodIdThree, true);
            categoryId = Utils.getCategoryIdFromTag(catIdThree);
            Utils.setProductIdOrCategoryIdInMap(idSelectorGrid, categoryId, prodIdlist, attributes);
            
            // Only set CTA when both button label and link are present
            if (StringUtils.isNotBlank(this.buttonLabelThree) && StringUtils.isNotBlank(this.buttonLinkThree))
            {
                attributes.put(Constants.PRIMARY_CTA_KEY,
                        getPrimaryCta(this.buttonLabelThree, this.buttonLinkThree, this.buttonLinkTargetThree));
            }
            return attributes;
        }

    }

    /**
     * It Constructs the Map having data required for primaryCTA
     * 
     * @param buttonLabel
     * @param buttonLink
     * @param buttonLinkTarget
     * @return
     */
    public Map<String, Object> getPrimaryCta(String buttonLabel, String buttonLink, String buttonLinkTarget)
    {
        Map<String, Object> primaryCTA = new LinkedHashMap<>();

        primaryCTA.put(Constants.TEXT_KEY, buttonLabel);
        primaryCTA.put(Constants.URL_KEY, buttonLink);

        setTargetKeyInPrimaryCta(primaryCTA, buttonLinkTarget);
        
        return primaryCTA;
    }
    /**
     * This is to get component title
     * 
     * @return
     */
    @Override
    public String getComponentTitle()
    {
        return COMPONENT_TITLE;
    }
}
