package com.ftd.platform.core.servlets;

import java.io.IOException;
import java.util.Objects;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.ftd.platform.core.services.SeoMetadataService;

@Component(
        service = { Servlet.class },
        property = { "service.description=Get AEM SEO Metadata Servlet",
                "sling.servlet.paths=/bin/aemseometadata", "sling.servlet.methods = GET",
                "sling.auth.requirements = -{/bin/aemseometadata}" })
public class GetSeoMetadataServlet extends SlingSafeMethodsServlet
{
    private static final long serialVersionUID = 9179895523313781531L;

    @Reference
    SeoMetadataService seoMetadataService;

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
     * org.apache.sling.api.SlingHttpServletResponse) doGet method will get the JSON Response from Service
     */
    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServletException, IOException
    {
        String siteId = Objects.isNull(request.getParameter("siteId")) ? "" : request.getParameter("siteId");
        String pageType = Objects.isNull(request.getParameter("pageType")) ? "" : request.getParameter("pageType");
        String pageId = Objects.isNull(request.getParameter("pageId")) ? "" : request.getParameter("pageId");
        
        response.setContentType("application/json");
        response.getWriter().write(seoMetadataService.getSeoMetadata(siteId, pageType, pageId));
    }

}
