package com.ftd.platform.core.models;

import com.ftd.platform.core.beans.CategoryIdBean;
import com.ftd.platform.core.constants.Constants;
import com.ftd.platform.core.utils.Utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.ExporterOption;
import org.apache.sling.models.annotations.Model;

import javax.inject.Inject;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Sling Model for GiftFinder
 * 
 * @author abhranja1
 *
 */
@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = "/apps/web/platform/components/content/giftfinder")
@Exporter(
        name = "jackson",
        extensions = "json",
        options = { @ExporterOption(
                name = "SerializationFeature.WRITE_DATES_AS_TIMESTAMPS",
                value = "true") })
public class GiftFinderModel extends BaseModel
{

    private static final String GIFT_CATEGORY_ID_KEY = "giftCategoryId";

    @Inject
    private String heading;

    @Inject
    private String helpLabel;

    @Inject
    private String helpText;

    @Inject
    private String buttonlabel;

    @Inject
    private String categoryId;

    @Inject
    private String idSelectorGift = Constants.NO_VALUE;

    @Inject
    private Resource categoryIdGift;

    @Inject
    private String version;

    @Inject
    private boolean disableLocationType;
    
    private static final String COMPONENT_TITLE = "Gift Finder";

    /**
     * This method returns the component identifier.
     * 
     * @return
     */
    @Override
    public String getComponent()
    {
        return Constants.GIFT_FINDER_PLACEHOLDER_TITLE;
    }

    /**
     * This method constructs the attributes map object and returns the same.
     * 
     * @return
     */
    public Map<String, Object> getAttributes()
    {
        Map<String, Object> attributes = new LinkedHashMap<>();

        attributes.put(Constants.HEADING_KEY, this.heading);
        attributes.put(Constants.HELP_LABEL_KEY, this.helpLabel);
        attributes.put(Constants.HELP_TEXT_KEY, this.helpText);
        attributes.put(Constants.BUTTON_LABEL_KEY, this.buttonlabel);
        attributes.put(GIFT_CATEGORY_ID_KEY, categoryId);
        attributes.put(Constants.VERSION_KEY, this.version);
        if (this.disableLocationType)
        {
            attributes.put("showLocationType", false);
        } else
        {
            attributes.put("showLocationType", true);
        }

        if (StringUtils.equals("yes", idSelectorGift))
        {
            List<CategoryIdBean> catIdlist = Utils.getBeanDataFromResource(categoryIdGift, CategoryIdBean.class);

            attributes.put(Constants.CATEGORIES_KEY, catIdlist);
        }

        return attributes;
    }

    /**
     * Setter method for heading variable
     * 
     * @param heading
     */
    public void setHeading(String heading)
    {
        this.heading = heading;
    }

    /**
     * Setter method for helpLabel variable
     * 
     * @param helpLabel
     */
    public void setHelpLabel(String helpLabel)
    {
        this.helpLabel = helpLabel;
    }

    /**
     * Setter method for helpText variable
     * 
     * @param helpText
     */
    public void setHelpText(String helpText)
    {
        this.helpText = helpText;
    }

    /**
     * Setter method for buttonLabel variable
     * 
     * @param buttonlabel
     */
    public void setButtonlabel(String buttonlabel)
    {
        this.buttonlabel = buttonlabel;
    }

    /**
     * Setter method for categoryId variable
     * 
     * @param categoryid
     */
    public void setCategoryId(String categoryId)
    {
        this.categoryId = categoryId;
    }
    /**
     * This is to get component title
     * 
     * @return
     */
    @Override
    public String getComponentTitle()
    {
        return COMPONENT_TITLE;
    }
}
