package com.ftd.platform.core.services;

/**
 * @author abhranja1
 *
 */
public interface ExperienceContentPageListService
{
    public String getExperienceContentPageList(String siteId);
}
