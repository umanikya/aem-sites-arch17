package com.ftd.platform.core.beans;

import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.ftd.platform.core.constants.Constants;

/**
 * This is the Bean class holding the list of Icons required for Video brand content component.
 * 
 * @author chakrish1
 *
 */
@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class IconBean
{
    // Ideally should be loaded as in memory config
    private static final String OLD_SCENE7_DOMAIN = "//s7d5.scene7.com/";
    private static final String NEW_SCENE7_DOMAIN = "//s7img.ftdi.com/";
    
    Map<String, String> image = new HashMap<>();

    @Inject
    private String title;
    
    @Inject
    private String svgIcon;

    @Inject
    public String text;

    @Inject
    private String iconAltText;

    @Inject
    private String iconLink;

    /**
     * Getter method for Image
     * 
     * @return
     */
    public Map<String, String> getImage()
    {
        image.put(Constants.URL_KEY, StringUtils.replace(this.iconLink, OLD_SCENE7_DOMAIN, NEW_SCENE7_DOMAIN));
        image.put(Constants.ALT_KEY, this.iconAltText);
        return image;
    }

    /**
     * Setter method for iconAltTxt variable
     * 
     * @param iconAltTxt
     */
    public void setIconAltText(String iconAltText)
    {
        this.iconAltText = iconAltText;
    }

    /**
     * Setter method for iconLink variable
     * 
     * @param iconLink
     */
    public void setIconLink(String iconLink)
    {
        this.iconLink = iconLink;
    }

    /**
     * Gets the Image Title
     * 
     * @return
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * Setter method for Title
     * 
     * @param title
     */
    public void setTitle(String title)
    {
        this.title = title;
    }
    
    /**
     * Gets the SVG icon
     * 
     * @return
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getSvgIcon()
    {
        return svgIcon;
    }

    /**
     * Sets method for SVG icon
     * 
     * @param svgIcon
     */
    public void setSvgIcon(String svgIcon)
    {
        this.svgIcon = svgIcon;
    }

    /**
     * Getter method of Image Description Text
     * 
     * @return
     */
    public String getText()
    {
        return text;
    }

    /**
     * Setter method for Image Description Text
     * 
     * @param text
     */
    public void setText(String text)
    {
        this.text = text;
    }

}
