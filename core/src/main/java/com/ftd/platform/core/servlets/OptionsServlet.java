package com.ftd.platform.core.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.Servlet;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceMetadata;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.api.wrappers.ValueMapDecorator;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.ui.components.ds.DataSource;
import com.adobe.granite.ui.components.ds.SimpleDataSource;
import com.adobe.granite.ui.components.ds.ValueMapResource;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ftd.platform.core.utils.ComponentOptionsVO;

@Component(
        service = Servlet.class,
        configurationPolicy = ConfigurationPolicy.REQUIRE,
        immediate = true,
        property = { "service.description" + "= Service to get drop down list", "sling.servlet.methods = GET",
                "sling.servlet.paths=" + "/apps/web/optionList" })
@Designate(
        ocd = OptionsServlet.Config.class)
public class OptionsServlet extends SlingSafeMethodsServlet
{

    @ObjectClassDefinition(
            name = "options servlet",
            description = "options servlet to populate dropdown fields in dialogs")
    public static @interface Config
    {

        @AttributeDefinition(
                name = "default")
        String default_path() default "/content/data";

    }
    private String defaultoptions;
    private static final Logger logger = LoggerFactory.getLogger(OptionsServlet.class);
    private static final long serialVersionUID = 1L;
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
    {
        try
        {
            List<Resource> optionList = new ArrayList<>();
            String url = request.getRequestURI();
            ResourceResolver resourceResolver = request.getResourceResolver();
            String[] substrings = url.split("/_cq_dialog.html/");
            String componentName = StringUtils.substringAfterLast(substrings[0], "/");
            String siteoptions = "/content/data/" + StringUtils.split(substrings[1], "/")[1] + "/components/" + componentName;
            Resource siteResource = resourceResolver.getResource(siteoptions);
            if (null != siteResource && siteResource.getValueMap().containsKey("dropdownList"))
            {
                setdata(siteResource, request, optionList);
            } else
            {
                Resource resource = resourceResolver.getResource(defaultoptions+"/components/"+componentName);
                setdata(resource, request, optionList);
            }
            DataSource dataSource = new SimpleDataSource(optionList.iterator());
            request.setAttribute(DataSource.class.getName(), dataSource);

        } catch (IOException e)
        {
            logger.error(e.getMessage());
        }
        catch(Exception e)
        {
            logger.error(e.getMessage());
        }
        
    }
    public void setdata(Resource resource, SlingHttpServletRequest request, List<Resource> dropdownList)
            throws JsonParseException, JsonMappingException, IOException
    {
        if (resource != null)
        {
            ValueMap valuemap = resource.getValueMap();
            String[] jsonInString = valuemap.get("dropdownList", String[].class);
            ObjectMapper mapper = new ObjectMapper();
            ComponentOptionsVO componentOptionsVO;
            for (String jsonString : jsonInString)
            {
                componentOptionsVO = mapper.readValue(jsonString, ComponentOptionsVO.class);
                ValueMap vm = new ValueMapDecorator(new HashMap<String, Object>());
                vm.put("text", componentOptionsVO.getText());
                vm.put("value", componentOptionsVO.getValue());
                dropdownList.add(new ValueMapResource(request.getResourceResolver(), new ResourceMetadata(),
                        "nt:unstructured", vm));
            }

        }
    }

    @Activate
    protected void activate(Config config)
    {
        this.defaultoptions = config.default_path();
    }
}
