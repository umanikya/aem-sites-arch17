package com.ftd.platform.core.models;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.ftd.platform.core.beans.IconBean;
import com.ftd.platform.core.constants.Constants;

/**
 * 
 * Sling Model for Video Brand Content component
 * 
 * @author chakrish1
 *
 */
@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = Constants.VIDEO_BRAND_CONTENT_RESOURCE_TYPE)
@Exporter(
        name = Constants.JACKSON_KEY,
        extensions = Constants.JSON_KEY)
@JsonPropertyOrder({ Constants.COMPONENT_KEY, Constants.ATTRIBUTES_KEY })
public class VideoBrandContentModel extends BaseModel
{

    @Inject
    private String title;

    @Inject
    private String description;

    @Inject
    private String iconHeading;

    @Inject
    private String iconDescription;

    @Inject
    private String theme;

    @Inject
    private String buttonLabel;

    @Inject
    private String buttonLink;

    @Inject
    private String buttonLinkTarget;

    @Inject
    private String videoAsset;

    @Inject
    private String serverUrl;

    @Inject
    private String videContentUrl;

    @Inject
    private String videoEmailUrl;

    @Inject
    private String videoServerUrl;

    @Inject
    private String videoPosterImage;

    @Inject
    private Resource imageResource;

    Map<String, Object> attributes = new LinkedHashMap<>();
    private static final String COMPONENT_TITLE = "Video Brand Content";
    /**
     * This method returns the component identifier.
     * 
     * @return
     */
    @Override
    @JsonProperty(Constants.COMPONENT_KEY)
    public String getComponent()
    {
        return Constants.VIDEO_BRAND_CONTENT_TITLE;
    }

    /**
     * This method constructs the attributes map object and returns the same.
     * 
     * @return
     */
    @JsonProperty(Constants.ATTRIBUTES_KEY)
    public Map<String, Object> getAttributes()
    {
        if (StringUtils.isNotBlank(this.videoAsset))
        {
            Map<String, Object> video = new LinkedHashMap<>();

            video.put(Constants.ASSET_KEY, this.videoAsset);
            video.put(Constants.SERVER_URL_KEY, this.serverUrl);
            video.put(Constants.CONTENT_URL_KEY, this.videContentUrl);
            video.put(Constants.EMAIL_URL_KEY, this.videoEmailUrl);
            video.put(Constants.VIDEO_SERVER_URL_KEY, this.videoServerUrl);
            video.put(Constants.POSTER_IMAGE_KEY, this.videoPosterImage);

            attributes.put(Constants.VIDEO_KEY, video);
        }

        // Only set CTA when both button label and link are present
        if (StringUtils.isNotBlank(this.buttonLabel) && StringUtils.isNotBlank(this.buttonLink))
        {
            Map<String, Object> primaryCTA = new LinkedHashMap<>();

            primaryCTA.put(Constants.TEXT_KEY, this.buttonLabel);
            primaryCTA.put(Constants.URL_KEY, this.buttonLink);

            setTargetKeyInPrimaryCta(primaryCTA, buttonLinkTarget);

            attributes.put(Constants.PRIMARY_CTA_KEY, primaryCTA);
        }

        attributes.put(Constants.TITLE_KEY, this.title);
        attributes.put(Constants.TEXT_KEY, this.description);

        if (StringUtils.isNotBlank(this.iconHeading))
        {
            attributes.put("iconHeading", this.iconHeading);
        }
        if (StringUtils.isNotBlank(this.iconDescription))
        {
            attributes.put("iconDescription", this.iconDescription);
        }

        attributes.put("theme", this.theme);

        if (imageResource != null)
        {
            List<IconBean> iconList = new LinkedList<>();
            Iterator<Resource> iterator = imageResource.listChildren();
            while (iterator.hasNext())
            {
                Resource childResource = iterator.next();
                if (childResource != null)
                    iconList.add(childResource.adaptTo(IconBean.class));
            }
            attributes.put(Constants.BLOCK_KEY, iconList);
        }
        return attributes;
    }
    /**
     * This is to get component title
     * 
     * @return
     */
    @Override
    public String getComponentTitle()
    {
        return COMPONENT_TITLE;
    }
}
