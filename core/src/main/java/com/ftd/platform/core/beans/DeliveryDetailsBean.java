package com.ftd.platform.core.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class DeliveryDetailsBean
{
    @Inject
    @JsonProperty("text")
    private String label;

    @Inject
    private String value;

    @Inject
    @JsonIgnore
    private Resource giftMessage;

    public String getLabel()
    {
        return label;
    }

    public void setLabel(String label)
    {
        this.label = label;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public List<Map<String, String>> getGiftMessages()
    {
        if (giftMessage != null)
        {
            List<Map<String, String>> beanList = new ArrayList<>();
            Iterator<Resource> iterator = giftMessage.listChildren();
            while (iterator.hasNext())
            {
                Resource childResource = iterator.next();
                if (childResource != null)
                {
                    Map<String, String> messages = new HashMap<String, String>();
                    messages.put("message", (String) childResource.getValueMap().get("message"));
                    beanList.add(messages);
                }
            }

            return beanList;
        }

        return null;
    }

}
