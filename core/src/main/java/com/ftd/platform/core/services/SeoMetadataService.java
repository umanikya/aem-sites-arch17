package com.ftd.platform.core.services;

public interface SeoMetadataService
{
    String getSeoMetadata(String siteId, String pageType, String categoryId);
}
