package com.ftd.platform.core.services.impl;

import java.util.Date;
import java.util.Iterator;
import java.util.Objects;

import javax.inject.Inject;

import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.sling.api.resource.Resource;

import com.ftd.platform.core.beans.ReplicationDataBean;

import org.apache.sling.models.factory.ModelClassException;
import org.apache.sling.models.factory.ModelFactory;
import com.ftd.platform.core.services.ContentPageDataService;
import com.ftd.platform.core.services.ContentPageListService;
import com.ftd.platform.core.utils.ContentPageListUtil;
import com.ftd.platform.core.utils.Utils;

/**
 * This is a Service implementation class of the service ContentPageDataService.
 * 
 * @author sunkatep
 * 
 */
@Component(
        name = "ContentPageDataService", service = ContentPageDataService.class, immediate = true)
public class ContentPageDataServiceImpl implements ContentPageDataService
{
    @Reference
    private SlingSettingsService slingSettingsService;
    
    @Reference
    protected ModelFactory modelFactory;
    
    private static final Logger logger = LoggerFactory.getLogger(ContentPageListService.class);

    public String getStartDate(ReplicationDataBean replicationDataBean)
    {
        if (Utils.getEnvironment(slingSettingsService).contains("author"))
        {
            Date activateTime = ContentPageListUtil.getActivateTime(replicationDataBean.getLastModified(),
                    replicationDataBean.getLastReplicated(), replicationDataBean.getPublishDate(),
                    replicationDataBean.getUnPublishDate(), replicationDataBean.getLastReplicationAction());
            logger.debug("activateTime :: ", activateTime);
            if (activateTime != null)
                return Utils.getFormattedDate(activateTime);
            else
                return null;
        } else
        {
            return Utils.getFormattedDate(replicationDataBean.getLastModified());
        }
    }

    /**
     * This method gets the deactivation time in the String format
     * 
     * @return
     */
    public String getEndDate(ReplicationDataBean replicationDataBean)
    {
        if (Utils.getEnvironment(slingSettingsService).contains("author"))
        {
            Date deActivateTime = ContentPageListUtil.getDeActivateTime(replicationDataBean.getLastReplicated(),
                    replicationDataBean.getPublishDate(), replicationDataBean.getUnPublishDate(),
                    replicationDataBean.getLastReplicationAction());
            logger.debug("deActivateTime :: ", deActivateTime);
            if (deActivateTime != null)
                return Utils.getFormattedDate(deActivateTime);
            else
                return null;
        } else if (replicationDataBean.getUnPublishDate() != null)
            return Utils.getFormattedDate(replicationDataBean.getUnPublishDate());
        else
            return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.ftd.platform.core.services.ContentPageDataService#getReplicationDataFromResource()
     */
    public ReplicationDataBean getReplicationDataFromResource(ValueMap pageProperties)
    {
        ReplicationDataBean replicationDataBean = new ReplicationDataBean();
        replicationDataBean.setLastModified(pageProperties.get("cq:lastModified", Date.class));
        replicationDataBean.setPublishDate(pageProperties.get("cq:publishDate", Date.class));
        replicationDataBean.setUnPublishDate(pageProperties.get("cq:unPublishDate", Date.class));
        replicationDataBean.setLastReplicated(pageProperties.get("cq:lastReplicated", Date.class));
        replicationDataBean.setLastReplicationAction(pageProperties.get("cq:lastReplicationAction", String.class));

        logger.debug("ReplicationData Bean :: {}", replicationDataBean);

        return replicationDataBean;
    }

    @Override
    public Object getModelFromResource(Resource componentResource)
    {
        try
        {
            return modelFactory.getModelFromResource(componentResource);
        } catch (ModelClassException e)
        {
            logger.error("Component model class not found :: ", componentResource.getPath(), e);
        }

        return null;
    }
    
    /**
     * @return - it return the Model based on the resource
     */
    @Override
    public Object getModelJsonFromResource(Resource componentResource)
    {
        Object modelObject = null;
        if (Objects.nonNull(componentResource))
        {
            Iterator<Resource> resourceIterator = componentResource.listChildren();
            while (resourceIterator.hasNext())
            {
                Resource childResource = resourceIterator.next();
                modelObject = getModelFromResource(childResource);
            }
        }
        return modelObject;
    }

}
