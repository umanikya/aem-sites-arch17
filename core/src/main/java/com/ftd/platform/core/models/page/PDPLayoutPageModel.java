package com.ftd.platform.core.models.page;

import com.ftd.platform.core.constants.Constants;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;

/**
 * Sling Model for PDP Layout Page Component.
 * 
 * @author manpati
 *
 */
@Model(
        adaptables = SlingHttpServletRequest.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = Constants.PDP_LAYOUT_PAGE_MODEL_RESOURCE_TYPE)
@Exporter(
        name = Constants.JACKSON_KEY,
        extensions = Constants.JSON_KEY)
public class PDPLayoutPageModel extends PageBaseModel
{
    public static final String PDP_LAYOUT_TITLE = "pdp-layout";

    /**
     * It returns the pdp layout title in the page level json.
     * 
     * @return
     */
    @Override
    public String getLayout()
    {
        return PDP_LAYOUT_TITLE;
    }

}
