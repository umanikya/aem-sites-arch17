package com.ftd.platform.core.models;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.ftd.platform.core.constants.Constants;

/**
 * Sling Model for Category Tiles
 * 
 * @author prabasva
 * 
 */
@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = Constants.CATEGORY_TILES_RESOURCE_TYPE)
@Exporter(
        name = Constants.JACKSON_KEY,
        extensions = Constants.JSON_KEY)
public class FeaturedTilesModel extends BaseModel
{

    @Inject
    @Default(
            values = StringUtils.EMPTY)
    private String numberOfTiles;

    @Inject
    private String tileGroupOneImageURL;

    @Inject
    private String tileGroupOneImageAltText;

    @Inject
    private String tileGroupOneURL;

    @Inject
    private String tileGroupOneLinkLabel;

    @Inject
    private String tileGroupOneLinkTarget;

    @Inject
    private String tileGroupTwoImageURL;

    @Inject
    private String tileGroupTwoImageAltText;

    @Inject
    private String tileGroupTwoURL;

    @Inject
    private String tileGroupTwoLinkLabel;

    @Inject
    private String tileGroupTwoLinkTarget;

    @Inject
    private String tileGroupThreeImageURL;

    @Inject
    private String tileGroupThreeImageAltText;

    @Inject
    private String tileGroupThreeURL;

    @Inject
    private String tileGroupThreeLinkLabel;

    @Inject
    private String tileGroupThreeLinkTarget;

    @Inject
    private String tileGroupFourImageURL;

    @Inject
    private String tileGroupFourImageAltText;

    @Inject
    private String tileGroupFourURL;

    @Inject
    private String tileGroupFourLinkLabel;

    @Inject
    private String tileGroupFourLinkTarget;

    @Inject
    private String tileGroupFiveImageURL;

    @Inject
    private String tileGroupFiveImageAltText;

    @Inject
    private String tileGroupFiveURL;

    @Inject
    private String tileGroupFiveLinkLabel;

    @Inject
    private String tileGroupFiveLinkTarget;

    @Inject
    private String tileGroupSixImageURL;

    @Inject
    private String tileGroupSixImageAltText;

    @Inject
    private String tileGroupSixURL;

    @Inject
    private String tileGroupSixLinkLabel;

    @Inject
    private String tileGroupSixLinkTarget;

    @Inject
    private String heading;

    @Inject
    @Default(
            values = Constants.FEATURED_TILES_TITLE)
    private String component;
    private static final String COMPONENT_TITLE = "Category Tiles";

    /**
     * This method returns the component identifier.
     * 
     * @return
     */
    @Override
    public String getComponent()
    {
        return component;
    }

    /**
     * This method constructs the attributes map object for Category Tiles
     * 
     * @return
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Map<String, Object> getAttributes()
    {

        Map<String, Object> componentAttributesMap = new LinkedHashMap<>();
        List<Object> tilesAttributeList = new ArrayList<>();
        tilesAttributeList.add(getTilesAttributesMap(tileGroupOneImageAltText, tileGroupOneImageURL,
                tileGroupOneLinkLabel, tileGroupOneURL, tileGroupOneLinkTarget));
        tilesAttributeList.add(getTilesAttributesMap(tileGroupTwoImageAltText, tileGroupTwoImageURL,
                tileGroupTwoLinkLabel, tileGroupTwoURL, tileGroupTwoLinkTarget));
        tilesAttributeList.add(getTilesAttributesMap(tileGroupThreeImageAltText, tileGroupThreeImageURL,
                tileGroupThreeLinkLabel, tileGroupThreeURL, tileGroupThreeLinkTarget));

        if (numberOfTiles.equals(Constants.FOUR_CATEGORY_TILES_CONSTANT))
        {
            tilesAttributeList.add(getTilesAttributesMap(tileGroupFourImageAltText, tileGroupFourImageURL,
                    tileGroupFourLinkLabel, tileGroupFourURL, tileGroupFourLinkTarget));
        }

        if (numberOfTiles.equals(Constants.SIX_CATEGORY_TILES_CONSTANT))
        {
            tilesAttributeList.add(getTilesAttributesMap(tileGroupFourImageAltText, tileGroupFourImageURL,
                    tileGroupFourLinkLabel, tileGroupFourURL, tileGroupFourLinkTarget));
            tilesAttributeList.add(getTilesAttributesMap(tileGroupFiveImageAltText, tileGroupFiveImageURL,
                    tileGroupFiveLinkLabel, tileGroupFiveURL, tileGroupFiveLinkTarget));
            tilesAttributeList.add(getTilesAttributesMap(tileGroupSixImageAltText, tileGroupSixImageURL,
                    tileGroupSixLinkLabel, tileGroupSixURL, tileGroupSixLinkTarget));
        }

        componentAttributesMap.put(Constants.TITLE_KEY, heading);
        componentAttributesMap.put(Constants.CARDS_KEY, tilesAttributeList);
        return componentAttributesMap;
    }

    /**
     * This method constructs the attributes map object for tiles and returns the same.
     * 
     * @return
     */
    private Map<String, Object> getTilesAttributesMap(String imageAltText, String imageURL, String linkLabel,
            String linkURL, String linkTargetType)
    {

        Map<String, Object> tilesAttributesMap = new LinkedHashMap<>();
        Map<String, Object> linkAttributesMap = new LinkedHashMap<>();

        linkAttributesMap.put(Constants.TEXT_KEY, linkLabel);
        linkAttributesMap.put(Constants.URL_KEY, linkURL);

        if (Objects.nonNull(linkTargetType) && linkTargetType.equals(Constants.NEW_TAB_VALUE))
        {
            linkAttributesMap.put(Constants.NEW_WINDOW_KEY, true);
        } else
        {
            linkAttributesMap.put(Constants.NEW_WINDOW_KEY, false);
        }

        tilesAttributesMap.put(Constants.IMAGE_KEY, getImageMap(imageURL, imageAltText));
        tilesAttributesMap.put(Constants.LINK_KEY, linkAttributesMap);

        return tilesAttributesMap;
    }
    
    /**
     * This is to get component title
     * 
     * @return
     */
    @Override
    public String getComponentTitle()
    {
        return COMPONENT_TITLE;
    }
}
