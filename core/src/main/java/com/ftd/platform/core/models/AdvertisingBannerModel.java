package com.ftd.platform.core.models;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;

import com.ftd.platform.core.constants.Constants;

/**
 * Sling Model for Advertising Banner component
 * 
 * @author manpati
 *
 */
@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = "web/platform/components/content/advertisingbanner")
@Exporter(
        name = "jackson",
        extensions = "json")
public class AdvertisingBannerModel extends BaseModel
{

    @Inject
    private String heading;

    @Inject
    private String subHeading;

    @Inject
    private String image;

    @Inject
    private String color;

    @Inject
    private String buttonLabel;

    @Inject
    private String buttonLink;

    @Inject
    private String linkTarget;

    @Inject
    private String altText;

    String component;

    Map<String, Object> attributes = new LinkedHashMap<>();

    private static final String COMPONENT_TITLE = "Advertising Banner";

    /**
     * This method returns the component identifier.
     * 
     * @return
     */
    @Override
    public String getComponent()
    {
        if (StringUtils.isNotBlank(color) && (color.equals("gold") || color.equals("gold-landing")))
        {
            return Constants.BANNER_COMPONENT_TITLE;
        }

        return Constants.ADVERTISING_BANNER_COMPONENT_TITLE;
    }

    /**
     * This method constructs the attributes map object and returns the same.
     * 
     * @return
     */
    public Map<String, Object> getAttributes()
    {
        Map<String, Object> primarycta = new LinkedHashMap<>();

        primarycta.put(Constants.TEXT_KEY, buttonLabel);
        primarycta.put(Constants.URL_KEY, buttonLink);

        setTargetKeyInPrimaryCta(primarycta, linkTarget);

        attributes.put(Constants.TITLE_KEY, heading);
        attributes.put(Constants.DESCRIPTIONS_KEY, subHeading);
        attributes.put(Constants.THEME_KEY, color);

        attributes.put(Constants.IMAGE_KEY, getImageMap(this.image, this.altText));
        attributes.put(Constants.PRIMARY_CTA_KEY, primarycta);
        return attributes;
    }

    /**
     * Setter method for heading variable
     * 
     * @param heading
     */
    public void setHeading(String heading)
    {
        this.heading = heading;
    }

    /**
     * Setter method for subHeading variable
     * 
     * @param subHeading
     */
    public void setSubHeading(String subHeading)
    {
        this.subHeading = subHeading;
    }

    /**
     * Setter method for image variable
     * 
     * @param image
     */
    public void setImage(String image)
    {
        this.image = image;
    }

    /**
     * Setter method for color variable
     * 
     * @param color
     */
    public void setColor(String color)
    {
        this.color = color;
    }

    /**
     * Setter method for buttonLabel variable
     * 
     * @param buttonLabel
     */
    public void setButtonLabel(String buttonLabel)
    {
        this.buttonLabel = buttonLabel;
    }

    /**
     * Setter method for buttonLink variable
     * 
     * @param buttonLink
     */
    public void setButtonLink(String buttonLink)
    {
        this.buttonLink = buttonLink;
    }

    /**
     * Setter method for linkTarget variable
     * 
     * @param linkTarget
     */
    public void setLinkTarget(String linkTarget)
    {
        this.linkTarget = linkTarget;
    }
    /**
     * This is to get component title
     * 
     * @return
     */
    @Override
    public String getComponentTitle()
    {
        return COMPONENT_TITLE;
    }
}
