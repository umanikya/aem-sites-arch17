package com.ftd.platform.core.models;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.ftd.platform.core.constants.Constants;

/**
 * Base Class for all Model Classes
 * 
 * @author sunkatep
 * 
 */
public class BaseModel
{
    private final Logger log = LoggerFactory.getLogger(getClass());
    
    // Ideally should be loaded as in memory config
    private static final String OLD_SCENE7_DOMAIN = "//s7d5.scene7.com/";
    private static final String NEW_SCENE7_DOMAIN = "//s7img.ftdi.com/";

    @Inject
    protected String variationId;
    
    @Inject
    protected String isConditional;

    @Inject
    private String conditionalContentPath;
    
    @Self
    public Resource resource;

    protected String componentTitle = null;

    /**
     * This method returns the component identifier.
     *
     * @return
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getComponent()
    {
        return null;
    }

    /**
     * This method sets the newWindow field in JSON.
     *
     * @param primaryCTA
     * @param buttonLinkTarget
     */
    public void setTargetKeyInPrimaryCta(Map<String, Object> primaryCTA, String buttonLinkTarget)
    {

        if (StringUtils.isNotBlank(buttonLinkTarget) && !StringUtils.equals(buttonLinkTarget, "_self"))
        {
            primaryCTA.put(Constants.NEW_WINDOW_KEY, true);
        } else
        {
            primaryCTA.put(Constants.NEW_WINDOW_KEY, false);
        }

    }

    /**
     * This method will ignore the Resource object in the JSON
     * 
     * @return Resource
     */
    @JsonIgnore
    public Resource getResource()
    {
        return resource;
    }

    /**
     * This is to get component title
     * 
     * @return
     */
    @JsonIgnore
    public String getComponentTitle()
    {
        log.debug("componentTitle in Base Model= " + componentTitle);
        return componentTitle;
    }
    
    public String getComponentUniqueId()
    {
        if (null != resource)
        {
            return Integer.toString(resource.getPath().hashCode());
        }

        return null;
    }
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getVariationId()
    {
        return variationId;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getConditionalContentPath()
    {
        if (StringUtils.equals(this.isConditional, "true"))
        {
            return conditionalContentPath;
        }

        return null;
    }
    
    /**
     * Get method to retrieve Conditional Content Check
     * 
     * @return
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getIsCondition() {
        if(StringUtils.isNotBlank(conditionalContentPath) && StringUtils.equals(this.isConditional, "true")) {
            return "true";
        }
        
        return null;
    }
    
    public Map<String, String> getImageMap(String imageLink, String altText)
    {

        Map<String, String> image = new LinkedHashMap<>();

        image.put(Constants.URL_KEY, StringUtils.replace(imageLink, OLD_SCENE7_DOMAIN, NEW_SCENE7_DOMAIN));
        image.put(Constants.ALT_KEY, altText);

        return image;
    }
}
