package com.ftd.platform.core.beans;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import com.ftd.platform.core.constants.Constants;

/**
 * Bean Class for holding Section Field details.
 * 
 * @author chakrish1
 *
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SectionsBean {
	@Inject
	private String icon;

	@Inject
	private String text;

	@Inject
	private String content;

	@Inject
	private String linkType;

	@Inject
	private String modalLinkLabel;

	@Inject
	private String modalUrl;

	/**
	 * Getter method for iconLink
	 * 
	 * @return
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * This method constructs the Title map object and returns the same.
	 * 
	 * @return
	 */
	public Map<String, Object> getTitle() {
		Map<String, Object> titleMap = new LinkedHashMap<>();
		titleMap.put(Constants.TEXT_KEY, this.text);
		if (linkType.equals(Constants.NEW_TAB_VALUE)) {
			titleMap.put(Constants.NEW_WINDOW_KEY, true);
		} else {
			titleMap.put(Constants.NEW_WINDOW_KEY, false);
		}

		return titleMap;
	}

	/**
	 * Getter method for content
	 * 
	 * @return
	 */
	public String getContent() {
		return content;
	}

	/**
	 * This method constructs the CTA list object and returns the same.
	 * 
	 * @return
	 */
	public List<Object> getCta() {
		Map<String, Object> ctaMap = new LinkedHashMap<>();
		Map<String, String> onClick = new LinkedHashMap<>();
		List<Object> ctaList = new LinkedList<>();
		if (Objects.nonNull(modalLinkLabel) && Objects.nonNull(modalUrl)) {
			ctaMap.put(Constants.TEXT_KEY, this.modalLinkLabel);
			ctaMap.put(Constants.LINK_KEY, "");

			onClick.put(Constants.MODAL_KEY, modalUrl);
			ctaMap.put(Constants.ON_CLICK_KEY, onClick);
		}

		ctaList.add(ctaMap);
		return ctaList;

	}
}
