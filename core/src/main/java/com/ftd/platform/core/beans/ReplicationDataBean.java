package com.ftd.platform.core.beans;

import java.util.Date;

/**
 * Bean for holding Replication details.
 * 
 * @author sunkatep
 * 
 */
public class ReplicationDataBean
{

    Date lastModified;
    Date publishDate;
    Date unPublishDate;
    Date lastReplicated;
    String lastReplicationAction;

    public ReplicationDataBean(Date lastModified, Date publishDate, Date unPublishDate, Date lastReplicated,
            String lastReplicationAction)
    {
        super();
        this.lastModified = lastModified;
        this.publishDate = publishDate;
        this.unPublishDate = unPublishDate;
        this.lastReplicated = lastReplicated;
        this.lastReplicationAction = lastReplicationAction;
    }

    public ReplicationDataBean()
    {
    }

    public Date getLastModified()
    {
        return lastModified;
    }
    public void setLastModified(Date lastModified)
    {
        this.lastModified = lastModified;
    }
    public Date getPublishDate()
    {
        return publishDate;
    }
    public void setPublishDate(Date publishDate)
    {
        this.publishDate = publishDate;
    }
    public Date getUnPublishDate()
    {
        return unPublishDate;
    }
    public void setUnPublishDate(Date unPublishDate)
    {
        this.unPublishDate = unPublishDate;
    }
    public Date getLastReplicated()
    {
        return lastReplicated;
    }
    public void setLastReplicated(Date lastReplicated)
    {
        this.lastReplicated = lastReplicated;
    }
    public String getLastReplicationAction()
    {
        return lastReplicationAction;
    }
    public void setLastReplicationAction(String lastReplicationAction)
    {
        this.lastReplicationAction = lastReplicationAction;
    }
    @Override
    public String toString()
    {
        return "ReplicationDataBean [lastModified=" + lastModified + ", publishDate=" + publishDate
                + ", unPublishDate=" + unPublishDate + ", lastReplicated=" + lastReplicated
                + ", lastReplicationAction=" + lastReplicationAction + "]";
    }

}
