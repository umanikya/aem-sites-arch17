package com.ftd.platform.core.models;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.ExporterOption;
import org.apache.sling.models.annotations.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ftd.platform.core.beans.PageSEOBean;
import com.ftd.platform.core.constants.Constants;

/**
 * Sling Model for Checkout Labels.
 * 
 */
@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = "web/platform/components/content/checkoutlabels")
@Exporter(
        name = "jackson",
        extensions = "json",
        options = { @ExporterOption(
                name = "SerializationFeature.WRITE_DATES_AS_TIMESTAMPS",
                value = "true") })
public class CheckoutLabelsModel extends BaseModel
{
    private static final String COMPONENT_TITLE = "checkout-labels";
    
    @Inject
    @JsonIgnore
    private Resource checkoutLabels;

    /**
     * This method returns the component identifier.
     * 
     * @return
     */
    @Override
    public String getComponent()
    {
        return COMPONENT_TITLE;
    }

    /**
     * This method constructs the attributes map object and returns the same.
     * 
     * @return
     */
    public Map<String, Object> getAttributes()
    {
        Map<String, Object> attributes = new LinkedHashMap<>();

        if (null != checkoutLabels)
        {
            Iterator<Resource> iterator = checkoutLabels.listChildren();
            while (iterator.hasNext())
            {
                Resource childResource = iterator.next();
                if (childResource != null)
                {
                    ValueMap properties = childResource.getValueMap();

                    if (properties.containsKey("key"))
                    {
                        attributes.put(properties.get("key", ""), properties.get("value", ""));
                    }
                }
            }
        }

        return attributes;
    }

    /**
     * This is to get component title
     * 
     * @return
     */
    @Override
    public String getComponentTitle()
    {
        return "Checkout Labels";
    }

}
