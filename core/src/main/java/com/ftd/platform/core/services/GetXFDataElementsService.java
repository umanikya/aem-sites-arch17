package com.ftd.platform.core.services;

public interface GetXFDataElementsService
{
    String getComponentModelJson(String path);
}
