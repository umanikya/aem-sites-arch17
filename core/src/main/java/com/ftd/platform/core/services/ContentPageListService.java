package com.ftd.platform.core.services;

/**
 * @author abhranja1
 *
 */
public interface ContentPageListService
{
    public String getContentPageList(String siteId);

}
