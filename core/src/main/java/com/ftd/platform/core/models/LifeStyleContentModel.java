package com.ftd.platform.core.models;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class LifeStyleContentModel extends BaseModel
{
    @Inject
    private String lifeStyleContent;
    @Inject
    private String title;
    @Inject
    private String details;
    @Inject
    private String anchor;
    @Inject
    private String videoAsset;
    @Inject
    private String serverUrl;
    @Inject
    private String videoContentUrl;
    @Inject
    private String videoEmailUrl;
    @Inject
    private String videoServerUrl;
    @Inject
    private String imageUrl;
    @Inject
    private String imageAltText;
    @Inject
    private String ctaLabel;
    @Inject
    private String ctaUrl;
    @Inject
    private String ctaTarget;

    public String getLifeStyleContent()
    {
        return lifeStyleContent;
    }

    public String getTitle()
    {
        return title;
    }

    public String getDetails()
    {
        return details;
    }

    public String getAnchor()
    {
        return anchor;
    }

    public String getVideoAsset()
    {
        return videoAsset;
    }

    public String getServerUrl()
    {
        return serverUrl;
    }

    public String getVideoContentUrl()
    {
        return videoContentUrl;
    }

    public String getVideoEmailUrl()
    {
        return videoEmailUrl;
    }

    public String getVideoServerUrl()
    {
        return videoServerUrl;
    }

    public String getImageUrl()
    {
        return imageUrl;
    }

    public String getImageAltText()
    {
        return imageAltText;
    }

    public String getCtaLabel()
    {
        return ctaLabel;
    }

    public String getCtaUrl()
    {
        return ctaUrl;
    }

    public String getCtaTarget()
    {
        return ctaTarget;
    }

}
