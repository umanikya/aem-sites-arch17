package com.ftd.platform.core.servlets;

import com.ftd.platform.core.services.GetXFDataElementsService;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This servlet is a resource based and will provide an end point to get the component json which XF holds along with
 * the tags that are being assigned to particular experience fragment variant.
 */

@Component(
        service = Servlet.class,
        property = { "service.description=Condition Content Search Servlet", "sling.servlet.methods=GET",
                "sling.servlet.resourceTypes=" + "sling/servlet/default", "sling.servlet.selectors=" + "data",
                "sling.servlet.extensions=json" })
public class GetXFDataElementsServlet extends SlingSafeMethodsServlet
{
    private final Logger log = LoggerFactory.getLogger(getClass());

    @Reference
    GetXFDataElementsService getXFDataElementsService;

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("application/json");

        final String path = request.getRequestPathInfo().getResourcePath();
        log.debug("Path => " + path);

        String responseStr = getXFDataElementsService.getComponentModelJson(path);

        if (StringUtils.isNotEmpty(responseStr))
        {
            response.getWriter().write(responseStr);
        } else
        {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);

        }
    }

}
