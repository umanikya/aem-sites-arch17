package com.ftd.platform.core.models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ftd.platform.core.constants.Constants;

/**
 * Sling Model for Membership Slider component
 * 
 */
@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = "web/platform/components/content/membershipslider")
@Exporter(
        name = "jackson",
        extensions = "json")
public class MembershipSliderModel extends BaseModel
{
    @Inject
    @JsonIgnore
    public Resource benefits;

    @Inject
    @JsonIgnore
    public Resource termsAndConditions;

    @Inject
    @JsonIgnore
    public Resource termsAndConditionsDetails;

    @Inject
    private String header;

    @Inject
    private String bannerDescription;

    @Inject
    private String termsAndConditionsDetailsHeader;

    @Inject
    private String membershipPid;

    @Inject
    private String theme;

    String component;

    Map<String, Object> attributes = new LinkedHashMap<>();

    private static final String COMPONENT_TITLE = "Membership Slider";

    /**
     * This method returns the component identifier.
     * 
     * @return
     */
    @Override
    public String getComponent()
    {
        return Constants.MEMBERSHIP_SLIDER_COMPONENT_TITLE;
    }

    /**
     * This method constructs the attributes map object and returns the same.
     * 
     * @return
     */
    public Map<String, Object> getAttributes()
    {
        attributes.put("header", header);
        attributes.put("bannerDescription", bannerDescription);
        attributes.put("termsAndConditionsDetailsHeader", termsAndConditionsDetailsHeader);
        attributes.put("membershipPid", membershipPid);
        attributes.put("theme", theme);

        if (null != benefits)
        {
            attributes.put("benefits", getBenefits(benefits));
        }

        if (null != termsAndConditions)
        {
            attributes.put("termsAndConditions", getTermsAndConditions(termsAndConditions));
        }

        if (null != termsAndConditionsDetails)
        {
            attributes.put("termsAndConditionsDetails", getTermsAndConditions(termsAndConditionsDetails));
        }

        return attributes;
    }

    /**
     * Setter method for header.
     * 
     * @param header
     */
    public void setHeader(String header)
    {
        this.header = header;
    }

    /**
     * Setter method for termsAndConditionsDetailsHeader
     * 
     * @param termsAndConditionsDetailsHeader
     */
    public void setTermsAndConditionsDetailsHeader(String termsAndConditionsDetailsHeader)
    {
        this.termsAndConditionsDetailsHeader = termsAndConditionsDetailsHeader;
    }

    /**
     * Setter method for bannerDescription
     * 
     * @param bannerDescription
     */
    public void setBannerDescription(String bannerDescription)
    {
        this.bannerDescription = bannerDescription;
    }

    /**
     * Setter method for membershipPid
     * 
     * @param membershipPid
     */
    public void setMembershipPid(String membershipPid)
    {
        this.membershipPid = membershipPid;
    }

    /**
     * Sets theme.
     * 
     * @param theme
     */
    public void setTheme(String theme)
    {
        this.theme = theme;
    }

    /**
     * This is to get component title
     * 
     * @return
     */
    @Override
    public String getComponentTitle()
    {
        return COMPONENT_TITLE;
    }

    /**
     * Gets terms and conditions.
     * 
     * @return
     */
    private List<Map<String, String>> getTermsAndConditions(Resource termsAndConditions)
    {
        List<Map<String, String>> termsAndConditionsList = new ArrayList<>();

        Iterator<Resource> iterator = termsAndConditions.listChildren();

        while (iterator.hasNext())
        {
            Resource childResource = iterator.next();
            if (childResource != null)
            {
                Map<String, String> termsAndConditionsMap = new LinkedHashMap<>();
                termsAndConditionsMap.put("header", childResource.getValueMap().get("header", ""));
                termsAndConditionsMap.put("description", childResource.getValueMap().get("description", ""));

                termsAndConditionsList.add(termsAndConditionsMap);
            }
        }

        return termsAndConditionsList;
    }

    /**
     * Gets benefits
     * 
     * @return
     */
    private List<String> getBenefits(Resource benefits)
    {
        List<String> benefitsList = new ArrayList<>();
        Iterator<Resource> iterator = benefits.listChildren();
        while (iterator.hasNext())
        {
            Resource childResource = iterator.next();
            if (childResource != null)
            {
                benefitsList.add(childResource.getValueMap().get("benefit", ""));
            }
        }

        return benefitsList;
    }

}
