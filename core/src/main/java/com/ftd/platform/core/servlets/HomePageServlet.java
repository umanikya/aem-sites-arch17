package com.ftd.platform.core.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ftd.platform.core.services.ContentPageDataService;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.day.cq.search.Query;

@Component(
        service = { Servlet.class },
        property = { "service.description=Condition Content insert Servlet", "sling.servlet.methods=GET",
                "sling.servlet.paths=" + "/bin/home" })
public class HomePageServlet extends SlingSafeMethodsServlet
{
    private static final long serialVersionUID = 1L;

    @Reference
    ContentPageDataService contentPageDataService;

    @Reference
    private QueryBuilder queryBuilder;

    String responseStr;
    JsonParser parser = new JsonParser();
    ObjectMapper mapper = new ObjectMapper();
    boolean ccSucess = false;

    private static final Logger logger = LoggerFactory.getLogger(HomePageServlet.class);

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServletException, IOException
    {
        ccSucess = false;
        JsonArray jarray = new JsonArray();
        response.setContentType("application/json");
        JsonArray ccpathlist = new JsonArray();
        JsonArray ccpathurllist = new JsonArray();
        Resource res = request.getResourceResolver()
                .getResource("/content/ftd/en-us/main/home/home" + "/jcr:content/rootResource");
        Iterator<Resource> resourceIterator = res.listChildren();
        JsonObject total = new JsonObject();
        while (resourceIterator.hasNext())
        {
            Resource childResource = resourceIterator.next();
            ValueMap vm = childResource.getValueMap();
            if (!(vm.containsKey("isConditional")))
            {
                responseStr = mapper.writeValueAsString(contentPageDataService.getModelFromResource(childResource));
                jarray.add(parser.parse(responseStr).getAsJsonObject());
            } else
            {
                JsonObject job=new JsonObject();
                job.addProperty("conditionalContentPathList", vm.get("conditionalContentPath").toString());
                ccpathlist.add(job);
                Resource ccResource = getConditionalComponent(vm.get("conditionalContentPath").toString(), request,
                        childResource);
                responseStr = mapper.writeValueAsString(contentPageDataService.getModelFromResource(ccResource));
                JsonObject jo = parser.parse(responseStr).getAsJsonObject();
                if (ccSucess)
                {
                   JsonObject j= new JsonObject();
                   j.addProperty("conditionalComponentUriList",
                           vm.get("conditionalContentPath").toString() + "/" + ccResource.getName());
                    ccpathurllist.add(j);
                    jo.addProperty("conditionalComponentUri",
                            vm.get("conditionalContentPath").toString() + "/" + ccResource.getName());
                }
                jarray.add(jo);
            }
        }
        jarray.add(ccpathlist);
        jarray.add(ccpathurllist);
        total.add("components", jarray);
        response.getWriter().print(total);
    }

    private Resource getConditionalComponent(String ccpath, SlingHttpServletRequest request, Resource exp)
    {
        String markcode = request.getParameter("markcode");
        logger.info("inside the cc method markcode is:" + markcode);
        if (null != markcode)
        {
            ResourceResolver rr = request.getResourceResolver();
            final Map<String, String> map = new HashMap<String, String>();
            map.put("type", "cq:Page");
            map.put("path", ccpath);
            map.put("tagid.property", "jcr:content/cq:tags");
            map.put("tagid.1_value", "ftdi:ftd/sources/512");
            Query query = queryBuilder.createQuery(PredicateGroup.create(map), rr.adaptTo(Session.class));
            SearchResult result = query.getResult();
            for (final Hit hit : result.getHits())
            {
                try
                {
                    String[] s = hit.getProperties().get("cq:tags", String[].class);
                    logger.info("tags of the variation : " + s[0]);
                    for (String tag : s)
                    {
                        if (tag.equals("ftdi:ftd/sources/512"))
                        {
                            Iterator<Resource> it = rr.getResource(hit.getResource().getPath() + "/jcr:content/root")
                                    .listChildren();
                            logger.info(hit.getResource().getPath() + "/jcr:content/root");
                            while (it.hasNext())
                            {
                                Resource child = it.next();
                                if (child.getValueMap().get("sling:resourceType")
                                        .equals(exp.getValueMap().get("sling:resourceType")))
                                {
                                    ccSucess = true;
                                    return child;
                                }
                            }

                        }
                    }
                } catch (RepositoryException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        return exp;
    }
}
