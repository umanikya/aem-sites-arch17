package com.ftd.platform.core.models.page;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;

import com.ftd.platform.core.constants.Constants;

/**
 * Sling Model for One Column Layout Page Component.
 * 
 * @author sunkatep
 * 
 */
@Model(
        adaptables = SlingHttpServletRequest.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = Constants.ONE_COLUMN_LAYOUT_PAGE_MODEL_RESOURCE_TYPE)
@Exporter(
        name = Constants.JACKSON_KEY,
        extensions = Constants.JSON_KEY)
public class OneColumnLayoutPageModel extends PageBaseModel
{

    /**
     * It returns the one column layout title in the page level json.
     * 
     * @return
     */
    @Override
    public String getLayout()
    {
        return Constants.ONE_COLUMN_LAYOUT_TITLE;
    }

}
