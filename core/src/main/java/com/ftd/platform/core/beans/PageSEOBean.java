package com.ftd.platform.core.beans;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.inject.Inject;

@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@JsonPropertyOrder({ "name", "content" })
public class PageSEOBean
{
    @Inject
    @JsonProperty("name")
    private String name;

    @Inject
    @JsonProperty("content")
    private String content;

    /**
     * Gets Name
     * @return name attribute for meta
     */
    public String getName()
    {
        return name;
    }

    /**
     * Gets Content attribute
     * @return content attribute for meta
     */
    public String getContent()
    {
        return content;
    }

    /**
     * Sets Name attribute
     * @param name -- name meta for head
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Sets Content attribute
     * @param content -- content meta for head
     */
    public void setContent(String content)
    {
        this.content = content;
    }
}
