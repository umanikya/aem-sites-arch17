package com.ftd.platform.core.beans;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import com.ftd.platform.core.constants.Constants;
import com.ftd.platform.core.models.BaseModel;

/**
 * Bean Class for holding Product Information details.
 * 
 * @author chakrish1
 *
 */
@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ProductInfoBean extends BaseModel
{
    @Inject
    private String title;

    @Inject
    private String anchor;

    @Inject
    private String details;

    @Inject
    private String label;

    @Inject
    private String url;

    @Inject
    private String target;

    @Inject
    private String serverUrl;

    @Inject
    private String videoAsset;

    @Inject
    private String videContentUrl;

    @Inject
    private String videoEmailUrl;

    @Inject
    private String videoServerUrl;

    /**
     * This method constructs the Content map object and returns the same.
     * 
     * @return
     */

    public Map<String, Object> getContent()
    {
        Map<String, Object> content = new HashMap<>();
        Map<String, Object> contentMap = new HashMap<>();
        Map<String, String> ctaMap = new HashMap<>();

        ctaMap.put(Constants.LABEL_KEY, this.label);
        ctaMap.put(Constants.URL_KEY, this.url);
        ctaMap.put(Constants.TARGET_KEY, this.target);

        contentMap.put(Constants.TITLE_KEY, this.title);
        contentMap.put(Constants.ANCHOR_KEY, this.anchor);
        contentMap.put(Constants.DETAILS_KEY, this.details);
        contentMap.put(Constants.CTA_KEY, ctaMap);

        content.put(Constants.DISPLAY_TYPE_KEY, Constants.CONTENT_KEY);
        content.put(Constants.CONTENT_KEY, contentMap);

        return content;
    }

    /**
     * This method constructs the Content map object and returns the same.
     * 
     * @return
     */
    public Map<String, Object> getVideo()
    {
        Map<String, Object> videoContent = new HashMap<>();
        Map<String, Object> videoMap = new HashMap<>();

        videoMap.put(Constants.ASSET_KEY, this.videoAsset);
        videoMap.put(Constants.SERVER_URL_KEY, this.serverUrl);
        videoMap.put(Constants.CONTENT_URL_KEY, this.videContentUrl);
        videoMap.put(Constants.EMAIL_URL_KEY, this.videoEmailUrl);
        videoMap.put(Constants.VIDEO_SERVER_URL_KEY, this.videoServerUrl);
        videoContent.put(Constants.DISPLAY_TYPE_KEY, Constants.VIDEO_KEY);
        videoContent.put(Constants.CONTENT_KEY, videoMap);

        return videoContent;
    }

}
