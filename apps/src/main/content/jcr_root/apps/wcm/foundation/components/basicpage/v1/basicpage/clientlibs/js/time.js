(function (document, $) {
    "use strict";

     $(document).on("foundation-contentloaded", function (e) {
         var startTime = $("#cq-sites-properties-form input[name='./cq:publishDate']").val();
         var endTime = $("#cq-sites-properties-form input[name='./cq:unPublishDate']").val();

         var cstStartTime = convertToCstTime(startTime);
         var cstEndTime =  convertToCstTime(endTime);

         $("#cq-sites-properties-form input[name='./cq:publishDate']").val(cstStartTime);
         $("#cq-sites-properties-form input[name='./cq:unPublishDate']").val(cstEndTime);
    });

    function convertToCstTime(time){
      if(time!="")
      {
         return moment(time).tz('America/Chicago').format("YYYY-MM-DD hh:mm a");
      }
    }
})(document, Granite.$);