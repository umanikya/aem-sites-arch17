(function(document, $, ns) {

	$(document).on("click",	".cq-dialog-submit", function(e) {

	var $form = $(this).closest("form.foundation-form"), url = $form.find("[name='./url']").val(), 
	productlist = document.querySelector('#productlist'), 
	$formItems = $(this).closest("form.foundation-form"), 
	promocard = $formItems.find("[data-granite-coral-multifield-name='./promocard']"), 
	multifieldItems = promocard.children('coral-multifield-item'), multifieldSize = multifieldItems.length, urlArray = [];

	for (var i = 0; i < multifieldSize; i++) {
		urlArray.push($(multifieldItems[i]).find("foundation-autocomplete input").val());
	}

	if (null != productlist && !productlist.checked) {
		var urlArrayLength = urlArray.length;
		for (var i = 0; i < urlArrayLength; i++) {
			if (urlArray[i] == 0) {
				window.stop();
			} else if (i+1 == urlArrayLength) {
				$form.submit();
			}
		}
	}
});
})(document, Granite.$, Granite.author);