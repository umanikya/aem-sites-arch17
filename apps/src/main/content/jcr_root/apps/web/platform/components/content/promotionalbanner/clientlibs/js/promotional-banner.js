(function(document, $, ns) {
    "use strict";


    $(document).on("click", ".cq-dialog-submit", function(e) {

        var $form = $(this).closest("form.foundation-form"),
            ctaTarget = $form.find("[name='./ctaTarget']").val(),
            heading = $form.find("[name='./heading']").val(),
            ctaType = $form.find("[name='./ctaType']").val(),
            changeRef = $form.find("[name='./changeRef']").val(),
            refCode = $form.find("[name='./refCode']").val(),
            ctaURL = $form.find("[name='./ctaURL']").val(),
            message, clazz = "coral-Button ",
            patterns = {};

        var ui = $(window).adaptTo("foundation-ui");

        if (changeRef == 'new-page') {
            if (ctaURL.length == 0) {
                e.stopPropagation();
                e.preventDefault();
                ui.alert("Warning", "Please provide Button Link.", "notice");
                window.stop();
            } else {
                $form.submit();
            }

        }

    });
})(document, Granite.$, Granite.author);