(function(document, $, ns) {
    "use strict";

    $(document).on("click", ".cq-dialog-submit", function(e) {

        var $form = $(this).closest("form.foundation-form"),

            idSelector = $form.find("[name='./idSelector']").val(),
            catId = $form.find("[name='./catId']").val(),
            productid = $form.find("[name='./productid']").val(),
            prodId = $form.find("[name='./prodId']").val(),
            heading = $form.find("[name='./heading']").val(),
            imageLink = $form.find("[name='./imageLink']").val(),
            imgAltTxt = $form.find("[name='./imgAltTxt']").val(),
            buttonLabel = $form.find("[name='./buttonLabel']").val(),
            buttonLink = $form.find("[name='./buttonLink']").val(),
            ui = $(window).adaptTo("foundation-ui");


        var $formminmax = $(this).closest("form.foundation-form");   
        var field = $formminmax.find("[data-granite-coral-multifield-name='./prodId']");
        var totalLinkCount = field.children('coral-multifield-item').length;

        var prodData = [],
            alphaNumError = [];

        for (var i = 0; i < totalLinkCount; i++) {
            prodData.push(field.find($("[name='./productid']")[i]).val());
        }

        var prodDataFilter = prodData.filter(function(e) {
            return e
        });


        for (var i = 0; i < totalLinkCount; i++) {
            if (field.find($("[name='./productid']")[i]).val().length != ((field.find($("[name='./productid']")[i]).val()).replace(/[^a-zA-Z0-9-]/gi, "").length)) {
                alphaNumError.push(field.find($("[name='./productid']")[i]).val());
            }
        }


        if (idSelector == 'prod') {

            if ((heading == 0) || (imageLink == 0) || (imgAltTxt == 0) || (buttonLabel == 0) || (buttonLink == 0) || (totalLinkCount != prodDataFilter.length) || (productid == undefined)) {
                window.stop();
            } else if (alphaNumError.length > 0) {
                ui.alert("Warning", "Product Id's can only be Alphanumeric!!", "notice");
                window.stop();
            } else {
                $form.submit()
            }
        }

        if (idSelector == 'cat') {

            if ((heading == 0) || (imageLink == 0) || (imgAltTxt == 0) || (buttonLabel == 0) || (buttonLink == 0) || (catId.length == 0)) {
                window.stop();
            } else {
                $form.submit()
            }
        }


    });
})(document, Granite.$, Granite.author);