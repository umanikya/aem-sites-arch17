(function(document, $, ns) {
    "use strict";

    $(document).on("click", ".cq-dialog-submit", function(e) {

        var $form = $(this).closest("form.foundation-form"),
            prodSizeSelector = $form.find("[name='./prodSizeSelector']").val(),
            idSelectorGrid = $form.find("[name='./idSelectorGrid']").val(),
            catIdTwo = $form.find("[name='./catIdTwo']").val(),
            catIdThree = $form.find("[name='./catIdThree']").val(),
            productid = $form.find("[name='./productid']").val(),
            prodIdTwo = $form.find("[name='./prodIdTwo']").val(),
            prodIdThree = $form.find("[name='./prodIdThree']").val(),
            titleTwo = $form.find("[name='./titleTwo']").val(),
            description = $form.find("[name='./description']").val(),
            buttonLabelTwo = $form.find("[name='./buttonLabelTwo']").val(),
            buttonLinkTwo = $form.find("[name='./buttonLinkTwo']").val(),

            titleThree = $form.find("[name='./titleThree']").val(),
            buttonLabelThree = $form.find("[name='./buttonLabelThree']").val(),
            buttonLinkThree = $form.find("[name='./buttonLinkThree']").val(),

            message, clazz = "coral-Button ",
            patterns = {};

        var $formminmax = $(this).closest("form.foundation-form");   
        var fieldTwo = $formminmax.find("[data-granite-coral-multifield-name='./prodIdTwo']");
        var fieldThree = $formminmax.find("[data-granite-coral-multifield-name='./prodIdThree']");
        var ui = $(window).adaptTo("foundation-ui");

        var totalLinkCountForTwo = fieldTwo.children('coral-multifield-item').length;
        var totalLinkCountForThree = fieldThree.children('coral-multifield-item').length;

        var prodDataTwo = [],
            prodDataThree = [],
            alphaNumErrorTwo = [],
            alphaNumErrorThree = [];

        for (var i = 0; i < totalLinkCountForTwo; i++) {
            prodDataTwo.push(fieldTwo.find($("[name='./productid']")[i]).val());
        }

        for (var i = 0; i < totalLinkCountForThree; i++) {
            prodDataThree.push(fieldThree.find($("[name='./productidThreeProd']")[i]).val());
        }

        var prodDataFilterTwo = prodDataTwo.filter(function(e) {
            return e
        });

        var prodDataFilterThree = prodDataThree.filter(function(e) {
            return e
        });

        for (var i = 0; i < totalLinkCountForTwo; i++) {
            if (fieldTwo.find($("[name='./productid']")[i]).val().length != ((fieldTwo.find($("[name='./productid']")[i]).val()).replace(/[^a-zA-Z0-9-]/gi, "").length)) {
                alphaNumErrorTwo.push(fieldTwo.find($("[name='./productid']")[i]).val());
            }
        }

        for (var i = 0; i < totalLinkCountForThree; i++) {
            if (fieldThree.find($("[name='./productidThreeProd']")[i]).val().length != ((fieldThree.find($("[name='./productidThreeProd']")[i]).val()).replace(/[^a-zA-Z0-9-]/gi, "").length)) {
                alphaNumErrorThree.push(fieldThree.find($("[name='./productidThreeProd']")[i]).val());
            }
        }

        var threeProductsIdOne = fieldThree.find($("[name='./productidThreeProd']")[0]).val();
        var threeProductsIdTwo = fieldThree.find($("[name='./productidThreeProd']")[1]).val();
        var threeProductsIdThree = fieldThree.find($("[name='./productidThreeProd']")[2]).val();


        var threeProductsIds = [threeProductsIdOne, threeProductsIdTwo, threeProductsIdThree].filter(function(e) {
            return e
        });

        if (prodSizeSelector == 'two') {


            if ((titleTwo == 0) || (description == 0) || ((idSelectorGrid == 'cat') && (catIdTwo.length == 0)) || (idSelectorGrid == 'prod') && (prodDataFilterTwo.length != totalLinkCountForTwo)) {

                window.stop();

            } else if (alphaNumErrorTwo.length > 0) {
                ui.alert("Warning", "Product Ids can only be Alphanumeric!!", "notice");
                window.stop();
            } else {
                $form.submit()
            }
        }

        if (prodSizeSelector == 'three') {

            if ((titleThree == 0) || ((idSelectorGrid == 'cat') && (catIdThree.length == 0)) || ((idSelectorGrid == 'prod') && (prodDataFilterThree.length != totalLinkCountForThree))) {

                window.stop();
            } else if (alphaNumErrorThree.length > 0) {
                ui.alert("Warning", "Product Ids can only be Alphanumeric!!", "notice");
                window.stop();
            } else {
                $form.submit();
            }
        }


    });
})(document, Granite.$, Granite.author);