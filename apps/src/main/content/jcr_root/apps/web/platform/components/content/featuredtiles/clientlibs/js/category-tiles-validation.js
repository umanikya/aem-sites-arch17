(function(document, $, ns) {

	$(document).on("keyup", '.coral-Form-fieldwrapper > input[type="text"][data-totitlecase]', function() {
        var textFieldValue = this.value;
		textFieldValue = textFieldValue.split(" ");
        for(var i = 0; i < textFieldValue.length; i++) {
            textFieldValue[i] = textFieldValue[i].toLowerCase();
            textFieldValue[i] = textFieldValue[i].charAt(0).toUpperCase() + textFieldValue[i].slice(1);
        }
        this.value = textFieldValue.join(" ");
    });

    $(document).on("click", ".cq-dialog-submit", function(e) {

        var $form = $(this).closest("form.foundation-form"),
            numberOfTiles = $form.find("[name='./numberOfTiles']").val(),

			tileGroupOneImageURL = $form.find("[name='./tileGroupOneImageURL']").val(),
            tileGroupOneImageAltText = $form.find("[name='./tileGroupOneImageAltText']").val(),
            tileGroupOneURL = $form.find("[name='./tileGroupOneURL']").val(),
            tileGroupOneLinkLabel = $form.find("[name='./tileGroupOneLinkLabel']").val(),
            tileGroupOneLinkTarget = $form.find("[name='./tileGroupOneLinkTarget']").val(),

            tileGroupTwoImageURL = $form.find("[name='./tileGroupTwoImageURL']").val(),
            tileGroupTwoImageAltText = $form.find("[name='./tileGroupTwoImageAltText']").val(),
            tileGroupTwoURL = $form.find("[name='./tileGroupTwoURL']").val(),
            tileGroupTwoLinkLabel = $form.find("[name='./tileGroupTwoLinkLabel']").val(),
            tileGroupTwoLinkTarget = $form.find("[name='./tileGroupTwoLinkTarget']").val(),

            tileGroupThreeImageURL = $form.find("[name='./tileGroupThreeImageURL']").val(),
            tileGroupThreeImageAltText = $form.find("[name='./tileGroupThreeImageAltText']").val(),
            tileGroupThreeURL = $form.find("[name='./tileGroupThreeURL']").val(),
            tileGroupThreeLinkLabel = $form.find("[name='./tileGroupThreeLinkLabel']").val(),
            tileGroupThreeLinkTarget = $form.find("[name='./tileGroupThreeLinkTarget']").val(),

            tileGroupFourImageURL = $form.find("[name='./tileGroupFourImageURL']").val(),
            tileGroupFourImageAltText = $form.find("[name='./tileGroupFourImageAltText']").val(),
            tileGroupFourURL = $form.find("[name='./tileGroupFourURL']").val(),
            tileGroupFourLinkLabel = $form.find("[name='./tileGroupFourLinkLabel']").val(),
            tileGroupFourLinkTarget = $form.find("[name='./tileGroupFourLinkTarget']").val(),

            tileGroupFiveImageURL = $form.find("[name='./tileGroupFiveImageURL']").val(),
            tileGroupFiveImageAltText = $form.find("[name='./tileGroupFiveImageAltText']").val(),
            tileGroupFiveURL = $form.find("[name='./tileGroupFiveURL']").val(),
            tileGroupFiveLinkLabel = $form.find("[name='./tileGroupFiveLinkLabel']").val(),
            tileGroupFiveLinkTarget = $form.find("[name='./tileGroupFiveLinkTarget']").val(),

            tileGroupSixImageURL = $form.find("[name='./tileGroupSixImageURL']").val(),
            tileGroupSixImageAltText = $form.find("[name='./tileGroupSixImageAltText']").val(),
            tileGroupSixURL = $form.find("[name='./tileGroupSixURL']").val(),
            tileGroupSixLinkLabel = $form.find("[name='./tileGroupSixLinkLabel']").val(),
            tileGroupSixLinkTarget = $form.find("[name='./tileGroupSixLinkTarget']").val(),

            message, clazz = "coral-Button ",
            patterns = {};

        if (numberOfTiles == 'fourth') {

            if ((tileGroupOneImageURL == 0) || (tileGroupOneImageAltText == 0) || (tileGroupOneURL == 0) || (tileGroupOneLinkLabel == 0) || (tileGroupOneLinkLabel.length > 25) || (tileGroupOneLinkTarget == 0) || (tileGroupTwoImageURL == 0) || (tileGroupTwoImageAltText == 0) || (tileGroupTwoURL == 0) || (tileGroupTwoLinkLabel == 0) || (tileGroupTwoLinkLabel.length > 25) || (tileGroupTwoLinkTarget == 0) || (tileGroupThreeImageURL == 0) || (tileGroupThreeImageAltText == 0) || (tileGroupThreeURL == 0) || (tileGroupThreeLinkLabel == 0) || (tileGroupThreeLinkLabel.length > 25) || (tileGroupThreeLinkTarget == 0) || (tileGroupFourImageURL == 0) || (tileGroupFourImageAltText == 0) || (tileGroupFourURL == 0) || (tileGroupFourLinkLabel == 0) || (tileGroupFourLinkLabel.length > 25) || (tileGroupFourLinkTarget == 0)) {
                window.stop();
            } else {
                $form.submit();
            }
        }

        if (numberOfTiles == 'sixth') {

            if ((tileGroupOneImageURL == 0) || (tileGroupOneImageAltText == 0) || (tileGroupOneURL == 0) || (tileGroupOneLinkLabel == 0) || (tileGroupOneLinkLabel.length > 25) || (tileGroupOneLinkTarget == 0) || (tileGroupTwoImageURL == 0) || (tileGroupTwoImageAltText == 0) || (tileGroupTwoURL == 0) || (tileGroupTwoLinkLabel == 0) || (tileGroupTwoLinkLabel.length > 25) || (tileGroupTwoLinkTarget == 0) || (tileGroupThreeImageURL == 0) || (tileGroupThreeImageAltText == 0) || (tileGroupThreeURL == 0) || (tileGroupThreeLinkLabel == 0) || (tileGroupThreeLinkLabel.length > 25) || (tileGroupThreeLinkTarget == 0) || (tileGroupFourImageURL == 0) || (tileGroupFourImageAltText == 0) || (tileGroupFourURL == 0) || (tileGroupFourLinkLabel == 0) || (tileGroupFourLinkLabel.length > 25) || (tileGroupFourLinkTarget == 0) (tileGroupFiveImageURL == 0) || (tileGroupFiveImageAltText == 0) || (tileGroupFiveURL == 0) || (tileGroupFiveLinkLabel == 0) || (tileGroupFiveLinkLabel.length > 25) || (tileGroupFiveLinkTarget == 0) (tileGroupSixImageURL == 0) || (tileGroupSixImageAltText == 0) || (tileGroupSixURL == 0) || (tileGroupSixLinkLabel == 0) || (tileGroupSixLinkLabel.length > 25) || (tileGroupSixLinkTarget == 0)) {
                window.stop();
            } else {
                $form.submit();
            }
        }

        if (numberOfTiles == 'third') {

            if ((tileGroupOneImageURL == 0) || (tileGroupOneImageAltText == 0) || (tileGroupOneURL == 0) || (tileGroupOneLinkLabel == 0) || (tileGroupOneLinkLabel.length > 25) || (tileGroupOneLinkTarget == 0) || (tileGroupTwoImageURL == 0) || (tileGroupTwoImageAltText == 0) || (tileGroupTwoURL == 0) || (tileGroupTwoLinkLabel == 0) || (tileGroupTwoLinkLabel.length > 25) || (tileGroupTwoLinkTarget == 0) || (tileGroupThreeImageURL == 0) || (tileGroupThreeImageAltText == 0) || (tileGroupThreeURL == 0) || (tileGroupThreeLinkLabel == 0) || (tileGroupThreeLinkLabel.length > 25) || (tileGroupThreeLinkTarget == 0)) {
                window.stop();
            } else {
                $form.submit();
            }
        }
    });
})(document, Granite.$, Granite.author);
