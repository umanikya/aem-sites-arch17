(function(document, $, ns) {
    "use strict";



    $(document).on("click", ".cq-dialog-submit", function(e) {

        var $form = $(this).closest("form.foundation-form"),

            idSelectorGift = $form.find("[name='./idSelectorGift']").val(),
            categoryIdGift = $form.find("[name='./categoryIdGift']").val(),

            catGiftLabel = $form.find("[name='./catGiftLabel']").val(),

            catGiftId = $form.find("[name='./catGiftId']").val(),

             version = $form.find("[name='./version']").val(),

            heading = $form.find("[name='./heading']").val(),

            helpLabel = $form.find("[name='./helpLabel']").val(),

            helpText = $form.find("[name='./helpText']").val(),

            buttonlabel = $form.find("[name='./buttonlabel']").val(),

            categoryId = $form.find("[name='./categoryId']").val(),


            message, clazz = "coral-Button ",
            patterns = {};


        var $formminmax = $(this).closest("form.foundation-form");   
        var categoryIdGift = $formminmax.find("[data-granite-coral-multifield-name='./categoryIdGift']");

        var countCatIds = categoryIdGift.children('coral-multifield-item').length;

        var catGiftLabel = [],
            catGiftId = [];

        for (var i = 0; i < countCatIds; i++) {
            catGiftLabel.push(categoryIdGift.find($("[name='./catGiftLabel']")[i]).val());
        }

        for (var j = 0; j < countCatIds; j++) {
            catGiftId.push(categoryIdGift.find($("[name='./catGiftId']")[j]).val());
        }

        var catGiftLabelFilter = catGiftLabel.filter(function(e) {
            return e
        });

        var catGiftIdFilter = catGiftId.filter(function(e) {
            return e
        });

        //alert(idSelectorGift);


        if (idSelectorGift == 'no') {


            if ((heading == 0) || (helpLabel == 0) || (helpText == 0) || (buttonlabel == 0) || (categoryId == undefined)) {

                window.stop();

            } else {
                $form.submit()
            }
        }
        if (idSelectorGift == 'yes') {


            if ((heading == 0) || (helpLabel == 0) || (helpText == 0) || (buttonlabel == 0) || (countCatIds != catGiftLabelFilter.length) || (countCatIds != catGiftIdFilter.length) || (version == 0)) {

                window.stop();



            } else {
                $form.submit();
            }
        }




    });
})(document, Granite.$, Granite.author);