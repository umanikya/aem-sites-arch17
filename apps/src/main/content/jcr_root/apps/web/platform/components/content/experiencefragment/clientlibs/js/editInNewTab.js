(function($, ns){

    var MSG_NO_FRAGMENT_PATH = Granite.I18n.get("The experience fragment doesn't have an associated variation");
    var MSG_NOTVALID_FRAGMENT_PATH = Granite.I18n.get("Enter a valid Conditional Content Path");

    
    ns.editInNewTab = function() {

        var ui = $(window).adaptTo("foundation-ui");
        $.get(this.path + ".json")
            .then(function(response) {
                if (typeof response["conditionalContentPath"] !== "undefined" && response["isConditional"] == "true") {
                	var path = Granite.HTTP.externalize("/aem/experience-fragments.html" + response["conditionalContentPath"]);
                	
                	$.get(path,function(res,status){
                		if(status === "success")
                			window.open(path);
                	}).fail(function(){
                		ui.notify("",  MSG_NOTVALID_FRAGMENT_PATH, "error");
                	})
                    
                } else {
                    ui.notify("", MSG_NO_FRAGMENT_PATH , "notice");
                }
            });
    }

})(jQuery, Granite.author);
