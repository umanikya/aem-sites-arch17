(function(document, $, ns) {
    "use strict";



    $(document).on("click", ".cq-dialog-submit", function(e) {

        var $form = $(this).closest("form.foundation-form"),
            columnSelector = $form.find("[name='./columnSelector']").val(),
            menuOneLabel = $form.find("[name='./menuOneLabel']").val(),
            menuTwoLabel = $form.find("[name='./menuTwoLabel']").val(),
            menuThreeLabel = $form.find("[name='./menuThreeLabel']").val(),
            menuFourLabel = $form.find("[name='./menuFourLabel']").val(),
            menuFiveLabel = $form.find("[name='./menuFiveLabel']").val(),

            message, clazz = "coral-Button ",
            patterns = {};



        if (columnSelector == 'one') {

            if (menuOneLabel == 0) {
                window.stop()
            } else {
                $form.submit();
            }

        }
        if (columnSelector == 'two') {

            if (menuOneLabel == 0 || menuTwoLabel == 0) {
                window.stop()
            } else {
                $form.submit();
            }

        }
        if (columnSelector == 'three') {

            if (menuOneLabel == 0 || menuTwoLabel == 0 || menuThreeLabel == 0) {
                window.stop()
            } else {
                $form.submit();
            }

        }
        if (columnSelector == 'four') {

            if (menuOneLabel == 0 || menuTwoLabel == 0 || menuThreeLabel == 0 || menuFourLabel == 0) {
                window.stop()
            } else {
                $form.submit();
            }

        }
        if (columnSelector == 'five') {

            if (menuOneLabel == 0 || menuTwoLabel == 0 || menuThreeLabel == 0 || menuFourLabel == 0 || menuFiveLabel == 0) {
                window.stop()
            } else {
                $form.submit();
            }

        }

    });
})(document, Granite.$, Granite.author);