(function (document, $, ns) {
    "use strict";

$(document).on("dialog-ready", function() {

        $(".coral-Button").click(function() {

            var attr = $(this).attr('coral-multifield-add');

            if (typeof attr !== typeof undefined && attr !== false) {

                var field = $(this).parent();

                var attrDV = field.attr("data-validation");

                var size;

                if (typeof attrDV !== undefined && attrDV !== false) {

                    if(typeof attrDV.split(':')[1] !== undefined){

                    size = attrDV.split(':')[1];

                    }

                }

                //console.log(size);

                if (size) {

                    var ui = $(window).adaptTo("foundation-ui");

                    var totalLinkCount = $(this).parent().children('coral-multifield-item').length;

                    if (totalLinkCount >= size) {

                        ui.alert("Warning", "Maximum " + size + " multifield are allowed!", "notice");

                        return false;

                    }

                }

            }

        });

    });
    })(document, Granite.$, Granite.author);