(function(document, $, ns) {
    "use strict";
    $.validator.register({
        selector: '.coral-Form-fieldwrapper > input[type="text"]',
        validate: function(el) {
          var value,
              maxLength,
              activeValidation;
    
          activeValidation = el.data('activevalidation');
        
          if (activeValidation || (!activeValidation && !el.is(':focus'))) {
    
            value = el.val();
            maxLength = el.data('maximumlength');
    
            if (value) {
              if (maxLength && value.length > maxLength) {
                return Coral.i18n.get('The field must not be greater than {0} characters long.', maxLength);
              }
            }
          }
        }
  	});

    $(document).on("click", ".cq-dialog-submit", function(e) {

        var $form = $(this).closest("form.foundation-form"),
            displayOptions = $form.find("[name='./displayOptions']").val(),

            leftVideoText = $form.find("[name='./leftVideoText']").val(),
            leftVideoAsset = $form.find("[name='./leftVideoAsset']").val(),
            leftServerURL = $form.find("[name='./leftServerURL']").val(),
            leftVideoContentURL = $form.find("[name='./leftVideoContentURL']").val(),
            leftVideoEmailURL = $form.find("[name='./leftVideoEmailURL']").val(),
            leftVideoServerURL = $form.find("[name='./leftVideoServerURL']").val(),
            leftVideoPosterImage = $form.find("[name='./leftVideoPosterImage']").val(),
            leftVideoTranscript = $form.find("[name='./leftVideoTranscript']").val(),

            rightVideoText = $form.find("[name='./rightVideoText']").val(),
            rightVideoAsset = $form.find("[name='./rightVideoAsset']").val(),
            rightServerURL = $form.find("[name='./rightServerURL']").val(),
            rightVideoContentURL = $form.find("[name='./rightVideoContentURL']").val(),
            rightVideoEmailURL = $form.find("[name='./rightVideoEmailURL']").val(),
            rightVideoServerURL = $form.find("[name='./rightVideoServerURL']").val(),
            rightVideoPosterImage = $form.find("[name='./rightVideoPosterImage']").val(),
            rightVideoTranscript = $form.find("[name='./rightVideoTranscript']").val(),

            leftImageURLRef = $form.find("[name='./leftImageURLRef']").val(),
            leftImageAltText = $form.find("[name='./leftImageAltText']").val(),
            leftImageHeading = $form.find("[name='./leftImageHeading']").val(),
            leftImageButtonLabel = $form.find("[name='./leftImageButtonLabel']").val(),
            leftImageButtonLink = $form.find("[name='./leftImageButtonLink']").val(),
            leftImageLinkTarget = $form.find("[name='./leftImageLinkTarget']").val(),

            rightImageURLRef = $form.find("[name='./rightImageURLRef']").val(),
            rightImageAltText = $form.find("[name='./rightImageAltText']").val(),
            rightImageHeading = $form.find("[name='./rightImageHeading']").val(),
            rightImageButtonLabel = $form.find("[name='./rightImageButtonLabel']").val(),
            rightImageButtonLink = $form.find("[name='./rightImageButtonLink']").val(),
            rightImageLinkTarget = $form.find("[name='./rightImageLinkTarget']").val(),

            titleThree = $form.find("[name='./titleThree']").val(),
            buttonLabelThree = $form.find("[name='./buttonLabelThree']").val(),
            buttonLinkThree = $form.find("[name='./buttonLinkThree']").val(),

            message, clazz = "coral-Button ",
            patterns = {};

        if (displayOptions == 'video-left') {

            if ((leftVideoText == 0) || (leftVideoAsset == 0) || (leftServerURL == 0) || (leftVideoContentURL == 0) || (leftVideoEmailURL == 0) || (leftVideoServerURL == 0) || (leftVideoPosterImage == 0) || (leftVideoTranscript == 0) || (rightImageURLRef == 0) || (rightImageAltText == 0) || (rightImageHeading == 0) || (rightImageButtonLabel == 0) || (rightImageButtonLink == 0)) {
                window.stop();
            } else {
                $form.submit();
            }
        }

        if (displayOptions == 'video-right') {

            if ((rightVideoText == 0) || (rightVideoAsset == 0) || (rightServerURL == 0) || (rightVideoContentURL == 0) || (rightVideoEmailURL == 0) || (rightVideoServerURL == 0) || (rightVideoPosterImage == 0) || (rightVideoTranscript == 0) || (leftImageURLRef == 0) || (leftImageAltText == 0) || (leftImageHeading == 0) || (leftImageButtonLabel == 0) || (leftImageButtonLink == 0)) {
                window.stop();
            } else {
                $form.submit();
            }
        }

        if (displayOptions == 'no-video') {

            if ((leftImageURLRef == 0) || (leftImageAltText == 0) || (leftImageHeading == 0) || (leftImageButtonLabel == 0) || (leftImageButtonLink == 0) || (rightImageURLRef == 0) || (rightImageAltText == 0) || (rightImageHeading == 0) || (rightImageButtonLabel == 0) || (rightImageButtonLink == 0)) {
                window.stop();
            } else {
                $form.submit();
            }
        }

    });
})(document, Granite.$, Granite.author);
