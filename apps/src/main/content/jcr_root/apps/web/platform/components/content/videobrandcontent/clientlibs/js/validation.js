(function(document, $, ns) {
    "use strict";

    $(document).ready( function() {

        $(document).on("click", ".cq-dialog-submit", function(e) {
    
            e.stopPropagation();
            e.preventDefault();
    
            var $form = $(this).closest("form.foundation-form"); 
            var ui = $(window).adaptTo("foundation-ui");
            var videoAsset = $form.find("[name='./videoAsset']").val();
    
            if(videoAsset != undefined && videoAsset != ''){
    
                var serverUrl = $form.find("[name='./serverUrl']").val();
                var videContentUrl = $form.find("[name='./videContentUrl']").val();
                var videoEmailUrl = $form.find("[name='./videoEmailUrl']").val();
                var videoServerUrl = $form.find("[name='./videoServerUrl']").val();
                var videoPosterImage = $form.find("[name='./videoPosterImage']").val();

                if(serverUrl.length == 0 || videContentUrl == 0 || videoEmailUrl == 0 || videoServerUrl == 0
                   || videoPosterImage == 0)
                {
                    alert("Either provide all the values in video fields or remove all.");                
                    window.stop();
                }
    
            }
        } );

    } );
})(document, Granite.$, Granite.author);