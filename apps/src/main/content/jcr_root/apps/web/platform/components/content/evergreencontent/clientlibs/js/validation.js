(function(document, $, ns) {
    "use strict";

    $(document).on("click", ".cq-dialog-submit", function(e) {

        var $form = $(this).closest("form.foundation-form"),

            lifeStyleContent = $form.find("[name='./lifeStyleContent']").val(),


            lifeStyleContentFirst = $form.find("[name='./lifeStyleContentFirst']").val(),
            titleFirst = $form.find("[name='./titleFirst']").val(),
            detailsFirst = $form.find("[name='./detailsFirst']").val(),
            anchorFirst = $form.find("[name='./anchorFirst']").val(),
            videoAssetFirst = $form.find("[name='./videoAssetFirst']").val(),
            serverUrlFirst = $form.find("[name='./serverUrlFirst']").val(),
            videoContentUrlFirst = $form.find("[name='./videoContentUrlFirst']").val(),
            videoEmailUrlFirst = $form.find("[name='./videoEmailUrlFirst']").val(),
            videoServerUrlFirst = $form.find("[name='./videoServerUrlFirst']").val(),
            imageUrlFirst = $form.find("[name='./imageUrlFirst']").val(),
            imageAltTextFirst = $form.find("[name='./imageAltTextFirst']").val(),

            lifeStyleContentSecond = $form.find("[name='./lifeStyleContentSecond']").val(),
            titleSecond = $form.find("[name='./titleSecond']").val(),
            detailsSecond = $form.find("[name='./detailsSecond']").val(),
            anchorSecond = $form.find("[name='./anchorSecond']").val(),
            videoAssetSecond = $form.find("[name='./videoAssetSecond']").val(),
            serverUrlSecond = $form.find("[name='./serverUrlSecond']").val(),
            videoContentUrlSecond = $form.find("[name='./videoContentUrlSecond']").val(),
            videoEmailUrlSecond = $form.find("[name='./videoEmailUrlSecond']").val(),
            videoServerUrlSecond = $form.find("[name='./videoServerUrlSecond']").val(),
            imageUrlSecond = $form.find("[name='./imageUrlSecond']").val(),
            imageAltTextSecond = $form.find("[name='./imageAltTextSecond']").val(),


            lifeStyleContentThird = $form.find("[name='./lifeStyleContentThird']").val(),
            titleThird = $form.find("[name='./titleThird']").val(),
            detailsThird = $form.find("[name='./detailsThird']").val(),
            anchorThird = $form.find("[name='./anchorThird']").val(),
            videoAssetThird = $form.find("[name='./videoAssetThird']").val(),
            serverUrlThird = $form.find("[name='./serverUrlThird']").val(),
            videoContentUrlThird = $form.find("[name='./videoContentUrlThird']").val(),
            videoEmailUrlThird = $form.find("[name='./videoEmailUrlThird']").val(),
            videoServerUrlThird = $form.find("[name='./videoServerUrlThird']").val(),
            imageUrlThird = $form.find("[name='./imageUrlThird']").val(),
            imageAltTextThird = $form.find("[name='./imageAltTextThird']").val(),

            lifeStyleContentFourth = $form.find("[name='./lifeStyleContentFourth']").val(),
            titleFourth = $form.find("[name='./titleFourth']").val(),
            detailsFourth = $form.find("[name='./detailsFourth']").val(),
            anchorFourth = $form.find("[name='./anchorFourth']").val(),
            videoAssetFourth = $form.find("[name='./videoAssetFourth']").val(),
            serverUrlFourth = $form.find("[name='./serverUrlFourth']").val(),
            videoContentUrlFourth = $form.find("[name='./videoContentUrlFourth']").val(),
            videoEmailUrlFourth = $form.find("[name='./videoEmailUrlFourth']").val(),
            videoServerUrlFourth = $form.find("[name='./videoServerUrlFourth']").val(),
            imageUrlFourth = $form.find("[name='./imageUrlFourth']").val(),
            imageAltTextFourth = $form.find("[name='./imageAltTextFourth']").val(),

            lifeStyleContentFifth = $form.find("[name='./lifeStyleContentFifth']").val(),
            titleFifth = $form.find("[name='./titleFifth']").val(),
            detailsFifth = $form.find("[name='./detailsFifth']").val(),
            anchorFifth = $form.find("[name='./anchorFifth']").val(),
            videoAssetFifth = $form.find("[name='./videoAssetFifth']").val(),
            serverUrlFifth = $form.find("[name='./serverUrlFifth']").val(),
            videoContentUrlFifth = $form.find("[name='./videoContentUrlFifth']").val(),
            videoEmailUrlFifth = $form.find("[name='./videoEmailUrlFifth']").val(),
            videoServerUrlFifth = $form.find("[name='./videoServerUrlFifth']").val(),
            imageUrlFifth = $form.find("[name='./imageUrlFifth']").val(),
            imageAltTextFifth = $form.find("[name='./imageAltTextFifth']").val(),
            ui = $(window).adaptTo("foundation-ui");


        var $formminmax = $(this).closest("form.foundation-form");   
        var field = $formminmax.find("[data-granite-coral-multifield-name='./iconsListResource']");
        var totalLinkCount = field.children('coral-multifield-item').length;

        var headingData = [],
            headingUrlData = [],
            iconDescriptionData = [],
            alphaNumError = [];

        for (var i = 0; i < totalLinkCount; i++) {
            headingData.push(field.find($("[name='./text']")[i]).val());
        }

        for (var i = 0; i < totalLinkCount; i++) {
            headingUrlData.push(field.find($("[name='./url']")[i]).val());
        }
        for (var i = 0; i < totalLinkCount; i++) {
            iconDescriptionData.push(field.find($("[name='./content']")[i]).val());
        }




        var headingDataFilter = headingData.filter(function(e) {
            return e
        });
        var headingUrlDataFilter = headingUrlData.filter(function(e) {
            return e
        });
        var iconDescriptionDataFilter = iconDescriptionData.filter(function(e) {
            return e
        });


        // alert(lifeStyleContentFirst);

        if (lifeStyleContent == 'one') {

            if ((headingDataFilter.length != totalLinkCount) || (headingUrlDataFilter.length != totalLinkCount) || (iconDescriptionDataFilter.length != totalLinkCount)) {

                window.stop();

            } else if (((lifeStyleContentFirst == 'videoleftFirst') || (lifeStyleContentFirst == 'videorightFirst')) && ((titleFirst == 0) || (detailsFirst == 0) || (anchorFirst == 0) || (videoAssetFirst == 0) || (serverUrlFirst == 0) || (videoContentUrlFirst == 0) || (videoEmailUrlFirst == 0) || (videoServerUrlFirst == 0))) {

                window.stop();

            } else if (((lifeStyleContentFirst == 'imageleftFirst') || (lifeStyleContentFirst == 'imagerightFirst')) && ((titleFirst == 0) || (detailsFirst == 0) || (anchorFirst == 0) || (imageUrlFirst == 0) || (imageAltTextFirst == 0))) {

                window.stop();

            } else {
                $form.submit();
            }

        }

        if (lifeStyleContent == 'two') {

            if ((headingDataFilter.length != totalLinkCount) || (headingUrlDataFilter.length != totalLinkCount) || (iconDescriptionDataFilter.length != totalLinkCount)) {

                window.stop();

            } else if (((lifeStyleContentFirst == 'videoleftFirst') || (lifeStyleContentFirst == 'videorightFirst')) && ((titleFirst == 0) || (detailsFirst == 0) || (anchorFirst == 0) || (videoAssetFirst == 0) || (serverUrlFirst == 0) || (videoContentUrlFirst == 0) || (videoEmailUrlFirst == 0) || (videoServerUrlFirst == 0))) {

                window.stop();

            } else if (((lifeStyleContentFirst == 'imageleftFirst') || (lifeStyleContentFirst == 'imagerightFirst')) && ((titleFirst == 0) || (detailsFirst == 0) || (anchorFirst == 0) || (imageUrlFirst == 0) || (imageAltTextFirst == 0))) {

                window.stop();

            } else if (((lifeStyleContentSecond == 'videoleftSecond') || (lifeStyleContentSecond == 'videorightSecond')) && ((titleSecond == 0) || (detailsSecond == 0) || (anchorSecond == 0) || (videoAssetSecond == 0) || (serverUrlSecond == 0) || (videoContentUrlSecond == 0) || (videoEmailUrlSecond == 0) || (videoServerUrlSecond == 0))) {

                window.stop();

            } else if (((lifeStyleContentSecond == 'imageleftSecond') || (lifeStyleContentSecond == 'imagerightSecond')) && ((titleSecond == 0) || (detailsSecond == 0) || (anchorSecond == 0) || (imageUrlSecond == 0) || (imageAltTextSecond == 0))) {

                window.stop();

            } else {
                $form.submit();
            }


        }

        if (lifeStyleContent == 'three') {

            if ((headingDataFilter.length != totalLinkCount) || (headingUrlDataFilter.length != totalLinkCount) || (iconDescriptionDataFilter.length != totalLinkCount)) {

                window.stop();

            } else if (((lifeStyleContentFirst == 'videoleftFirst') || (lifeStyleContentFirst == 'videorightFirst')) && ((titleFirst == 0) || (detailsFirst == 0) || (anchorFirst == 0) || (videoAssetFirst == 0) || (serverUrlFirst == 0) || (videoContentUrlFirst == 0) || (videoEmailUrlFirst == 0) || (videoServerUrlFirst == 0))) {

                window.stop();

            } else if (((lifeStyleContentFirst == 'imageleftFirst') || (lifeStyleContentFirst == 'imagerightFirst')) && ((titleFirst == 0) || (detailsFirst == 0) || (anchorFirst == 0) || (imageUrlFirst == 0) || (imageAltTextFirst == 0))) {

                window.stop();

            } else if (((lifeStyleContentSecond == 'videoleftSecond') || (lifeStyleContentSecond == 'videorightSecond')) && ((titleSecond == 0) || (detailsSecond == 0) || (anchorSecond == 0) || (videoAssetSecond == 0) || (serverUrlSecond == 0) || (videoContentUrlSecond == 0) || (videoEmailUrlSecond == 0) || (videoServerUrlSecond == 0))) {

                window.stop();

            } else if (((lifeStyleContentSecond == 'imageleftSecond') || (lifeStyleContentSecond == 'imagerightSecond')) && ((titleSecond == 0) || (detailsSecond == 0) || (anchorSecond == 0) || (imageUrlSecond == 0) || (imageAltTextSecond == 0))) {

                window.stop();

            } else if (((lifeStyleContentThird == 'videoleftThird') || (lifeStyleContentThird == 'videorightThird')) && ((titleThird == 0) || (detailsThird == 0) || (anchorThird == 0) || (videoAssetThird == 0) || (serverUrlThird == 0) || (videoContentUrlThird == 0) || (videoEmailUrlThird == 0) || (videoServerUrlThird == 0))) {

                window.stop();

            } else if (((lifeStyleContentThird == 'imageleftThird') || (lifeStyleContentThird == 'imagerightThird')) && ((titleThird == 0) || (detailsThird == 0) || (anchorThird == 0) || (imageUrlThird == 0) || (imageAltTextThird == 0))) {

                window.stop();

            } else {
                $form.submit();
            }


        }

        if (lifeStyleContent == 'four') {

            if ((headingDataFilter.length != totalLinkCount) || (headingUrlDataFilter.length != totalLinkCount) || (iconDescriptionDataFilter.length != totalLinkCount)) {

                window.stop();

            } else if (((lifeStyleContentFirst == 'videoleftFirst') || (lifeStyleContentFirst == 'videorightFirst')) && ((titleFirst == 0) || (detailsFirst == 0) || (anchorFirst == 0) || (videoAssetFirst == 0) || (serverUrlFirst == 0) || (videoContentUrlFirst == 0) || (videoEmailUrlFirst == 0) || (videoServerUrlFirst == 0))) {

                window.stop();

            } else if (((lifeStyleContentFirst == 'imageleftFirst') || (lifeStyleContentFirst == 'imagerightFirst')) && ((titleFirst == 0) || (detailsFirst == 0) || (anchorFirst == 0) || (imageUrlFirst == 0) || (imageAltTextFirst == 0))) {

                window.stop();

            } else if (((lifeStyleContentSecond == 'videoleftSecond') || (lifeStyleContentSecond == 'videorightSecond')) && ((titleSecond == 0) || (detailsSecond == 0) || (anchorSecond == 0) || (videoAssetSecond == 0) || (serverUrlSecond == 0) || (videoContentUrlSecond == 0) || (videoEmailUrlSecond == 0) || (videoServerUrlSecond == 0))) {

                window.stop();

            } else if (((lifeStyleContentSecond == 'imageleftSecond') || (lifeStyleContentSecond == 'imagerightSecond')) && ((titleSecond == 0) || (detailsSecond == 0) || (anchorSecond == 0) || (imageUrlSecond == 0) || (imageAltTextSecond == 0))) {

                window.stop();

            } else if (((lifeStyleContentThird == 'videoleftThird') || (lifeStyleContentThird == 'videorightThird')) && ((titleThird == 0) || (detailsThird == 0) || (anchorThird == 0) || (videoAssetThird == 0) || (serverUrlThird == 0) || (videoContentUrlThird == 0) || (videoEmailUrlThird == 0) || (videoServerUrlThird == 0))) {

                window.stop();

            } else if (((lifeStyleContentThird == 'imageleftThird') || (lifeStyleContentThird == 'imagerightThird')) && ((titleThird == 0) || (detailsThird == 0) || (anchorThird == 0) || (imageUrlThird == 0) || (imageAltTextThird == 0))) {

                window.stop();

            } else if (((lifeStyleContentFourth == 'videoleftFourth') || (lifeStyleContentFourth == 'videorightFourth')) && ((titleFourth == 0) || (detailsFourth == 0) || (anchorFourth == 0) || (videoAssetFourth == 0) || (serverUrlFourth == 0) || (videoContentUrlFourth == 0) || (videoEmailUrlFourth == 0) || (videoServerUrlFourth == 0))) {

                window.stop();

            } else if (((lifeStyleContentFourth == 'imageleftFourth') || (lifeStyleContentFourth == 'imagerightFourth')) && ((titleFourth == 0) || (detailsFourth == 0) || (anchorFourth == 0) || (imageUrlFourth == 0) || (imageAltTextFourth == 0))) {

                window.stop();

            } else {
                $form.submit();
            }


        }

        if (lifeStyleContent == 'five') {

            if ((headingDataFilter.length != totalLinkCount) || (headingUrlDataFilter.length != totalLinkCount) || (iconDescriptionDataFilter.length != totalLinkCount)) {

                window.stop();

            } else if (((lifeStyleContentFirst == 'videoleftFirst') || (lifeStyleContentFirst == 'videorightFirst')) && ((titleFirst == 0) || (detailsFirst == 0) || (anchorFirst == 0) || (videoAssetFirst == 0) || (serverUrlFirst == 0) || (videoContentUrlFirst == 0) || (videoEmailUrlFirst == 0) || (videoServerUrlFirst == 0))) {

                window.stop();

            } else if (((lifeStyleContentFirst == 'imageleftFirst') || (lifeStyleContentFirst == 'imagerightFirst')) && ((titleFirst == 0) || (detailsFirst == 0) || (anchorFirst == 0) || (imageUrlFirst == 0) || (imageAltTextFirst == 0))) {

                window.stop();

            } else if (((lifeStyleContentSecond == 'videoleftSecond') || (lifeStyleContentSecond == 'videorightSecond')) && ((titleSecond == 0) || (detailsSecond == 0) || (anchorSecond == 0) || (videoAssetSecond == 0) || (serverUrlSecond == 0) || (videoContentUrlSecond == 0) || (videoEmailUrlSecond == 0) || (videoServerUrlSecond == 0))) {

                window.stop();

            } else if (((lifeStyleContentSecond == 'imageleftSecond') || (lifeStyleContentSecond == 'imagerightSecond')) && ((titleSecond == 0) || (detailsSecond == 0) || (anchorSecond == 0) || (imageUrlSecond == 0) || (imageAltTextSecond == 0))) {

                window.stop();

            } else if (((lifeStyleContentThird == 'videoleftThird') || (lifeStyleContentThird == 'videorightThird')) && ((titleThird == 0) || (detailsThird == 0) || (anchorThird == 0) || (videoAssetThird == 0) || (serverUrlThird == 0) || (videoContentUrlThird == 0) || (videoEmailUrlThird == 0) || (videoServerUrlThird == 0))) {

                window.stop();

            } else if (((lifeStyleContentThird == 'imageleftThird') || (lifeStyleContentThird == 'imagerightThird')) && ((titleThird == 0) || (detailsThird == 0) || (anchorThird == 0) || (imageUrlThird == 0) || (imageAltTextThird == 0))) {

                window.stop();

            } else if (((lifeStyleContentFourth == 'videoleftFourth') || (lifeStyleContentFourth == 'videorightFourth')) && ((titleFourth == 0) || (detailsFourth == 0) || (anchorFourth == 0) || (videoAssetFourth == 0) || (serverUrlFourth == 0) || (videoContentUrlFourth == 0) || (videoEmailUrlFourth == 0) || (videoServerUrlFourth == 0))) {

                window.stop();

            } else if (((lifeStyleContentFourth == 'imageleftFourth') || (lifeStyleContentFourth == 'imagerightFourth')) && ((titleFourth == 0) || (detailsFourth == 0) || (anchorFourth == 0) || (imageUrlFourth == 0) || (imageAltTextFourth == 0))) {

                window.stop();

            } else if (((lifeStyleContentFifth == 'videoleftFifth') || (lifeStyleContentFifth == 'videorightFifth')) && ((titleFifth == 0) || (detailsFifth == 0) || (anchorFifth == 0) || (videoAssetFifth == 0) || (serverUrlFifth == 0) || (videoContentUrlFifth == 0) || (videoEmailUrlFifth == 0) || (videoServerUrlFifth == 0))) {

                window.stop();

            } else if (((lifeStyleContentFifth == 'imageleftFifth') || (lifeStyleContentFifth == 'imagerightFifth')) && ((titleFifth == 0) || (detailsFifth == 0) || (anchorFifth == 0) || (imageUrlFifth == 0) || (imageAltTextFifth == 0))) {

                window.stop();

            } else {
                $form.submit();
            }


        }


    });
})(document, Granite.$, Granite.author);